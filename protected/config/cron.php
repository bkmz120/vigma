<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Cron',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
    ),



	// application components
	'components'=>array(
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=vigma',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),

        /*
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=galina111_vi',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'MyVigaQl4p',
            'charset' => 'utf8',
        ),
        */

		/*'catalogDb'=>array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=5.101.152.141;dbname=techiq_catalog',
            'emulatePrepare' => true,
            'username' => 'techiq_catalog',
            'password' => '*Y}{]x@I',
            'charset' => 'utf8',
        ),*/

        'catalogDb'=>array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=techiq_catalog',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
	),
);