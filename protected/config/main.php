<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Вигма',
    'defaultController' => 'page',


    // preloading 'log' component
    'preload'=>array('log'),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'ext.SimpleExcel',
        'ext.phpexcel.*',
        'ext.phpexcel.PHPExcel.*',
    ),


    'modules'=>array(
        // uncomment the following to enable the Gii tool

        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),



    ),

    // application components
    'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'loginUrl'=>array('login/index'),
        ),

        'clientScript' => array(
           'class' => 'ClientScript',
        ),

        'sphinx' => array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=127.0.0.1;port=9306',
        ),


        /*
        'vitrinaDb' => array(
                      'connectionString' => 'mysql:host=192.168.57.132;dbname=galina111_cat',
                      'username' => 'ssluser1',
                      'password' => '123',
                      'charset' => 'utf8',
                      'attributes' => array(
                         PDO::MYSQL_ATTR_SSL_KEY => '/certs/client-key.pem',
                         PDO::MYSQL_ATTR_SSL_CERT => '/certs/client-cert.pem',
                         PDO::MYSQL_ATTR_SSL_CA => '/certs/ca.pem',
                      ),
                      'class'  => 'CDbConnection'
                   ),
        */

        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=vigma',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),

        /*
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=vigma',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'MyVigaQl4p',
            'charset' => 'utf8',
        ),
        */

        /*'catalogDb'=>array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=5.101.152.141;dbname=techiq_catalog',
            'emulatePrepare' => true,
            'username' => 'techiq_catalog',
            'password' => '*Y}{]x@I',
            'charset' => 'utf8',
        ),*/

        'catalogDb'=>array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=techiq_catalog',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),

        // uncomment the following to enable URLs in path-format
        /*
        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        */

        /*
        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
        ),
        */



        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',

                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'webmaster@example.com',
        'myDateFormat' => 'd-m-Y',
        'assetsVersion' => '1003',
    ),
);