<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $original_name
 * @property string $original_chod
 * @property string $chod_display
 * @property string $catalog_name
 * @property string $catalog_chod
 * @property string $product_num
 * @property string $comment
 * @property string $tovar_type
 * @property string $other_info
 * @property integer $defaultprovider_id
 * @property integer $defaultprovprod_id
 * @property double $price_chtz
 * @property string $price_catalog
 * @property integer $hidden
 * @property double $price_out
 * @property string $size
 * @property double $weight
 * @property string $img
 * @property string $url
 * @property integer $rating
 * @property integer $parts_id
 * @property integer $partof_id
 * @property string $use_deafult
 *
 * The followings are the available model relations:
 * @property InputRequests[] $inputRequests
 * @property ProvProds[] $provProds
 * @property ReqProds[] $reqProds
 */
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('defaultprovider_id, defaultprovprod_id, hidden, rating, parts_id, partof_id', 'numerical', 'integerOnly'=>true),
			array('price_chtz, price_out, weight', 'numerical'),
			array('original_name, original_chod, product_num, size', 'length', 'max'=>256),
			array('chod_display, catalog_name, catalog_chod, img, url', 'length', 'max'=>255),
			array('tovar_type', 'length', 'max'=>20),
			array('price_catalog', 'length', 'max'=>12),
			array('comment, other_info, use_deafult', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, original_name, original_chod, chod_display, catalog_name, catalog_chod, product_num, comment, tovar_type, other_info, defaultprovider_id, defaultprovprod_id, price_chtz, price_catalog, hidden, price_out, size, weight, img, url, rating, parts_id, partof_id, use_deafult', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inputRequests' => array(self::HAS_MANY, 'InputRequests', 'product_id'),
			'provProds' => array(self::HAS_MANY, 'ProvProds', 'product_id'),
			'reqProds' => array(self::HAS_MANY, 'ReqProds', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'original_name' => 'Original Name',
			'original_chod' => 'Original Chod',
			'chod_display' => 'Chod Display',
			'catalog_name' => 'Catalog Name',
			'catalog_chod' => 'Catalog Chod',
			'product_num' => 'Product Num',
			'comment' => 'Comment',
			'tovar_type' => 'Tovar Type',
			'other_info' => 'Other Info',
			'defaultprovider_id' => 'Defaultprovider',
			'defaultprovprod_id' => 'Defaultprovprod',
			'price_chtz' => 'Price Chtz',
			'price_catalog' => 'Price Catalog',
			'hidden' => 'Hidden',
			'price_out' => 'Price Out',
			'size' => 'Size',
			'weight' => 'Weight',
			'img' => 'Img',
			'url' => 'Url',
			'rating' => 'Rating',
			'parts_id' => 'Parts',
			'partof_id' => 'Partof',
			'use_deafult' => 'Use Deafult',
		);
	}


	public function addNew($provider_id, $price_in, $price_orig, $price_date,$delivery_time=null)
	{
		$res= $this->save();
		if ($res==false)
		{
			var_dump($this);
			return false;
		}
		
		if ($delivery_time==null) $delivery_time = 0;
		$provProd = new ProvProds;
		$provProd->provider_id = $provider_id;
		$provProd->price_in =  $price_in;
		$provProd->price_orig =  $price_orig;
		$provProd->price_date = $price_date;
		$provProd->product_id = $this->id;
		$provProd->delivery_time =$delivery_time;
		$res =  $provProd->save();
		if ($res==false)
		{
			var_dump($provProd);
			return false;
		}

		$this->defaultprovprod_id = $provProd->id;
		$res= $this->save();
		if ($res==false)
		{
			var_dump($this);
			return false;
		}

		$inputRequest = new InputRequests;
		$inputRequest->product_id = $this->id;
		$inputRequest->name = $this->original_name;
		$inputRequest->chod = $this->original_chod;
		$inputRequest->chod_display = $this->chod_display;
		$inputRequest->original_item = 1;
		$inputRequest->provider_id=$provider_id;
		$inputRequest->provprod_id = $provProd->id;
		$res= $inputRequest->save();
		if ($res==false)
		{
			var_dump($inputRequest);
			return false;
		}

		return true;
	}

	public function deleteProduct($product_id)
	{
		//проверим есть ли этот товар в заявках, если есть, то удалять нельзя
		$criteria =  new CDbCriteria;
		$criteria->condition = 'product_id =:product_id';
		$criteria->params = array(':product_id'=>$product_id);
		$reqProds = ReqProds::model()->findAll($criteria);

		if ($reqProds!=null)
		{
			return "inRequisitions";
		}
		
		$res= ProvProds::model()->deleteAll($criteria);
				
		$res = InputRequests::model()->deleteAll($criteria);

		$criteria =  new CDbCriteria;
		$criteria->condition = 'id =:product_id';
		$criteria->params = array(':product_id'=>$product_id);

		$res = Products::model()->deleteAll($criteria);
		if ($res==false)
		{
			echo "false";
		}

		return true;
	}

	public function changeDefaultProvider($product_id,$provprod_id)
	{
		$product = Products::model()->findByPk($product_id);
		if ($product==null) return false;
		$product->defaultprovprod_id = $provprod_id;
		$res = $product->update();
		if ($res==false) return false;
		return true;
	}

	public function copyProductInfoFromTo($fromProductId,$toProductId)
	{
		//скопировать синонимы
		$criteria = new CDbCriteria;
		$criteria->condition = "product_id =:product_id";
		
		$criteria->params = array(':product_id'=>$fromProductId);
		$inputRequestsFrom = InputRequests::model()->findAll($criteria);

		$criteria->params = array(':product_id'=>$toProductId);
		$inputRequestsTo = InputRequests::model()->findAll($criteria);



		foreach ($inputRequestsFrom as $inputRequestFrom)
		{
			$notFound=true;

			//проверим есть ли уже этот синоним, в таком случае копировать не надо
			foreach ($inputRequestsTo as $inputRequestTo)
			{
				//var_dump($inputRequestFrom->name.'  '.$inputRequestTo->name);
				if ($inputRequestFrom->name==$inputRequestTo->name && $inputRequestFrom->chod==$inputRequestTo->chod)
				{
					$notFound=false;
					break;
				}
			}
			
			if ($notFound)
			{
				$newInputRequest = new InputRequests;
				$newInputRequest->product_id = $toProductId;
				$newInputRequest->name = $inputRequestFrom->name;
				$newInputRequest->chod = $inputRequestFrom->chod;
				$newInputRequest->chod_display = $inputRequestFrom->chod_display;
				$newInputRequest->original_item = $inputRequestFrom->original_item;
				$newInputRequest->provider_id = $inputRequestFrom->provider_id;
				
				
				$res = $newInputRequest->save();
				if ($res==false)
				{
					//var_dump($newInputRequest);
					return false;
				}
			}
		}

		//скопировать поставщиков
		$criteria = new CDbCriteria;
		$criteria->condition = "product_id =:product_id";
		
		$criteria->params = array(':product_id'=>$fromProductId);
		$provProdsFrom = ProvProds::model()->findAll($criteria);

		$criteria->params = array(':product_id'=>$toProductId);
		$provProdsTo = ProvProds::model()->findAll($criteria);

		foreach ($provProdsFrom as $provProdFrom)
		{
			$notFound=true;

			//проверим есть ли уже этот поставщик, в таком случае копировать не надо
			foreach ($provProdsTo as $provProdTo)
			{
				
				if ($provProdFrom->provider_id==$provProdTo->provider_id)
				{
					$notFound=false;
					break;
				}
			}
			
			if ($notFound)
			{
				$newProvProd = new ProvProds;
				$newProvProd->product_id = $toProductId;
				$newProvProd->provider_id = $provProdFrom->provider_id;
				$newProvProd->price_in = $provProdFrom->price_in;
				$newProvProd->price_orig = $provProdFrom->price_orig;
				$newProvProd->price_date = $provProdFrom->price_date;
				
				$res = $newProvProd->save();
				if ($res==false)
				{
					//var_dump($newInputRequest);
					return false;
				}
			}
		}


		//скопировать коммент
		$productFrom = Products::model()->findByPk($fromProductId);
		if ($productFrom->comment!=null)
		{
			$productTo = Products::model()->findByPk($toProductId);

			$productTo->comment = $productTo->comment.'   '.$productFrom->comment;
			$res = $productTo->save();
			if ($res==false)
			{
				return false;
			}
		}

		//скопировать размеры
		if ($productFrom->size!=null)
		{
			$productTo = Products::model()->findByPk($toProductId);

			$productTo->size = $productFrom->size;
			$res = $productTo->save();
			if ($res==false)
			{
				return false;
			}
		}

		return true;
	}
	
	public function getInfoArray()
	{

		$productInfo = array();
		$productInfo['id'] = $this->id;
		$productInfo['original_name'] = $this->original_name;
		$productInfo['original_chod'] = $this->original_chod;
		$productInfo['chod_display'] = $this->chod_display;
		$productInfo['product_num']= $this->product_num;
		$productInfo['comment'] = $this->comment;
		$productInfo['tovar_type'] = $this->tovar_type;
		$productInfo['other_info'] = $this->other_info;
		$productInfo['defaultprovprod_id'] = $this->defaultprovprod_id;
		$productInfo['price_out'] = $this->price_out;
		$productInfo['size'] = $this->size;
		$productInfo['weight'] = $this->weight;
		$productInfo['img'] = $this->img;
		$productInfo['parts_id'] = $this->parts_id;
		$productInfo['partof_id'] = $this->partof_id;
		$productInfo['use_deafult'] = $this->use_deafult;
		return $productInfo;

	}

	public function getInfoArrayWithInputs()
	{
		$productInfo=$this->getInfoArray();
		$productInfo['inputRequests'] = InputRequests::model()->getInfoArrayFromArray($this->inputRequests);
		return $productInfo;
	}

	public static function getInfoArrayWithInputsFromArray($products)
	{
		$productsArray = array();
		foreach ($products as $product)
		{
			array_push($productsArray, $product->getInfoArrayWithInputs());
		}
		return $productsArray;
	}


	public function getInfoArrayWithProvInfo()
	{
		$productInfo = $this->getInfoArray();
		//для каждого продукта найти поставщиков
		$criteria = new CDbCriteria;
		$criteria->condition = 'product_id=:product_id';
		$criteria->params = array(":product_id"=>$this->id);
		$provProds = ProvProds::model()->findAll($criteria);

		$providersInfo = array();
		foreach ($provProds as $provProd)
		{
			array_push($providersInfo, $provProd->getInfoArrayWithProvNames());
		}

		$productInfo['providers_info'] = $providersInfo;

		return $productInfo;
	}

	//инфа для карточки товара
	public function getProductWithFullInfo($product_id)
	{
		$sql="SELECT 
		    products.*,
		    input_requests.id AS inpId, input_requests.name AS inpName, input_requests.chod AS inpChod, input_requests.chod_display AS inpChodDisplay, input_requests.original_item AS inpOriginalItem, input_requests.provider_id AS inpProviderId, input_requests.provprod_id AS inpProvProdId,
		    provprod.id AS ppId, provprod.provider_id AS ppProvId, provprod.price_in AS ppPriceIn, provprod.price_orig AS ppPriceOrig, provprod.delivery_time AS ppDelivTime, provprod.delivery_comment AS ppDelivComment, provprod.price_date AS ppPriceDate, provprod.notInPrice AS ppNotInPrice,
		    prov.name AS pvName,
		    stock.id AS stId, stock.stockcity_id AS stCityId, stock.chodname AS stChodname, stock.val1 AS stVal1, stock.val2 AS stVal2, stock.val3 AS stVal3, stock.price1 AS stPrice1, stock.price2 AS stPrice2, stock.price3 AS stPrice3,
			stockCity.name AS stCityName, stockCity.priority as stCityPriority, stockCity.date as stCityDate
			FROM
			products
			LEFT OUTER JOIN input_requests ON products.id = input_requests.product_id
			LEFT OUTER JOIN prov_prods provprod ON products.id = provprod.product_id
			LEFT OUTER JOIN providers prov ON provprod.provider_id = prov.id
			LEFT OUTER JOIN stocks stock ON products.id = stock.product_id
			LEFT OUTER JOIN stock_cities stockCity ON stock.stockcity_id = stockCity.id
			WHERE products.id = $product_id			
			;";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$dataReader=$command->query();
		
		$lastInputRequestId = -1;
		$lastProductId = -1;

		$product = null;


		foreach($dataReader as $row)
		{
			if ($row['id']!=$lastProductId)
			{
				if ($product!=null) {
					return null;
				}

				$product=array();
				$product['id'] = $row['id'];
				$product['original_name'] = $row['original_name'];
				$product['original_chod'] = $row['original_chod'];
				$product['chod_display'] = $row['chod_display'];
				$product['catalog_name'] = $row['catalog_name'];
				$product['catalog_chod'] = $row['catalog_chod'];
				$product['product_num'] = $row['product_num'];
				$product['defaultprovprod_id'] = $row['defaultprovprod_id'];
				$product['price_chtz'] = $row['price_chtz'];
				$product['size'] = $row['size'];
				$product['weight'] = $row['weight'];
				$product['img'] = $row['img'];
				$product['comment'] = $row['comment'];
				$product['hidden'] = $row['hidden'];
				$product['inputRequests'] = array();
				$product['inputRequests'][0]['id'] = $row['inpId'];
				$product['inputRequests'][0]['name'] = $row['inpName'];
				$product['inputRequests'][0]['chod'] = $row['inpChod'];
				$product['inputRequests'][0]['chod_display'] = $row['inpChodDisplay'];
				$product['inputRequests'][0]['original_item'] = $row['inpOriginalItem'];
				$product['inputRequests'][0]['provider_id'] = $row['inpProviderId'];
				$product['inputRequests'][0]['provprod_id'] = $row['inpProvProdId'];
				$product['provProds']=array();
				$product['stocks'] = array();
				
				if ($row['ppId']!=null)
				{
					$product['provProds'][0]['id'] = $row['ppId'];
					$product['provProds'][0]['provider_id'] = $row['ppProvId'];
					$product['provProds'][0]['price_in'] = $row['ppPriceIn'];
					$product['provProds'][0]['price_orig'] = $row['ppPriceOrig'];
					$product['provProds'][0]['delivery_time'] = $row['ppDelivTime'];
					$product['provProds'][0]['delivery_comment'] = $row['ppDelivComment'];
					$product['provProds'][0]['provider_name'] = $row['pvName'];
					$product['provProds'][0]['price_date'] = $row['ppPriceDate'];
					$product['provProds'][0]['notInPrice'] = $row['ppNotInPrice'];
				}

				if ($row['stId']!=null)
				{
					$stockInfo = array();
					$stockInfo['id'] = $row['stId'];
					$stockInfo['stockcity_id'] = $row['stCityId'];
					$stockInfo['val1'] = $row['stVal1'];
					$stockInfo['val2'] = $row['stVal2'];
					$stockInfo['val3'] = $row['stVal3'];
					$stockInfo['price1'] = $row['stPrice1'];
					$stockInfo['price2'] = $row['stPrice2'];
					$stockInfo['price3'] = $row['stPrice3'];
					$stockInfo['stockcity_name'] = $row['stCityName']; 
					$stockInfo['city_priority'] = $row['stCityPriority'];
					$stockInfo['date'] = $row['stCityDate']; 
					$stockInfo['chodname'] = $row['stChodname'];
					array_push($product['stocks'], $stockInfo);
				}

				$lastProductId = $row['id'];
				$lastInputRequestId = $row['inpId'];
			}
			else
			{
				//синонимы
				$notFound=true;
				for ($i=0;$i<count($product['inputRequests']);$i++)
				{
					if ($product['inputRequests'][$i]['id']==$row['inpId'])
					{
						$notFound=false;
						break;
					}
				}
				if ($notFound)
				{
					$inputRequest = array();
					$inputRequest['id'] = $row['inpId'];
					$inputRequest['name'] = $row['inpName'];
					$inputRequest['chod'] = $row['inpChod'];
					$inputRequest['chod_display'] = $row['inpChodDisplay'];
					$inputRequest['original_item'] = $row['inpOriginalItem'];
					$inputRequest['provider_id'] = $row['inpProviderId'];
					$inputRequest['provprod_id'] = $row['inpProvProdId'];
					array_push($product['inputRequests'], $inputRequest);
					$lastInputRequestId = $row['inpId'];
				}
	
				//поставщики
				$notFound=true;
				for ($i=0;$i<count($product['provProds']);$i++)
				{
					if ($product['provProds'][$i]['id']==$row['ppId'])
					{
						$notFound=false;
						break;
					}
				}

				if ($notFound)
				{
					$provProd = array();
					$provProd['id'] = $row['ppId'];
					$provProd['provider_id'] = $row['ppProvId'];
					$provProd['price_in'] = $row['ppPriceIn'];
					$provProd['price_orig'] = $row['ppPriceOrig'];
					$provProd['delivery_time'] = $row['ppDelivTime'];
					$provProd['delivery_comment'] = $row['ppDelivComment'];
					$provProd['provider_name'] = $row['pvName'];
					$provProd['price_date'] = $row['ppPriceDate'];
					$provProd['notInPrice'] = $row['ppNotInPrice'];
					array_push($product['provProds'], $provProd);
				}

				//остатки на складах
				$notFound=true;
				for ($i=0;$i<count($product['stocks']);$i++)
				{
					if ($product['stocks'][$i]['id']==$row['stId'])
					{
						$notFound=false;
						break;
					}
				}



				if ($notFound)
				{
					$stockInfo = array();
					$stockInfo['id'] = $row['stId'];
					$stockInfo['stockcity_id'] = $row['stCityId'];
					$stockInfo['val1'] = $row['stVal1'];
					$stockInfo['val2'] = $row['stVal2'];
					$stockInfo['val3'] = $row['stVal3'];
					$stockInfo['price1'] = $row['stPrice1'];
					$stockInfo['price2'] = $row['stPrice2'];
					$stockInfo['price3'] = $row['stPrice3'];
					$stockInfo['stockcity_name'] = $row['stCityName'];
					$stockInfo['city_priority'] = $row['stCityPriority'];
					$stockInfo['date'] = $row['stCityDate'];
					$stockInfo['chodname'] = $row['stChodname'];
					array_push($product['stocks'], $stockInfo);
				}

				
			}
		}

		return $product;
	}

	// посик для автоподстановки, при добавлении товара в заявку
	public static function searchType($searchStr)
	{
		//найдём в строке слова похожие на ЧОД и удалим из них лишние разделители (оставив ток буквы и цифры)
		$words=preg_split("/[\s,]+/", $searchStr);
		$chodDetection = 3; // количество цифр, которое должно быть в слове что бы это был ЧОД
		
		for ($wC=0;$wC<count($words);$wC++)
		{
			$numericCount = 0;
			$itIsChod=false;
			for ($i=0;$i<strlen($words[$wC]);$i++)
			{
				if (ctype_digit($words[$wC][$i]))
				{
					$numericCount++;
					if ($numericCount==$chodDetection)
					{
						// у ЧОД звездочки и слева и справа
						$words[$wC] = '*'.preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]).'*';
						$itIsChod = true;
						break;
					}
				}
			}
			if (!$itIsChod) $words[$wC] = $words[$wC].'*';
		}
		
		$searchStr =implode(" ", $words);

		//Экранирурованием слешами символов в поисковой строке
		$from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=', "'", "\x00", "\n", "\r", "\x1a" );
	    $to   = array ( '\\\\', '\\\(','\\\)','\\\|','\\\-','\\\!','\\\@','\\\~','\\\"', '\\\&', '\\\/', '\\\^', '\\\$', '\\\=', "\\'", "\\x00", "\\n", "\\r", "\\x1a" );
	    $searchStr = str_replace ( $from, $to, $searchStr );

		$searchStr = "'".$searchStr."'";
		// $searchStr='"'.$searchStr.'"/2';
		// $searchStr ="'".$searchStr."'";
		
		$sSql = 'SELECT *,WEIGHT() as weight FROM inputRequests WHERE MATCH('.$searchStr.") "; //matchany
		$ids_raw = Yii::app()->sphinx->createCommand($sSql)->queryAll();

		if (count($ids_raw)==0) return null;

		$ids = array();
		foreach ($ids_raw as $id_raw)
		{
			array_push($ids,$id_raw['id']);
		}
		$ids_str = implode(",", $ids);

		$criteria = new CDbCriteria();
		$criteria->addInCondition("id", $ids);
		$ids_str = implode(",", $ids);
		$criteria->order = "FIELD(id, $ids_str)";
		$inputRequests = InputRequests::model()->findAll($criteria);
		$results = array();
		$i=0;
		foreach ($inputRequests as $inputRequest)
		{
			$tmp = array();
			$tmp['name'] = $inputRequest->name.' '.$inputRequest->chod_display;
			$tmp['id'] =  $i;//$inputRequest->id;
			$i++;
			array_push($results, $tmp);
		}

      return $results;
	}

	// c помощью sphinx ищет строку по полям name,chod в индексе inputRequests
	// далее из mysql выбираем product-ы соответствующие найденны inputRequest.id
	// разные inputRequest могут относится к одному и томуже product
	// поэтому выборку product делаем без повтора
	// нас интересует ситуация, когда таким образом найдется лишь один товар
	// поскольку только в этом случае можно однозначно автоматически подставить его в 
	// список заказываемых позиций
	public static function findProductForPosition($searchStr)
	{
		//Экранирурованием слешами символов в поисковой строке
		$from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=', "'", "\x00", "\n", "\r", "\x1a" );
	    $to   = array ( '\\\\', '\\\(','\\\)','\\\|','\\\-','\\\!','\\\@','\\\~','\\\"', '\\\&', '\\\/', '\\\^', '\\\$', '\\\=', "\\'", "\\x00", "\\n", "\\r", "\\x1a" );
	    $searchStr = str_replace ( $from, $to, $searchStr );
	    
	    $searchStr = "'".$searchStr."'";
		// sum((4*lcs+2*(min_hit_pos==1)+exact_hit*100)*user_weight)*1000+bm25
		// sum(lcs*user_weight)*1000+bm25
		//ORDER BY weight DESC LIMIT 0,1000 OPTION ranker=expr('sum(IF(exact_hit,1,0))+bm25')
		$sSql = "SELECT *,WEIGHT() as weight FROM inputRequests WHERE MATCH(".$searchStr.") "; //matchany
		$ids_raw = Yii::app()->sphinx->createCommand($sSql)->queryAll();
		
		if (count($ids_raw)==0) return null;
		$ids = array();
		foreach ($ids_raw as $id_raw)
		{
			//var_dump($id_raw);
			//echo $id_raw['id'].' | '.$id_raw['weight'].'<br>';
			array_push($ids,$id_raw['id']);
		}		
		
		$ids_str = implode(",", $ids);
		/*		
		$sql="SELECT *
				FROM products
				WHERE products.id IN
				(
					SELECT DISTINCT input_requests.product_id
					FROM input_requests
					WHERE input_requests.id IN ($ids_str)
				)
				;";
		*/

		$sql="SELECT findedprod.*, input_requests.id AS inpId, input_requests.name AS inpName, input_requests.chod AS inpChod, input_requests.chod_display AS inpChodDisplay, input_requests.original_item AS inpOriginalItem, input_requests.provider_id AS inpProviderId, input_requests.provprod_id AS inpProvProdId, 
		             provprod.id AS ppId, provprod.provider_id AS ppProvId, provprod.price_in AS ppPriceIn, provprod.price_orig AS ppPriceOrig, provprod.delivery_time AS ppDelivTime, provprod.delivery_comment AS ppDelivComment, provprod.notInPrice AS ppNotInPrice,
		             prov.name AS pvName
			FROM
			(
			SELECT *
			FROM products
			WHERE products.hidden=0 AND products.id IN
			(
					SELECT DISTINCT input_requests.product_id
					FROM input_requests
					WHERE input_requests.id IN ($ids_str)
			)
			) findedprod
			LEFT OUTER JOIN input_requests ON findedprod.id = input_requests.product_id
			LEFT OUTER JOIN prov_prods provprod ON input_requests.product_id = provprod.product_id
			LEFT OUTER JOIN providers prov ON provprod.provider_id = prov.id

			ORDER BY findedprod.id ASC , input_requests.id ASC 
			;";

		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$dataReader=$command->query();
	   
		$lastProductId=-1;
		$lastInputRequestId = -1;

		$product = null;
		

		// Объединим одинаковый товары с разными inputRequest
		// если будет только один товар - выдадим его, если несколько вернём null
		// (один нашедшийся - это однозначное соответствие с поисковым запросом,
		//  в случае если их несколько - надо воспользоваться liveSearch)
		
		foreach($dataReader as $row)
		{
			//var_dump($row['id']);
			if ($row['id']!=$lastProductId)
			{
				if ($product!=null) {
					//var_dump($row['id'].' '.$row['original_name'].' '.$row['chod_display']);
					return null;
				}

				$product=array();
				$product['id'] = $row['id'];
				$product['original_name'] = $row['original_name'];
				$product['original_chod'] = $row['original_chod'];
				$product['chod_display'] = $row['chod_display'];
				$product['product_num'] = $row['product_num'];
				$product['defaultprovprod_id'] = $row['defaultprovprod_id'];
				$product['price_chtz'] = $row['price_chtz'];
				$product['size'] = $row['size'];
				$product['weight'] = $row['weight'];
				$product['comment'] = $row['comment'];
				$product['img'] = $row['img'];
				$product['hidden'] = $row['hidden'];
				$product['inputRequests'] = array();
				$product['inputRequests'][0]['id'] = $row['inpId'];
				$product['inputRequests'][0]['name'] = $row['inpName'];
				$product['inputRequests'][0]['chod'] = $row['inpChod'];
				$product['inputRequests'][0]['chod_display'] = $row['inpChodDisplay'];
				$product['inputRequests'][0]['original_item'] = $row['inpOriginalItem'];
				$product['inputRequests'][0]['provider_id'] = $row['inpProviderId'];
				$product['inputRequests'][0]['provprod_id'] = $row['inpProvProdId'];
				$product['provProds']=array();
				$product['provProds'][0]['id'] = $row['ppId'];
				$product['provProds'][0]['provider_id'] = $row['ppProvId'];
				$product['provProds'][0]['price_in'] = $row['ppPriceIn'];
				$product['provProds'][0]['price_orig'] = $row['ppPriceOrig'];
				$product['provProds'][0]['delivery_time'] = $row['ppDelivTime'];
				$product['provProds'][0]['delivery_comment'] = $row['ppDelivComment'];
				$product['provProds'][0]['provider_name'] = $row['pvName'];
				$product['provProds'][0]['price_date'] = $row['ppPriceDate'];
				$product['provProds'][0]['notInPrice'] = $row['ppNotInPrice'];

				$lastProductId = $row['id'];
				$lastInputRequestId = $row['inpId'];
			}
			else
			{
				if ($lastInputRequestId != $row['inpId'])
				{
					$inputRequest = array();
					$inputRequest['id'] = $row['inpId'];
					$inputRequest['name'] = $row['inpName'];
					$inputRequest['chod'] = $row['inpChod'];
					$inputRequest['chod_display'] = $row['inpChodDisplay'];
					$inputRequest['original_item'] = $row['inpOriginalItem'];
					$inputRequest['provider_id'] = $row['inpProviderId'];
					$inputRequest['provprod_id'] = $row['inpProvProdId'];
					array_push($product['inputRequests'], $inputRequest);
					$lastInputRequestId = $row['inpId'];
				}
				else
				{
					$notFound=true;
					for ($i=0;$i<count($product['provProds']);$i++)
					{
						if ($product['provProds'][$i]['id']==$row['ppId'])
						{
							$notFound=false;
							break;
						}
					}

					if ($notFound)
					{
						$provProd = array();
						$provProd['id'] = $row['ppId'];
						$provProd['provider_id'] = $row['ppProvId'];
						$provProd['price_in'] = $row['ppPriceIn'];
						$provProd['price_orig'] = $row['ppPriceOrig'];
						$provProd['delivery_time'] = $row['ppDelivTime'];
						$provProd['delivery_comment'] = $row['ppDelivComment'];
						$provProd['provider_name'] = $row['pvName'];
						$provProd['price_date'] = $row['ppPriceDate'];
						$provProd['notInPrice'] = $row['ppNotInPrice'];
						array_push($product['provProds'], $provProd);
					}
				}
			}
		}

		return $product;
	}

	public static function findProductsByStocksChod($chod)
	{
		
		$chod = preg_replace('/[^a-zа-яё\d]/ui','',$chod);
		
		$sql=  "SELECT *
				FROM products
				WHERE products.id IN
				(
					SELECT DISTINCT input_requests.product_id
					FROM input_requests
					WHERE input_requests.chod='".$chod."'

				) 
				;";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$rows=$command->queryAll();
		
		return $rows; 	
	}	

	/*
	public static function findProductsByChodName($chodname)
	{
		$words=preg_split("/[\s,]+/", $chodname);
		
		$chodname = preg_replace('/[^a-zа-яё\d]/ui','',$words[0]);

		$from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=', "'", "\x00", "\n", "\r", "\x1a" );
	    $to   = array ( '\\\\', '\\\(','\\\)','\\\|','\\\-','\\\!','\\\@','\\\~','\\\"', '\\\&', '\\\/', '\\\^', '\\\$', '\\\=', "\\'", "\\x00", "\\n", "\\r", "\\x1a" );
	    $chodname = str_replace ( $from, $to, $chodname );
	    $chodname = "'".$chodname."'";
		// sum((4*lcs+2*(min_hit_pos==1)+exact_hit*100)*user_weight)*1000+bm25
		// sum(lcs*user_weight)*1000+bm25
		//ORDER BY weight DESC LIMIT 0,1000 OPTION ranker=expr('sum(IF(exact_hit,1,0))+bm25')


		// $sSql = 'SELECT *,WEIGHT() as weight FROM inputRequests WHERE MATCH(:chodname)'; //matchany
		
		// $command=Yii::app()->sphinx->createCommand($sSql);
		// $command->bindParam(":chodname",$chodname,PDO::PARAM_STR);

		// $ids = $command->queryColumn();
		$sSql = 'SELECT *,WEIGHT() as weight FROM inputRequests WHERE MATCH('.$chodname.') '; //matchany
		$ids = Yii::app()->sphinx->createCommand($sSql)->queryColumn();
		
		
		if (count($ids)==0) return null;
		$ids_str = implode(",", $ids);

		$sql=  "SELECT *
				FROM products
				WHERE products.hidden=0 AND products.id IN
				(
					SELECT DISTINCT input_requests.product_id
					FROM input_requests
					WHERE input_requests.id IN ($ids_str)
				) 
				;";
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$rows=$command->queryAll();
		
		return $rows; 
	}
	*/


	private static function sqlForPagination($where,$limit,$showHidden_where)
	{
		$sql="SELECT findedprod.*, input_requests.id AS inpId, input_requests.name AS inpName, input_requests.chod AS inpChod, input_requests.chod_display AS inpChodDisplay, input_requests.original_item AS inpOriginalItem, input_requests.provider_id AS inpProviderId, input_requests.provprod_id AS inpProvProdId,
					 provprod.id AS ppId, provprod.provider_id AS ppProvId, provprod.price_in AS ppPriceIn, provprod.price_orig AS ppPriceOrig, provprod.price_date AS ppPriceDate, provprod.delivery_time AS ppDelivTime, provprod.delivery_comment AS ppDelivComment, provprod.notInPrice as ppNotInPrice,
					 prov.name AS pvName 
			FROM
			(
				SELECT *
				FROM products
				WHERE $showHidden_where products.id IN
				(
					SELECT input_requests.product_id
					FROM input_requests
					$where
				)
				$limit
			) findedprod
			LEFT OUTER JOIN input_requests ON findedprod.id = input_requests.product_id
			LEFT OUTER JOIN prov_prods provprod ON input_requests.product_id = provprod.product_id
			LEFT OUTER JOIN providers prov ON provprod.provider_id = prov.id

			ORDER BY findedprod.id ASC , input_requests.id ASC 
			;";
		return $sql;
	}

	private static function countForPagination($where,$showHidden_where)
	{

        $sql="SELECT COUNT( * )
			FROM products
			WHERE $showHidden_where products.id
			IN (

			SELECT input_requests.product_id
			FROM input_requests
			$where
			)
			;";
		return $sql;
	}

	public static function getProductsWithInputsForPagination($filters,$offset,$count,$showHidden)
	{
		$connection=Yii::app()->db;
        $command=$connection->createCommand('');
        $where = "";
        $limit = "LIMIT $offset,$count";

        if ($showHidden) $showHidden_where = "";
        else $showHidden_where = "products.hidden=0 AND";
        
        $dataReader;
      	$resultsCount;
      	
      	
        //посик не задан
        if ($filters['name']==null && $filters['chod']==null)
        {
        	$where = "";
      		$command->text=Products::model()->sqlForPagination($where,$limit,$showHidden_where);     
			$dataReader=$command->query();

			$command->text=Products::model()->countForPagination($where,$showHidden_where); 
			$dataReaderCount=$command->query();
			$rowsCount = $dataReaderCount->readAll();
			$resultsCount = $rowsCount[0]['COUNT( * )'];
        }

        //посик только по имени
        if ($filters['name']!=null && $filters['chod']==null)
        {
        	$where = "WHERE input_requests.name LIKE :name";

        	$command->text=Products::model()->sqlForPagination($where,$limit,$showHidden_where); 
        	$filters['name'] = '%'.$filters['name'].'%';
        	$command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
        	$dataReader=$command->query();


			$command->text=Products::model()->countForPagination($where,$showHidden_where); 
			$command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
			$dataReaderCount=$command->query();
			$rowsCount = $dataReaderCount->readAll();
			$resultsCount = $rowsCount[0]['COUNT( * )'];
			
     	}

        //посик только по чод
        if ($filters['name']==null && $filters['chod']!=null)
        {
        	$where = "WHERE input_requests.chod LIKE :chod";
        	$command->text=Products::model()->sqlForPagination($where,$limit,$showHidden_where);
        	$filters['chod'] = '%'.$filters['chod'].'%';      
        	$command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
        	$dataReader=$command->query();

        	$command->text=Products::model()->countForPagination($where,$showHidden_where); 
			$command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
			$dataReaderCount=$command->query();
			$rowsCount = $dataReaderCount->readAll();
			$resultsCount = $rowsCount[0]['COUNT( * )'];
        }

        //поиск только по имени и чод
        if ($filters['name']!=null && $filters['chod']!=null)
        {
        	$where = "WHERE input_requests.name LIKE :name AND input_requests.chod LIKE :chod";
        	$command->text=Products::model()->sqlForPagination($where,$limit,$showHidden_where);  
        	$filters['name'] = '%'.$filters['name'].'%';
        	$filters['chod'] = '%'.$filters['chod'].'%';   
        	$command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
        	$command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
        	$dataReader=$command->query();

        	$command->text=Products::model()->countForPagination($where,$showHidden_where); 
        	$command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
			$command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
			$dataReaderCount=$command->query();
			$rowsCount = $dataReaderCount->readAll();
			$resultsCount = $rowsCount[0]['COUNT( * )'];
        }
    
		$lastProductId=-1;
		$lastInputRequestId = -1;
		

		$products = array();
		
		foreach($dataReader as $row)
		{
			if ($row['id']!=$lastProductId)
			{
				$product=array();
				$product['id'] = $row['id'];
				$product['original_name'] = $row['original_name'];
				$product['original_chod'] = $row['original_chod'];
				$product['chod_display'] = $row['chod_display'];
				$product['product_num'] = $row['product_num'];
				$product['defaultprovprod_id'] = $row['defaultprovprod_id'];
				$product['price_chtz'] = $row['price_chtz'];
				$product['size'] = $row['size'];
				$product['weight'] = $row['weight'];
				$product['comment'] = $row['comment'];
				$product['img'] = $row['img'];
				$product['hidden'] = $row['hidden'];
				$product['inputRequests'] = array();
				$product['inputRequests'][0]['id'] = $row['inpId'];
				$product['inputRequests'][0]['name'] = $row['inpName'];
				$product['inputRequests'][0]['chod'] = $row['inpChod'];
				$product['inputRequests'][0]['chod_display'] = $row['inpChodDisplay'];
				$product['inputRequests'][0]['original_item'] = $row['inpOriginalItem'];
				$product['inputRequests'][0]['provider_id'] = $row['inpProviderId'];
				$product['inputRequests'][0]['provider_id'] = $row['inpProvProdId'];
				$product['provProds']=array();
				$product['provProds'][0]['id'] = $row['ppId'];
				$product['provProds'][0]['provider_id'] = $row['ppProvId'];
				$product['provProds'][0]['price_in'] = $row['ppPriceIn'];
				$product['provProds'][0]['price_orig'] = $row['ppPriceOrig'];
				$product['provProds'][0]['delivery_time'] = $row['ppDelivTime'];
				$product['provProds'][0]['delivery_comment'] = $row['ppDelivComment'];
				$product['provProds'][0]['provider_name'] = $row['pvName'];
				$product['provProds'][0]['price_date'] = $row['ppPriceDate'];
				$product['provProds'][0]['notInPrice'] = $row['ppNotInPrice'];

				$tmp = array();
				$tmp['product'] = $product;
				array_push($products, $tmp);

				$lastProductId = $row['id'];
				$lastInputRequestId = $row['inpId'];
				
			}
			//значит либо есть еще один InputRequest, либо ProvProd, относящиеся к этому товару
			else
			{
				if ($lastInputRequestId != $row['inpId'])
				{
					$inputRequest = array();
					$inputRequest['id'] = $row['inpId'];
					$inputRequest['name'] = $row['inpName'];
					$inputRequest['chod'] = $row['inpChod'];
					$inputRequest['chod_display'] = $row['inpChodDisplay'];
					$inputRequest['original_item'] = $row['inpOriginalItem'];
					$inputRequest['provider_id'] = $row['inpProviderId'];
					$inputRequest['provider_id'] = $row['inpProvProdId'];
					$lastRowNumber = count($products)-1;
					array_push($products[$lastRowNumber]['product']['inputRequests'], $inputRequest);
					$lastInputRequestId=$row['inpId'];
				}
				else
				{
					$lastRowNumber = count($products)-1;
					$notFound=true;
					for ($i=0;$i<count($products[$lastRowNumber]['product']['provProds']);$i++)
					{
						if ($products[$lastRowNumber]['product']['provProds'][$i]['id']==$row['ppId'])
						{
							$notFound=false;
							break;
						}
					}

					if ($notFound)
					{
						$provProd = array();
						$provProd['id'] = $row['ppId'];
						$provProd['provider_id'] = $row['ppProvId'];
						$provProd['price_in'] = $row['ppPriceIn'];
						$provProd['price_orig'] = $row['ppPriceOrig'];
						$provProd['delivery_time'] = $row['ppDelivTime'];
						$provProd['provider_name'] = $row['pvName'];
						$provProd['price_date'] = $row['ppPriceDate'];
						$provProd['notInPrice'] = $row['ppNotInPrice'];
						array_push($products[$lastRowNumber]['product']['provProds'], $provProd);
					}
				}	
			}
		}

		$result = array();
		$result['products'] = $products;
		$result['resultsCount'] = $resultsCount;

		return $result; 
	}

	public function updateProductInfo($product_id,$original_name,$original_chod,$chod_display,$catalog_name,$catalog_chod,$product_num,$price_chtz,$weight)
	{
		$product = $this->findByPk($product_id);
		if ($product==null)
		{
			return false;
		}

		$product->original_name = $original_name;
		$product->original_chod = $original_chod;
		$product->chod_display = $chod_display;
		$product->catalog_name = $catalog_name;
		$product->catalog_chod = $catalog_chod;
		$product->product_num = $product_num;
		$product->price_chtz = $price_chtz;
		$product->weight = $weight;
		$res = $product->update();
		if ($res==false)
		{
			return false;
		}

		//так же поменять inputRequest c original_item==1, содержащий ориг. назв. ориг. чод.
		$criteria =  new CDbCriteria;
		$criteria->condition = 'product_id =:product_id AND original_item=1';
		$criteria->params = array(':product_id'=>$product_id);
		$inputRequest = InputRequests::model()->find($criteria);
		if ($inputRequest==null)
		{
			return false;
		}
		$inputRequest->name = $original_name;
		$inputRequest->chod = $original_chod;
		$inputRequest->chod_display = $chod_display;

		$res = $inputRequest->update();

		if ($res==false)
		{
			return false;
		}

		return true;
	}

	public function updateSize($product_id,$size)
	{
		$product = $this->findByPk($product_id);
		if ($product==null)
		{
			return false;
		}

		$product->size = $size;
		$res = $product->update();
		if ($res==false)
		{
			return false;
		}
		return true;
	}

	public function setHidden($product_id,$hidden)
	{
		$product = $this->findByPk($product_id);
		if ($product==null)
		{
			return false;
		}

		$product->hidden = $hidden;
		$res = $product->update();
		if ($res==false)
		{
			return false;
		}
		return true;
	}

	public static function updateImg($productId,$fileUrl)
	{
		try {
			$product = Products::model()->findByPk($productId);
			$product->img = $fileUrl;
			$res = $product->update(); 
			return $res;
		}
		catch (Exception $e) 
		{
			return false;
		}
	}

	public static function deleteImg($productId,$fileFolder)
	{
		$product = Products::model()->findByPk($productId);

		if ($product->img!=null)
		{

			$oldFileName = pathinfo($product->img,PATHINFO_BASENAME);
			$oldFilePath = $fileFolder.$oldFileName;
			if (file_exists($oldFilePath))
			{
				$fileDeleted = unlink($oldFilePath);
			}
			$product->img = null;
			$product->update();
		}

		return $fileDeleted;
	}



	public static function getProductsByCatalogcat($catalogcatId)
	{
		$sql=  "SELECT p.id,p.original_name,p.chod_display,group_prods.group_id, catalogcat_prods.id as catalogcat_prods_id
				FROM products p
				LEFT JOIN catalogcat_prods ON catalogcat_prods.product_id = p.id
				LEFT JOIN group_prods ON group_prods.product_id = p.id
				WHERE catalogcat_prods.catalogcat_id = :catalogcat_id 

				;";



		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":catalogcat_id",$catalogcatId,PDO::PARAM_INT);
		$productRows=$command->queryAll();

		

		$products = array();
		$lastProductId = null;
		$firstIteration = true;
		$groups = GroupsManager::getAllGroupsInfo();
		$groupsById = GroupsManager::getAllGroupsKeyById($groups);

		foreach ($productRows as $row)
		{
			$group = $groupsById[$row['group_id']];
			
			if ($row['id']!=$lastProductId)
			{
				$product = array();
				$product['id'] = $row['id'];
				$product['original_name'] = $row['original_name'];
				$product['chod_display'] = $row['chod_display'];
				
				
				$product['groups'] = array();
				$product['groups'][] = $group;
				$product['groupNumStr'] = $group['group_num'];

				$products[] = $product;
			}
			else
			{
				$lastProductsIndex = count($products)-1;
				$products[$lastProductsIndex]['groups'][] = $group;
				$products[$lastProductsIndex]['groupNumStr'].= ', '.$group['group_num'];
			}

			$lastProductId = $row['id'];

		}

		return $products;
	}

	/**
	 * Увеличить на 1 значение reuisitions_count для заданного товара
	 * @param int $product_id
	 * @return bool $result
	 */ 
	public static function increaseRating($product_id)
	{
		$product = Products::model()->findByPk($product_id);
		$product->rating = $product->rating + 1;
		$res = $product->update();
		return $res;
	}

	/**
	 * Уменьшить на 1 значение reuisitions_count для заданного товара
	 * @param int $product_id
	 * @return bool $result
	 */ 
	public static function decreaseRating($product_id)
	{
		$product = Products::model()->findByPk($product_id);
		
		$res = true;
		if ($product->rating!=0)
		{
			$product->rating = $product->rating - 1;
			$res = $product->update();
		}		

		return $res;
	}


	/*
	protected function beforeSave()
	{
        parent::beforeSave();

        if($this->isNewRecord)
        {  
        	$vitrinaProduct = new VitrinaProducts;
        	$vitrinaProduct->original_name = $this->original_name;
			$vitrinaProduct->original_chod = $this->original_chod;
			$vitrinaProduct->chod_display = $this->chod_display;
			$vitrinaProduct->product_num = $this->product_num;
			$vitrinaProduct->comment = $this->comment;
			$vitrinaProduct->tovar_type = $this->tovar_type;
			$vitrinaProduct->other_info = $this->other_info;
			$vitrinaProduct->defaultprovider_id = $this->defaultprovider_id;
			$vitrinaProduct->price_out = $this->price_out;
			$vitrinaProduct->size = $this->size;
			$vitrinaProduct->weight = $this->weight;
			$vitrinaProduct->parts_id = $this->parts_id;
			$vitrinaProduct->partof_id = $this->partof_id;
			$vitrinaProduct->use_deafult = $this->use_deafult;

			$res=$vitrinaProduct->save();
			if ($res==false)
			{
				var_dump($vitrinaProduct);
			}
        }
        else
        {
        	$product = Products::model()->findByPk($this->id);
        	$criteria = new CDbCriteria;
        	$criteria->compare('original_name',$product->original_name);
        	$criteria->compare('original_chod',$product->original_chod);
        	$criteria->compare('product_num',$product->product_num);

        	$vitrinaProduct = VitrinaProducts::model()->find($criteria);
        	$vitrinaProduct->original_name = $this->original_name;
			$vitrinaProduct->original_chod = $this->original_chod;
			$vitrinaProduct->chod_display = $this->chod_display;
			$vitrinaProduct->product_num = $this->product_num;
			$vitrinaProduct->comment = $this->comment;
			$vitrinaProduct->tovar_type = $this->tovar_type;
			$vitrinaProduct->other_info = $this->other_info;
			$vitrinaProduct->defaultprovider_id = $this->defaultprovider_id;
			$vitrinaProduct->price_out = $this->price_out;
			$vitrinaProduct->size = $this->size;
			$vitrinaProduct->weight = $this->weight;
			$vitrinaProduct->parts_id = $this->parts_id;
			$vitrinaProduct->partof_id = $this->partof_id;
			$vitrinaProduct->use_deafult = $this->use_deafult;

			$res=$vitrinaProduct->update();
			if ($res==false)
			{
				var_dump($vitrinaProduct);
			}

        }

        return true;
    }
	*/


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('original_chod',$this->original_chod,true);
		$criteria->compare('chod_display',$this->chod_display,true);
		$criteria->compare('catalog_name',$this->catalog_name,true);
		$criteria->compare('catalog_chod',$this->catalog_chod,true);
		$criteria->compare('product_num',$this->product_num,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('tovar_type',$this->tovar_type,true);
		$criteria->compare('other_info',$this->other_info,true);
		$criteria->compare('defaultprovider_id',$this->defaultprovider_id);
		$criteria->compare('defaultprovprod_id',$this->defaultprovprod_id);
		$criteria->compare('price_chtz',$this->price_chtz);
		$criteria->compare('price_catalog',$this->price_catalog,true);
		$criteria->compare('hidden',$this->hidden);
		$criteria->compare('price_out',$this->price_out);
		$criteria->compare('size',$this->size,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('parts_id',$this->parts_id);
		$criteria->compare('partof_id',$this->partof_id);
		$criteria->compare('use_deafult',$this->use_deafult,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
