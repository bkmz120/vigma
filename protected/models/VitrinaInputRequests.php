 <?php

/**
 * This is the model class for table "input_requests".
 *
 * The followings are the available columns in table 'input_requests':
 * @property integer $id
 * @property string $name
 * @property string $chod
 * @property string $chod_display
 * @property string $comment
 * @property integer $product_id
 * @property integer $original_item
 * @property string $chod_utochn
 * @property string $chod_chtz
 * @property string $name_chtz
 * @property string $pokupka
 * @property string $istochnik
 * @property double $price_in
 *
 * The followings are the available model relations:
 * @property Products $product
 */
class VitrinaInputRequests extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'input_requests';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_id, original_item', 'numerical', 'integerOnly'=>true),
            array('price_in', 'numerical'),
            array('name, chod', 'length', 'max'=>256),
            array('chod_display, chod_utochn, chod_chtz, name_chtz, pokupka, istochnik', 'length', 'max'=>255),
            array('comment', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, chod, chod_display, comment, product_id, original_item, chod_utochn, chod_chtz, name_chtz, pokupka, istochnik, price_in', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'chod' => 'Chod',
            'chod_display' => 'Chod Display',
            'comment' => 'Comment',
            'product_id' => 'Product',
            'original_item' => 'Original Item',
            'chod_utochn' => 'Chod Utochn',
            'chod_chtz' => 'Chod Chtz',
            'name_chtz' => 'Name Chtz',
            'pokupka' => 'Pokupka',
            'istochnik' => 'Istochnik',
            'price_in' => 'Price In',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('chod',$this->chod,true);
        $criteria->compare('chod_display',$this->chod_display,true);
        $criteria->compare('comment',$this->comment,true);
        $criteria->compare('product_id',$this->product_id);
        $criteria->compare('original_item',$this->original_item);
        $criteria->compare('chod_utochn',$this->chod_utochn,true);
        $criteria->compare('chod_chtz',$this->chod_chtz,true);
        $criteria->compare('name_chtz',$this->name_chtz,true);
        $criteria->compare('pokupka',$this->pokupka,true);
        $criteria->compare('istochnik',$this->istochnik,true);
        $criteria->compare('price_in',$this->price_in);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->vitrinaDb;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return InputRequests the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}