<?php

/**
 * This is the model class for table "input_requests".
 *
 * The followings are the available columns in table 'input_requests':
 * @property integer $id
 * @property string $name
 * @property string $chod
 * @property string $chod_display
 * @property string $comment
 * @property integer $product_id
 * @property integer $original_item
 * @property integer $provider_id
 * @property integer $provprod_id
 * @property string $chod_utochn
 * @property string $chod_chtz
 * @property string $name_chtz
 * @property string $pokupka
 * @property string $istochnik
 * @property double $price_in
 * @property integer $use_default
 *
 * The followings are the available model relations:
 * @property Products $product
 */
class InputRequests extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'input_requests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, original_item, provider_id, provprod_id, use_default', 'numerical', 'integerOnly'=>true),
			array('price_in', 'numerical'),
			array('name, chod', 'length', 'max'=>256),
			array('chod_display, chod_utochn, chod_chtz, name_chtz, pokupka, istochnik', 'length', 'max'=>255),
			array('comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, chod, chod_display, comment, product_id, original_item, provider_id, provprod_id, chod_utochn, chod_chtz, name_chtz, pokupka, istochnik, price_in, use_default', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'chod' => 'Chod',
			'chod_display' => 'Chod Display',
			'comment' => 'Comment',
			'product_id' => 'Product',
			'original_item' => 'Original Item',
			'provider_id' => 'Provider',
			'provprod_id' => 'Provprod',
			'chod_utochn' => 'Chod Utochn',
			'chod_chtz' => 'Chod Chtz',
			'name_chtz' => 'Name Chtz',
			'pokupka' => 'Pokupka',
			'istochnik' => 'Istochnik',
			'price_in' => 'Price In',
			'use_default' => 'Use Default',
		);
	}

		public function getInfoArray()
	{
		$infoArray = array();
		$infoArray['id'] = $this->id;
		$infoArray['name'] = $this->name;
		$infoArray['chod'] = $this->chod;
		$infoArray['chod_display'] = $this->chod_display;
		$infoArray['comment'] = $this->comment;
		$infoArray['product_id'] = $this->product_id;
		$infoArray['original_item'] = $this->original_item;
		$infoArray['chod_utochn'] = $this->chod_utochn;
		$infoArray['chod_chtz'] = $this->chod_chtz;
		$infoArray['name_chtz'] = $this->name_chtz;
		$infoArray['pokupka'] = $this->pokupka;
		$infoArray['istochnik'] = $this->istochnik;
		$infoArray['price_in'] = $this->price_in;
		
		return $infoArray;
	}

	public static function getInfoArrayFromArray($inputRequests)
	{
		$inputRequestsArray = array();
		foreach ($inputRequests as $inputRequest)
		{
			array_push($inputRequestsArray, $inputRequest->getInfoArray());
		}
		return $inputRequestsArray;
	}

	public static function deleteByChodNameProductId($product_id,$chod,$name)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "product_id=:product_id AND chod_display=:chod AND name=:name";
		$criteria->params = array('product_id'=>$product_id,
								  'chod'=>$chod,
								  'name'=>$name,
							);

		$inputRequest = InputRequests::model()->find($criteria);

		if ($inputRequest==null) throw new Exception('Синоним не найден!');

		if ($inputRequest->original_item==1) throw new Exception('Нельзя удалить оригинальное назвние товара');
		if ($inputRequest->provprod_id!=null) throw new Exception('Нельзя удалить сионим по поставщику');


		$res = $inputRequest->delete();
		return $res;
	}

	
	protected function afterSave()
	{
		parent::afterSave();
		if ($this->isNewRecord)
		{
			$sphinxSql = "INSERT INTO inputRequests VALUES (".$this->id.", '".$this->name."', '".$this->chod."')";
	
		}
		else
		{
			$sphinxSql = "REPLACE INTO inputRequests VALUES (".$this->id.", '".$this->name."', '".$this->chod."')";
		}
		$res=Yii::app()->sphinx->createCommand($sphinxSql)->execute();
	}

	protected function afterDelete() 
	{
		parent::afterDelete();
		//file_put_contents("/test.txt", "ok");
		$sphinxSql = "DELETE FROM inputRequests WHERE id=".$this->id;
		$res=Yii::app()->sphinx->createCommand($sphinxSql)->execute();
		
	}

	/*
	protected function beforeSave()
	{
        parent::beforeSave();

        if($this->isNewRecord)
        {  
        	
        	$vitrinaInputRequest = new VitrinaInputRequests;
        	$vitrinaInputRequest->name = $this->name;
        	$vitrinaInputRequest->chod = $this->chod;
			$vitrinaInputRequest->chod_display = $this->chod_display;
			$vitrinaInputRequest->comment = $this->comment;
			$vitrinaInputRequest->product_id = $this->product_id;
			$vitrinaInputRequest->original_item = $this->original_item;
			$vitrinaInputRequest->chod_utochn = $this->chod_utochn;
			$vitrinaInputRequest->chod_chtz = $this->chod_chtz;
			$vitrinaInputRequest->name_chtz = $this->name_chtz;
			$vitrinaInputRequest->pokupka = $this->pokupka;
			$vitrinaInputRequest->istochnik = $this->istochnik;
			$vitrinaInputRequest->price_in = $this->price_in;
        	$res = $vitrinaInputRequest->save();
        	if ($res==false)
        	{
        		var_dump($vitrinaInputRequest);
        	}
        	
        }
        else
        {
        	
        	$inputRequest = InputRequests::model()->findByPk($this->id);


        	$criteria = new CDbCriteria;
        	$criteria->compare('name',$inputRequest->name);
        	$criteria->compare('chod',$inputRequest->chod);
        	$criteria->compare('product_id',$inputRequest->product_id);
        	$criteria->compare('comment',$inputRequest->comment);
        	$criteria->compare('original_item',$inputRequest->original_item);

        	$vitrinaInputRequest =  VitrinaInputRequests::model()->find($criteria);
        	
        	
        	$vitrinaInputRequest->name = $this->name;
        	$vitrinaInputRequest->chod = $this->chod;
			$vitrinaInputRequest->chod_display = $this->chod_display;
			$vitrinaInputRequest->comment = $this->comment;
			$vitrinaInputRequest->product_id = $this->product_id;
			$vitrinaInputRequest->original_item = $this->original_item;
			$vitrinaInputRequest->chod_utochn = $this->chod_utochn;
			$vitrinaInputRequest->chod_chtz = $this->chod_chtz;
			$vitrinaInputRequest->name_chtz = $this->name_chtz;
			$vitrinaInputRequest->pokupka = $this->pokupka;
			$vitrinaInputRequest->istochnik = $this->istochnik;
			$vitrinaInputRequest->price_in = $this->price_in;
        	$res = $vitrinaInputRequest->update();
        	if ($res==false)
        	{
        		var_dump($vitrinaInputRequest);
        	}
        	

        }

        return true;
    }
    */

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('chod',$this->chod,true);
		$criteria->compare('chod_display',$this->chod_display,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('original_item',$this->original_item);
		$criteria->compare('provider_id',$this->provider_id);
		$criteria->compare('provprod_id',$this->provprod_id);
		$criteria->compare('chod_utochn',$this->chod_utochn,true);
		$criteria->compare('chod_chtz',$this->chod_chtz,true);
		$criteria->compare('name_chtz',$this->name_chtz,true);
		$criteria->compare('pokupka',$this->pokupka,true);
		$criteria->compare('istochnik',$this->istochnik,true);
		$criteria->compare('price_in',$this->price_in);
		$criteria->compare('use_default',$this->use_default);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InputRequests the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
