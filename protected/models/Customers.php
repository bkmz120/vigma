<?php

/**
 * This is the model class for table "customers".
 *
 * The followings are the available columns in table 'customers':
 * @property integer $id
 * @property string $name
 * @property integer $status_id
 * @property integer $hidden
 * @property string $full_name
 * @property string $contact_person
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $legal_address
 * @property string $mail_address
 * @property string $bank_name
 * @property string $payment_number
 * @property string $bik
 * @property string $corr_number
 * @property string $bank_inn
 * @property string $director_name
 * @property string $director_job
 * @property string $confirm_doc
 *
 * The followings are the available model relations:
 * @property Requisitions[] $requisitions
 */
class Customers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_id, hidden', 'numerical', 'integerOnly'=>true),
			array('name, contact_person, bank_name, payment_number, bik, corr_number, bank_inn, director_name, director_job, confirm_doc', 'length', 'max'=>255),
			array('full_name, address, legal_address, mail_address', 'length', 'max'=>500),
			array('phone, email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, status_id, hidden, full_name, contact_person, phone, email, address, legal_address, mail_address, bank_name, payment_number, bik, corr_number, bank_inn, director_name, director_job, confirm_doc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'requisitions' => array(self::HAS_MANY, 'Requisitions', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'status_id' => 'Status',
			'hidden' => 'Hidden',
			'full_name' => 'Full Name',
			'contact_person' => 'Contact Person',
			'phone' => 'Phone',
			'email' => 'Email',
			'address' => 'Address',
			'legal_address' => 'Legal Address',
			'mail_address' => 'Mail Address',
			'bank_name' => 'Bank Name',
			'payment_number' => 'Payment Number',
			'bik' => 'Bik',
			'corr_number' => 'Corr Number',
			'bank_inn' => 'Bank Inn',
			'director_name' => 'Director Name',
			'director_job' => 'Director Job',
			'confirm_doc' => 'Confirm Doc',
		);
	}

	public function getInfoArray()
	{
		$customerInfo = array();
		$customerInfo['name'] = $this->name;
		$customerInfo['status_id'] = $this->status_id;
		$customerInfo['hidden'] = $this->name;
		$customerInfo['id'] = $this->id;
		$customerInfo['full_name'] = $this->full_name;
		$customerInfo['contact_person'] = $this->contact_person;
		$customerInfo['phone'] = $this->phone;
		$customerInfo['email'] = $this->email;
		$customerInfo['address'] = $this->address;
		$customerInfo['legal_address'] = $this->legal_address;
		$customerInfo['mail_address'] = $this->mail_address;
		$customerInfo['bank_name'] = $this->bank_name;
		$customerInfo['payment_number'] = $this->payment_number;
		$customerInfo['bik'] = $this->bik;
		$customerInfo['corr_number'] = $this->corr_number;
		$customerInfo['bank_inn'] = $this->bank_inn;
		$customerInfo['director_name'] = $this->director_name;
		$customerInfo['director_job'] = $this->director_job;
		$customerInfo['confirm_doc'] = $this->confirm_doc;

		return $customerInfo;
	}

	public static function getInfoArrayFromArray($customers)
	{
		$result = array();
		foreach ($customers as $customer)
		{
			array_push($result, $customer->getInfoArray());
		}
		return $result;
	}

	public static function getInfoById($id) {
		$customer = Customers::model()->findByPk($id);
		if ($customer==null) return false;
		return $customer->getInfoArray();
	}

	public static function getShortInfoById($id) {
		$criteria = new CDbCriteria;
		$criteria->select = "id,name";
		$criteria->condition = "id=:id";
		$criteria->params = array(':id'=>$id);
		$customer = Customers::model()->find($criteria);

		if ($customer==null)
		{
			return null;
		}

		return array('id'=>$customer->id,
					 'name'=>$customer->name,
			);
	}

	public static function getAllInfosWithStatusesAndReqNum($withHidden=false)
	{
		$where = "";
		if (!$withHidden) $where = "WHERE customers.hidden='0' "; 

		$sql = "SELECT customers.*, 
				COUNT( requisitions.customer_id ) as reqNum, 
				customer_statuses.name AS status
				FROM customers
				LEFT OUTER JOIN requisitions ON customers.id = requisitions.customer_id
				LEFT OUTER JOIN customer_statuses ON customer_statuses.id = customers.status_id
				$where
				GROUP BY customers.id;";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$dataReader=$command->query();

		$customersInfos = array();

		foreach($dataReader as $row)
		{
			$customersInfos[]=$row;
		}
		
		return $customersInfos;
	}

	public static function getAllCustomersNames()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 'id,name';
		$criteria->order = 'name ASC';
		$customers = Customers::model()->findAll($criteria);
		$customersInfo = array();
		foreach ($customers as $customer)
		{
			$customerInfo = array('id'=>$customer->id,'name'=>$customer->name);
			array_push($customersInfo, $customerInfo);
		}
		return $customersInfo;
	}

	public function addCustomer($customerInfo)
	{
		$customer = new Customers;
		if (!isset($customerInfo->name) || $customerInfo->name=="") $customerInfo->name = "(без названия)";
		$customer->name = $customerInfo->name;
		$customer->status_id = $customerInfo->status_id;
		$customer->hidden = $customerInfo->hidden;
		$customer->full_name = $customerInfo->full_name;
		$customer->contact_person = $customerInfo->contact_person;
		$customer->phone = $customerInfo->phone;
		$customer->email = $customerInfo->email;
		$customer->address = $customerInfo->address;
		$customer->legal_address = $customerInfo->legal_address;
		$customer->mail_address = $customerInfo->mail_address;
		$customer->bank_name = $customerInfo->bank_name;
		$customer->payment_number = $customerInfo->payment_number;
		$customer->bik = $customerInfo->bik;
		$customer->corr_number = $customerInfo->corr_number;
		$customer->bank_inn = $customerInfo->bank_inn;
		$customer->director_name = $customerInfo->director_name;
		$customer->director_job = $customerInfo->director_job;
		$customer->confirm_doc = $customerInfo->confirm_doc;
		$res = $customer->save();
		if ($res==false) return null;
		else return $customer;
	}

	public function updateCustomer($customerInfo)
	{
		$customer = $this->findByPk($customerInfo->id);
		if ($customer==null) return false;
		$customer->name = $customerInfo->name;
		$customer->status_id = $customerInfo->status_id;
		$customer->hidden = $customerInfo->hidden;
		$customer->full_name = $customerInfo->full_name;
		$customer->contact_person = $customerInfo->contact_person;
		$customer->phone = $customerInfo->phone;
		$customer->email = $customerInfo->email;
		$customer->address = $customerInfo->address;
		$customer->legal_address = $customerInfo->legal_address;
		$customer->mail_address = $customerInfo->mail_address;
		$customer->bank_name = $customerInfo->bank_name;
		$customer->payment_number = $customerInfo->payment_number;
		$customer->bik = $customerInfo->bik;
		$customer->corr_number = $customerInfo->corr_number;
		$customer->bank_inn = $customerInfo->bank_inn;
		$customer->director_name = $customerInfo->director_name;
		$customer->director_job = $customerInfo->director_job;
		$customer->confirm_doc = $customerInfo->confirm_doc;
		$res = $customer->update();
		return $res;
	}

	public static function clearStatusById($status_id)
	{	
		
		try
		{
			$sql = "UPDATE customers SET status_id = NULL WHERE status_id = ".$status_id."";
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql);	
			$rowCount=$command->execute(); 
			
		}
		catch(Exception $e)
		{
			return -1;
		}		

		return $rowCount;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('hidden',$this->hidden);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('contact_person',$this->contact_person,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('legal_address',$this->legal_address,true);
		$criteria->compare('mail_address',$this->mail_address,true);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('payment_number',$this->payment_number,true);
		$criteria->compare('bik',$this->bik,true);
		$criteria->compare('corr_number',$this->corr_number,true);
		$criteria->compare('bank_inn',$this->bank_inn,true);
		$criteria->compare('director_name',$this->director_name,true);
		$criteria->compare('director_job',$this->director_job,true);
		$criteria->compare('confirm_doc',$this->confirm_doc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
