<?php

/**
 * This is the model class for table "stocks".
 *
 * The followings are the available columns in table 'stocks':
 * @property integer $id
 * @property string $chodname
 * @property integer $product_id
 * @property integer $stockcity_id
 * @property integer $val1
 * @property integer $val2
 * @property integer $val3
 * @property string $price1
 * @property string $price2
 * @property string $price3
 * @property integer $tmpInFileCheck
 */
class Stocks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stocks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, stockcity_id', 'required'),
			array('product_id, stockcity_id, val1, val2, val3, tmpInFileCheck', 'numerical', 'integerOnly'=>true),
			array('chodname', 'length', 'max'=>255),
			array('price1, price2, price3', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, chodname, product_id, stockcity_id, val1, val2, val3, price1, price2, price3, tmpInFileCheck', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'chodname' => 'Chodname',
			'product_id' => 'Product',
			'stockcity_id' => 'Stockcity',
			'val1' => 'Val1',
			'val2' => 'Val2',
			'val3' => 'Val3',
			'price1' => 'Price1',
			'price2' => 'Price2',
			'price3' => 'Price3',
			'tmpInFileCheck' => 'Tmp In File Check',
		);
	}

	public static function clearCityTemplate($stockCityId)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "stockcity_id=:stockcity_id";
		$criteria->params = array(':stockcity_id'=>$stockCityId,
								);
		try
		{
            $res=$stocks = Stocks::model()->deleteAll($criteria);
            return true;
        } catch (Exception $e) {
            return false;
        }
		
	}

	public static function countForCity($stockCityId)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "stockcity_id=:stockcity_id";
		$criteria->params = array(':stockcity_id'=>$stockCityId,
								);
		$count = Stocks::model()->count($criteria);
		return $count;
	}

	//обнуляет tmpInFileCheck, что бы помечать при импорте выявить какие товары исчезли из файла-остатков для импортируемого города
	//исчезнувшие из файла остатков значаения удаляем из БД
	public static function clearFileInCheck()
	{
		$sql = "update stocks s SET s.tmpInFileCheck=0";
		$connection=Yii::app()->db;		
		$command=$connection->createCommand($sql);
		$res=$command->execute();
		return true;
	}


	public static function import($filePath,$stockcity_id,$stockCityTemplate)
	{
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel/IOFactory.php';
		
		$startRow = intval($stockCityTemplate['startrownum']);
		$chodNameClm = intval($stockCityTemplate['chodnameclm']);
		$val1Clm = intval($stockCityTemplate['val1clm']);
		$val2Clm = intval($stockCityTemplate['val2clm']);
		$val3Clm = intval($stockCityTemplate['val3clm']);
		$price1Clm = intval($stockCityTemplate['price1clm']);
		$price2Clm = intval($stockCityTemplate['price2clm']);
		$price3Clm = intval($stockCityTemplate['price3clm']);

		Stocks::model()->clearFileInCheck();

		/**  Identify the type of $inputFileName  **/
		$inputFileType = PHPExcel_IOFactory::identify($filePath);
		/**  Create a new Reader of the type that has been identified  **/
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		/**  Load $inputFileName to a PHPExcel Object  **/
		$objPHPExcel = $objReader->load($filePath);

		$objWorksheet = $objPHPExcel->getActiveSheet();
		// Get the highest row and column numbers referenced in the worksheet
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

		$result = array('updateProds'=>array(),
						'oneProds'=>array(),
						'severalProds'=>array(),
						'notFoundProds'=>array(),
						);

		$result['oneProds']['fileRows'] = array();
		$result['updateProds']['fileRows'] = array();
		$result['notNeedUpdateProds']['fileRows'] = array();
		$result['severalProds']['fileRows'] = array();
		$result['notFoundProds']['fileRows'] = array();
		$result['notInFile']['rows'] = array();

		$k=0;


		for ($row = $startRow; $row <= $highestRow; $row++) {
		   

		    $chodname = $objWorksheet->getCellByColumnAndRow($chodNameClm, $row)->getValue();
			
		 	// $chodname= mb_convert_encoding($chodname,"CP1252", 'UTF8');
			// $chodname= mb_convert_encoding($chodname,'UTF8', "CP1251");
		    

		    if ($chodname==null || $chodname=="") continue;

		    if ($val1Clm!=null) $val1 = $objWorksheet->getCellByColumnAndRow($val1Clm, $row)->getValue();
		    else $val1 = null;
			if ($val2Clm!=null) $val2 = $objWorksheet->getCellByColumnAndRow($val2Clm, $row)->getValue();
		    else $val2= null;
		    if ($val3Clm!=null) $val3 = $objWorksheet->getCellByColumnAndRow($val3Clm, $row)->getValue();
		    else $val13= null;

		    if ($price1Clm!=null )$price1 = $objWorksheet->getCellByColumnAndRow($price1Clm, $row)->getValue();
			else $price1=0;			
			if ($price2Clm!=null )$price2 = $objWorksheet->getCellByColumnAndRow($price2Clm, $row)->getValue();
			else $price2=0;
			if ($price3Clm!=null )$price3 = $objWorksheet->getCellByColumnAndRow($price3Clm, $row)->getValue();
			else $price3=0;
			
			$val1 = intval(trim($val1));
			$val2 = intval(trim($val2));
			$val3 = intval(trim($val3));

			$price1 = floatval(trim($price1));
			$price2 = floatval(trim($price2));
			$price3 = floatval(trim($price3));
			
			
			if ($val1!=null && $val1!=0 && $price1!=0) $price1 = number_format(floatval(trim($price1))/$val1, 2, '.', '');
			if ($val2!=null && $val2!=0 && $price2!=0) $price2 = number_format(floatval(trim($price2))/$val2, 2, '.', '');
			if ($val3!=null && $val3!=0 && $price3!=0) $price3 = number_format(floatval(trim($price3))/$val3, 2, '.', '');

			//выделим из chodname ЧОД - всё что до первого пробела
			$words=preg_split("/[\s]+/", $chodname);
			$chod = $words[0];

			$fileRow = array('chodname'=>$chodname,
							 'chod'=>$chod,
							 'val1'=>$val1,
							 'val2'=>$val2,
							 'val3'=>$val3,
							 'price1'=>$price1,
							 'price2'=>$price2,
							 'price3'=>$price3,
						);	

			$res = Stocks::model()->importRow($stockcity_id,$fileRow,$result);
			$k++;

			if ($res==false)
			{
				return false;
			}

		    //if ($row==10) break;
		}

		//var_dump($k);

		// найдем исчезнувшие из файла отстаков товары, это те у которых tmpInFileCheck остался равен нулю для импортируемого города
		$criteria = new CDbCriteria;
		$criteria->condition = "tmpInFileCheck=0 AND stockcity_id=:stockcity_id";
		$criteria->params = array(':stockcity_id'=>$stockcity_id,
								);

		$stocks = Stocks::model()->findAll($criteria);
		foreach ($stocks as $stock)
		{
			$row = array('chodname'=>$stock->chodname,
						 
				);
			$row['product'] = Products::model()->getProductWithFullInfo($stock->product_id); //для карточки товара
			$result['notInFile']['fileRows'][] = $row;
			$stock->val1 = 0;
			$stock->val2 = 0;
			$stock->val3 = 0;
			$stock->price1 = 0;
			$stock->price2 =0;
			$stock->price3 = 0;
			$stock->update();
				
		}

		$result['count'] = Stocks::model()->countForCity($stockcity_id);

		
		return $result;

	}

	

	private static function importRow($stockcity_id,$fileRow,&$result)
	{
		//проверим есть ли для $fileRow['chodname'] запись в таблице Stocks
		$criteria = new CDbCriteria;
		$criteria->condition = "chodname=:chodname AND stockcity_id=:stockcity_id";
		$criteria->params = array(':chodname'=>$fileRow['chodname'],
								  ':stockcity_id'=>$stockcity_id,
								);
		$stock = Stocks::model()->find($criteria);


		//если нашелся,то в случае необходимости - обновим
		if ($stock!=null)
		{
			if ($stock->val1!=$fileRow['val1'] || $stock->val2!=$fileRow['val2'] || $stock->val3!=$fileRow['val3'] || $stock->price1!=$fileRow['price1']  || $stock->price2!=$fileRow['price2']  || $stock->price3!=$fileRow['price3'])
			{
				$stock->val1 = $fileRow['val1'];
				$stock->val2 = $fileRow['val2'];
				$stock->val3 = $fileRow['val3'];
				$stock->price1 = $fileRow['price1'];
				$stock->price2 = $fileRow['price2'];
				$stock->price3 = $fileRow['price3'];
				$stock->tmpInFileCheck = 1;
				$stock->update();
				$fileRow['product'] = Products::model()->getProductWithFullInfo($stock->product_id); //для карточки товара
				$result['updateProds']['fileRows'][] = $fileRow;
			}
			else
			{
				$stock->tmpInFileCheck = 1;
				$stock->update();
				$fileRow['product'] = Products::model()->getProductWithFullInfo($stock->product_id); //для карточки товара
				$result['notNeedUpdateProds']['fileRows'][] = $fileRow;
			}
		}
		//если не нашелся, выделим ЧОД из chodname и сделаем поиск по ЧОД в inputRequest
		else
		{
			$products = Products::model()->findProductsByStocksChod($fileRow['chod']);
			if (count($products)>1)
			{
				$result['severalProds']['fileRows'][] = $fileRow;
			}
			else if (count($products)==1)
			{
				$fileRow['original_name'] = $products[0]['original_name'];
				$fileRow['chod_display'] = $products[0]['chod_display'];
				$fileRow['product_id'] = $products[0]['id'];
				$result['oneProds']['fileRows'][] = $fileRow;
			}
			else if (count($products)==0)
			{
				$result['notFoundProds']['fileRows'][] = $fileRow;
			}
		}		
		
		return true;
	}

	public static function newStock($data)
	{
		$result = array('status'=>"",
						'info'=>array(),
			);
		//проверим если есть ли в таблице stock запись с таикм же product_id и stockcity_id
		$criteria = new CDbCriteria;
		$criteria->condition = "product_id=:product_id AND stockcity_id=:stockcity_id";
		$criteria->params = array(':product_id'=>$data->product_id,
								  ':stockcity_id'=>$data->stockcity_id,
								);
		$stock = Stocks::model()->find($criteria);

		if ($stock!=null)
		{
			$product = Products::model()->findByPk($data->product_id);
			$result['status'] = "alreadyHaveThisCityStock";
			$result['info'] = array('prodcut_namechod'=>$product->original_name." ".$product->chod_display);
			return $result;
		}


        $stock = new Stocks;
        $stock->product_id = $data->product_id;
        $stock->stockcity_id = $data->stockcity_id;
        $stock->chodname = $data->chodname;
        $stock->val1 = $data->val1;
        $stock->val2 = $data->val2;
        $stock->val3 = $data->val3;
        $stock->price1 = $data->price1;
        $stock->price2 = $data->price2;
        $stock->price3 = $data->price3;
        $res = $stock->save();
        if ($res==false)
        {
            $result['status'] = "error";
            return $result;
        }
        $result['status'] = "ok";
        return $result;        
	}

	public static function updateChodname($id,$chodname)
	{
		$stock = Stocks::model()->findByPk($id);
		if ($stock == null) return false;

		$stock->chodname = $chodname;
		$res = $stock->update();

		return $res;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('chodname',$this->chodname,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('stockcity_id',$this->stockcity_id);
		$criteria->compare('val1',$this->val1);
		$criteria->compare('val2',$this->val2);
		$criteria->compare('val3',$this->val3);
		$criteria->compare('price1',$this->price1,true);
		$criteria->compare('price2',$this->price2,true);
		$criteria->compare('price3',$this->price3,true);
		$criteria->compare('tmpInFileCheck',$this->tmpInFileCheck);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stocks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
