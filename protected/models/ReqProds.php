<?php

/**
 * This is the model class for table "req_prods".
 *
 * The followings are the available columns in table 'req_prods':
 * @property integer $id
 * @property integer $position_id
 * @property integer $position_num
 * @property integer $requisition_id
 * @property integer $product_id
 * @property string $price_outsum
 * @property integer $count
 * @property integer $provider_id
 * @property integer $provprod_id
 * @property string $list_type
 * @property string $input_name
 * @property string $input_chod
 * @property string $question_text
 * @property string $question_status
 *
 * The followings are the available model relations:
 * @property Requisitions $requisition
 * @property Products $product
 * @property Providers $provider
 */
class ReqProds extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'req_prods';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('position_id, requisition_id, count, list_type, price_outsum', 'required'),
			array('position_id, position_num, requisition_id, product_id, count, provider_id, provprod_id', 'numerical', 'integerOnly'=>true),
			array('price_outsum', 'length', 'max'=>12),
			array('list_type, input_name, input_chod', 'length', 'max'=>255),
			array('question_status', 'length', 'max'=>20),
			array('question_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, position_id, position_num, requisition_id, product_id, price_outsum, count, provider_id, provprod_id, list_type, input_name, input_chod, question_text, question_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'requisition' => array(self::BELONGS_TO, 'Requisitions', 'requisition_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'provider' => array(self::BELONGS_TO, 'Providers', 'provider_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'position_id' => 'Position',
			'position_num' => 'Position Num',
			'requisition_id' => 'Requisition',
			'product_id' => 'Product',
			'price_outsum' => 'Price Outsum',
			'count' => 'Count',
			'provider_id' => 'Provider',
			'provprod_id' => 'Provprod',
			'list_type' => 'List Type',
			'input_name' => 'Input Name',
			'input_chod' => 'Input Chod',
			'question_text' => 'Question Text',
			'question_status' => 'Question Status',
		);
	}

	public function updateInfo($reqProd_id,$provider_id,$provprod_id,$count,$price_outsum)
	{
		$reqProd = ReqProds::model()->findByPk($reqProd_id);
		$reqProd->provider_id = $provider_id;
		$reqProd->provprod_id = $provprod_id;
		$reqProd->count = $count;
		$reqProd->price_outsum = $price_outsum;
		$res = $reqProd->update();
		if ($res==false)
		{
			return false;
		}
		return true;
	}

	public function getInfoArray()
	{
		$reqProdsInfo = array();
		$reqProdsInfo['id'] = $this->id;
		$reqProdsInfo['requisition_id'] = $this->requisition_id;
		$reqProdsInfo['product_id'] = $this->product_id;
		$reqProdsInfo['count'] = $this->count;
		$reqProdsInfo['provider_id'] = $this->provider_id;
		$reqProdsInfo['list_type'] = $this->list_type;
		$reqProdsInfo['input_name'] = $this->input_name;
		$reqProdsInfo['input_chod'] = $this->input_chod;
		$reqProdsInfo['price_outsum'] = $this->price_outsum;
		if ($this->product!=null) $reqProdsInfo['product'] = $this->product->getInfoArray();
		else $reqProdsInfo['product'] = null;

		return $reqProdsInfo;
	}

	public function findInfoArrayByRequistionId($requisition_id)
	{
		/*
		$criteria = new CDbCriteria;
		$criteria->compare('requisition_id',$requisition_id);

		$reqProds = $this->with('product')->findAll($criteria);
		$reqProdsInfoArrays = array();
		foreach ($reqProds as $reqProd)
		{
			array_push($reqProdsInfoArrays,$reqProd->getInfoArray());
		}
		return $reqProdsInfoArrays;
		*/
		$sql="SELECT req_prods.id AS reqProdId, req_prods.position_id AS reqProdPositionId, req_prods.position_num AS reqProdPositionNum, req_prods.requisition_id AS reqProdReqId, req_prods.product_id AS reqProdProdId, req_prods.price_outsum AS reqProdPriceOutSum, req_prods.count AS reqProdCount, req_prods.provider_id AS reqProdProviderId, req_prods.provprod_id AS reqProdProvProdId,req_prods.list_type AS reqProdListType, req_prods.input_name AS reqProdInputName,req_prods.input_chod AS reqProdInputChod, req_prods.question_text AS reqProdQuestionText ,req_prods.question_status AS reqProdQuestionStatus,
		    products.*, input_requests.id AS inpId, input_requests.name AS inpName, input_requests.chod AS inpChod, input_requests.chod_display AS inpChodDisplay, input_requests.original_item AS inpOriginalItem, input_requests.provider_id AS inpProviderId, input_requests.provprod_id AS inpProvProdId,
		    provprod.id AS ppId, provprod.provider_id AS ppProvId, provprod.price_in AS ppPriceIn, provprod.delivery_time AS ppDelivTime, provprod.price_date AS ppPriceDate, provprod.notInPrice as ppNotInPrice,
		    prov.name AS pvName
			FROM
			req_prods
			LEFT OUTER JOIN products ON products.id = req_prods.product_id
			LEFT OUTER JOIN input_requests ON products.id = input_requests.product_id
			LEFT OUTER JOIN prov_prods provprod ON products.id = provprod.product_id
			LEFT OUTER JOIN providers prov ON provprod.provider_id = prov.id
			WHERE req_prods.requisition_id = $requisition_id
			ORDER BY req_prods.id ASC, products.id ASC , input_requests.id ASC 
			;";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$dataReader=$command->query();
		$reqProds = array();

		$lastReqProdId = -1;
		$lastInputRequestId = -1;

		foreach($dataReader as $row)
		{
			if ($row['reqProdId']!=$lastReqProdId)
			{
				$reqProd = array();
				$reqProd['id'] = $row['reqProdId'];
				$reqProd['position_id'] = $row['reqProdPositionId'];
				$reqProd['position_num'] = $row['reqProdPositionNum'];
				$reqProd['requisition_id'] = $row['reqProdReqId'];
				$reqProd['product_id'] = $row['reqProdProdId'];				
				$reqProd['count'] = $row['reqProdCount'];
				$reqProd['provider_id'] = $row['reqProdProviderId'];
				$reqProd['provprod_id'] = $row['reqProdProvProdId'];
				$reqProd['list_type'] = $row['reqProdListType'];
				$reqProd['input_name'] = $row['reqProdInputName'];
				$reqProd['input_chod'] = $row['reqProdInputChod'];
				$reqProd['price_outsum'] = $row['reqProdPriceOutSum'];
				$reqProd['question_text'] = $row['reqProdQuestionText'];
				$reqProd['question_status'] = $row['reqProdQuestionStatus'];
				$reqProd['product'] = array();
				$reqProd['product']['id'] = $row['id'];
				$reqProd['product']['original_name'] = $row['original_name'];
				$reqProd['product']['original_chod'] = $row['original_chod'];
				$reqProd['product']['chod_display'] = $row['chod_display'];
				$reqProd['product']['product_num'] = $row['product_num'];
				$reqProd['product']['defaultprovprod_id'] = $row['defaultprovprod_id'];
				$reqProd['product']['price_chtz'] = $row['price_chtz'];
				$reqProd['product']['size'] = $row['size'];
				$reqProd['product']['weight'] = $row['weight'];
				$reqProd['product']['comment'] = $row['comment'];
				$reqProd['product']['img'] = $row['img'];
				$reqProd['product']['hidden'] = $row['hidden'];
				$reqProd['product']['inputRequests'] = array();
				$reqProd['product']['inputRequests'][0]['id'] = $row['inpId'];
				$reqProd['product']['inputRequests'][0]['name'] = $row['inpName'];
				$reqProd['product']['inputRequests'][0]['chod'] = $row['inpChod'];
				$reqProd['product']['inputRequests'][0]['chod_display'] = $row['inpChodDisplay'];
				$reqProd['product']['inputRequests'][0]['original_item'] = $row['inpOriginalItem'];
				$reqProd['product']['inputRequests'][0]['provider_id'] = $row['inpProviderId'];
				$reqProd['product']['inputRequests'][0]['provprod_id'] = $row['inpProvProdId'];
				$reqProd['product']['provProds']=array();
				$reqProd['product']['provProds'][0]['id'] = $row['ppId'];
				$reqProd['product']['provProds'][0]['provider_id'] = $row['ppProvId'];
				$reqProd['product']['provProds'][0]['price_in'] = $row['ppPriceIn'];
				$reqProd['product']['provProds'][0]['price_orig'] = $row['ppPriceOrig'];
				$reqProd['product']['provProds'][0]['delivery_time'] = $row['ppDelivTime'];
				$reqProd['product']['provProds'][0]['provider_name'] = $row['pvName'];
				$reqProd['product']['provProds'][0]['price_date'] = $row['ppPriceDate'];
				$reqProd['product']['provProds'][0]['notInPrice'] = $row['ppNotInPrice'];

				array_push($reqProds, $reqProd);
				$lastReqProdId = $row['reqProdId'];
				$lastInputRequestId = $row['inpId'];
			}
			//значит либо есть еще один InputRequest, либо ProvProd, относящиеся к этому товару
			else
			{
				if ($lastInputRequestId != $row['inpId'])
				{
					$inputRequest = array();
					$inputRequest['id'] = $row['inpId'];
					$inputRequest['name'] = $row['inpName'];
					$inputRequest['chod'] = $row['inpChod'];
					$inputRequest['chod_display'] = $row['inpChodDisplay'];
					$inputRequest['original_item'] = $row['inpOriginalItem'];
					$inputRequest['provider_id'] = $row['inpProviderId'];
					$inputRequest['provprod_id'] = $row['inpProvProdId'];
					$lastRowNumber = count($reqProds)-1;
					array_push($reqProds[$lastRowNumber]['product']['inputRequests'], $inputRequest);
					$lastInputRequestId=$row['inpId'];
				}
				else
				{
					$lastRowNumber = count($reqProds)-1;
					$notFound=true;
					for ($i=0;$i<count($reqProds[$lastRowNumber]['product']['provProds']);$i++)
					{
						if ($reqProds[$lastRowNumber]['product']['provProds'][$i]['id']==$row['ppId'])
						{
							$notFound=false;
							break;
						}
					}

					if ($notFound)
					{
						$provProd = array();
						$provProd['id'] = $row['ppId'];
						$provProd['provider_id'] = $row['ppProvId'];
						$provProd['price_in'] = $row['ppPriceIn'];
						$provProd['price_orig'] = $row['ppPriceOrig'];
						$provProd['delivery_time'] = $row['ppDelivTime'];
						$provProd['provider_name'] = $row['pvName'];
						$provProd['price_date'] = $row['ppPriceDate'];
						$provProd['notInPrice'] = $row['ppNotInPrice'];
						array_push($reqProds[$lastRowNumber]['product']['provProds'], $provProd);
					}
				}	
			}
		}

		return $reqProds;
	}

	public function exportToXlsCustomer($requisition_id,$list_type,$filename)
	{
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel/IOFactory.php';

		$criteria = new CDbCriteria;
		$criteria->condition = "requisition_id=:requisition_id AND list_type=:list_type";
		$criteria->params = array(':requisition_id'=>$requisition_id,
								  ':list_type'=>$list_type,
			);
		$criteria->order= "position_num ASC";

		$reqProds = $this->findAll($criteria);

		$ds=DIRECTORY_SEPARATOR;
		$exportBasePath = Yii::app()->basePath .$ds.'exceltemplates'.$ds.'export-base.xlsx';
		$objPHPExcel = PHPExcel_IOFactory::load($exportBasePath);
			
		$rows = array();
		$priceOutSum = 0; 
		for ($i=0;$i<count($reqProds);$i++)
		{
			if ($reqProds[$i]->price_outsum==0)
			{
				$price_outsum = '-';
				$price_outone = '-';
			}

			else
			{
				$price_outsum = round($reqProds[$i]->price_outsum,2);
				$price_outsum = number_format($price_outsum,2,'.','');

				$price_outone = round($reqProds[$i]->price_outsum/$reqProds[$i]->count,2);
				$price_outone = number_format($price_outone,2,'.','');
			}			

			

			$question = "";
			if ($reqProds[$i]->question_status =='active') $question = $reqProds[$i]->question_text;

			$row = array($i+1,$reqProds[$i]->input_name.' '.$reqProds[$i]->input_chod,$reqProds[$i]->count,$price_outone,$price_outsum,$question);
			array_push($rows, $row);
			$priceOutSum += $reqProds[$i]->price_outsum;			
		}
		$priceOutSum = round($priceOutSum,2);
		$priceOutSum = number_format($priceOutSum,2,'.','');

		$lastBaseRow = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
		
		
		$objPHPExcel->getActiveSheet()->fromArray($rows,null,'A'.$lastBaseRow,false);
		$lastRow = count($reqProds) + $lastBaseRow-1;

		$styleArray = array(
		    
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastBaseRow.':F'.$lastRow)->applyFromArray($styleArray);

		$lastRow++;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$lastRow,'Итого к оплате');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$lastRow,$priceOutSum);

		$styleArray = array(
		    
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		    ),
		    'font' => array(
		        'bold' => true,
			),
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastRow.':E'.$lastRow)->applyFromArray($styleArray);

		$styleArray = array(
		       
		    'font' => array(
		        'bold' => true,
			),
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$lastRow)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->getStyle('E'.$lastBaseRow.':E'.$lastRow)->getNumberFormat()->setFormatCode('#,##0.00');


		$ds=DIRECTORY_SEPARATOR;
		$newFilePath = Yii::app()->basePath .$ds.'..'.$ds."uploads".$ds.$filename;
		//$newFilePath = iconv('utf-8','windows-1251',$newFilePath);

		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($newFilePath);
		
		
		return $newFilePath;
	}

	public function exportToXlsUA($requisition_id,$list_type,$filename)
	{
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel/IOFactory.php';

		$criteria = new CDbCriteria;
		$criteria->condition = "requisition_id=:requisition_id AND list_type=:list_type";
		$criteria->params = array(':requisition_id'=>$requisition_id,
								  ':list_type'=>$list_type,
			);
		$criteria->order= "position_num ASC";

		$reqProds = $this->findAll($criteria);

		$ds=DIRECTORY_SEPARATOR;
		$exportBasePath = Yii::app()->basePath .$ds.'exceltemplates'.$ds.'export-base-ua.xlsx';
		$objPHPExcel = PHPExcel_IOFactory::load($exportBasePath);
			
		$rows = array();
		$priceOutSum = 0; 
		for ($i=0;$i<count($reqProds);$i++)
		{
			$product = Products::model()->findByPk($reqProds[$i]->product_id);
			$originalChodName = "";
			$price_chtz = "";
			if ($product!=null)
			{
				$originalChodName = $product->original_name.' '.$product->chod_display;
				$price_chtz = number_format($product->price_chtz,2,'.','');
			}
			
			$provider = Providers::model()->findByPk($reqProds[$i]->provider_id);
			$providerName = "";
			if ($provider!=null)
			{
				$providerName = $provider->name;
			}

			$provProd = ProvProds::model()->findByPk($reqProds[$i]->provprod_id);
			$priceIn = "";
			if ($provProd!=null)
			{
				$priceIn = number_format($provProd->price_in,2,'.','');
			}

			$priceOut = $reqProds[$i]->price_outsum / $reqProds[$i]->count;
			$priceOut = number_format($priceOut,2,'.','');
			
			$row = array($i+1,
						 $reqProds[$i]->input_name.' '.$reqProds[$i]->input_chod,
						 $originalChodName,
						 $reqProds[$i]->count,
						 $price_chtz,
						 $providerName,
						 $priceIn,
						 $priceOut,
						);

			array_push($rows, $row);		
		}

		$lastBaseRow = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
		$lastRow = count($reqProds) + $lastBaseRow-1;
			
		$objPHPExcel->getActiveSheet()->fromArray($rows,null,'A'.$lastBaseRow,false);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$lastBaseRow.':E'.$lastRow)->getNumberFormat()->setFormatCode('#,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('G'.$lastBaseRow.':G'.$lastRow)->getNumberFormat()->setFormatCode('#,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('H'.$lastBaseRow.':H'.$lastRow)->getNumberFormat()->setFormatCode('#,##0.00');
	
		$ds=DIRECTORY_SEPARATOR;
		$newFilePath = Yii::app()->basePath .$ds.'..'.$ds."uploads".$ds.$filename;
		//$newFilePath = iconv('utf-8','windows-1251',$newFilePath);

		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($newFilePath);
		
		
		return $newFilePath;
	}


	public function exportToXlsInvoice($requisition_id,$list_type,$filename)
	{
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel/IOFactory.php';

		$criteria = new CDbCriteria;
		$criteria->condition = "requisition_id=:requisition_id AND list_type=:list_type";
		$criteria->params = array(':requisition_id'=>$requisition_id,
								  ':list_type'=>$list_type,
			);
		$criteria->order= "position_num ASC";

		$reqProds = $this->findAll($criteria);

		$ds=DIRECTORY_SEPARATOR;
		$exportBasePath = Yii::app()->basePath .$ds.'exceltemplates'.$ds.'export-base-invoice.xlsx';
		$objPHPExcel = PHPExcel_IOFactory::load($exportBasePath);
			
		$rows = array();
		$requisitionPriceOutSum = 0; 
		for ($i=0;$i<count($reqProds);$i++)
		{
			$customerChodName = $reqProds[$i]->input_name.' '.$reqProds[$i]->input_chod;
			$product = Products::model()->findByPk($reqProds[$i]->product_id);
			$originalChod = "";
			$originalName = "";
			if ($product!=null)
			{
				$originalChod = $product->chod_display;
				$originalName = $product->original_name;				
			}

			$priceOut = $reqProds[$i]->price_outsum / $reqProds[$i]->count;
			$priceOut = number_format($priceOut,2,'.','');
			$priceOutSum = number_format($reqProds[$i]->price_outsum,2,'.','');
			$requisitionPriceOutSum += $reqProds[$i]->price_outsum;

			$row = array($i+1,
						 $originalChod,
						 $originalName,
						 $customerChodName,
						 $reqProds[$i]->count,
						 $priceOut,
						 $priceOutSum,
						);

			array_push($rows, $row);		
		}
		$requisitionPriceOutSum = number_format($requisitionPriceOutSum,2,'.','');

		$lastBaseRow = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
		$lastRow = count($reqProds) + $lastBaseRow-1;
		$objPHPExcel->getActiveSheet()->fromArray($rows,null,'A'.$lastBaseRow,false);
		

		$styleArray = array(
		    
		    'borders' => array(
		        'bottom' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'top' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'left' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'right' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		        'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		        ),
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastBaseRow.':G'.$lastRow)->applyFromArray($styleArray);

		$lastRow++;
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRow,'Итого');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$lastRow,$requisitionPriceOutSum);

		$styleArray = array(   
		    
		    'font' => array(
		        'bold' => true,
			),
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$lastRow.':F'.$lastRow)->applyFromArray($styleArray);

		$styleArray = array(
		       
		    'font' => array(
		        'bold' => true,
			),
		    'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
		    ),
		);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$lastRow)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->getStyle('G'.$lastBaseRow.':G'.$lastRow)->getNumberFormat()->setFormatCode('#,##0.00');


		$ds=DIRECTORY_SEPARATOR;
		$newFilePath = Yii::app()->basePath .$ds.'..'.$ds."uploads".$ds.$filename;
		//$newFilePath = iconv('utf-8','windows-1251',$newFilePath);

		
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($newFilePath);
		
		
		return $newFilePath;
	}

	public function updatePositionsNums($positionNumInfos)
	{
		foreach ($positionNumInfos as $positionNumInfo)
		{
			$reqProd = ReqProds::model()->findByPk($positionNumInfo->reqProdId);
			$reqProd->position_num = $positionNumInfo->num;
			$res= $reqProd->save();
			if ($res==false) return false;
		}
		return true;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('position_id',$this->position_id);
		$criteria->compare('position_num',$this->position_num);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('price_outsum',$this->price_outsum,true);
		$criteria->compare('count',$this->count);
		$criteria->compare('provider_id',$this->provider_id);
		$criteria->compare('provprod_id',$this->provprod_id);
		$criteria->compare('list_type',$this->list_type,true);
		$criteria->compare('input_name',$this->input_name,true);
		$criteria->compare('input_chod',$this->input_chod,true);
		$criteria->compare('question_text',$this->question_text,true);
		$criteria->compare('question_status',$this->question_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReqProds the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
