<?php

/**
 * This is the model class for table "requisitions".
 *
 * The followings are the available columns in table 'requisitions':
 * @property integer $id
 * @property integer $num
 * @property string $created
 * @property string $modified
 * @property integer $new
 * @property integer $from_catalog
 * @property string $status
 * @property integer $customer_id
 */
class Requisitions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisitions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('num, created, modified, status', 'required'),
			array('num, new, from_catalog, customer_id', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, num, created, modified, new, from_catalog, status, customer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'num' => 'Num',
			'created' => 'Created',
			'modified' => 'Modified',
			'new' => 'New',
			'from_catalog' => 'From Catalog',
			'status' => 'Status',
			'customer_id' => 'Customer',
		);
	}

	public function attachCustomer($requisition_id,$customerId)
	{
		$requisition = $this->findByPk($requisition_id);
		if ($requisition==null) return false;
		$requisition->customer_id = $customerId;
		$res = $requisition->save();
		return $res;
	}

	/**
	 * Перед сохранением новой заявки
	 */
	protected function beforeValidate() {
		if ($this->isNewRecord) {
			$this->num = RequisitionsManager::getLastNumber()+1;
			$this->created = new CDbExpression('NOW()');
            $this->modified = new CDbExpression('NOW()');	
		}        
        return parent::beforeValidate();
    } 

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('num',$this->num);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('new',$this->new);
		$criteria->compare('from_catalog',$this->from_catalog);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('customer_id',$this->customer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Requisitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
