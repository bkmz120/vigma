<?php

/**
 * This is the model class for table "prov_prods".
 *
 * The followings are the available columns in table 'prov_prods':
 * @property integer $id
 * @property integer $product_id
 * @property integer $provider_id
 * @property string $price_in
 * @property string $price_orig
 * @property string $price_date
 * @property integer $delivery_time
 * @property string $delivery_comment
 * @property integer $notInPrice
 * @property integer $tmpInPriceCheck
 *
 * The followings are the available model relations:
 * @property Providers $provider
 * @property Products $product
 */
class ProvProds extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prov_prods';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, provider_id, price_in, price_orig', 'required'),
			array('product_id, provider_id, delivery_time, notInPrice, tmpInPriceCheck', 'numerical', 'integerOnly'=>true),
			array('price_in, price_orig', 'length', 'max'=>12),
			array('price_date, delivery_comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, provider_id, price_in, price_orig, price_date, delivery_time, delivery_comment, notInPrice, tmpInPriceCheck', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'provider' => array(self::BELONGS_TO, 'Providers', 'provider_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'provider_id' => 'Provider',
			'price_in' => 'Price In',
			'price_orig' => 'Price Orig',
			'price_date' => 'Price Date',
			'delivery_time' => 'Delivery Time',
			'delivery_comment' => 'Delivery Comment',
			'notInPrice' => 'Not In Price',
			'tmpInPriceCheck' => 'Tmp In Price Check',
		);
	}

	public function getInfoArray()
	{
		return array(
			'id' => $this->id,
			'product_id' => $this->product_id,
			'provider_id' => $this->provider_id,
			'price_in' => $this->price_in,
			'price_orig' => $this->price_orig,
			'price_date' => $this->price_date,
			'delivery_time' => $this->delivery_time,
		);
	}

	public function newProvProd($provider_id,$price_in,$price_orig,$delivery_time,$product_id,$price_date,$provChod,$provName)
	{
		if ($delivery_time==null) $delivery_time=0;
		
		

		$provProd = new ProvProds;
		$provProd->provider_id = $provider_id;
		$provProd->price_in = $price_in;
		$provProd->price_orig = $price_orig;
		$provProd->delivery_time = $delivery_time;
		$provProd->price_date = $price_date;
		$provProd->product_id = $product_id;
		$res = $provProd->save();

		if ($res==false)
		{
			var_dump($provProd);
			return false;
		}
		
		//надо добавить inputRequest, относящийся к этому поставщику
		$inputRequest = new InputRequests;
		$inputRequest->name = $provName;
		$inputRequest->chod_display = $provChod;
		$inputRequest->chod = preg_replace('/[^a-zа-яё\d]/ui','',$provChod);
		$inputRequest->original_item = 0;
		$inputRequest->product_id = $product_id;
		$inputRequest->provider_id = $provider_id;
		$inputRequest->provprod_id = $provProd->id;
		$inputRequest->save();

		if ($res==false)
		{
			var_dump($provProd);
			return false;
		}


		$provProdInfo = $provProd->getInfoArray();
		$provProdInfo['provName'] = $provName;
		$provProdInfo['provChod'] = $provChod;
		$provider = Providers::model()->findByPk($provider_id);
		$provProdInfo['provider_name'] = $provider->name;
		return $provProdInfo;
		
	}

	public function deleteProvProd($id)
	{
		$provProd = ProvProds::model()->findByPk($id);
		if ($provProd==null)
		{
			var_dump($provProd);
			return false;
		}

		$criteria = new CDbCriteria;
		$criteria->condition = "provider_id=:provider_id AND product_id=:product_id";  
		$criteria->params = array(':provider_id' => $provProd->provider_id,
								  ':product_id' => $provProd->product_id,
								);

		$criteria = new CDbCriteria;
		$criteria->condition = "provprod_id=:provprod_id";  
		$criteria->params = array(':provprod_id' => $provProd->id,								  
								);

		$res= InputRequests::model()->deleteAll($criteria);
		// if ($res==false)
		// {
			
		// 	return false;
		// }

		$res = $provProd->delete();
		if ($res==false)
		{
			var_dump('2');
			return false;
		}
		
		return true;
	}

	public function updateInfo($id,$price_in,$price_orig,$price_date,$delivery_time)
	{
		$provProd = ProvProds::model()->findByPk($id);
		$provProd->price_in = $price_in;
		$provProd->price_orig = $price_orig;
		$provProd->price_date = $price_date;
		$provProd->delivery_time = $delivery_time;
		$res = $provProd->update();
		if ($res==false)
		{
			return false;
		}
		return true;
	}

	public function updateDeliveryComment($id,$deliveryComment)
	{
		$provProd = ProvProds::model()->findByPk($id);
		$provProd->delivery_comment = $deliveryComment;
		$res = $provProd->update();
		if ($res==false)
		{
			return false;
		}
		return true;
	}

	
	// protected function beforeSave()
	// {
 //        parent::beforeSave();
        
 //        return true;
 //    }

    protected function beforeValidate()
	{
	    $this->price_in = str_replace(",",".",$this->price_in);
        $this->price_orig = str_replace(",",".",$this->price_orig);
	    return parent::beforeValidate();
	}
    

	/*
	public function getInfoArray()
	{
		$reqProdsInfo = array();
		$reqProdsInfo['id'] = $this->id;
		$reqProdsInfo['requisition_id'] = $this->requisition_id;
		$reqProdsInfo['product_id'] = $this->product_id;
		$reqProdsInfo['count'] = $this->count;
		$reqProdsInfo['provider_id'] = $this->provider_id;
		$reqProdsInfo['list_type'] = $this->list_type;
		$reqProdsInfo['input_name'] = $this->input_name;
		$reqProdsInfo['input_chod'] = $this->input_chod;
		$reqProdsInfo['price_out'] = $this->price_out;
		if ($this->product!=null) $reqProdsInfo['product'] = $this->product->getInfoArray();
		else $reqProdsInfo['product'] = null;

		return $reqProdsInfo;
	}

	/*
	public function findInfoArrayByRequistionId($requisition_id)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('requisition_id',$requisition_id);

		$reqProds = $this->with('product')->findAll($criteria);
		$reqProdsInfoArrays = array();
		foreach ($reqProds as $reqProd)
		{
			array_push($reqProdsInfoArrays,$reqProd->getInfoArray());
		}
		return $reqProdsInfoArrays;
	}
	*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('provider_id',$this->provider_id);
		$criteria->compare('price_in',$this->price_in,true);
		$criteria->compare('price_orig',$this->price_orig,true);
		$criteria->compare('price_date',$this->price_date,true);
		$criteria->compare('delivery_time',$this->delivery_time);
		$criteria->compare('delivery_comment',$this->delivery_comment,true);
		$criteria->compare('notInPrice',$this->notInPrice);
		$criteria->compare('tmpInPriceCheck',$this->tmpInPriceCheck);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProvProds the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
