<?php

/**
 * This is the model class for table "stock_cities".
 *
 * The followings are the available columns in table 'stock_cities':
 * @property integer $id
 * @property string $name
 * @property integer $priority
 * @property string $date
 * @property integer $startrownum
 * @property integer $chodnameclm
 * @property integer $val1clm
 * @property integer $val2clm
 * @property integer $val3clm
 * @property integer $price1clm
 * @property integer $price2clm
 * @property integer $price3clm
 */
class StockCities extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stock_cities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, priority', 'required'),
			array('priority, startrownum, chodnameclm, val1clm, val2clm, val3clm, price1clm, price2clm, price3clm', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, priority, date, startrownum, chodnameclm, val1clm, val2clm, val3clm, price1clm, price2clm, price3clm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'priority' => 'Priority',
			'date' => 'Date',
			'startrownum' => 'Startrownum',
			'chodnameclm' => 'Chodnameclm',
			'val1clm' => 'Val1clm',
			'val2clm' => 'Val2clm',
			'val3clm' => 'Val3clm',
			'price1clm' => 'Price1clm',
			'price2clm' => 'Price2clm',
			'price3clm' => 'Price3clm',
		);
	}

	public function getInfoArray()
	{
		if ($this->val1clm!=null) $val1clm = $this->val1clm+1;
		else $val1clm = null;

		if ($this->val2clm!=null) $val2clm = $this->val2clm+1;
		else $val2clm = null;

		if ($this->val3clm!=null) $val3clm = $this->val3clm+1;
		else $val3clm = null;

		if ($this->price1clm!=null) $price1clm = $this->price1clm+1;
		else $price1clm = null;

		if ($this->price2clm!=null) $price2clm = $this->price2clm+1;
		else $price2clm = null;

		if ($this->price3clm!=null) $price3clm = $this->price3clm+1;
		else $price3clm = null;

		return array('id'=>$this->id,
					 'name'=>$this->name,
					 'date'=>$this->date,
					 'startrownum' => $this->startrownum,
					 'chodnameclm' => $this->chodnameclm+1,
					 'val1clm' => $val1clm,
					 'val2clm' => $val2clm,
					 'val3clm' => $val3clm,
					 'price1clm' => $price1clm,
					 'price2clm' => $price2clm,
					 'price3clm' => $price3clm,
					 'priority' => $this->priority,
					);
	}

	public static function getTemplate($id,$forDisplay=true)
	{
		$stockCity = StockCities::model()->findByPk($id);
		if ($stockCity==null)
		{
			return false;
		}

		//если $forDisplay установлен, то номера колонок увеличиваем на один, что бы нумерация не с 0, а с 1 начиналась 
		if ($forDisplay)
		{	
			if ($stockCity->chodnameclm!=null) $chodnameclm = $stockCity->chodnameclm+1;
			else $chodnameclm = null;

			if ($stockCity->val1clm!=null) $val1clm = $stockCity->val1clm+1;
			else $val1clm = null;

			if ($stockCity->val2clm!=null) $val2clm = $stockCity->val2clm+1;
			else $val2clm = null;

			if ($stockCity->val3clm!=null) $val3clm = $stockCity->val3clm+1;
			else $val3clm = null;

			if ($stockCity->price1clm!=null) $price1clm = $stockCity->price1clm+1;
			else $price1clm = null;

			if ($stockCity->price2clm!=null) $price2clm = $stockCity->price2clm+1;
			else $price2clm = null;

			if ($stockCity->price3clm!=null) $price3clm = $stockCity->price3clm+1;
			else $price3clm = null;

		}
		else
		{
			$chodnameclm = $stockCity->chodnameclm;
			$val1clm = $stockCity->val1clm;
			$val2clm = $stockCity->val2clm;
			$val3clm = $stockCity->val3clm;
			$price1clm = $stockCity->price1clm;
			$price2clm = $stockCity->price2clm;
			$price3clm = $stockCity->price3clm;
		}

		

		return array('startrownum' => $stockCity->startrownum,
					 'chodnameclm' => $chodnameclm,
					 'val1clm' => $val1clm,
					 'val2clm' => $val2clm,
					 'val3clm' => $val3clm,
					 'price1clm' => $price1clm,
					 'price2clm' => $price2clm,
					 'price3clm' => $price3clm,
					);
	}

	public static function saveTemplate($stockCityId,$json_template)
	{
		$template = json_decode($json_template);

		if ($template->val1clm!=null) $val1clm = $template->val1clm-1;
		else $val1clm = null;

		if ($template->val2clm!=null) $val2clm = $template->val2clm-1;
		else $val2clm = null;

		if ($template->val3clm!=null) $val3clm = $template->val3clm-1;
		else $val3clm = null;

		if ($template->price1clm!=null) $price1clm = $template->price1clm-1;
		else $price1clm = null;

		if ($template->price2clm!=null) $price2clm = $template->price2clm-1;
		else $price2clm = null;

		if ($template->price3clm!=null) $price3clm = $template->price3clm-1;
		else $price3clm = null;


		
		if ($stockCityId==null)
		{

			$stockCity = new StockCities;
			$stockCity->name =  $template->name;
			$stockCity->startrownum =  $template->startrownum;
			$stockCity->chodnameclm =  $template->chodnameclm-1;
			$stockCity->val1clm =  $val1clm;
			$stockCity->val2clm =  $val2clm;
			$stockCity->val3clm =  $val3clm;
			$stockCity->price1clm =  $price1clm;
			$stockCity->price2clm =  $price2clm;
			$stockCity->price3clm =  $price3clm;

			if ($stockCity->priority==null || $stockCity->priority=="") $stockCity->priority = 9999;
			$res= $stockCity->save();
			if ($res==false) return false;
			$stockCityId = $stockCity->id;
		}
		else
		{
			$stockCity = StockCities::model()->findByPk($stockCityId);
			$stockCity->startrownum =  $template->startrownum;
			$stockCity->chodnameclm =  $template->chodnameclm-1;
			$stockCity->val1clm =  $val1clm;
			$stockCity->val2clm =  $val2clm;
			$stockCity->val3clm =  $val3clm;
			$stockCity->price1clm =  $price1clm;
			$stockCity->price2clm =  $price2clm;
			$stockCity->price3clm =  $price3clm;
			if ($template->priority!=null && $template->priority!="") $stockCity->priority = $template->priority;
			$res= $stockCity->update();
			if ($res==false) return false;
		}

		return $stockCityId;
		
	}

	public function setDate($stockCity_id,$date)
	{
		$stockCity = StockCities::model()->findByPk($stockCity_id);
		if ($stockCity==null)
		{
			return false;
		}
		else
		{
			$stockCity->date = $date;
			$res = $stockCity->update();
			if ($res==false) return false;
			return true;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('startrownum',$this->startrownum);
		$criteria->compare('chodnameclm',$this->chodnameclm);
		$criteria->compare('val1clm',$this->val1clm);
		$criteria->compare('val2clm',$this->val2clm);
		$criteria->compare('val3clm',$this->val3clm);
		$criteria->compare('price1clm',$this->price1clm);
		$criteria->compare('price2clm',$this->price2clm);
		$criteria->compare('price3clm',$this->price3clm);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StockCities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
