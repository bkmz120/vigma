<?php
/**
 * Переопределение методов менеджера скриптов для сброса кэша.
 * Добавочная строка задаётся в конфиге  params['assetsVersion']
 */


class ClientScript extends CClientScript {

	public function registerCssFile($url,$media='') {
		$url.="?".Yii::app()->params['assetsVersion'];
		parent::registerCssFile($url,$media);
	}

	public function registerScriptFile($url,$position=null,array $htmlOptions=array()) {
		$url.="?".Yii::app()->params['assetsVersion'];
		parent::registerScriptFile($url,$position,$htmlOptions);
	}

}