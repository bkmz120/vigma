<?php

class GroupsManager {

	/**
	 * Возвращает все засписи из таблицы groups
	 * @return array $groups
	 */
	public static function getAllGroupsInfo()
	{
		$sql=  "SELECT *
				FROM groups
				;";


		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$rows=$command->queryAll();
		
		return $rows; 
	}


	/**
	 * возращает массив всех групп с ключами - номерами групп (group_num)
	 * @param array $groups - массив с группами, полученный getAllGroupsInfo()
	 * @return array $groupsKeyByName
	 * 
	 */
	public static function getAllGroupsKeyByNum($groups)
	{
		$groupsKeyByName = array();
		foreach ($groups as $group)
		{
			$groupsKeyByName[$group['group_num']] = $group;
		}

		return $groupsKeyByName;
	}

	/**
	 * возращает массив всех групп с ключами - id группы 
	 * @param array $groups - массив с группами, полученный getAllGroupsInfo()
	 * @return array $groupsKeyById
	 * 
	 */
	public static function getAllGroupsKeyById($groups)
	{
		$groupsKeyById = array();
		foreach ($groups as $group)
		{
			$groupsKeyById[$group['id']] = $group;
		}

		return $groupsKeyById;
	}


}