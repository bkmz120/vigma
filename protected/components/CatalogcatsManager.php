<?php

class CatalogcatsManager {

	/**
	 * Возвращает все засписи из таблицы catalogcats
	 * @param bool $parseGroupIds - флаг указывает на то что group_id надо из json перевести в массив
	 * @return array $catalogcats
	 */
	public static function getAllCatalogcatsInfo($parseGroupIds=false)
	{
		$sql=  "SELECT *
				FROM catalogcats
				;";


		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$rows=$command->queryAll();

		if ($parseGroupIds)
		{
			for ($i=0;$i<count($rows);$i++)
			{
				if ($rows[$i]['group_ids']!=null)
				{
					$rows[$i]['group_ids'] = json_decode($rows[$i]['group_ids']);
				}
			}
		}
		
		return $rows; 
	}

	/**
	 * Добавляет id группы в массив group_id для строки из catalogcats c id $catalogcatId
	 * @param int $catalogcatId 
	 * @param int $groupId
	 * 
	 * @throws Exception если catalogcat не найден
	 * @throws Exception если groupId уже присутствует в catalogcat.group_ids
	 * 
	 * @return null
	 */ 
	
	public static function addGroupToCatalogcat($catalogcatId,$groupId)
	{		
		
		$catalogcat = Catalogcats::model()->findByPk($catalogcatId);
			
		if ($catalogcat==null) throw new Exception('catalogcat with id='.$catalogcatId." not found");

		if ($catalogcat->group_ids==null) $group_ids = array();
		else
		{
			$group_ids = json_decode($catalogcat->group_ids);
			foreach ($group_ids as $group_id)
			{
				if ($group_id==$groupId) throw new Exception('For catalogcat with id='.$catalogcatId.". group_id ".$groupId." already added.");
			}
		}

		$group_ids[] = $groupId;

		$catalogcat->group_ids = json_encode($group_ids);

		$catalogcat->update();	

	}
	

	/**
	 * Удаляет id группы из массива group_id для строки из catalogcats c id $catalogcatId
	 * @param int $catalogcatId 
	 * @param int $groupId
	 * 
	 * @throws Exception если catalogcat не найден
	 * 
	 * @return null
	 */ 
	
	public static function removeGroupFromCatalogcat($catalogcatId,$groupId)
	{		
		
		$catalogcat = Catalogcats::model()->findByPk($catalogcatId);
			
		if ($catalogcat==null) throw new Exception('catalogcat with id='.$catalogcatId." not found");

		if ($catalogcat->group_ids!=null)		
		{
			$group_ids = json_decode($catalogcat->group_ids);
			$new_group_ids = array();
			foreach ($group_ids as $group_id)
			{
				if ($group_id!=$groupId) $new_group_ids[] = $group_id;
			}

			$catalogcat->group_ids = json_encode($new_group_ids);
			$catalogcat->update();	

		}		

	}
	
	/**
	 * Заново заполнить таблицу связи категорий и товаров (catalogcat_prods), по
	 * таблицам связи группы с товаром (group_prods) и группы с категорией (catalogcats->group_id)
	 * 
	 * Данная функция должна вызывать после каждого изменения в таблице group_prods (при импорте групп из файла) и
	 * в поле group_ids, таблицы catalogcats.
	 * 
	 * Данная поцедура делается, что бы упростить выборку товаров, по категориям (выполняется выборкой из 
	 * products одним left join с catalogacats_prods).
	 * Инчае для выборки товаров относящихся к катеогории, нжуно было бы выбрать группы для данной категории,
	 * затем для каждой группы выбрать соответствующие товары.
	 * 
	 * Поскольку процедура выборки товаров по категориям нужна для работы в витрине, то хранить номера групп
	 * в витрине нет необходимости.
	 * 
	 * 
	 */
	public static function updateProductsCatalogcatIds()
	{
		$connection=Yii::app()->db;
		//очистить таблицу catalogcat_prods
		$sql=  "TRUNCATE TABLE catalogcat_prods";
		$command=$connection->createCommand($sql);
		$command->execute();

		$catalogcats = self::getAllCatalogcatsInfo(true);

		foreach ($catalogcats as $catalogcat)
		{
			if ($catalogcat['group_ids']!=null)
			{	
				$group_ids_str = implode(',', $catalogcat['group_ids']);

				/*
				$sql=  "SELECT group_prods.product_id FROM group_prods
						WHERE group_prods.group_id IN (".$group_ids_str.")
						;";
				$command=$connection->createCommand($sql);
				*/
							
				$sql=  "INSERT INTO catalogcat_prods (product_id,catalogcat_id)
							SELECT DISTINCT group_prods.product_id, '".$catalogcat['id']."' FROM group_prods
							WHERE group_prods.group_id IN (".$group_ids_str.")	

						;";
				
				$command=$connection->createCommand($sql);
				
				$res = $command->execute();
				//var_dump($res);
				
			}
			
		}
	} 
}