<?php

class UploadFile extends CComponent {

	public function attributeLabels()
	{
		return array();
	}

	public static function getUploadDirPath()
	{
		$ds=DIRECTORY_SEPARATOR;
		return Yii::app()->basePath.$ds."..".$ds."uploads".$ds;
	}

    public static function upload($file)
    {
    	
        $newFilePath = self::getUploadDirPath().$file["name"];
        //если запущен на виндоус - применить кодировку к файлу
        if (DIRECTORY_SEPARATOR == '\\')        $newFilePath = iconv('utf-8','windows-1251',$newFilePath);
        $res =move_uploaded_file($file["tmp_name"], $newFilePath);

        return $res;
    }

    public static function getPathByName($fileName)
    {
    	$filePath = self::getUploadDirPath().$fileName;
        if (DIRECTORY_SEPARATOR == '\\') $filePath = iconv('utf-8','windows-1251',$filePath);
        return $filePath;
    }

}