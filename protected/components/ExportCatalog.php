<?php

/**
 * Методы для экпорта товаров на сайт-каталог
 * 
 */ 

class ExportCatalog {
	
	/**
	 * Обновить поле price_catalog для всех товаров. Вызывается при экспорте каталога
	 * расчиывается умножением цены по порайсу поставщика по-умолчанию
	 * для товара на коээфициент для каталога у сооветствующего поставщика
	 */ 
	public static function updatePriceCatalog()
	{
		$connection=Yii::app()->db;

		$sql = "UPDATE products
				INNER JOIN prov_prods ON (prov_prods.id = products.defaultprovprod_id)
				INNER JOIN providers ON (providers.id = prov_prods.provider_id)
				SET products.price_catalog = providers.catalog_coef * prov_prods.price_orig
			   ";

		$command=$connection->createCommand($sql);
		$command->execute();
	}

	/**
	 * Задать поле url для товаров, у которых оно не задано.
	 * url - транслитерация имени и ЧОД
	 */ 

	public static function updateUrl()
	{
		$converter = array(
	        'а' => 'a',   'б' => 'b',   'в' => 'v',
	        'г' => 'g',   'д' => 'd',   'е' => 'e',
	        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
	        'и' => 'i',   'й' => 'y',   'к' => 'k',
	        'л' => 'l',   'м' => 'm',   'н' => 'n',
	        'о' => 'o',   'п' => 'p',   'р' => 'r',
	        'с' => 's',   'т' => 't',   'у' => 'u',
	        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
	        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
	        'ь' => '',    'ы' => 'y',   'ъ' => '',
	        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
	        
	        'А' => 'A',   'Б' => 'B',   'В' => 'V',
	        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
	        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
	        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
	        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
	        'О' => 'O',   'П' => 'P',   'Р' => 'R',
	        'С' => 'S',   'Т' => 'T',   'У' => 'U',
	        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
	        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
	        'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
	        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
	    );
		
		$results = "";

		$connection=Yii::app()->db;	

		/*
		$sql = "ALTER TABLE  `products` ADD  `url` VARCHAR( 255 ) NOT NULL ;";
		$command=$connection->createCommand($sql);
		try {
			$command->execute();
		}
		catch (Exception $e)
		{
			$results.="url column already exists<br>";
		}
		*/


		$sql = "SELECT * FROM products ORDER BY id;";
		$command=$connection->createCommand($sql);
		$dataReader=$command->query();
		
		$whenThenPart = "";
		$ids = "";
		foreach ($dataReader as $row)
		{
			
			$name = preg_replace('/[^a-zA-ZА-Яа-я\s]/ui', '',$row['original_name']);
			$name = trim($name);
			$name = preg_replace('/[\s]/ui', '_', $name);
			$name = strtr($name, $converter);
			$name = strtolower($name);

			$chod = preg_replace('/[^a-zA-ZА-Яа-я0-9]/ui', '', $row['chod_display']);
			$chod = preg_replace('/[\s]/ui', '_', $chod);
			$chod = strtr($chod,$converter);
			$chod = strtolower($chod);
			$url = $name."-".$chod;



			//var_dump($url);
			$whenThenPart = $whenThenPart." WHEN ".$row['id']." THEN '".$url."' ";
			if ($ids=="") $ids = $row['id'];
			else $ids = $ids.",".$row['id'];
			
		}
		

		$updateSql = "UPDATE products SET url = (CASE id".$whenThenPart." END) WHERE id IN(".$ids.");";
		$command=$connection->createCommand($updateSql);
		$res=$command->execute();
		
		return $res;
	}

	public static function getExportPath()
	{
		$ds=DIRECTORY_SEPARATOR;
		$rootPath = Yii::app()->basePath.$ds."..".$ds;
		$exportPath = $rootPath."catalogexport".$ds;

		return $exportPath;
	}


	public static function createDump()
	{
		$dumpFileName = "currentDump.sql";
		$ds=DIRECTORY_SEPARATOR;
		$exportPath = self::getExportPath();
		$dumpPath = $exportPath.$dumpFileName;
		if (file_exists($dumpPath)) unlink($dumpPath);

		$cmd = "mysqldump --user=root --password=MyVigaQl4p --host=localhost vigma products input_requests catalogcats catalogcat_prods groups group_prods stocks stock_cities> $dumpPath";   
		
		exec($cmd);
		
	}

	public static function createZip()
	{
		$dumpFileName = "currentDump.sql";
		$ds=DIRECTORY_SEPARATOR;
		$rootPath = Yii::app()->basePath.$ds."..".$ds;
		$exportPath = self::getExportPath();
		$exportFileName = time().".zip";


		$zip = new ZipArchive(); // подгружаем библиотеку zip


		$zip_name = $exportPath.$exportFileName; // имя файла
		if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
		{
			echo "* Sorry ZIP creation failed at this time";
			return;
		}

		$photosPath = $rootPath."photos".$ds;
		$files = scandir($photosPath);

		for ($i=2;$i<count($files);$i++)
		{
			$localPath = "photos".$ds.$files[$i];
			$zip->addFile($photosPath.$files[$i],$localPath); // добавляем файлы в zip архив
		}

		$zip->addFile($exportPath.$dumpFileName,"dump.sql");
		
		$zip->close();

		return $exportFileName;
	}	


	public static function export()
	{
		//self::updatePriceCatalog();
		//self::updateUrl();
		//$cmd = "mysqldump --user=root --password=MyVigaQl4p --host=localhost galina111_vi products input_requests catalogcats catalogcat_prods";   
		
		$exportFileName = self::createZip();
		return $exportFileName;
		
		
		
	}

	public static function outputExport()
	{
		self::updatePriceCatalog();
		self::updateUrl();
		self::createDump();
		
		$exportFileName = self::createZip();
		$exportFilePath = self::getExportPath().$exportFileName;
		header('Content-type: application/zip');
		header('Content-Disposition: attachment; filename="'.$exportFileName.'"');
		readfile($exportFilePath);
		
	}
}