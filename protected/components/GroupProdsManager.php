<?php
/**
 *  Класс для работы с GroupProds
 * 
 */
class GroupProdsManager {   


    /**
     * Поиск в таблицу связей товар-группа - group_prods, по id товара
     * Возвращает массив со строками из таблицы
     * @param int $productId
     * @return array $rows
     */
    public static function findByProductId($productId)
    {
        $sql=  "SELECT *
                FROM group_prods
                WHERE product_id=':productId'
                ;";


        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);  
        $command->bindParam(":productId",$productId,PDO::PARAM_INT);
        $rows=$command->queryAll();
        
        return $rows; 
    }

    /**
     * Поиск в таблицу связей товар-группа - group_prods, по ЧОД
     * Возвращает массив со строками из таблицы
     * @param string $chod
     * @return array $rows
     */
    public static function findByChod($chod)
    {
        $sql=  "SELECT *
                FROM group_prods
                WHERE chod = :chod 
                ;";


        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);
        $command->bindParam(":chod",$chod,PDO::PARAM_STR);
        //var_dump($command->getText());

        $rows=$command->queryAll();
        
        return $rows; 
    }



    /**
     * Импорт связей групп и товаров по ЧОД
     * 
     * @param string $filePath - путь до файла
     * @return array сгруппированные рузультаты
     */

    public static function importGroupsFromFile($filePath)
    {
        require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
        require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel/IOFactory.php';

        $startRow = 2;
        $chodClm = 2; //нумерация с нуля
        $nameClm = 3;
        $groupClm = 6;
        
        /**  Identify the type of $inputFileName  **/
        $inputFileType = PHPExcel_IOFactory::identify($filePath);
        /**  Create a new Reader of the type that has been identified  **/
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a PHPExcel Object  **/
        $objPHPExcel = $objReader->load($filePath);

        $objWorksheet = $objPHPExcel->getActiveSheet();
        // Get the highest row and column numbers referenced in the worksheet
        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

        $result = array('notFound'=>array(),
                        'addedAndItFirstGroupRelation'=>array(),
                        'addedAndItNotFirstGroupRelation'=>array(),
                        'alreadyHaveRelationWithThisGroup'=>array(),
                        );

        $groups = GroupsManager::getAllGroupsInfo();
        $groupsKeyByNum = GroupsManager::getAllGroupsKeyByNum($groups);
        $groupsKeyById = GroupsManager::getAllGroupsKeyById($groups);

        
        for ($row = $startRow; $row <= $highestRow; $row++)
        {
            $chod = $objWorksheet->getCellByColumnAndRow($chodClm, $row)->getValue();

            $name = $objWorksheet->getCellByColumnAndRow($nameClm, $row)->getValue();
            $groupNum = $objWorksheet->getCellByColumnAndRow($groupClm, $row)->getValue();
            $groupNum = intval($groupNum);

            if ($chod!=null && $chod!="" && $groupsKeyByNum[$groupNum]==null)
            {
                throw new Exception('Не найден указнный номер группы в БД. чод:'.print_r($fileRow,true));
            }

            $groupId = $groupsKeyByNum[$groupNum]['id'];

            //в некоторых строках в ячейке ЧОД находится "–", для таких ЧОД будет все после первого пробела в ячейке с наименованием
            if ($chod=="–") 
            {
                $firstSpacePos = null;
                $chod = "";
                $newName = "";
                for ($i=0;$i<strlen($name);$i++)
                {
                    if ($name[$i]==" " && $firstSpacePos==null) $firstSpacePos = $i;
                    if ($firstSpacePos!=null && $i>$firstSpacePos)
                    {
                        $chod .= $name[$i];
                    }
                    if ($firstSpacePos==null)
                    {
                        $newName .= $name[$i]; 
                    }
                }
                $name = $newName;

                //в ЧОД, котрый находится в ячейке Наименование, могут содержаться лишние пробелы, например 20 ОТ 65Г 09
                $chod = str_replace(' ','', $chod);
            }
            else
            {
                $chod = self::clearChod($chod);
            }

            self::importRow(array('chod'=>$chod,'name'=>$name,'groupNum'=>$groupNum,'groupId'=>$groupId),$groupsKeyById,$groupsKeyByNum,$result);
        }

        return $result;
    }
    
    /**
     * Очистак ЧОД из импортируемого файла от лишних символов
     * 
     * @param string $chod 
     * @return string $chod
     */
    public static function clearChod($chod)
    {
        $cleanChod = str_replace(';', '', $chod);

        $asteriskPos = strpos($cleanChod, '*');
        if ($asteriskPos!==false)
        {
            $cleanChod = substr($cleanChod,0,$asteriskPos);
        }

        $cleanChod = str_replace('или','',$cleanChod);
        return $cleanChod;
    }   

    /**
     * Импорт данных из одной строки файла
     * 
     * @param array $fileRow - содержит ЧОД и номер группы
     * @param array $groupsKeyById - ассоциативный массив содержащий поля таблицы Groups c ключом - id группы
     * @param array $groupsKeyByNum - ассоциативный массив содержащий поля таблицы Groups c ключом - номером группы
     * @param array $result - результаты импорта файла, передаваемые по ссылке
     * @return null
     */
    public static function importRow($fileRow,$groupsKeyById,$groupsKeyByNum,&$result)
    {
        if ($fileRow['chod']==null || $fileRow['chod']=='') return false;

        $chod = $fileRow['chod'];
        $groupNum = $fileRow['groupNum'];
        $groupId = $fileRow['groupId'];

        //все связи для данного ЧОД
        $groupProdsFoundedByChod = self::findByChod($chod);
                
        //если есть связи с данным ЧОД, проверим есть ли среди них нужная группа
        $groupIdNotFound = true;

        foreach ($groupProdsFoundedByChod as $groupProd)
        {
            if ($groupProd['group_id']==$groupId)
            {
                $foundedGroupProd = $groupProd;
                $groupIdNotFound = false;
                break;
            }
        }   

        if ($groupIdNotFound)
        {
            $products = self::findProductsForGroups($chod);

            if (count($products)==0)
            {

                $result['notFound'][] = array('fileRow'=>$fileRow);
            }
            else
            {
                //несколько товаров по одному чод могут найтись в случае когда ЧОД один, но разные коды товаров 
                //такие товары есть в прайсе ЧТЗ, поэтому допускам что в $products может быть не один товар
                foreach ($products as $product)
                {
                    //по $product['id'] надо найти group_prods
                    $groupProdsFoundedByProdId = self::findByProductId($product['id']);

                    if (count($groupProdsFoundedByProdId)==0) $itIsFirstRelation = true;
                    else $itIsFirstRelation = false;
                    
                    
                    self::newGroupProds($groupId,$product['id'],$chod);
                    
                    if ($itIsFirstRelation)
                    {
                        $result['addedAndItFirstGroupRelation'][]   = array('fileRow'=>$fileRow,'product'=>$product);
                    }
                    else
                    {
                        $otherGroups = array(); //номера групп уже привязанных к товару
                        foreach ($groupProdsFoundedByChod as $index => $groupProd)
                        {
                            $group = $groupsKeyById[$groupProd['group_id']];
                            $otherGroups[] = $group['group_num'];
                        }
                        sort($otherGroups);
                        $otherGroupsStr = implode(', ', $otherGroups);

                        $result['addedAndItNotFirstGroupRelation'][]    = array('fileRow'=>$fileRow,'product'=>$product,'otherGroups'=>$otherGroupsStr);
                    }               
                }
            }
        }       
        //заносим  эту строку в $result['alreadyHaveRelationWithThisGroup'] - список тех товаров, для которых уже есть связи с группами, среди которых есть groupId
        else
        {
            //проверим, возможно данная строка была добавлена в этом сеансе импорта, в таком случае она должна быть в 'addedAndItFirstGroupRelation'
            //или в 'addedAndItNotFirstGroupRelation' или в 'alreadyHaveRelationWithThisGroup'
            $notFound = true;
            foreach ($result['addedAndItFirstGroupRelation'] as $item)
            {
                if ($item['fileRow']['chod']===$chod &&  $item['fileRow']['groupNum']==$groupNum)
                {
                    $notFound = false;
                }
            }

            if ($notFound)
            {
                foreach ($result['addedAndItNotFirstGroupRelation'] as $item)
                {
                    if ($item['fileRow']['chod']===$chod &&  $item['fileRow']['groupNum']==$groupNum)
                    {
                        $notFound = false;
                    }
                }
            }

            if ($notFound)
            {
                foreach ($result['alreadyHaveRelationWithThisGroup'] as $item)
                {
                    if ($item['fileRow']['chod']===$chod &&  $item['fileRow']['groupNum']==$groupNum)
                    {
                        $notFound = false;
                    }
                }
            }

            if ($notFound) 
            {
                $otherGroups = array(); //номера групп уже привязанных к товару
                foreach ($groupProdsFoundedByChod as $index => $groupProd)
                {
                    $group = $groupsKeyById[$groupProd['group_id']];
                    $otherGroups[] = $group['group_num'];
                }
                sort($otherGroups);
                $otherGroupsStr = implode(', ', $otherGroups);

                $product = self::getProductRowById($foundedGroupProd['product_id']);
                $result['alreadyHaveRelationWithThisGroup'][]   = array('fileRow'=>$fileRow,'otherGroups'=>$otherGroupsStr,'product'=>$product);
            }   
        }
    }

    /**
     * Получить массив групп, к которым относится товара
     * @param $product_id
     * @return array
     */
    public function getGroupsByProductId($product_id) {
        
    }

    /**
     * Получить строку из таблицы Product по id
     * @param int $product_id
     * @return array
     */
    public function getProductRowById($product_id)
    {
        $connection=Yii::app()->db;
        $sql=  "SELECT *
                FROM products
                WHERE products.id = :product_id";
        
        $command=$connection->createCommand($sql);  
        $command->bindParam(":product_id",$product_id,PDO::PARAM_INT);
        $rows=$command->queryAll();
        if (count($rows)==0) return null;
        else return $rows[0];
    }

    /**
     * Поиск товаров по ЧОД из файла-импорта групп
     * @param string $chod
     * @return array
     */
    public static function findProductsForGroups($chod)
    {
        //оставить в чод только буквы и цифры, что бы искть в input_requests по ЧОД, а не по chod_display
        $chod = preg_replace('/[^a-zа-яё\d]/ui','',$chod);

        $connection=Yii::app()->db;

        //в импорируемом файле, у тех ЧОД, котрые начинаются с нуля, ноль отсутствует,
        //например для товара "01464 Трубка" в файл имеется строка "1464 Трубка"
        //поскльку ищется полное совпадение оригинального ЧОД, то по запросу "1464", ничего не найдётся
        //Что бы находить такие позиции, добавим спереди к ЧОД 0, если ЧОД - набор цифр,
        //добавим вначало $chod ноль и повторим поиск

        if (is_numeric($chod)) {
            $firstNullChod = '0'.$chod;
            $chodCondition = "(input_requests.chod='$chod' OR input_requests.chod='$firstNullChod')";
        }
        else {
            $chodCondition = "input_requests.chod='$chod'";
        }

        $sql=  "SELECT *
                FROM products
                WHERE products.id IN
                (
                    SELECT DISTINCT input_requests.product_id
                    FROM input_requests
                    WHERE $chodCondition AND input_requests.original_item=1
                ) 
                ;";
        
        $command=$connection->createCommand($sql);  
        $rows=$command->queryAll();

        //Если не найдено совпадений по оригинальному ЧОД (input_requests=1), то ищем по остальным чод закрепленным за товаром
        if (count($rows)==0) {
            $sql=  "SELECT *
                    FROM products
                    WHERE products.id IN
                    (
                        SELECT DISTINCT input_requests.product_id
                        FROM input_requests
                        WHERE $chodCondition AND input_requests.original_item=0
                    ) 
                    ;";
        
            $command=$connection->createCommand($sql);  
            $rows=$command->queryAll();

            //проверим нашелся один товар или несколько
            //если несколько, то считеам что корректного совпадения по ЧОД не удалось найти и возвращаем пустой массив
            if (count($rows)>1) {
                $rows = array();
            }
        }
        
        return $rows; 
    }

    /**
     * Cоздание новой записи в group_prods
     * 
     * @param int $groupId
     * @param int $productId
     * @param string $chod 
     * @return bool false, если запсиь с $groupId,$productId уже существует
     */
    public static function newGroupProds($groupId,$productId,$chod)
    {
        $row = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from('group_prods')
                        ->where('group_id=:group_id AND product_id=:product_id', array(':group_id'=>$groupId,':product_id'=>$product_id))
                        ->queryRow();
        if ($row!==false) return false;

        try
        {
            Yii::app()->db->createCommand()->insert('group_prods', array(
                                'product_id'=>$productId,
                                'group_id'=>$groupId,
                                'chod'=>$chod,
                            ));
        }       
        catch(Exception $e)
        {
            throw new Exception('Ошибка при создании записи в group_prods'.$e->getMessage());
        }

        return true;
    }

    /**
     * Удаление записи в group_prods по group_id и product_id
     * @param int $groupId
     * @param int $productId
     */ 
    public static function deleteGroupProd($groupId,$productId)
    {
        $sql = "DELETE FROM group_prods WHERE group_id=:group_id AND product_id=:product_id";
        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);
        $command->bindParam(":group_id",$groupId,PDO::PARAM_STR);
        $command->bindParam(":product_id",$productId,PDO::PARAM_STR);

        $command->execute();
    }

    /**
     * Выборка товаров по заданной группе. Если передать null, то будет выорка товаров,
     * не привязанных ни к одной группе. Так же есть фильтрация по имени и ЧОД
     * @param int $groupId
     * @return array products
     */     
    public static function getProductsByGroup($filters,$offset,$count) {
        $connection=Yii::app()->db;
        $command=$connection->createCommand('');
        $limit = "LIMIT $offset,$count";

        if ($filters['groupId']!=null) {
            $groupCondition = "group_prods.group_id=:group_id";
        }
        else {
            $groupCondition = "group_prods.id IS NULL";
        }

        //посик не задан
        if ($filters['name']==null && $filters['chod']==null)
        {
            $inputRequestsCondition = null;
            $command->text=self::sqlSelectForProductsByGroup($inputRequestsCondition,$groupCondition,$limit);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            }           
            $rows=$command->queryAll();

            $command->text=self::sqlCountForProductsByGroup($inputRequestsCondition,$groupCondition);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $resultsCount=$command->queryScalar();
        }

        //посик только по имени
        if ($filters['name']!=null && $filters['chod']==null)
        {
            $inputRequestsCondition = "input_requests.name LIKE :name";

            $command->text=self::sqlSelectForProductsByGroup($inputRequestsCondition,$groupCondition,$limit); 
            $filters['name'] = '%'.$filters['name'].'%';
            $command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            }   
            $rows=$command->queryAll();


            $command->text=self::sqlCountForProductsByGroup($inputRequestsCondition,$groupCondition); 
            $command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $resultsCount=$command->queryScalar();
            
        }

        //посик только по чод
        if ($filters['name']==null && $filters['chod']!=null)
        {
            $inputRequestsCondition = "input_requests.chod LIKE :chod";
            $command->text=self::sqlSelectForProductsByGroup($inputRequestsCondition,$groupCondition,$limit);
            $filters['chod'] = '%'.$filters['chod'].'%';      
            $command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $rows=$command->queryAll();

            $command->text=self::sqlCountForProductsByGroup($inputRequestsCondition,$groupCondition); 
            $command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $resultsCount=$command->queryScalar();
        }

        //поиск только по имени и чод
        if ($filters['name']!=null && $filters['chod']!=null)
        {
            $inputRequestsCondition = "input_requests.name LIKE :name AND input_requests.chod LIKE :chod";
            $command->text=self::sqlSelectForProductsByGroup($inputRequestsCondition,$groupCondition,$limit);  
            $filters['name'] = '%'.$filters['name'].'%';
            $filters['chod'] = '%'.$filters['chod'].'%';   
            $command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
            $command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $rows=$command->queryAll();

            $command->text=self::sqlCountForProductsByGroup($inputRequestsCondition,$groupCondition); 
            $command->bindParam(":name",$filters['name'],PDO::PARAM_STR);
            $command->bindParam(":chod",$filters['chod'],PDO::PARAM_STR);
            if ($filters['groupId']!=null) {
                $command->bindParam(":group_id",$filters['groupId'],PDO::PARAM_INT);   
            } 
            $resultsCount=$command->queryScalar();
        }

        $lastProductId=-1;
        $products = array();
        foreach ($rows as $row) {
            if ($row['id']!=$lastProductId) {
                $product=array();
                $product['id'] = $row['id'];
                $product['original_name'] = $row['original_name'];
                $product['chod_display'] = $row['chod_display'];
                $product['product_num'] = $row['product_num'];
                $product['img_exists'] = $row['img_exists'];
                $product['rating'] = $row['rating'];
                $product['groups_str'] = $row['group_name'];
                $product['groups_ids'] = array();
                if ($row['group_id']!=null) {
                    $product['groups_ids'][] = $row['group_id'];
                }                
                $products[] = $product;
                $lastProductId = $row['id'];
            }
            else {
                $lastRowNumber = count($products)-1;
                $products[$lastRowNumber]['groups_str'].=", ".$row['group_name'];
                $products[$lastRowNumber]['groups_ids'][] = $row['group_id'];
            }
        }
        
        $result = array();
        $result['products'] = $products;
        $result['resultsCount'] = $resultsCount;

        return $result; 
    }

    /**
     * Конструктор sql для поиска товаров группе, имени и ЧОД
     * @param string $inputRequestsWhere Условия выборки для таблицы input_requests
     * @param string $limit  LIMIT условие для products
     * @return string
     */
    private static function sqlSelectForProductsByGroup($inputRequestsCondition,$groupCondition,$limit)
    {
        if ($inputRequestsCondition!=null) {
            $inputRequestsWhere = "WHERE $inputRequestsCondition";
        }
        else {
            $inputRequestsWhere = "";
        }
        $sql = "SELECT DISTINCT 
                    findedproducts.id,
                    findedproducts.original_name,
                    findedproducts.chod_display,
                    findedproducts.product_num,
                    findedproducts.img,
                    findedproducts.rating,
                    groups.id AS group_id,
                    groups.name AS group_name, 
                    groups.group_num, 
                    IF (findedproducts.img IS NOT NULL,1,0) AS img_exists
                FROM
                (
                    SELECT p.*
                    FROM products p
                    LEFT JOIN group_prods ON group_prods.product_id = p.id                  
                    WHERE 
                        p.id IN
                        (
                            SELECT input_requests.product_id
                            FROM input_requests
                            $inputRequestsWhere
                        ) 
                        AND $groupCondition
                ) findedproducts
                LEFT JOIN group_prods allgp ON allgp.product_id = findedproducts.id
                LEFT JOIN groups ON  groups.id = allgp.group_id 
                ORDER BY img_exists DESC, findedproducts.rating DESC, findedproducts.id ASC, groups.group_num ASC 
                $limit
                ;";     
        return $sql;
    }

    /**
     * Конструктор sql для подсчета обещего количества товаров для поиска товаров группе, имени и ЧОД.
     * Необходимо для пагинации.
     * @param string $inputRequestsWhere Условия выборки для таблицы input_requests
     * @param string $limit  LIMIT условие для products
     * @return string
     */
    private static function sqlCountForProductsByGroup($inputRequestsCondition,$groupCondition) {
        if ($inputRequestsCondition!=null) {
            $inputRequestsWhere = "WHERE $inputRequestsCondition";
        }
        else {
            $inputRequestsWhere = "";
        }
        $sql="SELECT COUNT( * )
            FROM products p
            LEFT JOIN group_prods ON group_prods.product_id = p.id          
            WHERE
                p.id IN (
                    SELECT input_requests.product_id
                    FROM input_requests
                    $inputRequestsWhere
                )
                AND $groupCondition
            ;";
        return $sql;
    }
}