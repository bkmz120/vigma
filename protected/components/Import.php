<?php

require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';

/**
 * Импорт 
 * 
 */
class Import
{
	//первоначальная загрузка прайса ЧТЗ
	//для каждой строки фалай создаём новую запись в Product и InputRequests
	public function importChtzPrice()
	{
		//$productFile = fopen(Yii::app()->basePath."/import/chtz.csv","r");
		//products.csv - основной прайс чтз
		//чод;наименование;цена;цена с НДС;код продукции;дата изменения;вес;

		require_once Yii::app()->basePath . '/extensions/phpexcelreader/excel_reader2.php';
		$ds=DIRECTORY_SEPARATOR;

		$priceImportCoef = 0.835;
		$price_date = '2016-02-08';
		
		$data = new Spreadsheet_Excel_Reader(Yii::app()->basePath .$ds."import".$ds."chtz.xls",false);


		$stop=false;
		$k=0;
		$err_count = 0;

		$provider_id=1; //ЧТЗ

		$startRow = 4;
		$chodClm = 1;
		$nameClm = 2;
		$productNumClm = 5;
		$priceClm = 4;
		$weightClm = 7;			

		$row_count = $data->rowcount(0); 

		for ($i=$startRow;$i<=$row_count;$i++)
		{
			
			//$chod_display = preg_replace('/[.]/ui',',',$split[0]); //меняем точку на запятую. если в ЧОД есть запятая, то php-excel-reader (используемый при ообновлении праяса) читает её как запятую, а csv стоит точка
																   //в связи с чем при обновлении прайса товары не находятся. поэтому при изначальной загрузке из csv ставим запятую
			$chod_display = trim($data->val($i,$chodClm));
			$chod=preg_replace('/[^a-zа-яё\d]/ui','',$chod_display);
			$name = trim($data->val($i,$nameClm));
			//$productNum = $data->val($i,$productNumClm);
			$price_in = $data->val($i,$priceClm);
		    //$price_in = preg_replace('/[,]/ui','',$price_in); //удаляем запятые, в данном случае они разделяют тысячи
			$price_in = preg_replace('/[^0-9.]/ui', '',$price_in);//удаляем всё кроме цифр и точки
			
			$price_chtz = $price_in;
			$price_orig = $price_in;
			$price_in = round($price_in * $priceImportCoef,2);
			
			$product_num = $data->val($i,$productNumClm);
			$product_num = preg_replace('/[^0-9.]/ui', '',$product_num); //удаляем всё кроме цифр и точки

			$weight = trim($data->val($i,$weightClm));

			
			$product = new Products;
			$product->original_name = $name;
			$product->original_chod = $chod;
			$product->chod_display = $chod_display;
			$product->product_num = $product_num;
			$product->weight = $weight;
			$product->price_chtz = $price_chtz;

			

			$res=$product->addNew($provider_id, $price_in,$price_orig,$price_date);

			if ($res==false)
			{
				echo "false";
				return;
			}

			$k++;
		}

		echo $k;


	}

	

	public function importInmputs()
	{
		$inputFile = fopen(Yii::app()->basePath."/import/inputs.csv","r");
		//наименование заказчика;чод заказчика;;ЧОД заказчика уточненный;Ч. О. Д. ЧТЗ;наименование детали ЧТЗ
		//0                       1            2 3                        4           5
		//для нового input:
		//наименование заказчика	чод заказчика	коментарий	ЧОД правильный	ЧОД ЧТЗ	наименование ЧТЗ	Цена ЧТЗ с НДС	Код покупки	Поставщик	входящая стоимость
		//0                          1               2             3             4                           5               6             7          8

		//для причесанного
		//наименование заказчика	чод заказчика	коментарий	ЧОД уточненный	ЧОД ЧТЗ	наименование ЧТЗ	Код покупки	   Источник	 корректировка входящая стоимость
		//0                          1               2             3             4       5                   6             7          8

		$comment_add_count = 0;
		$comment_cancel_count = 0;
		$input_add_count = 0;
		$input_cancel_count = 0;
		$input_with_no_product_count = 0;

		while (!feof($inputFile))
		{
			$str = fgets($inputFile);
			$split = explode(';',$str);
			$products=null;
			$chod_chtz = $split[4];
			$chod_chtz=preg_replace('/[^a-zа-яё\d]/ui','', $chod_chtz);

			
            //поиск детали из input в products по ЧОД ЧТЗ
			if ($chod_chtz!="") {
				//var_dump("step".$step." if1 ");

				//бывает, что отдному ЧОД соответствует нексолько записей в Product, отличающихся product_num (код товара)
				$products = Products::model()->findAll(array('condition'=>'original_chod=:chod',
															 'params'=>array(':chod'=>$chod_chtz)));
				//if (count($products)>1) var_dump($chod." - ".count($products));
			}

			/* поиск по имени не точен, так как оно не уникально
			//если поиск по ЧОД не дал результатов - ищем по имени
			if ($products==null && $split[4]!="") {
				//var_dump("step".$step." if2");
				$chod=$split[4];
				$products = Products::model()->findAll(array('condition'=>'original_chod=:chod',
															 'params'=>array(':chod'=>$chod)));
			}
			*/

			


			if ($products!=null)
			{
				//var_dump("step".$step." pr");
				foreach ($products as $product) 
				{

					//получим список синонимов для этого продукта
					$inputRequests = InputRequests::model()->findAll(array('condition'=>'product_id=:product_id',
															 'params'=>array(':product_id'=>$product->id)));
					
					$chod_display = $split[1];
					$chod=preg_replace('/[^a-zа-яё\d]/ui','', $chod_display);

					//проверим есть ли этот среди них
					$it_is_new_input=true;
					foreach ($inputRequests as $inputRequest)
					{
						if (strcasecmp($inputRequest->name, $split[0])==0 && strcasecmp($inputRequest->chod, $chod)==0)
						{
							$it_is_new_input=false;
						}
					}

					if ($it_is_new_input)
					{
						$inputRequest = new InputRequests;
						$inputRequest->name=$split[0];
						$inputRequest->chod=$chod;
						$inputRequest->chod_display=$chod_display;
						$inputRequest->comment = $split[2];
						$inputRequest->product_id = $product->id;
						$inputRequest->original_item = 0;
						$inputRequest->chod_utochn = $split[3];
						$inputRequest->chod_chtz = $split[4];
						$inputRequest->name_chtz = $split[5];
						$inputRequest->pokupka = $split[6];
						$inputRequest->istochnik = $split[7];
						$res=$inputRequest->save();
						if($res==false)
						{
							var_dump($inputRequest);
							return;
						}
						$input_add_count++;
					}
					else $input_cancel_count++;

					$_comment = $split[2];
					$needSaveComment = false;
					$comment = "";

					if ($_comment!='')
					{
						$comment = $_comment."\r\n";
						$needSaveProduct = true;
						$comment_add_count++;
						/*
						//получим список комментариев для этого продукта
						$comments = Comments::model()->findAll(array('condition'=>'product_id=:product_id',
																 'params'=>array(':product_id'=>$product->id)));
						
						$it_is_new_comment = true;
						foreach ($comments as $comment)
						{
							if (strcasecmp($comment->comment, $_comment)==0) $it_is_new_comment=false;
						}

						if ($it_is_new_comment)
						{
							$comment = new Comments;
							$comment->product_id = $product->id;
							$comment->comment = $_comment;
							$res=$comment->save();
							if ($res==false)
							{
								var_dump($comment);
								return;
							}
							$comment_add_count++;
						}
						else $comment_cancel_count++;
						*/
					}


					//если в памяти указан поставщик - оставим его в комментариях к товару

					if ($split[7]!='')
					{
						$commentAboutProvider= 'Поставщик из памяти: '.$split[7];
						if ($split[8]!='') $commentAboutProvider .= ' корр. вх.: '.$split[8];
						$comment = $comment.$commentAboutProvider." ";
						$needSaveProduct = true;
						$comment_add_count++;
						/*
						$comment = new Comments;
						$comment->product_id = $product->id;
						$comment->comment = $commentAboutProvider;
						$res=$comment->save();
						if ($res==false)
						{
							var_dump($comment);
							return;
						}
						*/
					}

					if ($needSaveComment)
					{
						$product->comment = $comment;
						$product->save();
					}
				}
			}
			/*
			else //  если нет записи в products соответствующей этому input, то импортируем её с product_id=null
			{
				$chod_display = $split[1];
				if ($chod_display=='НД' || $chod_display=='#Н/Д') $chod_display='';
				$chod=preg_replace('/[^a-zа-яё\d]/ui','', $chod_display);

				$name = $split[0];
				$needToAdd = true;
				if ($name== '' && $chod == '') $needToAdd = false;
				if ($name=='наименование заказчика') $needToAdd = false;

				if ($needToAdd)
				{
					$inputRequest = new InputRequests;
					$inputRequest = new InputRequests;
					$inputRequest->name=$split[0];
					$inputRequest->chod=$chod;
					$inputRequest->chod_display=$chod_display;
					$inputRequest->comment = $split[2];
					$inputRequest->original_item = 0;
					$inputRequest->chod_utochn = $split[3];
					$inputRequest->chod_chtz = $split[4];
					$inputRequest->name_chtz = $split[5];
					$inputRequest->pokupka = $split[6];
					$inputRequest->istochnik = $split[7];

					$ptice_in = $split[8];
					$price_in = preg_replace('/[,]/ui','.',$ptice_in); //меняем запятую на точку
					$price_in = preg_replace('/[^0-9.]/ui', '',$price_in);//удаляем всё кроме цифр и точки

					if (is_numeric($price_in)) $inputRequest->price_in = (float)$ptice_in;

					$res=$inputRequest->save();

					if ($res==false)
					{
						var_dump($price_in);
						var_dump($inputRequest);
						return;
					}
					$input_with_no_product_count++;
				}
			}
			*/
			
		}


	
		echo "Inputs with products added: ".$input_add_count."  Inputs with NO products: ".$input_with_no_product_count."  Inputs cancel: ".$input_cancel_count." Comments added: ".$comment_add_count." Comments cancel: ".$comment_cancel_count;

		fclose($inputFile);
	}

	//функция для отладки, поиск позиций в памяти, чод которых не найден в products.original_chod
	public function findNotFoundFromMemomry()
	{
		$inputFile = fopen(Yii::app()->basePath."/import/inputs.csv","r");
		//наименование заказчика;чод заказчика;;ЧОД заказчика уточненный;Ч. О. Д. ЧТЗ;наименование детали ЧТЗ
		//0                       1            2 3                        4           5
		//для нового input:
		//наименование заказчика	чод заказчика	коментарий	ЧОД правильный	ЧОД ЧТЗ	наименование ЧТЗ	Цена ЧТЗ с НДС	Код покупки	Поставщик	входящая стоимость
		//0                          1               2             3             4                           5               6             7          8

		//для причесанного
		//наименование заказчика	чод заказчика	коментарий	ЧОД уточненный	ЧОД ЧТЗ	наименование ЧТЗ	Код покупки	   Источник	 корректировка входящая стоимость
		//0                          1               2             3             4       5                   6             7          8

		$comment_add_count = 0;
		$comment_cancel_count = 0;
		$input_add_count = 0;
		$input_cancel_count = 0;
		$input_with_no_product_count = 0;

		$notFoundProducts = array();

		while (!feof($inputFile))
		{
			$str = fgets($inputFile);
			$split = explode(';',$str);
			$products=null;
			$chod_chtz = $split[4];
			$chod_chtz=preg_replace('/[^a-zа-яё\d]/ui','', $chod_chtz);
			$chod_chtzDisplay = $split[4];
			$name = $split[0];
			$chod_display = $split[1];
			
            //поиск детали из input в products по ЧОД ЧТЗ
			if ($chod_chtz!="") {
				//var_dump("step".$step." if1 ");

				//бывает, что отдному ЧОД соответствует нексолько записей в Product, отличающихся product_num (код товара)
				$products = Products::model()->findAll(array('condition'=>'original_chod=:chod',
															 'params'=>array(':chod'=>$chod_chtz)));
				//if (count($products)>1) var_dump($chod." - ".count($products));
			}

			if ($products==null)
			{		
				if ($chod_chtz==null || $chod_chtz=="" || $chod_chtz==" ")	
				{
					$chod_chtz = "(пусто)";
					$chod_chtzDisplay = $chod_chtz;
				}	
				$inNotFoundProducts = false;
				for ($i=0; $i<count($notFoundProducts);$i++ )
				{
					if (strcmp($notFoundProducts[$i]['chod_chtz'], $chod_chtz) == 0)
					{
						$inInputs = false;
						foreach ($notFoundProducts[$i]['inputRequests'] as $inputRequest)
						{
							if (strcmp($inputRequest['name'],$name)==0 && strcmp($inputRequest['chod_display'],$chod_display)==0) 
							{
								$inInputs = true;
								break;
							}
						}

						if (!$inInputs)
						{
							$inputRequestInfo = array('name'=>$name,
											 	  'chod_display'=>$chod_display,
											 	  );
							array_push($notFoundProducts[$i]['inputRequests'], $inputRequestInfo);
							$inNotFoundProducts = true;
						}

						
						break;
					}
				}

				if (!$inNotFoundProducts)
				{
					$inputRequestInfo = array('name'=>$name,
											  'chod_display'=>$chod_display,
											);
					$notFoundProduct = array('chod_chtz' => $chod_chtz,
											 'chod_chtzDisplay' => $chod_chtzDisplay, 
											 'inputRequests' => array(0=>$inputRequestInfo),										 				
											);

					
					//array_push($notFoundProduct['inputRequests'],$inputRequestInfo);

					array_push($notFoundProducts, $notFoundProduct);
				}
			}

			
		}
		fclose($inputFile);

		$myfile = fopen(Yii::app()->basePath."/import/memory10.csv", "w");

		echo "start_write:";
		$k=0;
		foreach ($notFoundProducts as $notFoundProduct)
		{
				$str = $notFoundProduct['chod_chtz'].";;;\n";
				fwrite($myfile, $str);

				foreach ($notFoundProduct['inputRequests'] as $inputRequest)
				{
					$str = ";".$inputRequest['name'].";".$inputRequest['chod_display'].";\n";
					fwrite($myfile, $str);
				}



				$k++;
				//if ($k==1000) break;
		}
			
		fclose($myfile);

		return true;;
		
	}

	//для отладки, преобразование базы в новый формат
	function addProvprodidFieldToInputRequest()
	{
		$connection=Yii::app()->db;
		$sql = "SELECT * FROM input_requests WHERE input_requests.provider_id IS NOT NULL";
		$command=$connection->createCommand($sql);	
		$inputRequestsDR=$command->query();

		$updatedCount =0;
		foreach ($inputRequestsDR as $inputRequest)
		{
			$sql = "SELECT id FROM prov_prods WHERE prov_prods.provider_id=".$inputRequest['provider_id']." AND prov_prods.product_id=".$inputRequest['product_id'].";";
			$command=$connection->createCommand($sql);	
			$provProdsDR=$command->query();
			$provProdsCount = $provProdsDR->rowCount;


			if ($provProdsCount==1)
			{
				$provProd = $provProdsDR->read();
				$sql = "update input_requests i SET i.provprod_id=".$provProd['id']." WHERE i.id=".$inputRequest['id'].";";
				$connection=Yii::app()->db;		
				$command=$connection->createCommand($sql);
				$res=$command->execute();
				$updatedCount++;
			}
			if ($provProdsCount>1)
			{
				echo "$provProdsCount provProds for inputRequestId: ".$inputRequest['id']."<br>";
			}

			//if ($updatedCount>5) break;
		}

		echo "<br> updateCount: $updatedCount";
	}


	//очистить tmpInPriceCheck у всех prov_prods
	function clearPriceInCheck()
	{
		$sql = "update prov_prods pp SET pp.tmpInPriceCheck=0";
		$connection=Yii::app()->db;		
		$command=$connection->createCommand($sql);
		$res=$command->execute();
		
		return true;
	}

	//пометим строку в таблице ProvProd как присутствующую в импортируемом файле (для дальнейшей выборке строк, которых там не было)
	function setPriceInProvProdById($id)
	{
		$sql = "update prov_prods pp SET pp.tmpInPriceCheck=1 WHERE pp.id=$id";
		$connection=Yii::app()->db;		
		$command=$connection->createCommand($sql);
		$res=$command->execute();
		// if ($res==false) не нужна проверка т.к. если tmpInPriceCheck  уже был "1", то возвратит false, что не есть ошибка
		// {
		//
		// 	echo "set error";
		// 	return false;
		// }
		// else 
		return true;
	}

	//пометим строку в таблице ProvProd как присутствующую в импортируемом файле (для дальнейшей выборке строк, которых там не было)
	function setPriceInProvProdByProductId($product_id)
	{
		$sql = "update prov_prods pp SET pp.tmpInPriceCheck=1 WHERE pp.product_id=$product_id";
		$connection=Yii::app()->db;		
		$command=$connection->createCommand($sql);
		$res=$command->execute();
		if ($res==false) 
		{
			echo "set error";
			return false;
		}
		else return true;
	}

	function findNotInPriceFile($provider_id)
	{
		$sql = "SELECT * FROM products
				WHERE products.id IN (
					SELECT prov_prods.product_id FROM prov_prods WHERE prov_prods.provider_id='".$provider_id."' AND prov_prods.tmpInPriceCheck=0
				);";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);	
		$provProdsDataReader=$command->query();
		
		$notInPriceFile = array();
		foreach($provProdsDataReader as $row)
		{
			$notInPriceFileRow=array();
			$notInPriceFileRow['id'] = $row['id'];
			$notInPriceFileRow['name'] = $row['original_name'];
			$notInPriceFileRow['chod'] = $row['chod_display'];

			array_push($notInPriceFile, $notInPriceFileRow);
		}

		//отметим эти prov_prods как исчезнувшие
		$sql = "UPDATE prov_prods SET notInPrice=1 WHERE prov_prods.provider_id='".$provider_id."' AND prov_prods.tmpInPriceCheck=0";
		$command=$connection->createCommand($sql);
		$command->execute();


		return $notInPriceFile;
		
	}

	function updateChtzPrice($template,$priceFilePath,$priceImportCoef,$priceActualDate)
	{  
		$objPHPExcel = PHPExcel_IOFactory::load($priceFilePath);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$priceImportCoef = floatval($priceImportCoef);
		$provider_id = 1;
		$startRow = $template->startRowNum;
		$chodClm = $template->chodClm-1;
		$nameClm = $template->nameClm-1;
		$productNumClm = $template->productNumClm-1;
		$priceClm = $template->priceClm-1;
		$weightClm = $template->weightClm-1;
		// $startRow = 4;
		// $chodClm = 0;
		// $nameClm = 1;
		// $productNumClm = 4;
		// $priceClm = 3;
		// $weightClm = 6;

		$res=$this->clearPriceInCheck();

		//$priceActualDate=date('Y-m-d',strtotime($priceActualDate));

		//$row_count = $data->rowcount(0);
		$row_count = $objWorksheet->getHighestRow();

		$criteria = new CDbCriteria();

		$updatedRows = array(); //колчество обновленных товаров
		$notNeedUpdateRows = array(); // количество товаров, для которых цена не поменялась
		$newProducts = array(); // не найденные в БД товары
		$dublikatChodProducts = array(); // список ЧОД, для которых нашлось нескольео product, например если ЧОД у товаров одинаковый, а код продукции разный
		$productsWhichNeedProvider = array(); // товары, у которых нет поставщика "каток", нужно доабвить
		$newProducts = array();

		$connection=Yii::app()->db;

		for ($i=$startRow;$i<=$row_count;$i++)
		{
			$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
			//$chod = preg_replace('/[^a-zа-яё\d]/ui','', $chod);
			$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
			
			$chod = mb_convert_encoding($chod, "UTF-8", "UTF-8"); //без это функции неотрые utf-8 символы вызывают ошибку 
			$name = mb_convert_encoding($name, "UTF-8", "UTF-8"); //в json_encode(). Было замечано для позиции 24-21-119СП	УПОР в прайс Каток 15.02.16.(13%)


			//$productNum = $data->val($i,$productNumClm);
			$price_orig = trim($objWorksheet->getCellByColumnAndRow($priceClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
		    //$price_in = preg_replace('/[,]/ui','',$price_in); //удаляем запятые, в данном случае они разделяют тысячи
			$price_orig = preg_replace('/[^0-9.]/ui', '',$price_orig);//удаляем всё кроме цифр и точки
			$price_orig = floatval($price_orig);

			$price_chtz = $price_orig;
			$price_in = round($price_orig * $priceImportCoef,2);
			$price_in = floatval($price_in);

			$product_num = trim($objWorksheet->getCellByColumnAndRow($productNumClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
			$product_num = preg_replace('/[^0-9.]/ui', '',$product_num); //удаляем всё кроме цифр и точки

			$weight = trim($objWorksheet->getCellByColumnAndRow($weightClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела

			$priceRow = array('provider_id'=>$provider_id,
							  'name'=>$name,
							  'chod'=>$chod,
							  'price_orig' => $price_orig,
							  'price_in'=>$price_in,
							  'product_num'=>$product_num,
							  'weight'=>$weight,
							  'priceActualDate'=>$priceActualDate,									  
						);

			//var_dump("ch: ".$chod." n: ".$name." pr: ".$price_in." pn: ".$product_num);
			//ищем Product c chod_display как в прайсе 
			//ищем именно по product, а не по InputRequest т.к. у каждой позиции прайса ЧТЗ должна быть отдельная запись в products, в отличии от других прайсов,
			//позиции в которых могут быть синонимами к уже существующим записям в products
			if ($product_num!="") $sql = "select id from products where chod_display='".$chod."' AND product_num='".$product_num."'";
			else $sql = "select id from products where chod_display='".$chod."'";

			//var_dump($sql);

			$command=$connection->createCommand($sql);	
			$productsDataReader=$command->query();
			$productsCount = $productsDataReader->rowCount;
			
			//нашелся один, проверим закрпелен ли за ним поставщик "чтз"
			if ($productsCount==1)
			{

				$productRow=$productsDataReader->read();
				$product_id = $productRow['id'];

				$this->setPriceInProvProdByProductId($product_id);

				//ищем ProvProd, для этого твара и с поставщиком "чтз"
				$sql = "select id from prov_prods pp where pp.product_id='".$product_id."' AND pp.provider_id='".$provider_id."';";
				$command=$connection->createCommand($sql);	
				$provProdsDataReader=$command->query();
				$provProdsCount = $provProdsDataReader->rowCount;

								
				
				if ($provProdsCount  == 1) //значит за товаром был уже закрплён постащик "чтз", обновим
				{		
					$date=date('Y-m-d',strtotime($priceActualDate));
					//снимем флаг notInPrice, на случай если товар был помечен как исчезнувший из прайса
					$sql = "update prov_prods pp SET 
								pp.price_in='$price_in', 
								pp.price_orig='$price_orig', 
								pp.price_date='".$date."',
								pp.notInPrice = 0
							WHERE pp.product_id='".$product_id."' AND pp.provider_id='".$provider_id."';";
					$command=$connection->createCommand($sql);
					$updatedProvProdNum=$command->execute();


					//так же обновим прайс ЧТЗ
					$sql = "update products p SET p.price_chtz='".$price_chtz."'  WHERE p.id='".$product_id."' ;";
					$command=$connection->createCommand($sql);
					$updatedProductsNum=$command->execute();

					$priceRow['product'] = array('id'=>$product_id);
					if ($updatedProvProdNum!=0)
					{	
						
						$row = array('priceRow'=>$priceRow);

						array_push($updatedRows, $row);	
					}

					if ($updatedProvProdNum==0) 
					{
						$row = array('priceRow'=>$priceRow);
						array_push($notNeedUpdateRows, $row);					
					}
				}

				if ($provProdsCount  == 0) //значит за товаром еще не был закрплён постащик "чтз", добавим в список на обновление
				{
					//ищем Product c chod_display как в прайсе
					$sql = "select original_name from products where id=".$product_id.";";
					$command=$connection->createCommand($sql);	
					$productDataReader=$command->query();
					$productRow=$productDataReader->read();

					$productWhichNeedProvider = array("priceRow"=>$priceRow,
													  "product_id"=>$product_id,
													  "product_originalName"=>$productRow['original_name'],
						);

					
					array_push($productsWhichNeedProvider, $productWhichNeedProvider);
				}

			}

			// если не нашлось товара с таким же с chod_display,значит надо предложить добавить новый product с этим товаром
			if ($productsCount==0)
			{
				$row = array('priceRow'=>$priceRow);
				array_push($newProducts, $row);
				$notFoundCount++;
			}

			if ($productsCount>1)
			{
				$row = array('priceRow'=>$priceRow);
				array_push($dublikatChodProducts, $row);
			}
		}

		/*
		$price = Prices::model()->findByPk(1);
		$price->date = date('Y-m-d',strtotime($priceActualDate));//date('Y-m-d'); 
		$res=$price->update();
		*/

		//var_dump($result);

		$result = array('updatedRows'=>$updatedRows,
						'notNeedUpdateRows'=>$notNeedUpdateRows,
						'dublikatChodProducts'=>$dublikatChodProducts,
						'productsWhichNeedProvider'=>$productsWhichNeedProvider,
						'newProducts' => $newProducts,
						'date'=> $priceActualDate,
			); 

		$result['notInPriceFileProducts'] = $this->findNotInPriceFile($provider_id);

		return $result;
	}

	/*
	function updateKatokPrice($priceFilePath,$priceImportCoef,$priceActualDate)
	{  
		$objPHPExcel = PHPExcel_IOFactory::load($priceFilePath);
		$objWorksheet = $objPHPExcel->getActiveSheet();
				
		$priceImportCoef = floatval($priceImportCoef);
		$provider_id = 2;
		$startRow = 6;
		$chodClm = 1;
		$nameClm = 2;
		//$productNumClm = 5;
		$priceClm = 3;

		
		$res=$this->clearPriceInCheck();
		if ($res==false) return;	

		//$priceActualDate=date('Y-m-d',strtotime($priceActualDate));

		//$priceImportCoef = (float)$priceImportCoef;

		//$row_count = $data->rowcount(0);
		$row_count = $objWorksheet->getHighestRow();


		$criteria = new CDbCriteria();

		$result = array('updatedRows'=>array(), //обновленные строки
						'notNeedUpdateRows'=>array(), // строки для которых цена не поменялась
						'dublikatChodProducts'=>array(), // список ЧОД, для которых нашлось нескольео product, например если ЧОД у товаров одинаковый, а код продукции разный
						'productsWhichNeedProvider'=>array(), // товары, у которых нет поставщика "каток", нужно доабвить
						'newProducts' => array(), // не найденные в БД товары
		);

		

		for ($i=$startRow;$i<=$row_count;$i++)
		{
			$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			//$chod = preg_replace('/[^a-zа-яё\d]/ui','', $chod);
			$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));

			if ($chod=="" && $name=="") continue;

			//$productNum = $data->val($i,$productNumClm);
			$price_orig = trim($objWorksheet->getCellByColumnAndRow($priceClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			//if ($chod == '66-22-1') var_dump($price_in);
			//var_dump($price_in);
		    $price_orig = preg_replace('/[^0-9.]/ui', '',$price_orig);//удаляем всё кроме цифр и точки
			$price_orig = floatval($price_orig);
			// echo "price_orig: ";
			// echo $price_orig;
			// echo "<br>";

			$price_in = round($price_orig * $priceImportCoef,2);
			//$price_in = $price_orig * $priceImportCoef;
			$price_in = floatval($price_in);
			// echo "price_in: ";
			// echo $price_in;
			// echo "<br>";

			$weight = null;
			$product_num = null;

			$priceRow = array('provider_id'=>$provider_id,
							  'name'=>$name,
							  'chod'=>$chod,
							  'price_orig' => $price_orig,
							  'price_in'=>$price_in,
							  'product_num'=>$product_num,
							  'weight'=>$weight,
							  'priceActualDate'=>$priceActualDate,									  
						);
			
			$this->importPriceRow($priceRow,$priceActualDate,$result);
		}

		$price = Prices::model()->findByPk(2);
		$price->date = date('Y-m-d',strtotime($priceActualDate));//date('Y-m-d'); 
		$res=$price->update();

		
		$result['notInPriceFileProducts'] = $this->findNotInPriceFile($provider_id);
		$result['date'] =  $priceActualDate;//date('d.m.Y');

		return $result;
	}

	function updateChazPrice($priceFilePath,$priceImportCoef,$priceActualDate)
	{  
		$sheetNum = 4;//номер листа (нумерация с 0)
		$objPHPExcel = PHPExcel_IOFactory::load($priceFilePath);
		$objPHPExcel->setActiveSheetIndex($sheetNum);
		$objWorksheet = $objPHPExcel->getActiveSheet();

		$priceImportCoef = floatval($priceImportCoef);
		$provider_id = 10;
		$startRow = 48;
		$chodClm = 2;
		$nameClm = 1;
		//$productNumClm = 5;
		$priceClm = 7;

		$weightClm = 6;
		

		$res=$this->clearPriceInCheck();

		//$priceActualDate=date('Y-m-d',strtotime($priceActualDate));

		//$row_count = $data->rowcount($sheetNum);
		$row_count = $objWorksheet->getHighestRow();

		$criteria = new CDbCriteria();

		$result = array('updatedRows'=>array(), //обновленные строки
						'notNeedUpdateRows'=>array(), // строки для которых цена не поменялась
						'dublikatChodProducts'=>array(), // список ЧОД, для которых нашлось нескольео product, например если ЧОД у товаров одинаковый, а код продукции разный
						'productsWhichNeedProvider'=>array(), // товары, у которых нет поставщика "каток", нужно доабвить
						'newProducts' => array(), // не найденные в БД товары
		);

		

		for ($i=$startRow;$i<=$row_count;$i++)
		{
			$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			//$chod = preg_replace('/[^a-zа-яё\d]/ui','', $chod);
			$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			//$productNum = $data->val($i,$productNumClm);

			if ($chod=="" && $name=="") continue;

			$price_orig = trim($objWorksheet->getCellByColumnAndRow($priceClm,$i)->getValue(),chr(0xC2).chr(0xA0));
		    //$price_in = preg_replace('/[,]/ui','',$price_in); //удаляем запятые, в данном случае они разделяют тысячи
			$price_orig = preg_replace('/[^0-9.]/ui', '',$price_orig);//удаляем всё кроме цифр и точки
			$price_orig = floatval($price_orig);

			$price_in = round($price_orig * $priceImportCoef,2);
			$price_in = floatval($price_in);

			$weight = trim($objWorksheet->getCellByColumnAndRow($weightClm,$i)->getValue(),chr(0xC2).chr(0xA0));

			$priceRow = array('provider_id'=>$provider_id,
							  'name'=>$name,
							  'chod'=>$chod,
							  'price_orig' => $price_orig,
							  'price_in'=>$price_in,
							  'product_num'=>$product_num,
							  'weight'=>$weight,
							  'priceActualDate'=>$priceActualDate,									  
						);
			
			$this->importPriceRow($priceRow,$priceActualDate,$result);
		}

		$price = Prices::model()->findByPk(3);
		$price->date = date('Y-m-d',strtotime($priceActualDate)); //date('Y-m-d'); 
		$res=$price->update();

		$result['date'] =  $priceActualDate;//date('d.m.Y');		
		$result['notInPriceFileProducts'] = $this->findNotInPriceFile($provider_id);
		
		return $result;
	}
	*/

	function updateSmirnyaginPrice($priceFilePath,$priceImportCoef,$priceActualDate,$sheetCount)
	{  
		$objPHPExcel = PHPExcel_IOFactory::load($priceFilePath);

		$priceImportCoef = floatval($priceImportCoef);
		$provider_id = 30;
		$startRowFirstSheet = 6;

		$chodClm1 = 0;
		$nameClm1= 1;
		$priceClm1 = 2;

		$chodClm2 = 4;
		$nameClm2= 5;
		$priceClm2 = 6;

		$res=$this->clearPriceInCheck();

		//$priceActualDate=date('Y-m-d',strtotime($priceActualDate));

		

		$result = array('updatedRows'=>array(), //обновленные строки
						'notNeedUpdateRows'=>array(), // строки для которых цена не поменялась
						'dublikatChodProducts'=>array(), // список ЧОД, для которых нашлось нескольео product, например если ЧОД у товаров одинаковый, а код продукции разный
						'productsWhichNeedProvider'=>array(), // товары, у которых нет поставщика "каток", нужно доабвить
						'newProducts' => array(), // не найденные в БД товары
		);

		
		//обойти все листы
		for ($sheetIndex = 0;$sheetIndex<$sheetCount;$sheetIndex++)
		{
			$objPHPExcel->setActiveSheetIndex($sheetIndex);
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$row_count = $objWorksheet->getHighestRow();

			
			
			//отступ сверху только у первого листа
			if ($sheetIndex==0) $startRow=$startRowFirstSheet;
			else $startRow = 1;

			//обойти две колонки
			for ($clmnIndex=1;$clmnIndex<=2;$clmnIndex++)
			{
				if ($clmnIndex==1)
				{
					$chodClm=$chodClm1;
					$nameClm=$nameClm1;
					$priceClm=$priceClm1;
				}

				if ($clmnIndex==2)
				{
					$chodClm=$chodClm2;
					$nameClm=$nameClm2;
					$priceClm=$priceClm2;
				}
			
				for ($i=$startRow;$i<=$row_count;$i++)
				{
					$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));
					//$chod = preg_replace('~\x{00a0}~siu', '', $chod);
					//if ($chod=="01265СП") var_dump("01265СП");
					//$chod = preg_replace('/[^a-zа-яё\d]/ui','', $chod);
					$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));
					//$productNum = $data->val($i,$productNumClm);

					$chod = mb_convert_encoding($chod, "UTF-8", "UTF-8"); //без это функции неотрые utf-8 символы вызывают ошибку 
					$name = mb_convert_encoding($name, "UTF-8", "UTF-8"); //в json_encode(). Было замечано для позиции 24-21-119СП	УПОР в прайс Каток 15.02.16.(13%)

					if ($chod=="" && $name=="") continue;

					$price_orig = trim($objWorksheet->getCellByColumnAndRow($priceClm,$i)->getValue(),chr(0xC2).chr(0xA0));
				    //$price_in = preg_replace('/[,]/ui','',$price_in); //удаляем запятые, в данном случае они разделяют тысячи
					$price_orig = preg_replace('/[^0-9.]/ui', '',$price_orig);//удаляем всё кроме цифр и точки
					$price_orig = floatval($price_orig);

					$price_in = round($price_orig * $priceImportCoef,2);
					$price_in = floatval($price_in);

					$priceRow = array('provider_id'=>$provider_id,
							  'name'=>$name,
							  'chod'=>$chod,
							  'price_orig' => $price_orig,
							  'price_in'=>$price_in,
							  'product_num'=>$product_num,
							  'weight'=>$weight,
							  'priceActualDate'=>$priceActualDate,									  
						);



					//ищем Product по inputrequests c chod_display как в прайсе
					if ($chod=="" && $name=="") continue;

					$this->importPriceRow($priceRow,$priceActualDate,false,$result);
				}
			}
			
		}

		
		// $price = Prices::model()->findByPk(4);
		// $price->date = date('Y-m-d',strtotime($priceActualDate));//date('Y-m-d'); 
		// $res=$price->update();

		$result['date'] =  $priceActualDate;//date('d.m.Y');
		$result['notInPriceFileProducts'] = $this->findNotInPriceFile($provider_id);
		return $result;
	}


	//обновить поставщика, заданного с помощью шаблона
	//$updatePriceChtz - флаг, указывающий нужно ли обновлять поле price_chtz (заносить в него цену из прайса). Это нужно
	//                   для импорта прайса ЧТЗ и ЧТЗ ГУСЕНИЦА, в таблице providers для которых установлен флаг chtzprice_rull

 	function updateCustomPrice($template,$provider_id,$priceFilePath,$priceImportCoef,$priceActualDate,$updatePriceChtz=false)
	{  	
		$objPHPExcel = PHPExcel_IOFactory::load($priceFilePath);
		$objWorksheet = $objPHPExcel->getActiveSheet();		

		$priceImportCoef = floatval($priceImportCoef);
		
		$provider_id = $provider_id;
		$startRow = $template->startRowNum;
		$chodClm = $template->chodClm-1;
		$nameClm = $template->nameClm-1;
		$productNumClm = $template->productNumClm-1;
		$priceClm = $template->priceClm-1;
		$weightClm = $template->weightClm-1;

		
		
		$res=$this->clearPriceInCheck();
		if ($res==false) return;	

		//$priceActualDate=date('Y-m-d',strtotime($priceActualDate));

		//$priceImportCoef = (float)$priceImportCoef;

		//$row_count = $data->rowcount(0);
		$row_count = $objWorksheet->getHighestRow();

		$criteria = new CDbCriteria();

		$result = array('updatedRows'=>array(), //обновленные строки
						'notNeedUpdateRows'=>array(), // строки для которых цена не поменялась
						'dublikatChodProducts'=>array(), // список ЧОД, для которых нашлось нескольео product, например если ЧОД у товаров одинаковый, а код продукции разный
						'productsWhichNeedProvider'=>array(), // товары, у которых нет поставщика "каток", нужно доабвить
						'newProducts' => array(), // не найденные в БД товары
		);

		

		for ($i=$startRow;$i<=$row_count;$i++)
		{
			$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			//$chod = preg_replace('/[^a-zа-яё\d]/ui','', $chod);
			$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));

			$chod = mb_convert_encoding($chod, "UTF-8", "UTF-8"); //без это функции неотрые utf-8 символы вызывают ошибку 
			$name = mb_convert_encoding($name, "UTF-8", "UTF-8"); //в json_encode(). Было замечано для позиции 24-21-119СП	УПОР в прайс Каток 15.02.16.(13%)


			if ($chod=="" && $name=="") continue;

			//$productNum = $data->val($i,$productNumClm);
			$price_orig = trim($objWorksheet->getCellByColumnAndRow($priceClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			
			//if ($chod == '66-22-1') var_dump($price_in);
			//var_dump($price_in);
		    $price_orig = preg_replace('/[^0-9.]/ui', '',$price_orig);//удаляем всё кроме цифр и точки
			$price_orig = floatval($price_orig);

			
			

			$price_in = round($price_orig * $priceImportCoef,2);
			
			$price_in = floatval($price_in);			

			
			if ($weightClm!=-1)
			{
				$weight = trim($objWorksheet->getCellByColumnAndRow($weightClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			}
			if ($productNumClm!=-1)
			{
				$product_num = trim($objWorksheet->getCellByColumnAndRow($productNumClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			}			

			$priceRow = array('provider_id'=>$provider_id,
							  'name'=>$name,
							  'chod'=>$chod,
							  'price_orig' => $price_orig,
							  'price_in'=>$price_in,
							  'product_num'=>$product_num,
							  'weight'=>$weight,
							  'priceActualDate'=>$priceActualDate,									  
						);
			
			$this->importPriceRow($priceRow,$priceActualDate,$updatePriceChtz,$result);
		}

		/*
		$price = Prices::model()->findByPk($priceId);
		$price->date = date('Y-m-d',strtotime($priceActualDate));//date('Y-m-d'); 
		$res=$price->update();
		*/
		
		$result['notInPriceFileProducts'] = $this->findNotInPriceFile($provider_id);
		$result['date'] = $priceActualDate;//date('d.m.Y');

		return $result;
	}

	// функци используется при импорте прайсов Катка, ЧАЗ, Смирнягина
	// передаются данные из каждой строки прайса, для обновления цены, добавления постащика или добавления товара
	function importPriceRow($priceRow,$priceActualDate,$updatePriceChtz,&$result)
	{
			$cleanChod =preg_replace('/[^a-zа-яё\d]/ui','',$priceRow['chod']); 
			//var_dump($priceRow['chod']);

			//сначала ищем полное совпадение имени и ЧОД для выбранного провайдера (input_requests.provider_id)
			$sql = "SELECT prov_prods.id, prov_prods.product_id FROM prov_prods
					WHERE prov_prods.id IN (
					SELECT input_requests.provprod_id FROM input_requests WHERE input_requests.provider_id='".$priceRow['provider_id']."' AND input_requests.chod='".$cleanChod."'  AND input_requests.name='".$priceRow['name']."'
					);"; //
			
			


			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql);	
			$provProdsFullMatchDataReader=$command->query();
			$provProdsFullMatchCount = $provProdsFullMatchDataReader->rowCount;

			if ($provProdsFullMatchCount!=0)
			{
				//обновим цену, дату прайса и вес для всех найденных в БД
				foreach ($provProdsFullMatchDataReader as $provProd)
				{
					//поставим в provProd tmpInPriceCheck=1, тем самым пометива товар как присутствующий в файле прайса, что бы потом найти 
					//отсутствующие в файле прайса товары этого поставщика
					$this->setPriceInProvProdById($provProd['id']);

					$date=date('Y-m-d',strtotime($priceActualDate));

					// при обновлении так же снимем флаг notInPrice
					$sql = "update prov_prods pp SET 
								pp.price_in='".$priceRow['price_in']."', 
								pp.price_orig='".$priceRow['price_orig']."', 
								pp.price_date='".$date."',
								pp.notInPrice = 0 
							WHERE pp.id='".$provProd['id']."';";
					$command=$connection->createCommand($sql);
					$updatedProvProdNum=$command->execute();
					
					if ($updatePriceChtz)
					{
						//так же обновим прайс ЧТЗ
						$sql = "update products p SET p.price_chtz='".$priceRow['price_orig']."'  WHERE p.id='".$provProd['product_id']."' ;";
						$command=$connection->createCommand($sql);
						$updatedProductsNum=$command->execute();
					}


					//проверим так же не поменялся ли вес товара, особенно актуально когда в БД вес не задан, а в прайсе появился
					$sql = "SELECT products.weight FROM products
							WHERE products.id=".$provProd['product_id'].";";
					$command=$connection->createCommand($sql);
					$productWeight = $command->query();

					if ($priceRow['weight']!=null && $productWeight!=$priceRow['weight'])
					{
						$sql = "update products p SET p.weight='".$priceRow['weight']."' WHERE p.id=".$provProd['product_id'].";";
						$command=$connection->createCommand($sql);
						$updatedByWeightNum=$command->execute();
					}

					$priceRow['product'] = array('id'=>$provProd['product_id']);
					//если небыло обновлений, заносим строку в $result['notNeedUpdateRows']
					if ($updatedProvProdNum==0 && $updatedByWeightNum==0) 
					{
						$row = array('priceRow'=>$priceRow);
						array_push($result['notNeedUpdateRows'], $row);					
					}
					//если были обновления, заносим строку в $result['updatedRows']
					else 
					{
						$row = array('priceRow'=>$priceRow);
						array_push($result['updatedRows'], $row);	
					}

					
				}
			}
			else
			{
					//если по полному совпадению имени и чод для выбранного провайдера (input_requests.provider_id) не нашлось,
					//ищем только по ЧОД  по всем input_requests, далее
					//1)если не нашлось ни одного  input_request то заносим стороку из прайса в "новое в прайсе"
					//2)Есди нашелось несколько проверим к одному ли товару они относятся
				    //2.1)относятся к одному - добавляем строку в productsWhichNeedProvider, это означает что товар с таким же ЧОД уже есть в БД и текщую строку из прайса можно
				    //    привязать как синоним. 
					//2.2)относится к нескольким - добавляем строку в dublikatChodProducts, это ощначает что с таким же ЧОД в БД находятся несколько товаров
					//	  и можно привязать текущую строку из прайса к одному или несколькоим из них, выбрав в ручную
					//productsWhichNeedProvider отличается от dublikatChodProducts, тем что строки из productsWhichNeedProvider можно назанчить как синонимы массово 
					//т.к. в БД есть лишь единственный товар с таким ЧОД. строки из dublikatChodProducts же требуют ручного сопаставления каждой строки с товарами из БД (т.к. соответствует больше одного товара)
					$sql = "SELECT prov_prods.id AS id,prov_prods.product_id AS product_id ,products.original_name AS original_name FROM prov_prods
							LEFT JOIN products ON products.id=prov_prods.product_id
							WHERE prov_prods.id IN (
								SELECT input_requests.provprod_id FROM input_requests WHERE input_requests.chod='".$cleanChod."'
							);";
					
					$connection=Yii::app()->db;
					$command=$connection->createCommand($sql);	
					$ppForOtherProviderRows=$command->queryAll();
					$ppForOtherCount = count($ppForOtherProviderRows);


					if ($ppForOtherCount==0)
					{
						$newRow = array('priceRow'=>$priceRow);

						$povtor = false;
						foreach ($result['newProducts'] as $row)
						{
							$rowCleanChod = preg_replace('/[^a-zа-яё\d]/ui','',$row['priceRow']['chod']); 
							$rowName = $row['priceRow']['name'];
							if (strcmp($cleanChod, $rowCleanChod) == 0 && strcmp($rowName, $priceRow['name'])==0)
							{
								$povtor = true;
								break;
							}
						}
						if ($povtor)
						{
							$result['messages'] = $result['messages'].'В файле прайса дублируется позиция: '.$priceRow['chod'].' '.$priceRow['name'].PHP_EOL;
						}

						array_push($result['newProducts'], $newRow);
						$result['notFoundCount']++;
					}
					else
					{
						//проверим к одному ли товару относятся найденный prov_prods
						//если к одному то добавляем в productsWhichNeedProvider
						//если к нескольким, то в dublikatChodProducts
						$product_id = $ppForOtherProviderRows[0]['product_id'];
						$original_name = $ppForOtherProviderRows[0]['original_name'];
						$oneProductWasFound = true;
						for ($i=1;$i<$ppForOtherCount;$i++)
						{
							if ($ppForOtherProviderRows[$i]['product_id'] != $product_id)
							{
								$oneProductWasFound = false;
								break;
							}
						}


						if ($oneProductWasFound )
						{
							$productWhichNeedProvider = array("priceRow"=>$priceRow,
														      "product_id"=>$product_id,
														      "product_originalName"=>$original_name,
							);

							$povtor = false;
							foreach ($result['productsWhichNeedProvider'] as $row)
							{
								$rowCleanChod = preg_replace('/[^a-zа-яё\d]/ui','',$row['priceRow']['chod']); 
								$rowName = $row['priceRow']['name'];
								if (strcmp($cleanChod, $rowCleanChod) == 0 && strcmp($rowName, $priceRow['name'])==0)
								{
									$povtor = true;
									break;
								}
							}
							if ($povtor) 
							{
								$result['messages'] = $result['messages'].'В файле прайса дублируется позиция: '.$priceRow['chod'].' '.$priceRow['name'].PHP_EOL;
							}

							array_push($result['productsWhichNeedProvider'], $productWhichNeedProvider);
						}

						else 
						{							
							$newRow = array('priceRow'=>$priceRow);
							array_push($result['dublikatChodProducts'], $newRow);
						}
					}
					
			}
	}


	

	//очистить sphinx индекс
	public function clearRealTimenputRequestsIndex() 
	{
		$sphinxSql = "TRUNCATE RTINDEX inputRequests";
		$res=Yii::app()->sphinx->createCommand($sphinxSql)->execute();

		var_dump($res);
	}

	// создать поисковый индекс для sphinx
	public function createRealTimeInputRequestsIndex()
	{
		$getInputsSql = "SELECT id,name,chod FROM input_requests";
		$inputsDataReader = Yii::app()->db->createCommand($getInputsSql)->query();
		$k=0;
		foreach ($inputsDataReader as $inputRequest)
		{
			//заменить одинарыне кафычки на двойные т.к. в поисковом запросе строки обернуты в одинарные
			$name = str_replace("'",'"',$inputRequest['name']);
			$sphinxSql = "INSERT INTO inputRequests VALUES (".$inputRequest['id'].", '".$name."', '".$inputRequest['chod']."')";
			try {
				$res=Yii::app()->sphinx->createCommand($sphinxSql)->execute();
			}
			catch(Exception $e) {
				echo "Exception: ".$e->getMessage()." ON:<br>id=".$inputRequest['id']."<br>name=".$inputRequest['name']."<br>chod=".$inputRequest['chod'];
			}
			
			
			$k++;
			
		}
		echo "$k - complite";
		return;
	}

	//для отладки
	public function deleteNotOriginalChods()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('original_item',0);
		$res =  InputRequests::model()->deleteAll($criteria);
		echo $res;
	}

}