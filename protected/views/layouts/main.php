<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
	<link href="/css/bootstrap/bootstrap-theme.css" rel="stylesheet">
	<link href="/css/bootstrap/non-responsive.css" rel="stylesheet">
	<link href="/css/main.css" rel="stylesheet">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<a name="top"></a>
<div class="container" id="page">
	<nav class="navbar navbar-custom">
	<div class="container-fluid">
	<div class="collapse navbar-collapse">
		<?php $url=Yii::app()->createUrl("page/allrequisitions"); ?>
		<a class="navbar-brand" href="<?=$url?>">ВИГМА</a>
		<ul class="nav navbar-nav">
			<?php $url=Yii::app()->createUrl("page/requisition"); ?>
			<li><a href="<?=$url?>">новая заявка</a></li>
			<?php $url=Yii::app()->createUrl("page/allrequisitions"); ?>
			<li><a href="<?=$url?>">заявки</a></li>
			<?php $url=Yii::app()->createUrl("page/allproducts"); ?>
			<li><a href="<?=$url?>">товары</a></li>
			<?php $url=Yii::app()->createUrl("page/allproviders"); ?>
			<li><a href="<?=$url?>">поставщики</a></li>
			<?php $url=Yii::app()->createUrl("page/allcustomers"); ?>
			<li><a href="<?=$url?>">контрагенты</a></li>
			<?php $url=Yii::app()->createUrl("page/stocks"); ?>
			<li><a href="<?=$url?>">склады</a></li>
			<?php $url=Yii::app()->createUrl("page/catalog"); ?>
			<li><a href="<?=$url?>">каталог</a></li>
		</ul>
		<?php $url=Yii::app()->createUrl("login/logout"); ?>
		<a href="<?=$url?>" class="navbar-nav_logout">выйти</a>
	</div>
	</div>
	</nav>
	<div id="alert_placeholder"> 
		<!-- тут появляются уведомления -->	
	</div>

	<?php echo $content; ?>

	<a href="#top" class="to-top-page">
		<img class="to-top-page_img" src="/images/totop.png">
	</a>
	

</div><!-- page -->
</body>

<script type="text/javascript">

$(window).scroll(function() {
    if ($(this).scrollTop()>700) {
        $('.to-top-page').addClass('to-top-page__visible');
    } else {
        $('.to-top-page').removeClass('to-top-page__visible');
    }
});


</script>
</html>
