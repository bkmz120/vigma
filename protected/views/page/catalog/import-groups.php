<?php $this->renderPartial("catalog/common/menu",array('tab'=>$tab)); ?>
<div ng-app="groups">
	<div ng-controller="importGroupsController" ng-cloak>
		<p class="groups-title">Импорт групп:</p>
		<div class="import-line">
			<div class="import-line_file-name" ng-bind="importGroups.file.name" ng-show="importGroups.file.name!=null"></div>
			<button class="btn btn-default import-line_loadfile-btn" ngf-select="importGroups.file.upload($file)">Выбрать файл</button>
			<img src="images/loading.gif" class="import-line_loading" ng-show="importGroups.file.loading">
			<button class="btn btn-success import-line_loadfile-btn" ng-disabled="!importGroups.file.loaded" ng-click="importGroups.startImport()">Начать импорт</button>
		</div>

		<div class="result-types_wrap" ng-show="importGroups.result!=null">
			<table class="table table-striped result-types_table">
				<thead>
					<tr class="result-types_head-tr">
						<th class="result-types_td-name result-types_td-name__head">Результаты импорта</th>
						<th class="result-types_td-count">Кол-во</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr class="result-types_body-tr">
						<td class="result-types_td-name">
							<b>Добавлена связь с группой</b> и это <b>первая</b> связь с группой для данного товара
						</td>
						<td class="result-types_td-count">
							{{importGroups.result.addedAndItFirstGroupRelation.length}}
						</td>
						<td class="result-types_td-show">
							<span class="result-types_show" ng-click="addedAndItFirstGroupRel.show()">просмотр</span>
						</td>				
					</tr>
					<tr class="result-types_body-tr">
						<td class="result-types_td-name">
							<b>Добавлена связь с группой</b> и у этого товара <b>уже были</b> связи с группой(группами)
						</td>
						<td class="result-types_td-count">
							{{importGroups.result.addedAndItNotFirstGroupRelation.length}}
						</td>
						<td class="result-types_td-show">
							<span class="result-types_show" ng-click="addedAndItNotFirstGroupRel.show()">просмотр</span>
						</td>				
					</tr>
					<tr class="result-types_body-tr">
						<td class="result-types_td-name">
							<b>Уже есть связь с группой</b> указанной в файле
						</td>
						<td class="result-types_td-count">
							{{importGroups.result.alreadyHaveRelationWithThisGroup.length}}
						</td>
						<td class="result-types_td-show">
							<span class="result-types_show" ng-click="alreadyHaveRel.show()">просмотр</span>
						</td>				
					</tr>
					<tr class="result-types_body-tr">
						<td class="result-types_td-name">
							<b>Товар не найден</b>
						</td>
						<td class="result-types_td-count">
							{{importGroups.result.notFound.length}}
						</td>
						<td class="result-types_td-show">
							<span class="result-types_show" ng-click="notFoundProds.show()">просмотр</span>
						</td>				
					</tr>			
				</tbody>
			</table>
		</div>

		<!-- ПЕРАВАЯ СВЯЗЬ С ГРУППОЙ -->

		<div class="reslut-box" ng-show="addedAndItFirstGroupRel.visible">
			<div class="reslut-box_bg"></div>
			<div class="reslut-box_head">
				<div class="result-box_close" ng-click="addedAndItFirstGroupRel.close()">закрыть</div>
				<div class="result-box_title"><b>Добавлена связь с группой</b> и это <b>первая</b> связь с группой для данного товара ({{importGroups.result.addedAndItFirstGroupRelation.length}})</div>
			</div>
			<div class="reslut-box_content">		
				<table class="table table-striped result-box-table">
						<thead>
							<th width="10" >№</th>
							<th width="400" >ЧОД и Название из файла импорта</th>
							<th width="400" >ЧОД и Название товара в БД</th>
							<th width="120" >Добавлена группа</th>
							
						</thead>
						<tbody>
							<tr ng-repeat="item in importGroups.result.addedAndItFirstGroupRelation">
								<td width="10">{{$index+1}}</td>
								<td width="400">{{item.fileRow.chod}} {{item.fileRow.name}}</td>
								<td width="400">{{item.product.chod_display}} {{item.product.original_name}}</td>
								<td width="120">{{item.fileRow.groupNum}}</td>
								<td ><img class="reslut-box_showProductInfo" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(item.product)"></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<!-- НЕ ПЕРАВАЯ СВЯЗЬ С ГРУППОЙ -->

		<div class="reslut-box" ng-show="addedAndItNotFirstGroupRel.visible">
			<div class="reslut-box_bg"></div>
			<div class="reslut-box_head">
				<div class="result-box_close" ng-click="addedAndItNotFirstGroupRel.close()">закрыть</div>
				<div class="result-box_title"><b>Добавлена связь с группой</b> и у этого товара <b>уже были</b> связи с группой(группами) ({{importGroups.result.addedAndItNotFirstGroupRelation.length}})</div>
			</div>
			<div class="reslut-box_content">		
				<table class="table table-striped result-box-table">
						<thead>
							<th width="10" >№</th>
							<th width="400" >ЧОД и Название из файла импорта</th>
							<th width="400" >ЧОД и Название товара в БД</th>
							<th width="120" >Добавлена группа</th>
							<th width="120" >Уже имеющиеся группы</th>
						</thead>
						<tbody>
							<tr ng-repeat="item in importGroups.result.addedAndItNotFirstGroupRelation">
								<td width="10">{{$index+1}}</td>
								<td width="400">{{item.fileRow.chod}} {{item.fileRow.name}}</td>
								<td width="400">{{item.product.chod_display}} {{item.product.original_name}}</td>
								<td width="120">{{item.fileRow.groupNum}}</td>
								<td width="120">{{item.otherGroups}}</td>
								<td ><img class="reslut-box_showProductInfo" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(item.product)"></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<!-- УЖЕ ЕСТЬ СВЯЗЬ С ЭТОЙ ГРУППОЙ -->

		<div class="reslut-box" ng-show="alreadyHaveRel.visible">
			<div class="reslut-box_bg"></div>
			<div class="reslut-box_head">
				<div class="result-box_close" ng-click="alreadyHaveRel.close()">закрыть</div>
				<div class="result-box_title"><b>Уже есть связь с группой</b> указанной в файле ({{importGroups.result.alreadyHaveRelationWithThisGroup.length}})</div>
			</div>
			<div class="reslut-box_content">		
				<table class="table table-striped result-box-table">
						<thead>
							<th width="10" >№</th>
							<th width="400" >ЧОД и Название из файла импорта</th>
							<th width="400" >ЧОД и Название товара в БД</th>
							<th width="120" >Номер группы из файла импорта</th>
							<th width="120" >Уже имеющиеся группы</th>
						</thead>
						<tbody>
							<tr ng-repeat="item in importGroups.result.alreadyHaveRelationWithThisGroup">
								<td width="10">{{$index+1}}</td>
								<td width="400">{{item.fileRow.chod}} {{item.fileRow.name}}</td>
								<td width="400">{{item.product.chod_display}} {{item.product.original_name}}</td>
								<td width="120">{{item.fileRow.groupNum}}</td>
								<td width="120">{{item.otherGroups}}</td>
								<td ><img class="reslut-box_showProductInfo" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(item.product)"></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<!-- НЕ НАЙДЕНЫ -->

		<div class="reslut-box" ng-show="notFoundProds.visible">
			<div class="reslut-box_bg"></div>
			<div class="reslut-box_head">
				<div class="result-box_close" ng-click="notFoundProds.close()">закрыть</div>
				<div class="result-box_title">Товары, не найденные в базе данных ({{importGroups.result.notFound.length}})</div>
			</div>
			<div class="reslut-box_content">		
				<table class="table table-striped result-box-table">
						<thead>
							<th width="10" >№</th>
							<th width="300" >ЧОД из файла импорт</th>
							<th width="300" >Наименов. из файла импорт</th>
							<th width="300" >номер группы из файла импорта</th>
							
						</thead>
						<tbody>
							<tr ng-repeat="item in importGroups.result.notFound">
								<td width="10">{{$index+1}}</td>
								<td width="300">{{item.fileRow.chod}}</td>
								<td width="300">{{item.fileRow.name}}</td>
								<td width="300">{{item.fileRow.groupNum}}</td>
								<td width="120" >
									<img class="reslut-box_add-label-img"
									     src="images/success.png"
									     ng-show="item.added"
									>
									<span class="reslut-box_findProd reslut-box_findProd__added"
										  ng-click="liveSearch.showLiveSearch(item)"
										  ng-show="item.added"								  
									>добавить ещё</span> 
									<span class="reslut-box_findProd"
										  ng-click="liveSearch.showLiveSearch(item)"
										  ng-show="!item.added"								  
									>выбрать товар</span> 							
								</td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<!-- Карточка товара -->
		<product-info-window windowshow="productInfoWindow.show"  product="productInfoWindow.product" providers="productInfoWindow.providers"></product-info-window>

		<div id="liveSearch" class="liveSearch" ng-show="liveSearch.visible">
			<img class="liveSearch_close" src="images/close.png" ng-click="liveSearch.closeLiveSearch()">

			<div></div>
			<div class="row allproducts-search" >
				<div class="col-xs-3">
					<label class="liveSearch_searchLabel">Наименование:</label>
					<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.name" />
					<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('name')">			
				</div>
				<div class="col-xs-3">
					<label class="liveSearch_searchLabel liveSearch_searchLabel__chod">ЧОД:</label>
					<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('chod')">	
					<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.chod" />
					
				</div>

				<div class="col-xs-6 liveSearch_providerInfo">
					<div>
						Название из файла:<span class="liveSearch_fileChodName">{{liveSearch.tableRow.fileRow.name}} {{liveSearch.tableRow.fileRow.chod}}</span>
						
					</div>
					
				</div>
			</div>

			<div  tasty-table  bind-resource-callback="liveSearch.getResource"  bind-init="liveSearch.initTasty" bind-filters="liveSearch.filter" >
					<table class="table table-striped table-condensed allproducts-table">
						<thead tasty-thead></thead>
						<tbody>
							<tr ng-repeat="row in rows" >
								<td >
									{{ row.product.original_name }}
									<coincident-input-requsets items="row.product.inputRequests" field="name" value="filter.name" />
								</td>
								<td >
									{{ row.product.chod_display }}
									<coincident-input-requsets items="row.product.inputRequests" field="chod" value="filter.chod" />
								</td>
								<td >
									{{ row.product.product_num }}
								</td>
								<td ><img class="liveSearch_showPIW" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>
								<td><span class="liveSearch_selectProduct" ng-click="liveSearch.selectProduct(row.product)">выбрать</span></td>
							</tr>
						</tbody>
					</table>
					<div tasty-pagination bind-items-per-page="liveSearch.itemsPerPage" bind-list-items-per-page="liveSearch.listItemsPerPage"></div>
			</div>
		</div>
		<div class="liveSearch-bg" ng-show="liveSearch.visible" ng-click="liveSearch.closeLiveSearch()"></div>
		<waitBox></waitBox>	
	</div> <!-- ng-controller -->
</div>

<script type="text/javascript">
	var providers = <?php echo CJavaScript::encode($providers)?>;
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var groups = <?php echo CJavaScript::encode($groups)?>;
</script>