<?php $this->renderPartial("catalog/common/menu",array('tab'=>$tab)); ?>

<div ng-app="groups">
	<div ng-controller="catalogcatsController" ng-init="init()" ng-cloak>
		<p class="groups-title">Категории</p>
		<div class="cats-table">
			<div class="cats-table_head">			
			</div>
			<div class="cats-table_body">
				<div class="cats-table_row" ng-repeat="catalogcat in catalogcats.items">
					<div class="cat-table_catagoryName">
						<div class="cat-table_catagoryName-val">{{catalogcat.name}}</div>
						<div class="cat-table_catagoryName-products" ng-click="catalogcats.showProducts(catalogcat)">показать товары</div>
					</div>
					<div class="cat-table_groups">
						<div class="cat-table_gr-list">
							<div class="cat-table_gr-item"
								 ng-repeat="group in catalogcat.groups  | orderBy:'group_num'"
							>
								<div class="cat-tablr_gr-num">{{group.group_num}}</div>
								<img class="cat-tablr_gr-remove"
									 src="/images/delete.png"
									 ng-click="catalogcats.removeGroupFromCatalogcat(catalogcat,group)"
								>
							</div>					
						</div>
						<div class="cat-table_gr-add">
							<select class="form-control cat-table_gr-add-list" 
									ng-model="catalogcat.selectedGroup"
									ng-options="group.name for group in groups.items"
									ng-init="catalogcat.selectedGroup=groups.items[0]"
							>
							</select>

							<div class="btn btn-info cat-table_gr-add-btn" ng-click="catalogcats.addGroupToCatalogcat(catalogcat,catalogcat.selectedGroup)">Присвоить</div>
						</div>
					</div>
				</div>

				
			</div>
		</div>

		<!-- ТОВАРЫ В ДАННОЙ КАТЕГОРИИ -->

		<div class="reslut-box" ng-show="productsByCat.visible">
			<div class="reslut-box_bg"></div>
			<div class="reslut-box_head">
				<div class="result-box_close" ng-click="productsByCat.close()">закрыть</div>
				<div class="result-box_title">{{productsByCat.catalogcatName}} ({{productsByCat.products.length}})</div>
			</div>
			<div class="reslut-box_content">		
				<table class="table table-striped result-box-table">
						<thead>
							<th width="10" >№</th>
							<th width="400" >Название</th>
							<th width="400" >ЧОД</th>
							<th width="200" >Группы</th>
						</thead>
						<tbody>
							<tr ng-repeat="item in productsByCat.products">
								<td width="10">{{$index+1}}</td>
								<td width="400">{{item.original_name}}</td>
								<td width="400">{{item.chod_display}}</td>
								<td width="200">{{item.groupNumStr}}</td>
								<td ><img class="reslut-box_showProductInfo" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(item)"></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>

		<!-- Карточка товара -->
		<product-info-window windowshow="productInfoWindow.show"  product="productInfoWindow.product" providers="productInfoWindow.providers"></product-info-window>
	</div> <!-- ng-controller --> 
</div> <!-- ng-app -->

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>

<script type="text/javascript">
	var providers = <?php echo CJavaScript::encode($providers)?>;
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var groups = <?php echo CJavaScript::encode($groups)?>;
</script>