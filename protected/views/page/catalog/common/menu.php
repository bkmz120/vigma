<ul class="catalog-menu">
	<li class="catalog-menu_item">
		<?php $url=Yii::app()->createUrl("page/catalog",array('tab'=>'group-products')); ?>
		<?php if ($tab=='group-products' ) $activeClass="catalog-menu_item-link__active"; else $activeClass=""; ?>
		<a href="<?php echo $url ?>" class="catalog-menu_item-link <?php echo $activeClass; ?>">Товары и связи с группами</a>
	</li>
	<li class="catalog-menu_item">
		<?php $url=Yii::app()->createUrl("page/catalog",array('tab'=>'import-groups')); ?>
		<?php if ($tab=='import-groups' ) $activeClass="catalog-menu_item-link__active"; else $activeClass=""; ?>
		<a href="<?php echo $url ?>" class="catalog-menu_item-link <?php echo $activeClass; ?>">Импорт групп</a>
	</li>
	<li class="catalog-menu_item">
		<?php $url=Yii::app()->createUrl("page/catalog",array('tab'=>'categories')); ?>
		<?php if ($tab=='categories' ) $activeClass="catalog-menu_item-link__active"; else $activeClass=""; ?>
		<a href="<?php echo $url ?>" class="catalog-menu_item-link <?php echo $activeClass; ?>">Категории каталога</a>
	</li>
	<li class="catalog-menu_item">
		<?php $url=Yii::app()->createUrl("page/catalog",array('tab'=>'export')); ?>
		<?php if ($tab=='export' ) $activeClass="catalog-menu_item-link__active"; else $activeClass=""; ?>
		<a href="<?php echo $url ?>" class="catalog-menu_item-link <?php echo $activeClass; ?>">Экпорт в каталог</a>
	</li>
</ul>