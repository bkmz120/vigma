<div ng-app="allProducts">
<div ng-controller="searchController" >
<h1 class="allproducts_title">Все товары</h1>


<div class="showNewProduct" ng-click="newProduct.show()"><img class="showNewProduct_img" src="/images/plus.png"> Добавить товар</div>
<div class="newProduct_wrap" ng-show="newProduct.visible">
	<div class="newProduct_line">
		<div class="newProduct_label">Ориг. назвние *:</div>
		<input type="text" class="form-control newProduct_value newProduct_value__big" ng-class="{'newProduct_value__error':!newProduct.original_name_valid}" ng-model="newProduct.original_name">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Ориг. ЧОД *:</div>
		<input type="text" class="form-control newProduct_value newProduct_value__big" ng-class="{'newProduct_value__error':!newProduct.original_chod_valid}" ng-model="newProduct.original_chod">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Код товара:</div>
		<input type="text" class="form-control newProduct_value" ng-model="newProduct.product_num">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Прайс ЧТЗ:</div>
		<input type="text" class="form-control newProduct_value" ng-model="newProduct.price_chtz">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Вес:</div>
		<input type="text" class="form-control newProduct_value" ng-model="newProduct.weight">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Длина:</div>
		<input type="text" class="form-control newProduct_value" ng-model="newProduct.sizeL">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Ширина:</div>
		<input type="text" class="form-control newProduct_value" ng-model="newProduct.sizeW">
	</div>	
	<div class="newProduct_line">
		<div class="newProduct_label">Выстоа:</div>
		<input type="text" class="form-control newProduct_value"  ng-model="newProduct.sizeH">
	</div>	

	<div class="newProduct_line">
		<div class="newProduct_label">Поставщик:</div>
		<select class="form-control newProduct_allproviders" ng-model="newProduct.provider_id">
		    <option ng-repeat="provider in providers" ng-selected="provider.id==1"   value="{{provider.id}}">
		    	{{provider.name}}
			</option>
		</select>
		<input type="text" class="form-control newProduct_provider-price" ng-class="{'newProduct_value__error':!newProduct.price_in_valid}" ng-model="newProduct.price_in" placeholder="Цена">
		<input type="text" class="form-control newProduct_provider-deltime" ng-model="newProduct.delivery_time" placeholder="Срок доставки">
	</div>

	<button class="btn btn-success newProduct_addBtn" ng-click="newProduct.addProduct()">Добавить</button>
</div>

<div class="row allproducts-search" >
	<div class="col-xs-3">
			<label class="allproducts_searchLabel">Наименование:</label>
			<input type="text" class="allproducts_searchInp" ng-model="filter.name" />
			<img class="allproducts_searchClear" src="/images/close-gray.png" ng-click="clearFilter('name')">			
		</div>
		<div class="col-xs-3">
			<label class="allproducts_searchLabel">ЧОД:</label>
			<input type="text" class="allproducts_searchInp" ng-model="filter.chod" />
			<img class="allproducts_searchClear" src="/images/close-gray.png" ng-click="clearFilter('chod')">	
		</div>
		

</div>

<div >
	<div  tasty-table
		  bind-resource-callback="getResource" 
		  bind-init="initTasty" 
		  bind-filters="filter"
		  bind-reload="reloadCallback"
	>
		<table class="table table-striped table-condensed allproducts-table">
			<thead tasty-thead></thead>
			<tbody>
				<tr ng-repeat="row in rows" ng-class="{'allproducts-table_hidden-row':row.product.hidden==1,'allproducts-table_active-row':row.product.active!=null && row.product.active==true}" ng-click="setProductRowActive(row.product)">
					<td >
						{{ row.product.original_name }}
						<coincident-input-requsets items="row.product.inputRequests" field="name" value="filter.name" />
					</td>
					<td >
						{{ row.product.chod_display }}
						<coincident-input-requsets items="row.product.inputRequests" field="chod" value="filter.chod" />
					</td>
					<td >
						{{ row.product.product_num }}
					</td>
					
					<td><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>
					<td><img class="hideProduct" src="/images/hidden3.png" ng-click="hideProduct(row.product)"></td>
					<td><img class="deleteProduct" src="/images/delete.png" ng-click="deleteProduct(row.product.id)"> </td>
				</tr>
			</tbody>
		</table>
		<div tasty-pagination bind-items-per-page="itemsPerPage" bind-list-items-per-page="listItemsPerPage"></div>
	</div>
</div>

<!-- Карточка товара -->
<product-info-window windowshow="productInfoWindow.show"  product="productInfoWindow.product" providers="providers"></product-info-window>



</div>
</div>

<script type="text/javascript">
	var providers = <?php echo CJavaScript::encode($providers)?>;
</script>
