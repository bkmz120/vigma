<div ng-app="importPrice">
<div ng-controller="importController" ng-init="init()" ng-cloak>

<p class="import-tile">Поставщики</p>
<div class="providers-table">
	<div class="providers-table_top" ng-class="{'providers-table_top__fixed':providersList.fixedTop}">
		<div class="providers-table_control">
			<input class="providers-table_filters-name" type="text" placeholder="Поиск по названию" ng-model="providersList.filter.name">
			<button class="btn btn-default providers-table_newBtn" ng-click="providerEdit.show(provider)">Добавить поставщика</button>
		</div>
	
		<div class="providers-table_head">
			<div class="providers-table_name providers-table_head-item">Название</div>
			<div class="providers-table_price-date providers-table_head-item">Дата прайса</div>
			<div class="providers-table_default-coef providers-table_head-item"></div>
		</div>
	</div>
	<div class="providers-table_body"
		 dnd-list="providers"
		 dnd-inserted="providersList.inserted(event, index, item, external, type, 'itemType')"
		 ng-class="{'providers-table_body__fixed':providersList.fixedTop}"
	>
		<div class="providers-table_row" 
			 ng-repeat="provider in providers | filter:providersList.filter"
			 ng-class="{'providers-table_row__even':$index % 2 == 0,'providers-table_row__selected':provider.selected}"
			 dnd-draggable="provider"
			 dnd-moved="providers.splice($index, 1)"
			 dnd-effect-allowed="move"
			 ng-click="providersList.select(provider)"
		>
			<dnd-nodrag class="providers-table_nodrag">
          		<div  class="providers-table_drag-handle-wrap" >
          			<div dnd-handle class="providers-table_drag-handle" ng-show="providersList.filter.name==''"></div>
          		</div>
				<div class="providers-table_name">
					<span class="providers-table_name-val" ng-click="providerEdit.show(provider)">{{provider.name}}</span> 				
				</div>
				<div class="providers-table_price-date">{{provider.price_date}}</div>
				<div class="providers-table_showPriceEdit" ng-click="priceTemplateEdit.show(provider)">шаблон прайса</div>
				<div class="providers-table_showImport" ng-show="provider.price_template!=null" ng-click="import.show(provider)">импорт</div>
				
				<div class="providers-table_default-coef"></div>
				<div class="providers-table_list-delete">
					<img class="providers-table_list-delete-img" src="/images/delete.png" ng-click="providersList.delete(provider)">
				</div>
			</dnd-nodrag>
		</div>
	</div>
</div>

<!-- Окошка с инфоррмацией о поставщике -->
<div class="provideredit-bg" ng-show="providerEdit.visible" ng-click="providerEdit.close()"></div>
<div class="provideredit" ng-show="providerEdit.visible" ng-style="providerEdit.popupStyle">
	<img class="provideredit_close" src="/images/close-thin.png" ng-click="providerEdit.close()">
	<div class="provideredit_head">
		<div class="provideredit_title">Досье поставщика</div> 
		<div class="provideredit_coefs">
			<div class="provideredit_coef">
				<div class="provideredit_coef-label">Коэф. по-умолчанию:</div>
				<input type="text" class="provideredit_coef-val" ng-model="providerEdit.tmpprovider.default_coef">
			</div>
			<div class="provideredit_coef">
				<div class="provideredit_coef-label">Минимальный коеф.:</div>
				<input type="text" class="provideredit_coef-val" ng-model="providerEdit.tmpprovider.min_coef">
			</div>
			<div class="provideredit_coef">
				<div class="provideredit_coef-label">Коэф. для каталога:</div>
				<input type="text" class="provideredit_coef-val" ng-model="providerEdit.tmpprovider.catalog_coef">
			</div>
		</div>
		
	</div>
	<div class="form-horizontal">
		<div class="form-group">
		    <label for="provider-name" class="col-xs-2 control-label">Название</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-nae" ng-model="providerEdit.tmpprovider.name" placeholder="Название">
			</div>
			<!-- <div class="col-xs-2">
				<div class="btn btn-default" ng-click="provider.clear()">Очистить</div>
			</div> -->
		</div>

		<div class="form-group">
			<label for="provider-full-name" class="col-xs-2 control-label">Полное назание</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-full-name"  ng-model="providerEdit.tmpprovider.full_name" placeholder="Полное назание">
			</div>

			
		</div>

		<div class="form-group">
			<label for="provider-contact-person" class="col-xs-2 control-label">Контактное лицо</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-contact-person" ng-model="providerEdit.tmpprovider.contact_person" placeholder="Контактное лицо">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-phone" class="col-xs-2 control-label">Телефон</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-phone" ng-model="providerEdit.tmpprovider.phone" placeholder="Телефон">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-email" class="col-xs-2 control-label">E-mail</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-email" ng-model="providerEdit.tmpprovider.email" placeholder="E-mail">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-address" class="col-xs-2 control-label">Адрес</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-address" ng-model="providerEdit.tmpprovider.address" placeholder="Адрес">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-legal-address" class="col-xs-2 control-label">Юридический адрес</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-legal-address" ng-model="providerEdit.tmpprovider.legal_address" placeholder="Юридический адрес">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-mail-address" class="col-xs-2 control-label">Почтовый адрес</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-mail-address" ng-model="providerEdit.tmpprovider.mail_address" placeholder="Почтовый адрес">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-bank-name" class="col-xs-2 control-label">Название банка</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-bank-name" ng-model="providerEdit.tmpprovider.bank_name" placeholder="Название банка">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-payment-number" class="col-xs-2 control-label">Номер счета</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-payment-number" ng-model="providerEdit.tmpprovider.payment_number" placeholder="Номер счета">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-bik" class="col-xs-2 control-label">БИК</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-bik" ng-model="providerEdit.tmpprovider.bik" placeholder="БИК">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-corr-number" class="col-xs-2 control-label">Корреспондентский счет</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-corr-number" ng-model="providerEdit.tmpprovider.corr_number" placeholder="Корреспондентский счет">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-bank-inn" class="col-xs-2 control-label">ИНН Банка</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-bank-inn" ng-model="providerEdit.tmpprovider.bank_inn" placeholder="ИНН Банка">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-director-name" class="col-xs-2 control-label">Руководитель компании</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-director-name" ng-model="providerEdit.tmpprovider.director_name" placeholder="Руководитель компании">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-director-job" class="col-xs-2 control-label">Должность руководителя</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-director-job" ng-model="providerEdit.tmpprovider.director_job" placeholder="Должность руководителя">
			</div>
		</div>

		<div class="form-group">
			<label for="provider-confirm-doc" class="col-xs-2 control-label">Подтверждающий документ</label>
			<div class="col-xs-8">
				<input type="text" class="form-control" id="provider-confirm-doc" ng-model="providerEdit.tmpprovider.confirm_doc" placeholder="На основании чего действует">
			</div>
		</div>
	</div>
	<div class="provideredit_btns">
		<button class="btn btn-success provideredit_saveBtn" ng-click="providerEdit.save()">Сохранить</button>
		<button class="btn btn-default" ng-click="providerEdit.close()">Отмена</button>
	</div>	
</div>

<div class="price-template-bg" ng-show="priceTemplateEdit.visible" ng-click="priceTemplateEdit.close()"></div>
<div class="price-template" ng-show="priceTemplateEdit.visible">
	<div class="price-template_title">Редактор шаблона прайса</div>

	<div ng-show="priceTemplateEdit.templateType=='default'">
		<div class="price-template_line">
			<div class="price-template_label">Номер листа</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.sheetNum}" ng-model="priceTemplateEdit.tmpTemplate.sheetNum">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер первой строки</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.startRowNum}" ng-model="priceTemplateEdit.tmpTemplate.startRowNum">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер кол-ки с ЧОД</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.chodClm}" ng-model="priceTemplateEdit.tmpTemplate.chodClm">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер кол-ки с Наименование</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.nameClm}" ng-model="priceTemplateEdit.tmpTemplate.nameClm">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер кол-ки с Кодом тов-ра</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.productNumClm}" ng-model="priceTemplateEdit.tmpTemplate.productNumClm">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер кол-ки с Ценой</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.priceClm}" ng-model="priceTemplateEdit.tmpTemplate.priceClm">
		</div>	
		<div class="price-template_line">
			<div class="price-template_label">Номер кол-ки с Весом</div>
			<input type="text" class="form-control price-template_value " ng-class="{'price-template_value__error':!priceTemplateEdit.templateValidation.weightClm}" ng-model="priceTemplateEdit.tmpTemplate.weightClm">
		</div>	
	</div>
	<div ng-show="priceTemplateEdit.templateType=='smirnyagin'">
		<div class="price-template_line">
			<div class="price-template_label">Количество учитываемых листов:</div>
			<input type="text" class="form-control price-template_value " ng-model="priceTemplateEdit.tmpTemplate.sheetCount">
		</div>
	</div>

	<div class="price-template_btns">
		<button class="btn btn-success price-templatet_saveBtn" ng-click="priceTemplateEdit.save()">Сохранить</button>
		<button class="btn btn-warning price-templatet_cancelBtn" ng-click="priceTemplateEdit.close()">Омена</button>
	</div>
</div>


<div class="importSettings-bg" ng-show="import.visible" ng-click="import.close()"></div>
<div class="importSettings" ng-show="import.visible">
	<div class="importSettings_labels">
		<div class="importSettings_price-coef-label">Коэф.</div>
		<div class="importSettings_import-date-label">Актуален на:</div>
		<div class="importSettings_smirnyaginSheetCount-label" ng-show="priceType==4">Количество листов:</div>
	</div>
	<div class="importSettings_values">
		<input type="text" class="form-control importSettings_price-coef" ng-model="import.priceImportCoef">
		<input id="priceActualDate"  ng-model="import.priceActualDate" type="text" class="form-control importSettings_import-date">
		<input type="text" class="form-control importSettings_smirnyaginSheetCount" ng-model="smirnyaginSheetCount" ng-show="priceType==4">
		<div class="importSettings_fileName">{{import.fileNameLabel}}</div>
		<button class="btn btn-default importSettings_loadfile-btn" ngf-select="import.uploadFile($file)">Выбрать файл</button>
		<img src="images/loading.gif" class="importSettings_loading" ng-show="import.fileUploading">
	</div>
	<div class="importSettings_help">
		Коэф. нужен для расчета вхожящей цены: Цена вх = Цена пр. * Коэф.
	</div>

	<div class="importSettings_comment">
		<textarea class="importSettings_comment-text" ng-model="import.provider.import_comment" placeholder="Комментарий к импорту"></textarea>
	</div>
	<div class="importSettings_btns">
		<button class="btn btn-success importSettings_startBtn" ng-click="import.startImport()" ng-disabled="import.fileName==null">Начать импорт</button>
		<button class="btn btn-warning importSettings_cancelBtn" ng-click="import.close()">Омена</button>
	</div>

	<div class="importSettings_process" ng-show="import.importProcessing">
		Импорт... 
		<img src="images/loading.gif" class="importSettings_process-gif">
	</div>

	<div class="importSettings_finish" ng-show="import.importFinish">
		Готово!<br>
		<button class="btn btn-success importSettings_finis-btn" ng-click="import.close()">Закрыть</button>
	</div>
</div>

<div class="result-types_wrap" ng-show="result!=null">
	<table class="table table-striped result-types_table">
		<thead>
			<tr class="result-types_head-tr">
				<th class="result-types_td-name result-types_td-name__head">Результаты импорта</th>
				<th class="result-types_td-count">Кол-во</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Обновлено цен</b>
				</td>
				<td class="result-types_td-count">
					{{result.updatedRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="updatedRows.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Цена не поменялась</b>
				</td>
				<td class="result-types_td-count">
					{{result.notNeedUpdateRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="notNeedUpdateRows.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Добавить поставщика к товарам</b><br>
					в базе данных есть <b>1 совпадение</b> с ЧОД поставщика (можно создать связи автоматически)
				</td>
				<td class="result-types_td-count">
					{{result.productsWhichNeedProvider.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="productsWhichNeedProvider.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Добавить поставщика к товарам</b><br>
					в базе данных есть <b>несколько совпадений</b> с ЧОД поставщика 
				</td>
				<td class="result-types_td-count">
					{{result.dublikatChodProducts.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="dublikatChodProducts.show()">просмотр</span>
				</td>				
			</tr>	
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Новые позиции в прайсе поставщика</b><br>
					относительно прошлого прайса, которых нет в базе данных. Нужно создать новую позицию либо добавить аналог (нового поставщика) к позиции (нужно будет выбрать) базы данных
				</td>
				<td class="result-types_td-count">
					{{result.newProducts.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="newProducts.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Не найдено в прайсе,но есть в базе для выбранного поставщика</b><br>
				</td>
				<td class="result-types_td-count">
					{{result.notInPriceFileProducts.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="notInPriceFileProducts.show()">просмотр</span>
				</td>				
			</tr>					
		</tbody>
	</table>
</div>

<div class="reslut-box" ng-show="updatedRows.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="updatedRows.close()">закрыть</div>
		<div class="result-box_title">Обновлено цен ({{result.updatedRows.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70" >№</th>
				<th width="400" >Название</th>
				<th width="400" >ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="updatedRow in result.updatedRows">
					<td width="70">{{$index+1}}</td>
					<td width="400">{{updatedRow.priceRow.name}}</td>
					<td width="400">{{updatedRow.priceRow.chod}}</td>
					<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="liveSearch.productInfoWindow.showProductsInfoWin(updatedRow.priceRow.product)"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="notNeedUpdateRows.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="notNeedUpdateRows.close()">закрыть</div>
		<div class="result-box_title">Цена не поменялась ({{result.notNeedUpdateRows.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70" >№</th>
				<th width="400" >Название</th>
				<th width="400" >ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="notNeedUpdateRow in result.notNeedUpdateRows">
					<td width="70">{{$index+1}}</td>
					<td width="400">{{notNeedUpdateRow.priceRow.name}}</td>
					<td width="400">{{notNeedUpdateRow.priceRow.chod}}</td>
					<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="liveSearch.productInfoWindow.showProductsInfoWin(notNeedUpdateRow.priceRow.product)"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="productsWhichNeedProvider.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="productsWhichNeedProvider.close()">закрыть</div>
		<div class="result-box_title">
			<b>Добавить поставщика к товарам </b>
			в базе данных есть <b>1 совпадение</b> с ЧОД поставщика (можно создать связи автоматически)
			({{result.productsWhichNeedProvider.length}})
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70" >№</th>
				<th width="250" >Назв. из прайса</th>
				<th width="250" >Ориг. назв</th>
				<th width="250" >Ориг. ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="productWhichNeedProvider in result.productsWhichNeedProvider">
					<td width="10">{{$index+1}}</td>
					<td width="250">{{productWhichNeedProvider.priceRow.name}}</td>
					<td width="250">{{productWhichNeedProvider.product_originalName}}</td>
					<td width="250">{{productWhichNeedProvider.priceRow.chod}}</td>
					<td>
						<input type="checkbox" ng-model="productWhichNeedProvider.needAdd" ng-show="!productWhichNeedProvider.added">
						<span class="reslut-box_add-label"  ng-show="productWhichNeedProvider.added"><img class="reslut-box_add-label-img" src="images/success.png"></span>
					</td>
					<td>
						
						<span class="reslut-box_selectProduct"  ng-click="liveSearch.showLiveSearch( productWhichNeedProvider.product_originalName,
																																				productWhichNeedProvider.priceRow.chod,
																																				productWhichNeedProvider.priceRow,
																																				productWhichNeedProvider)">
							Выбрать товар
						</span> 
					</td>	
				</tr>
			</tbody>
		</table>
	</div>
	<div class="reslut-box_bottom" ng-show="result.productsWhichNeedProvider.length>0">
		<button class="btn btn-default" ng-click="productsWhichNeedProvider.addProviderToSelected.start()">Связать с выбранными товарами</button>
		<span class="reslut-box_bottom-ch-all" ng-click="productsWhichNeedProvider.selectAll()">отметить все </span>
		<span class="reslut-box_bottom-unch-all" ng-click="productsWhichNeedProvider.unSelectAll()">снять все</span>
	</div>
</div>



<div class="reslut-box" ng-show="dublikatChodProducts.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="dublikatChodProducts.close()">закрыть</div>
		<div class="result-box_title">
			<b>Добавить поставщика к товарам </b>
			в базе данных есть <b>несколько совпадений</b> с ЧОД поставщика
			({{result.dublikatChodProducts.length}})
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70">№</th>
				<th width="400">Назв.</th>
				<th width="400">ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="dublikatChodProduct in result.dublikatChodProducts">
					<td width="70">{{$index+1}}</td>
					<td width="400">{{dublikatChodProduct.priceRow.name}}</td>
					<td width="400">{{dublikatChodProduct.priceRow.chod}}</td>
					<td>
						<span class="reslut-box_add-label"  ng-show="dublikatChodProduct.added"><img class="reslut-box_add-label-img" src="images/success.png"></span>
					</td>
					<td>
						<span class="reslut-box_selectProduct"  ng-click="liveSearch.showLiveSearch('',
																									  dublikatChodProduct.priceRow.chod,
																									  dublikatChodProduct.priceRow,
																									  dublikatChodProduct)">
							Выбрать товар
						</span>
					</td>	
				</tr>
			</tbody>
		</table>
	</div>	
</div>

<div class="reslut-box" ng-show="newProducts.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="newProducts.close()">закрыть</div>
		<div class="result-box_title">
			Новые позиции в прайсе поставщика 
			({{result.newProducts.length}})
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70">№</th>
				<th width="400">Назв.</th>
				<th width="400">ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="newProduct in result.newProducts">
					<td width="70">{{$index+1}}</td>
					<td width="400">{{newProduct.priceRow.name}}</td>
					<td width="400">{{newProduct.priceRow.chod}}</td>
					
					<td>
						<input type="checkbox" ng-model="newProduct.needAdd" ng-show="!newProduct.added">
						<span class="reslut-box_add-label" ng-show="newProduct.added"><img class="reslut-box_add-label-img" src="images/success.png"></span>
					</td>
					<td>
						<span class="reslut-box_selectProduct"  ng-click="liveSearch.showLiveSearch(newProduct.priceRow.name,
																																 newProduct.priceRow.chod,
																																 newProduct.priceRow,
																																 newProduct)">
							Выбрать товар
						</span>
					</td>	
				</tr>
			</tbody>
		</table>
	</div>
	<div class="reslut-box_bottom" ng-show="result.newProducts.length>0">
		<button class="btn btn-default" ng-click="newProducts.createNewProductsBySelected.start()">Создать новые товары, для отмеченных позиций</button>
		<span class="reslut-box_bottom-ch-all" ng-click="newProducts.selectAll()">отметить все </span>
		<span class="reslut-box_bottom-unch-all" ng-click="newProducts.unSelectAll()">снять все</span>
	</div>	
</div>

<div class="reslut-box" ng-show="notInPriceFileProducts.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="notInPriceFileProducts.close()">закрыть</div>
		<div class="result-box_title">Не найдено в прайсе,но есть в базе для выбранного поставщика ({{result.notInPriceFileProducts.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="70" >№</th>
				<th width="400" >Название</th>
				<th width="400" >ЧОД</th>
			</thead>
			<tbody>
				<tr ng-repeat="notInPriceFileProduct in result.notInPriceFileProducts">
					<td width="70">{{$index+1}}</td>
					<td width="400">{{notInPriceFileProduct.name}}</td>
					<td width="400">{{notInPriceFileProduct.chod}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div id="liveSearch" class="liveSearch" ng-show="liveSearch.visible">
	<img class="liveSearch_close" src="images/close.png" ng-click="liveSearch.closeLiveSearch()">

	<div></div>
	<div class="row allproducts-search" >
		<div class="col-xs-3">
			<label class="liveSearch_searchLabel">Наименование:</label>
			<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.name" />
			<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('name')">			
		</div>
		<div class="col-xs-3">
			<label class="liveSearch_searchLabel liveSearch_searchLabel__chod">ЧОД:</label>
			<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('chod')">	
			<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.chod" />			
		</div>
		<div class="col-xs-6 liveSearch_providerInfo">
			<div>
				Добавить поставщика <span class="liveSearch_providerName">{{liveSearch.provider.name}}</span>
				 Цена пр.: <span class="liveSearch_providerPrice">{{liveSearch.price_orig}} р.</span>
				 Цена вх.: <span class="liveSearch_providerPrice">{{liveSearch.price_in}} р.</span>
				 Код: <span class="liveSearch_providerPrice">{{liveSearch.product_num}}</span>
			</div>
			<div>
				Название из прайса: <span class="liveSearch_provName">{{liveSearch.priceName}}</span>
			</div>
			<div>
				ЧОД из прайса: <span class="liveSearch_provChod">{{liveSearch.priceChod}}</span>
			</div>
			<div>
				<button class="btn btn-info liveSearch_newProduct-btn" ng-click="liveSearch.addProduct()">Создать новый товар</button>
			</div>
		</div>
	</div>

	<div  tasty-table
		  bind-resource-callback="liveSearch.getResource"
		  bind-init="liveSearch.initTasty" 
		  bind-filters="liveSearch.filter" 
		  
	>
			<table class="table table-striped table-condensed allproducts-table">
				<thead tasty-thead></thead>
				<tbody>
					<tr ng-repeat="row in rows" >
						<td >
							{{ row.product.original_name }}
							<coincident-input-requsets items="row.product.inputRequests" field="name" value="filter.name" />
						</td>
						<td >
							{{ row.product.chod_display }}
							<coincident-input-requsets items="row.product.inputRequests" field="chod" value="filter.chod" />
						</td>
						<td >
							{{ row.product.product_num }}
						</td>
						<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="liveSearch.productInfoWindow.showProductsInfoWin(row.product)"></td>
						<td><span class="liveSearch_selectProduct" ng-click="liveSearch.selectProduct(row.product)">Добавить поставщика</span></td>
					</tr>
				</tbody>
			</table>
			<div tasty-pagination bind-items-per-page="liveSearch.itemsPerPage" bind-list-items-per-page="liveSearch.listItemsPerPage"></div>
	</div>
</div>
<div class="liveSearch-bg" ng-show="liveSearch.visible" ng-click="liveSearch.closeLiveSearch()"></div>

<!-- Карточка товара -->
<product-info-window windowshow="liveSearch.productInfoWindow.show"  product="liveSearch.productInfoWindow.product" providers="liveSearch.providers"></product-info-window>

<div class="progress-box" ng-show="progressBox.visible">
	<div class="progress-box_text">
		Обработка <span> {{progressBox.currentPosition}}</span>/{{progressBox.max}}
	</div>
	<div class="progress-box_finish" ng-show="progressBox.finish">
		<div class="progress-box_finish-text">Завершено!</div>
		<div class="btn btn-success" ng-click="progressBox.close()">Закрыть</div>
	</div>
</div>

</div>
</div>

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>

<script type="text/javascript">
	
	var providers = <?php echo CJavaScript::encode($providers)?>;
	var pricesInfo = <?php echo CJavaScript::encode($pricesInfo)?>;
</script>
