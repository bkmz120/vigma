<?php

class LoadFromCatalogCommand extends CConsoleCommand {
    public function run($args) {
        $log = RequisitionsManager::loadRequisitionsFromCatalog();
        echo $log;
    }
}