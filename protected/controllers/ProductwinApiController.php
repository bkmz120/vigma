<?php
/*
 *  Контроллер карточки товара, для обработки ajax-запросов
 *  Идея создать отдельный контроллер для карточки пришла не сразу, поэтуму часть функционал карточки 
 *  находится в RequestionComtroller, откуда его надо аккуртно пенести сюда
 */

class ProductwinApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

	public function actionUpdateStockChodnameAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$response = array("status"=>true,
						  "info"=>null,						  			  
						  );

		$res = Stocks::model()->updateChodname($data->id,$data->chodname);


		if ($res==false)
		{
			$response['status']=false;
			echo json_encode($response);
			return;
		}

		echo json_encode($response);
	}

	public function actionDeleteStockAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$response = array("status"=>true,
						  "info"=>null,						  			  
						  );

		$res = Stocks::model()->deleteByPk($data->id);


		if ($res==0)
		{
			$response['status']=false;
			echo json_encode($response);
			return;
		}
		
		echo json_encode($response);
	}

	public function actionUploadImgAjax()
	{
		$ds=DIRECTORY_SEPARATOR;
		$fileFolder = Yii::app()->basePath.$ds.'..'.$ds."photos".$ds;
		$fileName = time().'.'.pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$filePath = $fileFolder.$fileName;
		move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);

		$fileUrl = "/photos/".$fileName;

		$oldFileDeleted = Products::model()->deleteImg($_POST['productId'],$fileFolder);

		$response = array("status"=>true,
						  "img"=>$fileUrl,
						  "oldFileDeleted"=>$oldFileDeleted,
						  );

		$res = Products::model()->updateImg($_POST['productId'],$fileUrl);
		
		if ($res==false)
		{
			$response['status'] = false;
			echo json_encode($response);
			return;
		}

		echo json_encode($response);
	}

	public function actionDeleteImgAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$ds=DIRECTORY_SEPARATOR;
		$fileFolder = Yii::app()->basePath.$ds.'..'.$ds."photos".$ds;
		$fileDeleted = Products::model()->deleteImg($data->productId,$fileFolder);
		//if ($fileDeleted==null) $fileDeleted=false;

		$response = array("status"=>$fileDeleted,
						  
						  );
		echo json_encode($response);
	}

	public function actionGetProductInfoAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$productInfo = Products::model()->getProductWithFullInfo($data->product_id);
		if ($productInfo==null)
		{
			echo "false";
			return;
		}

		echo json_encode($productInfo);

	}

	//обновить комментарий к товару
	public function actionUpdateCommentToProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$product = Products::model()->findByPk($data->productId);
		$product->comment = $data->text;
		$res = $product->save();
		if ($res==false)
		{
			echo "false";
			return;
		}
	}

	//обновить синоним к продукту
	public function actionUpdateInputRequestAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$inputRequest = InputRequests::model()->findByPk($data->id);
		if ($inputRequest==null)
		{
			echo "false";
			return;
		}

		$inputRequest->name = $data->name;
		$inputRequest->chod_display = $data->chod_display;
		$inputRequest->chod = preg_replace('/[^a-zа-яё\d]/ui','',$data->chod_display);
		$res = $inputRequest->update();
		if ($res==false)
		{
			echo "false";
		}
		else echo "true";
	}


	//удалить синоним
	public function actionDeleteInputRequestAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$inputRequest = InputRequests::model()->findByPk($data->id);
		$res= $inputRequest->delete();
		echo $res;
	}

	public function actionAddInputRequestAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$inputRequest = new InputRequests;
		$inputRequest->name = $data->name;
		$inputRequest->chod_display = $data->chod_display;
		$inputRequest->chod = preg_replace('/[^a-zа-яё\d]/ui','',$data->chod_display);
		$inputRequest->original_item = 0;
		$inputRequest->product_id = $data->product_id;
		$res = $inputRequest->save();
		if ($res==false)
		{
			echo "false";
			return;
		}

		$result['id'] = $inputRequest->id;
		$result['name'] = $inputRequest->name;
		$result['chod'] = $inputRequest->chod;
		$result['chod_display'] = $inputRequest->chod_display;
		$result['original_item'] = $inputRequest->original_item;
		
		echo json_encode($result);
	}

	//прикрепить поставщика к товару
	public function actionAttachProviderToProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		//$price_date = date("Y-m-d");

		$res = ProvProds::model()->newProvProd($data->provider_id,$data->price_in,$data->price_orig,$data->delivery_time,$data->product_id,$data->price_date,$data->provChod,$data->provName);
		
		if ($res==false)
		{
			echo "false";
			return;
		}
		else
		{
			echo json_encode($res);
		}
	}

	//удалить ProvProd
	public function actionDeleteProvProdAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = ProvProds::model()->deleteProvProd($data->id);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}


	//изменить поставщика по-умолчанию для товара
	public function actionChangeDefaultProvidrForProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = Products::model()->changeDefaultProvider($data->product_id,$data->provprod_id);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}

	public function actionUpdateProvProdAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		
		$date = DateTime::createFromFormat('d-m-Y', $data->price_date);

		//если товар был создан выручную, то  $data->price_date == 0 и createFromFormat() вернёт false
		if ($date!=false) $price_date = $date->format('Y-m-d');
		else $price_date = null;

		
		$res= ProvProds::model()->updateInfo($data->id,$data->price_in, $data->price_orig,$price_date,$data->delivery_time);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}

	public function actionUpdateProductInfoAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$original_chod = preg_replace('/[^a-zа-яё\d]/ui','',$data->chod_display);
		$res= Products::model()->updateProductInfo($data->product_id,
												   $data->original_name,
												   $original_chod,
												   $data->chod_display,
												   $data->catalog_name,
												   $data->catalog_chod,
												   $data->product_num,
												   $data->price_chtz,
												   $data->weight
												  );
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}

	public function actionUpdateProductSizeAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res= Products::model()->updateSize($data->product_id,$data->size);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}

	public function actionUpdateDeliveryCommentAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$res= ProvProds::model()->updateDeliveryComment($data->id,$data->deliveryComment);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}
}

