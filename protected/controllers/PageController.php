<?php

//Контроллер для рендеринга основныхс страниц. Для каждой страницы отдельный контроллер для rest API

class PageController extends Controller
{
    public $defaultAction = 'AllRequisitions';

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(

             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

    public function actionAllCustomers()
    {
        $this->pageTitle = 'Вигма - Контрагенты';

        //$customers = Customers::model()->findAll();
        //$custmoersInfos = Customers::model()->getInfoArrayFromArray($customers);
        $statuses = CustomerStatuses::model()->getAllInfos();
        $custmoersInfos = Customers::model()->getAllInfosWithStatusesAndReqNum();

        $cs = Yii::app()->clientScript;
        $baeUrl = Yii::app()->getBaseUrl(true);
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/angular.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerScriptFile($baeUrl.'/js/customers/customerEdit.js');
        $cs->registerScriptFile($baeUrl.'/js/allcustomers/allcustomers.js');

        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');
        $cs->registerCssFile($baeUrl.'/css/customerEdit.css');

        $cs->registerCssFile($baeUrl.'/css/allCustomers.css');

        $this->render('allCustomers',array('customers'=>$custmoersInfos,
                                           'statuses'=>$statuses,
                      ));
    }

    public function actionAllProviders()
    {
        $this->pageTitle = 'Вигма - Импорт прайса';

        $prices = Prices::model()->findAll();

        $baeUrl = Yii::app()->getBaseUrl(true);

        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($baeUrl.'/js/libs/angular.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload-shim.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/angular-datepicker.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/moment.min.js');
        $cs->registerScriptFile($baeUrl.'/js/productInfoWindow/productInfoWin.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery-ui.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/decimal.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/angular-drag-and-drop-lists.min.js');

        $cs->registerScriptFile($baeUrl.'/js/importPrice/importprice.js');

        $cs->registerCssFile($baeUrl.'/css/importPrice.css');
        $cs->registerCssFile($baeUrl.'/css/jquery-ui.min.css');
        $cs->registerCssFile($baeUrl.'/css/angular-datepicker.min.css');
        $cs->registerCssFile($baeUrl.'/css/productInfoWindow.css');
        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');

        //загрузим список поставщиков (нужен для карточки товара)
        $providers = Providers::model()->getAllPovidersInfo(true);
        $prices = Prices::model()->findAll();
        $pricesInfo = array();
        foreach ($prices as $price)
        {
            if ($price->date!=null)
            {
                $date = date('d.m.Y',strtotime($price->date));
            }
            else $date = "(нет)";

            $priceInfo = array('id'=>$price->id,
                               'name'=>$price->name,
                               'date'=>$date,
                               'template'=>json_decode($price->template),
                                );

            array_push($pricesInfo,$priceInfo);
        }

        $this->render('allProviders',array('pricesInfo'=>$pricesInfo,
                                           'providers'=>$providers,
            ));
    }

    public function actionStocks()
    {
        $this->pageTitle = 'Вигма - Остатки';

        $baeUrl = Yii::app()->getBaseUrl(true);
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($baeUrl.'/js/libs/angular.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload-shim.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/productInfoWindow/productInfoWin.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery-ui.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/decimal.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');


        $cs->registerScriptFile($baeUrl.'/js/stock/stock.js');

        $cs->registerCssFile($baeUrl.'/css/stock.css');
        $cs->registerCssFile($baeUrl.'/css/jquery-ui.min.css');
        $cs->registerCssFile($baeUrl.'/css/productInfoWindow.css');
        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');

        $stockCities = StockCities::model()->findAll();
        $stockCitiesInfo = array();
        if ($stockCities!=null)
        {
            foreach ($stockCities as $stockCity)
            {
                $stockCityInfo = $stockCity->getInfoArray();
                $stockCityInfo['date'] = date('d.m.Y',strtotime($stockCityInfo['date']));
                $stockCityInfo['count'] = Stocks::model()->countForCity($stockCity->id);
                $stockCitiesInfo[] = $stockCityInfo;
            }

        }

        //загрузим список поставщиков (нужен для карточки товара)
        $providers = Providers::model()->getAllPovidersInfo();

        $this->render('stocks',array('stockCities'=>$stockCitiesInfo,
                                          'providers' => $providers,

            ));
    }

    public function actionAllProducts()
    {
        $this->pageTitle = 'Вигма - Все товары';

        $cs = Yii::app()->clientScript;
        $baeUrl = Yii::app()->getBaseUrl(true);
        $cs->registerScriptFile($baeUrl.'/js/libs/angular.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery-ui.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/decimal.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload-shim.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload.min.js');


        $cs->registerScriptFile($baeUrl.'/js/allproducts/allproducts.js');
        $cs->registerScriptFile($baeUrl.'/js/productInfoWindow/productInfoWin.js');

        $cs->registerCssFile($baeUrl.'/css/jquery-ui.min.css');
        $cs->registerCssFile($baeUrl.'/css/allProducts.css');
        $cs->registerCssFile($baeUrl.'/css/productInfoWindow.css');
        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');

        //загрузим список поставщиков (нужен для карточки товара)
        $providers = Providers::model()->getAllPovidersInfo();

        $this->render('allProducts',array('providers'=>$providers));
    }


    public function actionAllRequisitions()
    {
        $this->pageTitle = 'Вигма - Все заявки';
        
        //загрузить заявки из каталога
        RequisitionsManager::loadRequisitionsFromCatalog();

        $confirmedRequisitionsInfo = RequisitionsManager::getRequisitions('confirmed',20);
        $confirmedRequisitionsTotalCount = RequisitionsManager::getRequisitionsCount('confirmed');

        $waitForConfirmRequisitionsInfo = RequisitionsManager::getRequisitions('first',20);
        $waitForConfirmRequisitionsTotalCount = RequisitionsManager::getRequisitionsCount('first');



        $baeUrl = Yii::app()->getBaseUrl(true);
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($baeUrl.'/js/libs/angular_v1-6-4.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/pagination/pagination.js');
        $cs->registerCssFile($baeUrl.'/js/pagination/pagination.css');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');
        $cs->registerScriptFile($baeUrl.'/js/allRequisitions/allRequisitions.js');
        $cs->registerScriptFile($baeUrl.'/js/allRequisitions/RequisitionsService.js');
        $cs->registerScriptFile($baeUrl.'/js/allRequisitions/requisitionsList/requisitionsList.js');
        $cs->registerCssFile($baeUrl.'/css/allRequisition.css');

        $this->render('allRequisitions',array('confirmedRequisitionsInfo'=>$confirmedRequisitionsInfo,
                                        'confirmedRequisitionsTotalCount'=>$confirmedRequisitionsTotalCount,
                                        'waitForConfirmRequisitionsInfo'=>$waitForConfirmRequisitionsInfo,
                                        'waitForConfirmRequisitionsTotalCount'=>$waitForConfirmRequisitionsTotalCount,
                                        'compliteRequisitionsInfo'=>$compliteRequisitionsInfo,
        ));
    }

    public function actionRequisition($id=null)
    {
        $customer_id = -1;
        $itIsNewRequisition = 0; //false
        if ($id==null)
        {
            $this->pageTitle = 'Вигма - Новая заявка';
            //создаем новую запись для этой заявки
            $requisition = new Requisitions;            
            $requisition->status = "first";
            $requisition->save();
            
            $id=$requisition->id;
            $itIsNewRequisition = 1; //true
        }

        else {
            $this->pageTitle = 'Вигма - Заявка №'.$id;
            $requisition = Requisitions::model()->findByPk($id);
            $customerInfo = Customers::model()->getShortInfoById($requisition->customer_id);
            $reqProds = ReqProds::model()->findInfoArrayByRequistionId($id);
            if ($requisition->new==1) {
                $requisition->new=0;
                $requisition->update();
            }
        }

        $num = $requisition->num;

        //загрузим список поставщиков:
        $providers = Providers::model()->getAllPovidersInfo();


        $cs = Yii::app()->clientScript;
        $baeUrl = Yii::app()->getBaseUrl(true); 
        $cs->registerScriptFile($baeUrl.'/js/libs/angular.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery-ui.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/angucomplete-alt.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/angular-input-masks-standalone.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload-shim.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/decimal.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/clickAnywhere.js');


        $cs->registerScriptFile($baeUrl.'/js/customers/customerEdit.js');
        $cs->registerScriptFile($baeUrl.'/js/productInfoWindow/productInfoWin.js');
        $cs->registerScriptFile($baeUrl.'/js/requisitions/requisition.js');


        $cs->registerCssFile($baeUrl.'/css/requisition.css');
        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/jquery-ui.min.css');
        $cs->registerCssFile($baeUrl.'/css/angucomplete-alt.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');
        $cs->registerCssFile($baeUrl.'/css/productInfoWindow.css');
        $cs->registerCssFile($baeUrl.'/css/customerEdit.css');

        $this->render('requisition',array(
                    'requisition_id' => $id,
                    'requisition_num' => $num,
                    'requisition_from_catalog'=>$requisition->from_catalog,
                    'customerInfo' => $customerInfo,
                    'itIsNewRequisition' => $itIsNewRequisition,
                    'reqProds' => $reqProds,
                    'providers' => $providers,
            ));
    }

    // $tab: group-products | import-groups | categories | export
    public function actionCatalog($tab='export')
    {
        //загрузим список поставщиков:
        $providers = Providers::model()->getAllPovidersInfo();

        $catalogcats = CatalogcatsManager::getAllCatalogcatsInfo(true);

        $groups = GroupsManager::getAllGroupsInfo();


        $cs = Yii::app()->clientScript;
        $baeUrl = Yii::app()->getBaseUrl(true); //'ngTasty','xeditable','angucomplete-alt','ui.utils.masks'
        $cs->registerScriptFile($baeUrl.'/js/libs/angular_v1-6-4.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-tasty.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/xeditable.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/jquery-ui.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload-shim.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ng-file-upload.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/ngDialog.min.js');
        $cs->registerScriptFile($baeUrl.'/js/libs/decimal.min.js');
        $cs->registerScriptFile($baeUrl.'/js/productInfoWindow/productInfoWin.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/groupsApp.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/components/waitbox/waitbox.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/components/groupProdEditor/groupProdEditor.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/controllers/groupProductsController.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/controllers/importGroupsController.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/controllers/catalogcatsController.js');
        $cs->registerScriptFile($baeUrl.'/js/groups/controllers/exportController.js');

        $cs->registerCssFile($baeUrl.'/css/xeditable.css');
        $cs->registerCssFile($baeUrl.'/css/jquery-ui.min.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog.css');
        $cs->registerCssFile($baeUrl.'/css/ngDialog-theme-plain.css');
        $cs->registerCssFile($baeUrl.'/css/productInfoWindow.css');
        $cs->registerCssFile($baeUrl.'/css/catalog.css');
        $cs->registerCssFile($baeUrl.'/css/allProducts.css');

        $this->render("catalog/$tab",array('tab'=>$tab,
                                         'provider'=>$providers,
                                         'catalogcats'=>$catalogcats,
                                         'groups'=>$groups,
                     ));
    }

    public function actionTest()
    {
        CatalogcatsManager::updateProductsCatalogcatIds();
    }

}