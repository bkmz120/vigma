<?php


class CustomersApiController extends Controller
{
    
	public function filters()
    {
        return array(
            'accessControl',
        );
    }    
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }


    public function actionSaveCutomerAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                          'customer_id'=>null,
                         );
        if ($data->customer->id!=null)
        {
            $res = Customers::model()->updateCustomer($data->customer);
            if ($res==false)
            {
                $response['status'] = false;
                $response['info'] = 'error while updating custmoer';
                echo json_encode($response);
                return;
            } 
            $response['customer_id'] = $data->customer->id;          
        }
        else
        {
            $customer = Customers::model()->addCustomer($data->customer);
            if ($customer==null)
            {
                $response['status'] = false;
                $response['info'] = 'error while updating custmoer';
                echo json_encode($response);
                return;
            } 
            $response['customer_id'] = $customer->id;   
        }

        echo json_encode($response);
        return;
    }

    public function actionDeleteCutomerAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                         );
        //если заказчик фигурирует в каких-либо заявках, то вместо удаления, делаем его скрытым
        $criteria = new CDbCriteria;
        $criteria->condition = 'customer_id=:customer_id';
        $criteria->params = array(':customer_id'=>$data->customer_id);
        $requisitionCount = Requisitions::model()->count($criteria);

        if ($requisitionCount==0)
        {
            $res = Customers::model()->deleteByPk($data->customer_id);
            if ($res!=1)
            {
                $response['status'] = false;
                $response['info'] = 'error while deleting custmoer';
                echo json_encode($response);
                return;
            }

            $response['info'] = "delete";
        }
        else
        {
            $customer = Customers::model()->findByPk($data->customer_id);
            if ($customer==null)
            {
                $response['status'] = false;
                $response['info'] = 'custmer not found';
                echo json_encode($response);
                return;
            }
            $customer->hidden = 1;
            $res=$customer->update();
            if ($res==false)
            {
                $response['status'] = false;
                $response['info'] = 'error while hidding custmoer';
                echo json_encode($response);
                return;
            }
            $response['info'] = "hide";
        }

        echo json_encode($response);
        return;
    }

    public function actionUpdateCustomerStatusAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                         );
        $status = CustomerStatuses::model()->findByPk($data->id);
        if ($status==null)
        {
            $response['status'] = false;
            $response['info'] = 'status not found';
            echo json_encode($response);
            return;
        }

        $status->name = $data->name;
        $res = $status->update();

        if ($res==false)
        {
            $response['status'] = false;
            $response['info'] = 'error while updating';
            echo json_encode($response);
            return;
        }

        echo json_encode($response);
    }

    public function actionDeleteCustomerStatusAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                         );
        $res = CustomerStatuses::model()->deleteByPk($data->id);
        if ($res!=1)
        {
            $response['status'] = false;
            $response['info'] = 'not deleted';
            echo json_encode($response);
            return;
        }

        //если за какими-то заказчиками был закреплён этот статус, снимим эту связь
        $res = Customers::model()->clearStatusById($data->id);

        if ($res==-1)
        {
            $response['status'] = false;
            $response['info'] = 'error while clear customer statuses';
            echo json_encode($response);
            return;
        }

        echo json_encode($response);
    }

    public function actionCreateCustomerStatusAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                          'customerStatus'=>null,
                         );

        $status = new CustomerStatuses;
        $status->name = "Новый статус";
        $res = $status->save();
        if ($res==false)
        {
            $response['status'] = false;
            $response['info'] = 'error while creating customer statuses';
            echo json_encode($response);
            return;
        }

        $response['customerStatus'] = $status->getInfo();
        echo json_encode($response);
    }

    public function actionGetAllCustomersNamesAjax()
    {
       
        $customersInfos = Customers::model()->getAllCustomersNames();
        echo json_encode($customersInfos);
    }

    public function actionGetAllStatusesAjax()
    {
       
        $statuses = CustomerStatuses::model()->getAllInfos();
        echo json_encode($statuses);
    }

    public function actionGetCustomerInfoAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $customerInfo = Customers::model()->getInfoById($data->id);
        $response = array('status'=>true,
                          'info'=>null,
                          'customerInfo'=>null,
                         );
        if ($customerInfo == false)
        {
            $response['status'] = false;
            $response['info'] = 'customer not found';
            echo json_encode($response);
            return;
        }

        $response['customerInfo'] = $customerInfo;
        echo json_encode($response);
    }

}