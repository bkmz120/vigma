<?php


class StocksApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

    

    

    public function actionImportStockAjax()
    {
        $data = json_decode(file_get_contents('php://input'));

        $filePath = UploadFile::getPathByName($data->fileName);
        
        $stockCity_id = $data->stockCity_id;
        $date = $data->importDate;
        $date = date('Y-m-d',strtotime($date));

        $stockCityTemplate = StockCities::model()->getTemplate($stockCity_id,false);
        if ($stockCityTemplate==false)
        {
            echo "false T";
            return;
        }

        $res = Stocks::model()->import($filePath,$stockCity_id,$stockCityTemplate);
        if ($res==false)
        {
            echo "false";
            return;
        }
        else
        {
            $cityRes = StockCities::model()->setDate($stockCity_id,$date);
            if ($cityRes==false) 
            {
                echo "false";
                return;
            }
        }


        echo json_encode($res);
    }

    public function actionSaveCityTemplateAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
    
        $res=StockCities::model()->saveTemplate($data->stockCityId,$data->json_template);
        if ($res==false) echo "false";
        else echo $res;
    } 

    public function actionClearCityTemplateAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $res=Stocks::model()->clearCityTemplate($data->stockCityId);
        if ($res==false) echo "false";
        else echo $res;
    }  

    public function actionDeleteCityTemplateAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $res=Stocks::model()->clearCityTemplate($data->stockCityId);
        if ($res==false) 
        {
            echo "false";
            return;
        }

        $res=StockCities::model()->deleteByPk($data->stockCityId);
        if ($res==false) 
        {
            echo "false";
            return;
        }

        echo "true";
        
    }     

    public function actionNewStockAjax()
    {
        $data = json_decode(file_get_contents('php://input'));

        $res= Stocks::model()->newStock($data);
        echo json_encode($res);        
    }



}