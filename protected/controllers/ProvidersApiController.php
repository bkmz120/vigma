<?php


class ProvidersApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

	public function actionSaveProviderAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                          'provider_id'=>null,
                         );
        if ($data->provider->id!=null)
        {
            $res = Providers::model()->updateProvider($data->provider);
            if ($res==false)
            {
                $response['status'] = false;
                $response['info'] = 'error while updating provider';
                echo json_encode($response);
                return;
            } 
            $response['provider_id'] = $data->provider->id;          
        }        
        else
        {
        	try {
        		$provider = Providers::model()->addProvider($data->provider);
        	}
        	catch (Exception $e) {
        		$response['status'] = false;
                $response['info'] = $e->getMessage();
                echo json_encode($response);
                return;
        	}
            
            if ($provider==null)
            {
                $response['status'] = false;
                $response['info'] = 'error while updating provider';
                echo json_encode($response);
                return;
            } 
            $response['provider_id'] = $provider->id;   
        }        

        echo json_encode($response);
        return;
    }

    public function actionDeleteProviderAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array('status'=>true,
                          'info'=>null,
                         );
        //если поставщик прикреплен к какому-либо товару, то вместо удаления, делаем его скрытым
        $criteria = new CDbCriteria;
        $criteria->condition = 'provider_id=:provider_id';
        $criteria->params = array(':provider_id'=>$data->provider_id);
        $provProdsCount = ProvProds::model()->count($criteria);

        if ($provProdsCount==0)
        {
            $res = Providers::model()->deleteByPk($data->provider_id);
            if ($res!=1)
            {
                $response['status'] = false;
                $response['info'] = 'error while deleting custmoer';
                echo json_encode($response);
                return;
            }

            $response['info'] = "delete";
        }
        else
        {
            $provider = Providers::model()->findByPk($data->provider_id);
            if ($provider==null)
            {
                $response['status'] = false;
                $response['info'] = 'custmer not found';
                echo json_encode($response);
                return;
            }
            $provider->hidden = 1;
            $res=$provider->update();
            if ($res==false)
            {
                $response['status'] = false;
                $response['info'] = 'error while hidding custmoer';
                echo json_encode($response);
                return;
            }
            $response['info'] = "hide";
        }

        echo json_encode($response);
        return;
    }

	

	public function actionImportPriceAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$priceFilePath = UploadFile::getPathByName($data->fileName);

		$response = array("status"=>true);

		try {
			$response['result'] = Providers::model()->importPrice($data->provider_id,$priceFilePath,$data->priceImportCoef,$data->priceActualDate);
		}
		catch (Exception $e)
		{
			$response['status'] = false;
			$response['message'] = $e->getMessage();
		}
		
		echo json_encode($response);
	}

	public function actionChangeProvidersOrderAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = Providers::model()->changeProviderOrder($data->order);
		
		if ($res==false)
		{

			echo "false";
			return;
		}

		echo "true";
	}


	//прикрепить поставщика к товару
	public function actionAddProvideToProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$data->price_date = date('Y-m-d',strtotime($data->price_date));
		$res = ProvProds::model()->newProvProd($data->provider_id,$data->price_in,$data->price_orig,null,$data->product_id,$data->price_date,$data->provChod, $data->provName);
		
		if ($res==false)
		{

			echo "false";
			return;
		}
		echo $data->arrayIndex;
		
	}

	//добавить новый product
	public function actionAddNewProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$data->price_date = date('Y-m-d',strtotime($data->price_date));
		// echo gettype($data->price_in);
		// echo "<br>";
		// echo doubleval($data->price_in);

		$product = new Products;
		$product->original_name = $data->original_name;
		$product->original_chod = $data->original_chod;
		$product->chod_display = $data->chod_display;
		$product->catalog_name = $data->original_name;
		$product->catalog_chod = $data->chod_display;
		$product->product_num = $data->product_num;
		$product->price_chtz = $data->price_chtz;
		$product->weight = $data->weight;

		$res=$product->addNew($data->provider_id, $data->price_in, $data->price_orig,$data->price_date);
		
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo $data->arrayIndex;
	}

	//сохранить шаблон прайса
	public function actionSaveTemplateAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$res=Providers::model()->saveTemplate($data->provider_id,$data->template);
		if ($res==false) echo "false";
		else echo "true";
	}

	//удалить шаблон прайса
	public function actionDeleteTemplateAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
	
		$res=Prices::model()->deleteByPk($data->priceId);
		if ($res==false) echo "false";
		else echo "true";
	}

	//изменить комментарий к импорту для поставщика
	public function actionChangeImportCommentAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res= Providers::model()->updateImportComment($data->provider_id,$data->import_comment);
		
		$response = new AjaxResponse;

		if (!$res) $response->setError('error on update');

		$response->send();

	}

}	