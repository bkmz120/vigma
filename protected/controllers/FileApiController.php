<?php
class FileApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

    public function actionUploadFileAjax()
    {
        $res= UploadFile::upload($_FILES["file"]);
        echo $_FILES["file"]["name"];        
    }

}