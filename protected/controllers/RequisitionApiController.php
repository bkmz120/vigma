<?php


class RequisitionApiController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules()
	{
		return array(
			
			 array('allow', // allow authenticated users to perform any action
				'users'=>array('@'),
				),
			array('deny',  // deny all users
				'users'=>array('*'),
				),
		);
	}

	public function actionGetRequisitionsAjax() {
		$data = json_decode(file_get_contents('php://input'));
		$response = new AjaxResponse;
		try {
			$requisitions = RequisitionsManager::getRequisitions($data->status,$data->count,$data->offset);
			$response->setDataItem('requisitions',$requisitions);
		}
		catch (Exception $e) {
			$response->setError($e->getMessage());
		}

		$response->send();
	}

	public function actionDeleteRequisitionAjax() {
		$data = json_decode(file_get_contents('php://input'));
		$response = new AjaxResponse;
		try {
			RequisitionsManager::deleteRequisition($data->id);    		
		}
		catch (Exception $e) {
			$response->setError($e->getMessage());
		}
		$response->send();
	}
	

	//автоподстановка при вводе в троку поиска
	public function actionSearchTypeAjax($query)
	{
		
		$results = Products::model()->searchType($query);
		
		$answer=array();
		$answer['results'] = $results;
		echo json_encode($answer);
	}

	//поиск позиций, при нажатии на "поиск"
	public function actionSearchPositionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		//удалим выражения вида " - 23 шт." и т.д.
		//выражение состоит из двух шаблонов: [ ]{1,}[-]*[ ]*[0-9]{0,4}шт[.]?   и   [ ]{1,}[-]*[ ]*[0-9]{0,2}[ ]*шт[.]?
		//первое - если цифры слитно с "шт", то количество цифр от 0 до 4, если цифры раздельно с "шт", то цифр не больше 2-х, т.к. в это случае цифры могут быть ЧОДом
		$reg = "/(?:[ ]{1,}[-]*[ ]*[0-9]{0,4}шт$|[ ]{1,}[-]*[ ]*[0-9]{0,2}[ ]*шт$)/";
		$input = preg_replace($reg, " ", $data->input);
		
		//заменим неразрывный пробел на обычный
		$input = preg_replace('~\x{00a0}~siu', ' ', $input);
	
		//найдём в строке слова похожие на ЧОД и удалим из них лишние разделители (оставив ток буквы и цифры)
		$words=preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/", $input);

		$chodDetection = 3; // количество цифр, которое должно быть в слове что бы это был ЧОД
		$name ="";  //сформируем строки содержащие отдельно название и чод для отправки в Front-end
		$chod = "";
		for ($wC=0;$wC<count($words);$wC++)
		{
			$numericCount = 0;
			$itIsChod= false;
			for ($i=0;$i<strlen($words[$wC]);$i++)
			{
				if (ctype_digit($words[$wC][$i]))
				{
					$numericCount++;
					if ($numericCount==$chodDetection)
					{
						if ($chod=="")  $chod=$words[$wC];
						else $chod = $chod.' '.$words[$wC];

						$itIsChod = true;
						$words[$wC] = '*'.preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]).'*';
						//$words[$wC] = preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]);
						break;
					}
				}
			}
			if (!$itIsChod)
			{
				if ($name=="") $name = $words[$wC];
				else $name = $name.' '.$words[$wC];
			}
		}
		
		$searchStr =implode(" ", $words);
		$product = Products::model()->findProductForPosition($searchStr);
		
		$result = array();
		$result['product'] = $product;
		$result['name'] = $name;
		$result['chod'] = $chod;
	
		echo json_encode($result);
	}


	// после того как позиция была добавлена в заявку вo Front-End необходимо
	//сохранить изменения в бд
	public function actionAddPositionToRequisitionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		try
		{
			$reqProd = new ReqProds;
			$reqProd->requisition_id = $data->requisition_id;
			$reqProd->position_id = $data->positionId;
			$reqProd->position_num = $data->positionNum;
			$reqProd->product_id = $data->productId;
			$reqProd->count = $data->count;
			$reqProd->list_type = $data->listType;
			$reqProd->input_name = $data->inputName;
			$reqProd->input_chod = $data->inputChod;
			$reqProd->provider_id = $data->provider_id;
			$reqProd->provprod_id = $data->provprod_id;
			$reqProd->price_outsum = $data->priceOutSum;

			$res=$reqProd->save();
			if ($res==false)
			{
				echo "false";
				return;
			}

			if ($reqProd->product_id != null) {
				Products::model()->increaseRating($reqProd->product_id);
			}
		}
		catch (Exception $e)
		{
			echo "false";
			return;
		}	
		echo $reqProd->id;
	}

	//при смене товара закрпеленного за позицией в заявке
	public function actionChangeProductInPosition()
	{
		$data = json_decode(file_get_contents('php://input'));
		$reqProd = ReqProds::model()->findByPk($data->reqProdId);

		if ($reqProd==null)
		{
			echo "false";
			return;
		}

		$lastProductId = $reqProd->product_id;
		$newProductId = $data->productId;

		try
		{
			$reqProd->product_id = $data->productId;
			$reqProd->price_outsum = $data->price_outsum;
			$reqProd->count = $data->count;
			$reqProd->provprod_id = $data->provProdId;
			$reqProd->provider_id = $data->providerId;
		
			$res=$reqProd->update();

			if ($res==false)
			{
				echo "false";
				return;
			}
		}
		catch (Exception $e)
		{
			echo "false";
			return;
		}

		try 
		{
			if ($lastProductId!=null)
			{			
				$res = Products::model()->decreaseRating($lastProductId);
				if ($res==false)
				{
					echo "false";
					return;
				}
			}
			$res = Products::model()->increaseRating($newProductId);
			if ($res==false)
			{
				echo "false";
				return;
			}
		}
		catch (Exception $e)
		{
			echo "false";
			return;
		}

		echo "true";
	}



	// при удалени позиции из заявки - удаляем из БД
	public function actionDeletePositionFromRequisitionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$req_prod = ReqProds::model()->findByPk($data->reqProdId);

		if ($req_prod==null)
		{
			echo "false";
			return;
		}

		$product_id  = $req_prod->product_id;

		try
		{			
			$res = $req_prod->delete();
			if ($res==false)
			{
				echo "false";
				return;
			}
		}
		catch (Exception $e)
		{
			echo "false";
			return;
		}


		try
		{
			$res = Products::model()->decreaseRating($product_id);
			if ($res==false)
			{
				echo "false";
				return;
			}
		}
		catch (Exception $e)
		{
			echo "false";
			return;
		}

		echo "true";	
	}

	//отвязать товар от позиции в заявке
	public function actionUnbindProductFromPositionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$response = array('status'=>true);

		$reqProd= ReqProds::model()->findByPk($data->reqProdId);
		if ($reqProd==null)
		{
			$response['status'] = false;
			$response['message'] = 'reqProd not found';
			echo json_encode($response);
			return;
		}

		if ($reqProd->product_id!=null)
		{
			try
			{
				$res = Products::model()->decreaseRating($reqProd->product_id);
				if ($res==false)
				{
					echo "false";
					return;
				}
			}
			catch (Exception $e)
			{
				echo "false";
				return;
		}

		}

		$reqProd->product_id = null;
		$reqProd->price_outsum = 0;
		$reqProd->provider_id = null;
		$reqProd->provprod_id = null;
		$res = $reqProd->update();
		if ($res==false)
		{
			$response['status'] = false;
			$response['message'] = 'error when update reqProd';
			echo json_encode($response);
			return;
		}		

		echo json_encode($response);
	}

	//удаление синонима после отвязки товара от позиции
	public function actionRemoveInputRequetFromProductAfterUnbindingAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$response = array('status'=>true);
		//отвяжим привязанный синоним
		try {
			$res = InputRequests::model()->deleteByChodNameProductId($data->productId,$data->inputChod,$data->inputName);
		}
		catch (Exception $e)
		{
			$response['status'] = false;
			$response['message'] = 'error in removing InputRequests '.$e->getMessage();
			echo json_encode($response);
			return;
		}	
		

		echo json_encode($response);
	}

	
	// смена статуса заявки
	public function actionChangeRequisitionStatusAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$requisition = Requisitions::model()->findByPk($data->requisition_id);
		if ($requisition==null)
		{
			echo "false";
			return;
		}

		$requisition->status=$data->status;
		$res= $requisition->update();
		if ($res==false)
		{
			echo "false";
			return;
		}

		echo "true";	
	}


	//обновить поле вопроса к заявке
	public function actionUpdateQuestionReqProdAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$reqProd = ReqProds::model()->findByPk($data->reqProdId);
		if ($reqProd==null)
		{
			echo "false";
			return;
		}

		if ($data->questionText!=null) $reqProd->question_text = $data->questionText;

		$reqProd->question_status = $data->questionStatus;

		$res = $reqProd->update();
		
		if ($res==false)
		{
			echo "false";
			return;
		}
	}

	//удалить поле вопроса к заявке
	public function actionDeleteQuestionReqProdAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$reqProd = ReqProds::model()->findByPk($data->reqProdId);
		if ($reqProd==null)
		{
			echo "false";
			return;
		}

		$reqProd->question_text = null;

		$reqProd->question_status = null;

		$res = $reqProd->update();
		
		if ($res==false)
		{
			echo "false";
			return;
		}
	}
	
	
	//прикрепить заказчика к заявке
	public function actionAttachCustomerToRequisitionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = Requisitions::model()->attachCustomer($data->requisition_id,$data->customer_id);
		
		$response = array('status'=>true                          
						  );
		if ($res==false)
		{
			$response['status'] = false;
			echo json_encode($response);
			return;
		}
		echo json_encode($response);
	}
	

	public function actionUpdateReqProdAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = ReqProds::model()->updateInfo($data->reqProd_id,$data->provider_id,$data->provprod_id, $data->count,$data->price_outsum);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}
	
	

	//добавить текст к комментарию
	public function actionAddToCommentToProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$product = Products::model()->findByPk($data->productId);
		$product->comment = $product->comment."\n\r".$data->text;
		$res = $product->save();
		if ($res==false)
		{
			echo "false";
			return;
		}
	}

	//добавить синоним к продукту
	public function actionAddInputRequestToProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$inputRequest = new InputRequests;
		$inputRequest->name = $data->name;
		$inputRequest->chod = preg_replace('/[^a-zа-яё\d]/ui','',$data->chod);
		$inputRequest->chod_display = $data->chod;
		$inputRequest->product_id = $data->product_id;
		$res = $inputRequest->save();
		if ($res==false)
		{
			echo 'false';
		}
		else echo $inputRequest->id;
	}

	public function actionUpdatePositionsNumsAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res = ReqProds::model()->updatePositionsNums($data->positionNumInfos);
		
		if ($res==false)
		{
			echo "false";
			return;
		}

	}	
	/**
	 * 
	 * Экпорт заявки в xls для Заказчика
	 * 
	 */ 
	public function actionExportRequisitionToXlsCustomer($requisition_id,$list_type,$filename=null)
	{
		//$data = json_decode(file_get_contents('php://input'));
		// номер, пользовательское название/чод количество исходящая цена по каждой позиции общее итого
		// всё берём из reqProds, подсчитывая итого

		if ($filename==null)
		{
			$requisition_num = RequisitionsManager::getNum($requisition_id);
			$filename = "Ответ_заявка".$requisition_num."_".date("d-m-Y").".xls";
		}
	
		$filePath =  ReqProds::model()->exportToXlsCustomer($requisition_id,$list_type,$filename);

		header('Content-type: application/xls');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($filePath);
		unlink($filePath);
	}

	/**
	 * Экпорт заявки в xls для ЮА
	 * 
	 */ 
	public function actionExportRequisitionToXlsUA($requisition_id,$list_type,$filename=null)
	{
		//$data = json_decode(file_get_contents('php://input'));
		// номер, пользовательское название/чод количество исходящая цена по каждой позиции общее итого
		// всё берём из reqProds, подсчитывая итого

		if ($filename==null)
		{
			$requisition_num = RequisitionsManager::getNum($requisition_id);
			$filename = "ЮА_заявка".$requisition_num."_".date("d-m-Y").".xls";
		}

		$filePath =  ReqProds::model()->exportToXlsUA($requisition_id,$list_type,$filename);

		header('Content-type: application/xls');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($filePath);
		unlink($filePath);
	}

	/**
	 * Экпорт заявки в xls для счета
	 * 
	 */ 
	public function actionExportRequisitionToXlsInvoice($requisition_id,$list_type,$filename=null)
	{
		//$data = json_decode(file_get_contents('php://input'));
		// номер, пользовательское название/чод количество исходящая цена по каждой позиции общее итого
		// всё берём из reqProds, подсчитывая итого

		if ($filename==null)
		{
			$requisition_num = RequisitionsManager::getNum($requisition_id);
			$filename = "счет_заявка".$requisition_num."_".date("d-m-Y").".xls";
		}

		$filePath =  ReqProds::model()->exportToXlsInvoice($requisition_id,$list_type,$filename);

		header('Content-type: application/xls');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($filePath);
		unlink($filePath);
	}

	public function actionSearFromFileAjax()
	{
		//require_once Yii::app()->basePath . '/extensions/phpexcelreader/excel_reader2.php';
		require_once Yii::app()->basePath . '/extensions/phpexcel/PHPExcel.php';
		

		$ds=DIRECTORY_SEPARATOR;
		$newFilePath = Yii::app()->basePath.$ds.'..'.$ds."uploads".$ds.$_FILES["file"]["name"];
		//$newFilePath = iconv('utf-8','windows-1251',$newFilePath);
		move_uploaded_file($_FILES["file"]["tmp_name"], $newFilePath);
		//echo $_FILES["file"]["name"];

		//$fileData = new Spreadsheet_Excel_Reader($newFilePath,false);
		$objPHPExcel = PHPExcel_IOFactory::load($newFilePath);
		$objWorksheet = $objPHPExcel->getActiveSheet();

		$nameClm = 0;
		$chodClm = 1;
		$countClm = 2;

		//$row_count = $fileData->rowcount(0);
		$row_count = $objWorksheet->getHighestRow();

		$positionsFromFile = array();


		for ($i=1;$i<=$row_count;$i++)
		{
			// $name = trim($fileData->val($i,$nameClm),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
			// $chod = trim($fileData->val($i,$chodClm),chr(0xC2).chr(0xA0));
			// $count= trim($fileData->val($i,$countClm),chr(0xC2).chr(0xA0)); 
			$name = trim($objWorksheet->getCellByColumnAndRow($nameClm,$i)->getValue(),chr(0xC2).chr(0xA0));//улаление в том числе неразрывного пробела
			$chod = trim($objWorksheet->getCellByColumnAndRow($chodClm,$i)->getValue(),chr(0xC2).chr(0xA0));
			$count= trim($objWorksheet->getCellByColumnAndRow($countClm,$i)->getValue(),chr(0xC2).chr(0xA0)); 
			
			$input = $name.' '.$chod;



			$input = mb_convert_encoding($input, "UTF-8", "auto");
			$count = mb_convert_encoding($count, "UTF-8", "auto");

			if ($count==null || $count=="") $count = 1;

			$position = array('input'=>$input,
							  'count'=>$count,
				);
	

			array_push($positionsFromFile, $position);
		}
		
		echo json_encode($positionsFromFile);
	}

}