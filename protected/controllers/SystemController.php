<?php
/**
 * Методы выполняющиеся вручную, для началоной инициализации БД или корректировке данных,
 * при добавлении нового функционала
 */

class SystemController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(

             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }


    //первоначальная загрузка прайса ЧТЗ
	public function actionInitChtz()
	{
		$import = new Import;
		$import->importChtzPrice();
	}

	//создать индекс для Sphinx
	public function actionCreateIndex()
	{
		$import = new Import;
		$import->createRealTimeInputRequestsIndex();
	}

	//очистить индекс Sphinx
	public function actionClearIndex()
	{
		$import = new Import;
		$import->clearRealTimenputRequestsIndex();
	}

	public function actionDeleteNotOrig()
	{
		$import = new Import;
		$import->deleteNotOriginalChods();

	}

	public function actionDeleteAllProducts()
	{
		$res =  ProvProds::model()->deleteAll();
		echo $res;
		$res =  Products::model()->deleteAll();
		echo $res;
	}

	//изначальный импорт памяти
	public function actionImportMemmory()
	{
		$import = new Import;
		$import->importInmputs();
	}


	public function actionCreateGroups()
	{
		for ($i=1;$i<=71;$i++)
		{
			$group = new Groups;
			$group->group_num = $i;
			$group->name = $i;
			$group->save();
		}
	}


	//формирет catalog_name по original_name:если в original_name не встречается chod_display,
	//то catalogn_name присваеивается original_name. Если встречается, то catalog_name попадает
	//часть original_name до первой встречи chod_display. Это делается потому что в некоторых прайсах в поле "наименовние"
	//продублирован ЧОД, который для отображения в каталоге надо исключить из catalog_name
	public function actionCreateCatalogName() {

		$connection=Yii::app()->db;

		$productsCount = $connection->createCommand('select count(*) from products')->queryScalar();

		$partLength = 200;

		for ($offset=0;$offset<$productsCount;$offset=$offset+$partLength)
		{
			$sql = "SELECT * FROM products ORDER BY id LIMIT ".$offset.",".$partLength.";";
			$command=$connection->createCommand($sql);
			$dataReader=$command->query();

			$whenThenPart = "";
			$ids = "";
			foreach ($dataReader as $row)
			{

				$chodInNamePos = strpos($row['original_name'], $row['chod_display']);
				if ($chodInNamePos===false) $catalog_name = $row['original_name'];
				else
				{
					$catalog_name = substr($row['original_name'],0,$chodInNamePos);
				}
				$catalog_name = trim($catalog_name);

				//var_dump($url);
				$whenThenPart = $whenThenPart." WHEN ".$row['id']." THEN '".$catalog_name."' ";
				if ($ids=="") $ids = $row['id'];
				else $ids = $ids.",".$row['id'];

			}


			$updateSql = "UPDATE products SET catalog_name = (CASE id".$whenThenPart." END) WHERE id IN(".$ids.");";
			$command=$connection->createCommand($updateSql);
			$res=$command->execute();
			echo $res."<br>";
		}
	}

	/**
	 * Рассчитать рейтинг всех товаров (количество заявок, в которых встречался товар)
	 *
	 */
	public function actionUpdateProductRating()
	{
		$connection=Yii::app()->db;

		$sql = "UPDATE products SET rating=0";
		$command = $connection->createCommand($sql);
		$command->execute();


		$sql = "SELECT product_id, COUNT( product_id ) AS rating
				FROM req_prods rp
				WHERE product_id IS NOT NULL
				GROUP BY rp.product_id";


		$command = $connection->createCommand($sql);
		$rows=$command->queryAll();

		if (count($rows)==0) {
			echo "Products not found in requisitons";
			return;
		}

		$when = "";
		$productIds = "";
		foreach ($rows as $row) {
			$when .= " WHEN id = ".$row['product_id']." THEN ".$row['rating'];
			if ($productIds!="") {
				$productIds .= ','.$row['product_id'];
			}
			else {
				$productIds = $row['product_id'];
			}
		}
		$when.= " END ";
		$productIds = '('.$productIds.')';

		$sql = "UPDATE products SET rating = CASE".
			   $when.
			   "WHERE id IN ".$productIds;
		$command = $connection->createCommand($sql);
		$res = $command->execute();

		echo "Raiting was updated in $res rows";
	}

	/**
	 * Записать в номера заявок их id
	 */
	public function actionSetRequisitionsNumsById() {
		$sql= "UPDATE requisitions SET num=id WHERE num=0";
        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);
        $res=$command->execute();
        echo "Updated $res strings";
	}

	public function actionTest()
	{
		// $fileUrl = "/catalogexport/".ExportCatalog::export();
		// echo "<a href=".$fileUrl.">скачать</a>";
		//ExportCatalog::updateUrl();

	}

}