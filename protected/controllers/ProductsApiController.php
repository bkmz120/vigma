<?php


class ProductsApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }    

    public function actionGetProductsToCatalogAjax()
	{				
	    $headers = array();
        array_push($headers, array("key"=>"name","name"=>"Наименование"));
        array_push($headers, array("key"=>"chod","name"=>"ЧОД"));
        array_push($headers, array("key"=>"code","name"=>"Код"));

        $result = array();
        $result['header'] = $headers;
        
       	$page = intval($_GET['page']);
      	$count = intval($_GET['count']);
      	$offset = $page*$count-$count;

      	$filters=array();
      	$filters['name'] = $_GET['name'];
      	$filters['chod'] = preg_replace('/[^a-zа-яё\d]/ui','',$_GET['chod']);

      	if (!isset($_GET['showHidden'])) $showHidden=false;
      	else if ($_GET['showHidden']==='1') $showHidden = true;
      	else if ($_GET['showHidden']==='0') $showHidden = false;
      	
      	$res = Products::model()->getProductsWithInputsForPagination($filters,$offset,$count,$showHidden);
      	
      	$result['rows'] = $res['products'];
      	$resultsCount = $res['resultsCount'];

		$pagination = array();
        $pagination['count'] = $count;
        $pagination['page'] = $page;
        $pagination['pages'] = ($resultsCount-$resultsCount%$count)/$count;
        $pagination['size'] = intval($resultsCount);

        $result['pagination'] = $pagination;
        $result['sort-by'] = $_GET['sort-by'];
        $result['sort-order'] = $_GET['sort-order'];
        $result['products-count'] = count($products);

		echo json_encode($result);
	}
	

	public function actionAddProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$product = new Products;
		$product->original_name = $data->original_name;
		$product->original_chod = preg_replace('/[^a-zа-яё\d]/ui','',$data->original_chod);
		$product->chod_display = $data->original_chod;
		$product->catalog_name = $data->original_name;
		$product->catalog_chod = $data->original_chod;
		$product->product_num = $data->product_num;
		$product->weight = $data->weight;
		$product->size = $data->size;
		$product->price_chtz = $data->price_chtz;

		$price_orig = $data->price_in;

		$res=$product->addNew($data->provider_id, $data->price_in,$price_orig, $price_date,$data->delivery_time);
		if ($res==false)
		{
			echo "false";
			return;
		}
	}

	public function actionDeleteProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));

		$res = Products::model()->deleteProduct($data->product_id);
		echo $res;
	}

	public function actionExportForCatalog()
	{
		/*
		$filename = "catalog-" . date("d-m-Y") . ".sql";
		$mime = "application/sql";

		header( "Content-Type: " . $mime );
		header( 'Content-Disposition: attachment; filename="' . $filename . '"' );

		$cmd = "mysqldump --user=root --password=MyVigaQl4p --host=localhost galina111_vi products input_requests catalogcats catalogcat_prods";   

		passthru( $cmd );
		*/
		//echo exec('mysqldump --user=root --password=MyVigaQl4p --host=localhost galina111_vi products input_requests > /var/www/html/photos/dump.sql');
		ExportCatalog::outputExport();
	}

	public function actionHideProductAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res= Products::model()->setHidden($data->product_id,$data->hidden);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}	

	public function actionCopyProductInfoFromTotAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		$res= Products::model()->copyProductInfoFromTo($data->fromProductId,$data->toProductId);
		if ($res==false)
		{
			echo "false";
			return;
		}
		echo "true";
	}
	
}