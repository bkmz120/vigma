<?php

	function copyr($source, $dest)
	{
	    // Check for symlinks
	    if (is_link($source)) {
	        return symlink(readlink($source), $dest);
	    }
	    
	    // Simple copy for a file
	    if (is_file($source)) {
	        return copy($source, $dest);
	    }

	    // Make destination directory
	    if (!is_dir($dest)) {
	        mkdir($dest);
	    }

	    // Loop through the folder
	    $dir = dir($source);
	    while (false !== $entry = $dir->read()) {
	        // Skip pointers
	        if ($entry == '.' || $entry == '..') {
	            continue;
	        }

	        // Deep copy directories
	        copyr("$source/$entry", "$dest/$entry");
	    }

	    // Clean up
	    $dir->close();
	    return true;
	}

	date_default_timezone_set('Europe/Moscow');

	echo "Copy  /var/www/html-upload to /var/www/html-tmp \r\n";
	$res = copyr("/var/www/html-upload","/var/www/html-tmp");
	if ($res) echo "Success!  \r\n\r\n";
	else
	{
		echo "Error!  \r\n";
		die();
	}	

	echo "Copy photos from  /var/www/html/photos to /var/www/html-tmp/photos \r\n";
	$res = copyr("/var/www/html/photos","/var/www/html-tmp/photos");
	if ($res) echo "Success!  \r\n\r\n";
	else
	{
		echo "Error!  \r\n";
		die();
	}	

	echo "Copy PMA from  /var/www/pma to /var/www/html-tmp/pma \r\n";
	$res = copyr("/var/www/pma","/var/www/html-tmp/pma");
	if ($res) echo "Success!  \r\n\r\n";
	else
	{
		echo "Error!  \r\n";
		die();
	}

	
	echo "Renaming /var/www/html/ to /var/www/html-".date('d-m-Y_H:i:s')."/  \r\n";
	$res = rename('/var/www/html/', '/var/www/html-'.date('d-m-Y_H:i:s').'/');
	if ($res) echo "Success! \r\n\r\n";
	else
	{
		echo "Error!  \r\n";
		die();
	}

	echo "Renaming /var/www/html-tmp to /var/www/html  \r\n";
	$res = rename('/var/www/html-tmp/', '/var/www/html/');
	if ($res) echo "Success!\r\n\r\n";
	else
	{
		echo "Error!";
		die();
	}
	

	

?>