angular.module('customerEdit',[]).directive('customeredit', ['$http','$window', function ($http,$window) {
    return {
		templateUrl : "/js/customers/customeredit.html",
		replace: false,
		scope:{
			visible:'=',
			customer:'=',
			newCustSaveCback:'=',
			statuses:'=',
		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {
				
				scope.$watch('visible',function(newVal, oldVal) {
					if (scope.visible==true) scope.customerEdit.show(scope.customer);
				});

				scope.customerEdit = {
					visible:false,		
					tmpCustomer:null,
					linkToCustomer:null,
					pageYOffset:null,

					show:function(customer)
					{
						var top = $window.pageYOffset + 20;

						scope.customerEdit.popupStyle = {
							top: top + 'px',
						}

						scope.customerEdit.pageYOffset = $window.pageYOffset;

						

						if (customer!=null)
						{	scope.customerEdit.linkToCustomer = customer;

							scope.customerEdit.tmpCustomer = angular.copy(customer);
							
						}
						else
						{
							scope.customerEdit.linkToCustomer = null;
							scope.customerEdit.tmpCustomer = {				
								id : null,
								name : "",
								status_id: null,
								hidden:0,
								full_name : "",
								contact_person : "",
								phone : "",
								email : "",
								address : "",
								legal_address : "",
								mail_address : "",
								bank_name : "",
								payment_number : "",
								bik : "",
								corr_number : "",
								bank_inn : "",
								director_name : "",
								director_job : "",
								confirm_doc : "",
							}
						}
						scope.customerEdit.visible = true;
						
					},

					close:function()
					{
						$window.scrollTo(0,scope.customerEdit.pageYOffset);
						scope.customerEdit.visible = false;
						scope.visible = false;
					},

					save:function()
					{
						var url = '/index.php?r=customersApi/saveCutomerAjax';
			 			$http.post(url,{
							customer:scope.customerEdit.tmpCustomer,		
						})
						.success(function (data) {
							if (data.status==true)
							{
								scope.customerEdit.tmpCustomer.status = "";
								for (var i=0;i<scope.statuses.length;i++)
								{
									if (scope.statuses[i].id==scope.customerEdit.tmpCustomer.status_id)
									{
										scope.customerEdit.tmpCustomer.status = scope.statuses[i].name;
										break;
									}
								}

								if (scope.customerEdit.linkToCustomer!=null)
								{
									scope.customerEdit.linkToCustomer.name = scope.customerEdit.tmpCustomer.name;
									scope.customerEdit.linkToCustomer.status_id = scope.customerEdit.tmpCustomer.status_id;
									scope.customerEdit.linkToCustomer.status = scope.customerEdit.tmpCustomer.status;
									scope.customerEdit.linkToCustomer.hidden = scope.customerEdit.tmpCustomer.hidden;
									scope.customerEdit.linkToCustomer.full_name = scope.customerEdit.tmpCustomer.full_name;
									scope.customerEdit.linkToCustomer.contact_person = scope.customerEdit.tmpCustomer.contact_person;
									scope.customerEdit.linkToCustomer.phone = scope.customerEdit.tmpCustomer.phone;
									scope.customerEdit.linkToCustomer.email = scope.customerEdit.tmpCustomer.email;
									scope.customerEdit.linkToCustomer.address = scope.customerEdit.tmpCustomer.address;
									scope.customerEdit.linkToCustomer.legal_address = scope.customerEdit.tmpCustomer.legal_address;
									scope.customerEdit.linkToCustomer.mail_address = scope.customerEdit.tmpCustomer.mail_address;
									scope.customerEdit.linkToCustomer.bank_name = scope.customerEdit.tmpCustomer.bank_name;
									scope.customerEdit.linkToCustomer.payment_number = scope.customerEdit.tmpCustomer.payment_number;
									scope.customerEdit.linkToCustomer.bik = scope.customerEdit.tmpCustomer.bik;
									scope.customerEdit.linkToCustomer.corr_number = scope.customerEdit.tmpCustomer.corr_number;
									scope.customerEdit.linkToCustomer.bank_inn = scope.customerEdit.tmpCustomer.bank_inn;
									scope.customerEdit.linkToCustomer.director_name = scope.customerEdit.tmpCustomer.director_name;
									scope.customerEdit.linkToCustomer.director_job = scope.customerEdit.tmpCustomer.director_job;
									scope.customerEdit.linkToCustomer.confirm_doc = scope.customerEdit.tmpCustomer.confirm_doc;
								}
								else
								{
									scope.customerEdit.tmpCustomer.id = data.customer_id;
									scope.customerEdit.tmpCustomer.reqNum = 0;
									scope.newCustSaveCback(scope.customerEdit.tmpCustomer);
								}		

								scope.customerEdit.close();
							}
							else 
							{
								alert("При сохранении возникла ошибка!");
							}
						});
					},
				}
      		},
	}    
}]);