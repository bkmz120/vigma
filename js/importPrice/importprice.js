var importApp=angular.module('importPrice', ['ngTasty','productInfoWin','ngFileUpload','datePicker','dndLists','ngDialog']);

// для liveSearch
importApp.directive('coincidentInputRequsets', function() {        
	return {
		template:"<div id='direc' class='coincidentInputRequsets'> {{output}}</div>",
		replace: true,
		scope:{
			items:'=',
			field:'@',
			value:'=',

		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {
					
					scope.output='';
					
					if (typeof scope.value != 'undefined')
					{
						
						for (var i=0;i<scope.items.length;i++)
						{
							
							//внесем в output только те inputRequests, в которых присутствует искомое значение, при условии что это не оригинальное название
							//(оно уже выведено)
							if (scope.items[i].original_item==0 && scope.items[i][scope.field]!=null && scope.items[i][scope.field].toLowerCase().indexOf(scope.value.toLowerCase())>-1 )
							{
								scope.output=scope.output + ' ' + scope.items[i][scope.field];

							}
						}
					}
      		},
	}
});

importApp.controller('importController', function($scope,$http, Upload, ngDialog, $interval, $window, $document) {

$scope.init = function()
{
	$scope.providers = providers;
	for (var i=0;i<$scope.providers.length;i++)
	{
		$scope.providers[i].selected = false;		
	}

	angular.element($window).bind("scroll", function() {
		if (this.pageYOffset >= 114) {
			$scope.providersList.fixedTop = true;
		} else {
			$scope.providersList.fixedTop = false;
		}
        $scope.$apply();
    });
}




$scope.providersList = {
	filter:{
    	name:"",
    },
    fixedTop:false,
	//movedIndex:null,

	// moved:function(index)
	// {
	// 	$scope.providers.splice(index, 1);
	// 	this.movedIndex = index;
	// },

	// test:function(models)
	// {
	// 	console.log(models);
	// 	//models.selected = provider
	// },

	inserted:function(event, index, item, external, type, itemType)
	{
		var newOrder = [];
		for (var i=0;i<$scope.providers.length;i++)
		{
			newOrder.push({
				id:$scope.providers[i].id,				
			});
		}

		var url = '/index.php?r=providersApi/changeProvidersOrderAjax';
		$http.post(url,{
			order:newOrder,		
		})
		.success(function (data) {
			if (data=="false") alert("Ошибка при сохранении порядка");
		});
		return item;
	},

	select:function(provider) {
		for (var i=0;i<$scope.providers.length;i++)
		{
			$scope.providers[i].selected = false;		
		}
		provider.selected = true;
	},

	delete:function(provider)
	{
		$scope.showConfirm("Удалить поставщика? ").then(function(){
			
			var url = '/index.php?r=providersApi/deleteProviderAjax';
 			$http.post(url,{
				provider_id:provider.id,		
			})
			.success(function (data) {
				if (data.status==true)
				{
					for (var i=0;i<$scope.providers.length;i++)
					{
						if ($scope.providers[i].id == provider.id)
						{
							$scope.providers.splice(i,1);
							break;
						}
					}				
				}
				else 
				{
					alert("При удалении возникла ошибка!");
				}
			});					
		}			
		);			
		
	}

	// addProvider:function()
	// {

	// },
}

//редактор информации о поставщике
$scope.providerEdit = {
	visible:false,		
	tmpprovider:null,
	linkToprovider:null,
	pageYOffset:null,

	show:function(provider)
	{
		var top = $window.pageYOffset + 20;


		$scope.providerEdit.popupStyle = {
			top: top + 'px',
		}

		$scope.providerEdit.pageYOffset = $window.pageYOffset;

		if (provider!=null)
		{	$scope.providerEdit.linkToprovider = provider;

			$scope.providerEdit.tmpprovider = angular.copy(provider);
			
		}
		else
		{
			$scope.providerEdit.linkToprovider = null;
			$scope.providerEdit.tmpprovider = {				
				id : null,
				name : "",
				status_id: null,
				hidden:0,
				full_name : "",
				contact_person : "",
				phone : "",
				email : "",
				address : "",
				legal_address : "",
				mail_address : "",
				bank_name : "",
				payment_number : "",
				bik : "",
				corr_number : "",
				bank_inn : "",
				director_name : "",
				director_job : "",
				confirm_doc : "",
			}
		}
		$scope.providerEdit.visible = true;
		
	},

	close:function()
	{
		console.log($scope.providerEdit.pageYOffset);
		$window.scrollTo(0,$scope.providerEdit.pageYOffset);
		console.log($window.pageYOffse);
		$scope.providerEdit.visible = false;
	},

	save:function()
	{	
		//если это новый поставщик, вычислим последний sort_num
		var sort_num = 0;
		for (var i=0;i<$scope.providers.length;i++)
		{
			if (parseInt($scope.providers[i].sort_num)>sort_num) sort_num = parseInt($scope.providers[i].sort_num);
		}

		$scope.providerEdit.tmpprovider.sort_num = sort_num++;

		if ($scope.providerEdit.tmpprovider.default_coef!=null)
		{
			$scope.providerEdit.tmpprovider.default_coef = $scope.providerEdit.tmpprovider.default_coef.replace(',','.');
		}

		if ($scope.providerEdit.tmpprovider.min_coef!=null)
		{
			$scope.providerEdit.tmpprovider.min_coef = $scope.providerEdit.tmpprovider.min_coef.replace(',','.');
		}

		if ($scope.providerEdit.tmpprovider.catalog_coef!=null)
		{
			$scope.providerEdit.tmpprovider.catalog_coef = $scope.providerEdit.tmpprovider.catalog_coef.replace(',','.');
		}

		var url = '/index.php?r=providersApi/saveProviderAjax';
			$http.post(url,{
			provider:$scope.providerEdit.tmpprovider,		
		})
		.success(function (data) {
			if (data.status==true)
			{
				if ($scope.providerEdit.linkToprovider!=null)
				{
					$scope.providerEdit.linkToprovider.name = $scope.providerEdit.tmpprovider.name;
					$scope.providerEdit.linkToprovider.default_coef = $scope.providerEdit.tmpprovider.default_coef;
					$scope.providerEdit.linkToprovider.min_coef = $scope.providerEdit.tmpprovider.min_coef;
					$scope.providerEdit.linkToprovider.catalog_coef = $scope.providerEdit.tmpprovider.catalog_coef;
					$scope.providerEdit.linkToprovider.hidden = $scope.providerEdit.tmpprovider.hidden;
					$scope.providerEdit.linkToprovider.full_name = $scope.providerEdit.tmpprovider.full_name;
					$scope.providerEdit.linkToprovider.contact_person = $scope.providerEdit.tmpprovider.contact_person;
					$scope.providerEdit.linkToprovider.phone = $scope.providerEdit.tmpprovider.phone;
					$scope.providerEdit.linkToprovider.email = $scope.providerEdit.tmpprovider.email;
					$scope.providerEdit.linkToprovider.address = $scope.providerEdit.tmpprovider.address;
					$scope.providerEdit.linkToprovider.legal_address = $scope.providerEdit.tmpprovider.legal_address;
					$scope.providerEdit.linkToprovider.mail_address = $scope.providerEdit.tmpprovider.mail_address;
					$scope.providerEdit.linkToprovider.bank_name = $scope.providerEdit.tmpprovider.bank_name;
					$scope.providerEdit.linkToprovider.payment_number = $scope.providerEdit.tmpprovider.payment_number;
					$scope.providerEdit.linkToprovider.bik = $scope.providerEdit.tmpprovider.bik;
					$scope.providerEdit.linkToprovider.corr_number = $scope.providerEdit.tmpprovider.corr_number;
					$scope.providerEdit.linkToprovider.bank_inn = $scope.providerEdit.tmpprovider.bank_inn;
					$scope.providerEdit.linkToprovider.director_name = $scope.providerEdit.tmpprovider.director_name;
					$scope.providerEdit.linkToprovider.director_job = $scope.providerEdit.tmpprovider.director_job;
					$scope.providerEdit.linkToprovider.confirm_doc = $scope.providerEdit.tmpprovider.confirm_doc;
				}
				else
				{
					$scope.providerEdit.tmpprovider.id = data.provider_id;
					$scope.providers.push($scope.providerEdit.tmpprovider);
					//$scope.providersList.select($scope.providerEdit.tmpprovider);
				}

				

				$scope.providerEdit.close();
			}
			else 
			{
				alert("При сохранении возникла ошибка!");
			}
		});
	},
}

//редактор шаблона импорта прайса
$scope.priceTemplateEdit = {
	visible:false,		
	tmpTemplate:null,
	templateValidation: {
		sheetNum:true,
		startRowNum:true,
		chodClm:true,
		nameClm:true,
		productNumClm:true,
		priceClm:true,
		weightClm:true,
	},
	linkToProvider:null,
	provider_id:null,
	templateType:"default",


	show:function(provider)
	{
		this.provider_id = provider.id;
		this.linkToProvider = provider;

		if (provider.price_template!=null)
		{	
			this.tmpTemplate = angular.copy(provider.price_template);
			
		}
		else
		{
			if (this.templateType == "default")
			{
				this.tmpTemplate = {				
					sheetNum:"",
					startRowNum:"",
					chodClm:"",
					nameClm:"",
					productNumClm:"",
					priceClm:"",
					weightClm:"",	
				}			
			}
			if (this.templateType == "smirnyagin")
			{
				this.tmpTemplate = {
					sheetCount:""
				}
			}
		}

		this.templateValidation = {
			sheetNum:true,
			startRowNum:true,
			chodClm:true,
			nameClm:true,
			productNumClm:true,
			priceClm:true,
			weightClm:true,
		}
		
		if (this.linkToProvider.id!=30)
		{
			this.templateType = "default";
		}
		//для смирнягина отдельный шаблон
		else
		{
			this.templateType = "smirnyagin";
		}

		this.visible = true;
		
	},

	close:function()
	{
		this.visible = false;
	},

	save:function()
	{
		if (this.templateType == "default")
		{
			this.templateValidation = {
				sheetNum:true,
				startRowNum:true,
				chodClm:true,
				nameClm:true,
				productNumClm:true,
				priceClm:true,
				weightClm:true,
			}

			var validate = true;
			
			if (this.tmpTemplate.sheetNum=="") {
				this.templateValidation.sheetNum = false;
				validate = false;
			}
			if (this.tmpTemplate.startRowNum=="") {
				this.templateValidation.startRowNum = false;
				validate = false;
			}
			if (this.tmpTemplate.chodClm=="") {
				this.templateValidation.chodClm = false;
				validate = false;
			}
			if (this.tmpTemplate.nameClm=="") {
				this.templateValidation.nameClm = false;
				validate = false;
			}
			if (this.tmpTemplate.priceClm=="") {
				this.templateValidation.priceClm = false;
				validate = false;
			}		

			if (!validate) return;

			if (this.tmpTemplate.weightClm=="")
			{
				this.tmpTemplate.weightClm = 0;
			}

			if (this.tmpTemplate.productNumClm=="")
			{
				this.tmpTemplate.productNumClm = 0;
			}
		}


		var url = '/index.php?r=providersApi/saveTemplateAjax';
		$http.post(url,{
			provider_id:this.provider_id,
			template:this.tmpTemplate
			//json_template:JSON.stringify($scope.priceTemplate.items),
		}).success(function (data) {
			if (data!="false")
			{
				$scope.priceTemplateEdit.updateTemplate();
			}
		});
	},

	updateTemplate:function(){
		if (this.linkToProvider.price_template!=null)
		{

			this.linkToProvider.price_template.sheetNum = this.tmpTemplate.sheetNum;
			this.linkToProvider.price_template.startRowNum = this.tmpTemplate.startRowNum;
			this.linkToProvider.price_template.chodClm = this.tmpTemplate.chodClm;
			this.linkToProvider.price_template.nameClm = this.tmpTemplate.nameClm;
			this.linkToProvider.price_template.productNumClm = this.tmpTemplate.productNumClm;
			this.linkToProvider.price_template.priceClm = this.tmpTemplate.priceClm;
			this.linkToProvider.price_template.weightClm = this.tmpTemplate.weightClm;
			this.linkToProvider.price_template.sheetCount = this.tmpTemplate.sheetCount;
		}
		else
		{
			this.linkToProvider.price_template = angular.copy(this.tmpTemplate);
		}
		
		this.visible = false;
	},
}

$scope.import = {
	visible:false,
	fileName:null,
	fileNameLabel:"файл не выбран",
	fileUploading:false,
	priceActualDate:null,
	priceImportCoef:"1.00",
	provider_id:null,

	importProcessing:false,
	importFinish:false,
	provider:null,

	stopCommentCheck:null, // сохраняется индентификатор таймера для проверки изменений в комментарии
	lastCommentValue:null,

	show: function(provider) {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
		    dd='0'+dd
		} 

		if(mm<10) {
		    mm='0'+mm
		} 

		

		this.priceActualDate = dd+'.'+mm+'.'+yyyy;
		this.provider_id = provider.id;
		this.provider = provider;
		this.importFinish = false;
		this.visible = true;

		this.lastCommentValue = this.provider.import_comment;
		this.stopCommentCheck = $interval(this.checkComment.bind(this),1000);
	},

	close: function() {
		if (!this.importProcessing) this.visible = false;
		if (this.importFinish) $window.scrollTo(0,$document[0].body.scrollHeight);
		$interval.cancel(this.stopCommentCheck);
		this.checkComment();
	},

	uploadFile: function (file) {
	$scope.uploading = true;
	    Upload.upload({
	        url: '/index.php?r=fileApi/UploadFileAjax',
	        data: {file: file}
	    }).then(function (resp) {
	        console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
	   		$scope.import.fileName = resp.config.data.file.name;
	   		$scope.import.fileNameLabel = resp.config.data.file.name;
	   		$scope.import.fileUploading = false;
	    }, function (resp) {
	        console.log('Error status: ' + resp.status);
	        $scope.import.fileNameLabel = "Ошибка загрузки: ".resp.status;
	       	$scope.import.fileUploading = false;
	    }, function (evt) {
	    	$scope.import.fileUploading = true;
	        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	        // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
	    }); 
	},

	startImport: function()
	{	
		this.priceImportCoef = this.priceImportCoef.replace(",", ".");
		
		$scope.result = null;
		this.importProcessing =true;
		var url = '/index.php?r=providersApi/importPriceAjax';
		$http.post(url,{
			provider_id:this.provider_id,
			fileName:this.fileName,
			priceImportCoef:this.priceImportCoef,
			priceActualDate:this.priceActualDate,			
		}).success(function (data) {
			
			if (data.status)
			{
				$scope.result = data.result;

				for (var i=0;i<$scope.result.productsWhichNeedProvider.length;i++)
				{
					$scope.result.productsWhichNeedProvider[i].needAdd=false;
					$scope.result.productsWhichNeedProvider[i].added=false;
				}

				for (var i=0;i<$scope.result.newProducts.length;i++)
				{
					$scope.result.newProducts[i].needAdd=false;
					$scope.result.newProducts[i].added=false;
				}				

				for (var i=0;i<$scope.providers.length;i++)
				{
					if ($scope.providers[i].id==$scope.import.provider_id)
					{
						$scope.providers[i].price_date = $scope.result.date;
						break;
					}
				}

				if ($scope.result.messages!=null) alert($scope.result.messages);
			}
			else 
			{
				alert("Ошибка во время импорта: " + data.message);
			}
			
			$scope.import.importProcessing = false;
			$scope.import.importFinish = true;


		}).error(function(data, status, headers, config) {
			$scope.import.importProcessing = false;
			alert(status);
		});
	},

	checkComment:function() 
	{
		if (this.provider.import_comment!=this.lastCommentValue)
		{
			this.lastCommentValue = this.provider.import_comment;
			console.log('changed');
			var self = this;

			var url = '/index.php?r=providersApi/changeImportCommentAjax';
			$http.post(url,{
				provider_id:self.provider.id,
				import_comment:self.provider.import_comment,
			})
			.success(function (data) {

			});
		}
	},
}


$scope.result = null;


$scope.updatedRows = {
	visible:false,
	show:function()
	{
		$scope.updatedRows.visible = true;
	},
	close:function()
	{
		$scope.updatedRows.visible = false;
	}
}

$scope.notNeedUpdateRows = {
	visible:false,
	show:function()
	{
		$scope.notNeedUpdateRows.visible = true;
	},
	close:function()
	{
		$scope.notNeedUpdateRows.visible = false;
	}
}

$scope.productsWhichNeedProvider = {
	visible:false,
	show:function()
	{
		$scope.productsWhichNeedProvider.visible = true;
	},
	close:function()
	{
		$scope.productsWhichNeedProvider.visible = false;
	},

	selectAll:function()
	{
		for (var i=0;i<$scope.result.productsWhichNeedProvider.length;i++)
		{
			if ($scope.result.productsWhichNeedProvider[i].added==false) $scope.result.productsWhichNeedProvider[i].needAdd=true;
		}
	},

	unSelectAll:function()
	{
		for (var i=0;i<$scope.result.productsWhichNeedProvider.length;i++)
		{
			$scope.result.productsWhichNeedProvider[i].needAdd=false;
		}
	},

	addProviderToSelected:{
		currentPosition:0,
		start:function()
		{
			//посчитаем количество отмененых
			var selectedCount = 0;
			for (var i=0;i<$scope.result.productsWhichNeedProvider.length;i++)
			{
				if ($scope.result.productsWhichNeedProvider[i].needAdd==true)
				{
					selectedCount++;
				}
			}

			$scope.progressBox.start(selectedCount);

			$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition = 0;
			$scope.productsWhichNeedProvider.addProviderToSelected.iteration(); 

			
		},

		iteration: function()
		{
			if ($scope.productsWhichNeedProvider.addProviderToSelected.currentPosition<$scope.result.productsWhichNeedProvider.length)
			{
				
				$scope.productsWhichNeedProvider.addProviderToSelected.request();			
			}
			else
			{
				$scope.progressBox.stop();
			}
		},

		request: function()
		{
			if ($scope.result.productsWhichNeedProvider[$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition].needAdd==true)
			{
				var productsWhichNeedProvider = $scope.result.productsWhichNeedProvider[$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition];
				var url = '/index.php?r=providersApi/AddProvideToProductAjax';
				$http.post(url,{
					provider_id:productsWhichNeedProvider.priceRow.provider_id,
					product_id:productsWhichNeedProvider.product_id,
					price_in:productsWhichNeedProvider.priceRow.price_in,
					price_orig:productsWhichNeedProvider.priceRow.price_orig,
					price_date:productsWhichNeedProvider.priceRow.priceActualDate,
					provChod:productsWhichNeedProvider.priceRow.chod,
					provName:productsWhichNeedProvider.priceRow.name,
					arrayIndex:$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition, //вернётся обратно
				})
				.success(function (data) {
					if (data!="false")
					{
						$scope.result.productsWhichNeedProvider[data].needAdd=false;
	 					$scope.result.productsWhichNeedProvider[data].added=true;	

						$scope.progressBox.next();
						$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition++;
						$scope.productsWhichNeedProvider.addProviderToSelected.iteration(); 	
					}
					else
					{
						alert("Ошибка добавления поставщика");
					}
				});
			}
			else 
			{
				$scope.productsWhichNeedProvider.addProviderToSelected.currentPosition++;
				$scope.productsWhichNeedProvider.addProviderToSelected.iteration(); 	
			}
		},
	},
}

$scope.dublikatChodProducts = {
	visible:false,
	show:function()
	{
		$scope.dublikatChodProducts.visible = true;
	},
	close:function()
	{
		$scope.dublikatChodProducts.visible = false;
	}
}

$scope.newProducts = {
	visible:false,
	show:function()
	{
		$scope.newProducts.visible = true;
	},
	close:function()
	{
		$scope.newProducts.visible = false;
	},

	selectAll:function()
	{
		for (var i=0;i<$scope.result.newProducts.length;i++)
		{
			if ($scope.result.newProducts[i].added==false) $scope.result.newProducts[i].needAdd=true;
		}
	},

	unSelectAll:function()
	{
		for (var i=0;i<$scope.result.newProducts.length;i++)
		{
			$scope.result.newProducts[i].needAdd=false;
		}
	},

	createNewProductsBySelected:{
		currentPosition:0,

		porovider:null,		

		start:function()
		{
			//посчитаем количество отмеченных
			var selectedCount = 0;
			for (var i=0;i<$scope.result.newProducts.length;i++)
			{
				if ($scope.result.newProducts[i].needAdd==true)
				{
					selectedCount++;
				}
			}

			//сохраним импортируемого поставщика, т.к. при создании нового товара понадобиться поле provider.chtzprice_rull			
			for (var i=0;i<$scope.providers.length;i++)
			{
				if ($scope.providers[i].id==$scope.import.provider_id) 
				{
					$scope.newProducts.createNewProductsBySelected.provider = $scope.providers[i];
					break;
				}
			}

			$scope.progressBox.start(selectedCount);

			$scope.newProducts.createNewProductsBySelected.currentPosition = 0;
			$scope.newProducts.createNewProductsBySelected.iteration(); 

			console.log($scope.result.newProducts.length);
		},

		iteration: function()
		{
			if ($scope.newProducts.createNewProductsBySelected.currentPosition<$scope.result.newProducts.length)
			{
				
				$scope.newProducts.createNewProductsBySelected.request();			
			}
			else
			{
				$scope.progressBox.stop();
			}
		},

		request: function()
		{
			if ($scope.result.newProducts[$scope.newProducts.createNewProductsBySelected.currentPosition].needAdd==true)
			{
				var newProduct = $scope.result.newProducts[$scope.newProducts.createNewProductsBySelected.currentPosition];
				//поле price_chtz заполнять только для поставщика ЧТЗ
				var price_chtz = null;
				if ($scope.newProducts.createNewProductsBySelected.provider.chtzprice_rull==1) price_chtz=newProduct.priceRow.price_orig;
				//console.log(newProduct.priceRow.price_in);
				newProduct.priceRow.price_in = parseFloat(newProduct.priceRow.price_in);
				var url = '/index.php?r=providersApi/AddNewProductAjax';
				$http.post(url,{
					original_name:newProduct.priceRow.name,
					original_chod:newProduct.priceRow.chod.toLowerCase().replace(/[^a-zа-яё0-9]/ig,''),
					chod_display:newProduct.priceRow.chod,
					provider_id:newProduct.priceRow.provider_id,
					price_in:newProduct.priceRow.price_in,
					price_orig:newProduct.priceRow.price_orig,
					price_date:newProduct.priceRow.priceActualDate,
					product_num:newProduct.priceRow.product_num,
					price_chtz:price_chtz,
					weight:newProduct.priceRow.weight,
					arrayIndex:$scope.newProducts.createNewProductsBySelected.currentPosition, //вернётся обратно
				})
				.success(function (data) {
					if (data!="false")
					{
						$scope.result.newProducts[data].needAdd=false;
						$scope.result.newProducts[data].added=true;

						$scope.progressBox.next();
						$scope.newProducts.createNewProductsBySelected.currentPosition++;
						$scope.newProducts.createNewProductsBySelected.iteration(); 	
					}
					else
					{
						alert("Ошибка создания товара");
					}
				});
			}
			else 
			{
				$scope.newProducts.createNewProductsBySelected.currentPosition++;
				$scope.newProducts.createNewProductsBySelected.iteration(); 	
			}
		},
	},
}

$scope.notInPriceFileProducts = {
	visible:false,
	show:function()
	{
		$scope.notInPriceFileProducts.visible = true;
	},
	close:function()
	{
		$scope.notInPriceFileProducts.visible = false;
	}
}


$scope.progressBox = {
	currentPosition:0,
	max:0,
	visible:false,
	finish:false,
	start:function(max)
	{	
		$scope.progressBox.finish = false;
		$scope.progressBox.currentPosition = 0;
		$scope.progressBox.max = max;
		$scope.progressBox.visible = true;
	},

	next:function()
	{
		$scope.progressBox.currentPosition++;
	},

	stop:function()
	{
		$scope.progressBox.finish = true;
	},

	close:function()
	{	
		$scope.progressBox.visible = false;
	},
} 


//
//для liveSearch
//
$scope.liveSearch = {

	visible:false,


	//инфа из priceRow т.е. то что хранится в одной строке прайса 
	price_in:0, 
	price_orig:0,
	price_date:null,
	product_num:null,
	priceName:null,
	priceChod:null,

	provider:null,

	tableRow:null, // стока из таблицы, для которой открылся liveSearch. Нужно, что бы при выборе
	               // товара поставить флаг added=true

	

	closeLiveSearch:function()
	{
		$scope.liveSearch.visible = false;
	},

	showLiveSearch:function(filterName,filterChod,priceRow,tableRow)
	{
		$scope.liveSearch.filter.name = filterName;
		$scope.liveSearch.filter.chod = filterChod;
		$scope.liveSearch.priceName = priceRow['name'];
		$scope.liveSearch.priceChod = priceRow['chod'];
		$scope.liveSearch.price_orig = priceRow['price_orig'];
		$scope.liveSearch.price_in = priceRow['price_in'];
		$scope.liveSearch.price_date = priceRow['priceActualDate'];
		$scope.liveSearch.product_num = priceRow['product_num'];
		$scope.liveSearch.weight = priceRow['weight'];

		$scope.liveSearch.tableRow = tableRow;
		
		for (var i=0;i<$scope.liveSearch.providers.length;i++)
		{
			if ($scope.liveSearch.providers[i].id==priceRow['provider_id'])
			{
				$scope.liveSearch.provider = $scope.liveSearch.providers[i];
				break;
			}
		}

		var windowTopOffset = $window.pageYOffset +20;
		angular.element(document.getElementById('liveSearch')).css('top',windowTopOffset+'px');
		
		$scope.liveSearch.visible = true;
	},



	addProduct:function()
	{
		var price_chtz = null;

		if ($scope.liveSearch.provider.chtzprice_rull==1) price_chtz=$scope.liveSearch.price_orig;

		
		var url = '/index.php?r=providersApi/AddNewProductAjax';
		$http.post(url,{
				original_name:$scope.liveSearch.priceName,
				original_chod:$scope.liveSearch.priceChod.toLowerCase().replace(/[^a-zа-яё0-9]/ig,''),
				chod_display:$scope.liveSearch.priceChod,
				provider_id:$scope.liveSearch.provider.id,
				price_in:$scope.liveSearch.price_in,
				price_orig:$scope.liveSearch.price_orig,
				price_date:$scope.liveSearch.price_date,
				product_num:$scope.liveSearch.product_num,
				price_chtz:price_chtz,
				weight:$scope.liveSearch.weight,
				arrayIndex:0, //вернётся обратно
			}).success(function (data) {
				if (data!="false")
				{
					$scope.liveSearch.tableRow.added = true;

					//обновить содержимое таблицы
					// $scope.liveSearch.filter = {
					// 	name:'',
					// 	chod:'',
					// };

					// setTimeout(function(){
					// 	$scope.liveSearch.filter = {
					// 		name:$scope.liveSearch.priceName,
					// 		chod:$scope.liveSearch.priceChod,
					// 	};
					// },1000);

					
					alert('Товар создан!');
					// $scope.liveSearch.filter.name="";
					// $scope.liveSearch.filter.chod="";
					// $scope.liveSearch.filter.name = $scope.liveSearch.priceName;
					// $scope.liveSearch.filter.chod = $scope.liveSearch.priceChod;

					
				}
				else
				{
					alert('Ошибка!');
				}
		});
	},

	selectProduct:function(product)
	{
		var url = '/index.php?r=providersApi/AddProvideToProductAjax';
			$http.post(url,{
				provider_id:$scope.liveSearch.provider.id,
				product_id:product.id,
				price_in:$scope.liveSearch.price_in,
				price_orig:$scope.liveSearch.price_orig,
				price_date:$scope.liveSearch.price_date,
				provChod:$scope.liveSearch.priceChod,
				provName:$scope.liveSearch.priceName,
				arrayIndex:0, //вернётся обратно
			}).success(function (data) {
				if (data!="false")
				{
					$scope.liveSearch.tableRow.added = true;
					$scope.liveSearch.visible = false;

					
				}
			});
	},

	clearFilter: function(type)
	{
		switch (type) {
			case 'name':
				$scope.liveSearch.filter.name = "";
				break;
			case 'chod':
				$scope.liveSearch.filter.chod = "";
				break;
		}
	},

	// запрос данных 
	getResource:function (params, paramsObj) {
		var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&' + params;
		return $http.get(urlApi).then(function (response) {
			//составим список синонимов для отбражения      
			return {
				 'rows': response.data.rows,
				 'header': response.data.header,
				 'pagination': response.data.pagination,
				 'sortBy': response.data['sort-by'],
				 'sortOrder': response.data['sort-order']
				}
			});
	},

	initTasty:{
		'count': 50,
		'page': 1,
		'sortBy': 'name',
		'sortOrder': 'dsc'
	},
	
	itemsPerPage:50,

	listItemsPerPage:[50,100, 500, 1000],

	filter:{
		name:'',
		chod:'',
	},

	providers:providers,	

	productInfoWindow:{
    	product:null,
    	show:false,
    	showProductsInfoWin:function(product)
    	{

    		$scope.liveSearch.productInfoWindow.product = product;
    		$scope.liveSearch.productInfoWindow.show = true;
    	},
    }
}

$scope.showConfirm = function(message) {
	$scope.showConfirmMessage = message;

		return	ngDialog.openConfirm({
				template: 'confirmTemplate.html',
		    className: 'ngdialog-theme-plain',
		    closeByDocument:false,
		    closeByEscape:false,
		    showClose:false,
		    scope: $scope,
		});
}


});