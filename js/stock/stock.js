var importStockApp=angular.module('importStock', ['ngTasty','productInfoWin','ngFileUpload']);

// для liveSearch
importStockApp.directive('coincidentInputRequsets', function() {        
	return {
		template:"<div id='direc' class='coincidentInputRequsets'> {{output}}</div>",
		replace: true,
		scope:{
			items:'=',
			field:'@',
			value:'=',

		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {
					
					scope.output='';
					
					if (typeof scope.value != 'undefined')
					{
						
						for (var i=0;i<scope.items.length;i++)
						{
							
							//внесем в output только те inputRequests, в которых присутствует искомое значение, при условии что это не оригинальное название
							//(оно уже выведено)
							if (scope.items[i].original_item==0 && scope.items[i][scope.field]!=null && scope.items[i][scope.field].toLowerCase().indexOf(scope.value.toLowerCase())>-1 )
							{
								scope.output=scope.output + ' ' + scope.items[i][scope.field];

							}
						}
					}
      		},
	}
});


importStockApp.controller('importController', function($scope,$http, Upload, $window) {

$scope.init = function(){
	var now = new Date();
	var month = now.getMonth()+1;
	var now_str = now.getDate() +  '.' +  month + '.' + now.getFullYear();
	$scope.import.date = now_str;
	console.log(stockCities);
}

$scope.showResults = false;

$scope.stockCities = {
	items:stockCities,
	value:1,
	change:function(){
		console.log($scope.stockCities.value);
	},
}

$scope.stockTemplate = {

	visible:false,
	stockCityId:null, //id в таблице prices (для нового шаблона он пустой)

	items:{
		name:"",
		priority:"",
		startrownum:"",
		chodnameclm:"",
		val1clm:"",
		val2clm:"",
		val3clm:"",
		price1clm:"",
		price2clm:"",
		price3clm:"",
	},

	itemsValid: {
		name:true,
		startrownum:true,
		chodnameclm:true,
		val1clm:true,
		val2clm:true,
		val3clm:true,
		price1clm:true,
		price2clm:true,
		price3clm:true,
	},

	save:function() {
		$scope.stockTemplate.itemsValid = {
			name:true,
			startrownum:true,
			chodnameclm:true,
			val1clm:true,
			val2clm:true,
			val3clm:true,
			price1clm:true,
			price2clm:true,
			price3clm:true,
		};

		var validate = true;

		if ($scope.stockTemplate.items.name=="") {
			$scope.stockTemplate.itemsValid.name = false;
			validate = false;
		}
		
		if ($scope.stockTemplate.items.startrownum=="") {
			$scope.stockTemplate.itemsValid.startrownum = false;
			validate = false;
		}
		if ($scope.stockTemplate.items.chodnameclm=="") {
			$scope.stockTemplate.itemsValid.chodnameclm = false;
			validate = false;
		}

		if ($scope.stockTemplate.items.val1clm=="") {
			$scope.stockTemplate.itemsValid.val1clm = false;
			validate = false;
		}
		

		if (!validate) return;

		if ($scope.stockTemplate.items.priority=="") $scope.stockTemplate.items.priority = 9999;

		
		var url = '/index.php?r=stocksApi/saveCityTemplateAjax';
		$http.post(url,{
			stockCityId:$scope.stockTemplate.stockCityId,

			json_template:JSON.stringify($scope.stockTemplate.items),
		}).success(function (data) {
			if (data!="false")
			{
				
				$scope.stockTemplate.stockCityId = data;					
				
				var newStockInfo = true;
				for (var i=0;i<$scope.stockCities.items.length;i++)
				{
					
					if ($scope.stockCities.items[i].id==$scope.stockTemplate.stockCityId)
					{
						
						$scope.stockCities.items[i] = $scope.stockTemplate.items;
						newStockInfo = false;
						break;
					}
				}

				if (newStockInfo)
				{
					$scope.stockTemplate.items.count = 0;
					$scope.stockCities.items.push($scope.stockTemplate.items);
				}
				
				$scope.stockTemplate.visible = false;
				
			}
		});
			
	},
		
	edit:function(stockCityInfo)
	{
		console.log(stockCityInfo);
		$scope.stockTemplate.stockCityId = stockCityInfo.id;
		$scope.stockTemplate.items = stockCityInfo;
		$scope.stockTemplate.visible = true;
	},
	
	

	clear:function(stockCity)
	{
		if (!confirm("Очистить значения для выбранного города?")) return;
		var url = '/index.php?r=stocksApi/clearCityTemplateAjax';
		$http.post(url,{
			stockCityId:stockCity.id,
		}).success(function (data) {
			if (data!="false")
			{
				for (var i=0;i<$scope.stockCities.items.length;i++)
				{
					if ($scope.stockCities.items[i].id == stockCity.id)
					{
						$scope.stockCities.items[i].count = 0;
						break;
					}
				}
				alert("Готово!");				
			}
			else
			{
				alert("Во время очистки произошла ошибка");
			}
		});
	},

	delete:function(stockCity)
	{
		if (!confirm("Удалить выбранный шаблон со всеми значениями?")) return;

		var url = '/index.php?r=stocksApi/deleteCityTemplateAjax';
		$http.post(url,{
			stockCityId:stockCity.id,
		}).success(function (data) {
			if (data!="false")
			{
				for (var i=0;i<$scope.stockCities.items.length;i++)
				{
					if ($scope.stockCities.items[i].id == stockCity.id)
					{
						$scope.stockCities.items.splice(i,1);
						break;
					}
				}

				$scope.stockTemplate.visible = false;
				
				alert("Готово!");				
			}
			else
			{
				alert("Во время удаления произошла ошибка");
			}
		});
	},


	cancel:function()
	{
		$scope.stockTemplate.visible = false;
	},
	

	new:function()
	{
		$scope.stockTemplate.stockCityId=null;
		$scope.stockTemplate.items={
			name:"",
			startrownum:"",
			chodnameclm:"",
			val1clm:"",
			val2clm:"",
			val3clm:"",
			price1clm:"",
			price2clm:"",
			price3clm:"",
			priority:"",
		},

		$scope.stockTemplate.visible = true;
	}

	
		
}


$scope.file = {
	name:null,
	loading:false,
	loaded:false,

	upload:function(file)
	{
		$scope.file.loading =  true;
		$scope.file.loaded = false;
		Upload.upload({
	        url: '/index.php?r=fileApi/UploadFileAjax',
	        data: {file: file}
	    }).then(function (resp) {
	        console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
	   		$scope.file.name = resp.config.data.file.name;
	   		$scope.file.loading = false;
	   		$scope.file.loaded = true;
	    }, function (resp) {
	        $scope.file.name = "Ошибка загрузки: ".resp.status;
	        $scope.file.loading = false;
	    }, function (evt) {
	        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	        // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
	     });
	},
}


$scope.import = {
	date:null,
	wait:false, // для показа анимации загрузки

	start:function()
	{
		$scope.showResults = false;
		$scope.import.wait = true;
		var stockCity_id = $scope.stockCities.value;
		var url = '/index.php?r=stocksApi/importStockAjax';
		$http.post(url,{
			fileName:$scope.file.name,//"smiryagin.xls",//"smiryagin.xls",//"test-chtz.xls",//"chaz.xls",//"Прайс КАТОК.xls",//$scope.fileName,
			importDate:$scope.import.date,
			stockCity_id:stockCity_id,
		}).success(function (data) {
			$scope.result = data;

			for (var i=0;i<$scope.result.oneProds.fileRows.length;i++)
			{
				$scope.result.oneProds.fileRows[i].added = false;
				$scope.result.oneProds.fileRows[i].needAdd = false;
			}

			$scope.showResults = true;
			$scope.import.wait = false;
			for (var i=0;i<$scope.stockCities.items.length;i++)
			{
				if ($scope.stockCities.items[i]['id']==stockCity_id)
				{
					$scope.stockCities.items[i]['date'] = $scope.import.date;
					$scope.stockCities.items[i]['count'] = $scope.result.count;
					break;
				}
			}
		});
	},
}

$scope.updatedProds = {
	visible:false,
	show:function()
	{
		$scope.updatedProds.visible = true;
	},
	close:function()
	{
		$scope.updatedProds.visible = false;
	}
}


$scope.notNeedUpdateProds = {
	visible:false,
	show:function()
	{
		$scope.notNeedUpdateProds.visible = true;
	},
	close:function()
	{
		$scope.notNeedUpdateProds.visible = false;
	}
}

$scope.oneProds = {
	visible:false,
	show:function()
	{
		$scope.oneProds.visible = true;
	},

	close:function()
	{
		$scope.oneProds.visible = false;
	},

	multiAddToProduct:{
		currentPosition:0,
		start:function()
		{
			//посчитаем количество отмеченых
			var selectedCount = 0;
			for (var i=0;i<$scope.result.oneProds.fileRows.length;i++)
			{
				if ($scope.result.oneProds.fileRows[i].needAdd==true)
				{
					selectedCount++;
				}
			}

			$scope.progressBox.start(selectedCount);

			$scope.oneProds.multiAddToProduct.currentPosition = 0;
			$scope.oneProds.multiAddToProduct.iteration(); 
			
		},

		iteration: function()
		{
			if ($scope.oneProds.multiAddToProduct.currentPosition<$scope.result.oneProds.fileRows.length)
			{
				
				$scope.oneProds.multiAddToProduct.request();			
			}
			else
			{
				$scope.progressBox.stop();
			}
		},

		request: function()
		{
			if ($scope.result.oneProds.fileRows[$scope.oneProds.multiAddToProduct.currentPosition].needAdd==true)
			{
				var fileRow = $scope.result.oneProds.fileRows[$scope.oneProds.multiAddToProduct.currentPosition];
				var url = '/index.php?r=stocksApi/newStockAjax';
				$http.post(url,{
					product_id:fileRow.product_id,
					stockcity_id:$scope.stockCities.value,
					chodname:fileRow.chodname,
					val1:fileRow.val1,
					val2:fileRow.val2,
					val3:fileRow.val3,
					price1:fileRow.price1,
					price2:fileRow.price2,
					price3:fileRow.price3,
				})
				.success(function (data) {

					if (data.status=="ok")
					{
						$scope.result.oneProds.fileRows[$scope.oneProds.multiAddToProduct.currentPosition].needAdd=false;
	 					$scope.result.oneProds.fileRows[$scope.oneProds.multiAddToProduct.currentPosition].added=true;	
					}
					else if (data.status=="error")
					{
						$scope.progressBox.addError("Остатки " + fileRow.chodname + " не приявязаны к товару.");
					}
					else if (data.status=="alreadyHaveThisCityStock")
					{
						$scope.progressBox.addNotice("Для товара " + data.info.prodcut_namechod + " уже привязан синоним из файла остаков выбранного города, " + fileRow.chodname + " - привязка отменена");
					}

					$scope.progressBox.next();
					$scope.oneProds.multiAddToProduct.currentPosition++;
					$scope.oneProds.multiAddToProduct.iteration();
				});
			}
			else 
			{
				$scope.oneProds.multiAddToProduct.currentPosition++;
				$scope.oneProds.multiAddToProduct.iteration(); 	
			}
		},
	},

	selectAll:function(){
		for (var i=0;i<$scope.result.oneProds.fileRows.length;i++)
		{
			$scope.result.oneProds.fileRows[i].needAdd=true;
		}
	},

	unSelectAll:function() {
		for (var i=0;i<$scope.result.oneProds.fileRows.length;i++)
		{
			$scope.result.oneProds.fileRows[i].needAdd=false;
		}
	},
}

$scope.severalProds = {
	visible:false,
	show:function()
	{
		$scope.severalProds.visible = true;
	},
	close:function()
	{
		$scope.severalProds.visible = false;
	},
}

$scope.notFoundProds = {
	visible:false,
	show:function()
	{
		$scope.notFoundProds.visible = true;
	},
	close:function()
	{
		$scope.notFoundProds.visible = false;
	},
}

$scope.notInFile = {
	visible:false,
	show:function()
	{
		$scope.notInFile.visible = true;
	},
	close:function()
	{
		$scope.notInFile.visible = false;
	},
}

$scope.progressBox = {
	currentPosition:0,
	max:0,
	visible:false,
	finish:false,
	errors:[],
	notices:[],
	start:function(max)
	{	
		$scope.progressBox.finish = false;
		$scope.progressBox.currentPosition = 0;
		$scope.progressBox.max = max;
		$scope.progressBox.visible = true;
	},

	next:function()
	{
		$scope.progressBox.currentPosition++;
	},

	stop:function()
	{
		$scope.progressBox.finish = true;		
	},

	close:function()
	{	
		$scope.progressBox.visible = false;
		$scope.progressBox.errors = [];
		$scope.progressBox.notices = [];
	},

	addError:function(text) 
	{

		$scope.progressBox.errors.push(text);
	},

	addNotice:function(text) 
	{

		$scope.progressBox.notices.push(text);
	},
} 

//
//для liveSearch
//
$scope.liveSearch = {

	visible:false,
	
	tableRow:null, // стока из таблицы, для которой открылся liveSearch. Нужно, что бы при выборе
	               // товара поставить флаг added=true

	closeLiveSearch:function()
	{
		$scope.liveSearch.visible = false;
	},

	showLiveSearch:function(tableRow)
	{
		$scope.liveSearch.filter.name = tableRow.original_name;
		$scope.liveSearch.filter.chod = tableRow.chod;

		$scope.liveSearch.tableRow = tableRow;
		
		var windowTopOffset = $window.pageYOffset +20;
		angular.element(document.getElementById('liveSearch')).css('top',windowTopOffset+'px');
		
		$scope.liveSearch.visible = true;
	},

	selectProduct:function(product)
	{
		var url = '/index.php?r=stocksApi/newStockAjax';
		$http.post(url,{
			product_id:product.id,
			stockcity_id:$scope.stockCities.value,
			chodname:$scope.liveSearch.tableRow.chodname,
			val1:$scope.liveSearch.tableRow.val1,
			val2:$scope.liveSearch.tableRow.val2,
			val3:$scope.liveSearch.tableRow.val3,
			price1:$scope.liveSearch.tableRow.price1,
			price2:$scope.liveSearch.tableRow.price2,
			price3:$scope.liveSearch.tableRow.price3,
		})
		.success(function (data) {

			if (data.status=="ok")
			{
				$scope.liveSearch.tableRow.needAdd=false;
				$scope.liveSearch.tableRow.added=true;
				$scope.liveSearch.visible = false;
			}
			else if (data.status=="error")
			{
				alert("Возникла ошибка! Остатки " + $scope.liveSearch.tableRow.chodname + " не приявязаны к товару.");
			}
			else if (data.status=="alreadyHaveThisCityStock")
			{
				alert("Для товара " + data.info.prodcut_namechod + " уже привязан синоним из файла остаков выбранного города, " + $scope.liveSearch.tableRow.chodname + " - привязка отменена");
			}
			
		});
	},

	clearFilter: function(type)
	{
		switch (type) {
			case 'name':
				$scope.liveSearch.filter.name = "";
				break;
			case 'chod':
				$scope.liveSearch.filter.chod = "";
				break;
		}
	},

	// запрос данных 
	getResource:function (params, paramsObj) {
		var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&' + params;
		return $http.get(urlApi).then(function (response) {
			//составим список синонимов для отбражения      
			return {
				 'rows': response.data.rows,
				 'header': response.data.header,
				 'pagination': response.data.pagination,
				 'sortBy': response.data['sort-by'],
				 'sortOrder': response.data['sort-order']
				}
			});
	},

	initTasty:{
		'count': 50,
		'page': 1,
		'sortBy': 'name',
		'sortOrder': 'dsc'
	},
	
	itemsPerPage:50,

	listItemsPerPage:[50,100, 500, 1000],

	filter:{
		name:'',
		chod:'',
	},	
	
}

$scope.productInfoWindow={
	product:null,
	show:false,
	providers:providers,
	showProductsInfoWin:function(product)
	{

		$scope.productInfoWindow.product = product;
		$scope.productInfoWindow.show = true;
	},
}

});