var myApp=angular.module('myApp', ['ngTasty','xeditable','angucomplete-alt','clickAnywhere','customerEdit','productInfoWin','ui.utils.masks','ngFileUpload','ngDialog']);

myApp.run(function(editableOptions) {
	editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default' (для edit in place модуля)
});



myApp.directive('coincidentInputRequsets', function() {        
	return {
		template:"<div id='direc' class='coincidentInputRequsets'> {{output}}</div>",
		replace: true,
		scope:{
			items:'=',
			field:'@',
			value:'=',

		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {
					
					scope.output='';
					if (typeof scope.value != 'undefined')
					{
						for (var i=0;i<scope.items.length;i++)
						{
							
							//внесем в output только те inputRequests, в которых присутствует искомое значение, при условии что это не оригинальное название
							//(оно уже выведено)
							if (scope.items[i].original_item==0 && scope.items[i][scope.field]!=null && scope.items[i][scope.field].toLowerCase().indexOf(scope.value.toLowerCase())>-1 )
							{
								scope.output=scope.output + ' ' + scope.items[i][scope.field];

							}
						}
					}
      		},
	}
});

myApp.directive('liveSearch', ['$compile','$http','$window',function($compile,$http,$window) {        
	return {
		templateUrl : "/js/requisitions/livesearch.html",
		replace: true,
		scope:{
			initName:'@',
			initChod:'@',
			position:'='
		},
		restrict: 'E',
		link:{
			pre:function (scope, element, attrs) {
					
					//при первоначальной загрузке, проверить находится ли блок поиска в пределах экрана и 
					//если нет, то проскроллить сраницу вверх
					scope.needCheckFocus = true;					
					
					scope.selectProduct = function(product)
					{
						scope.$parent.positions.selectProductForPostion(scope.position,product);
					}

					scope.showProductsInfoWin = function(product){
						scope.$parent.productInfoWindow.showProductsInfoWinByProduct(product);
						scope.needCheckFocus = true;
					}
					
					scope.liveSearch = {
				   	//ngTasty
						initTasty:{
							'count': 10,
							'page': 1,
							'sortBy': 'name',
							'sortOrder': 'dsc'
						},

						filter: {
							name:scope.initName,
							chod:scope.initChod,
						},						

						clearFilter: function(type)
						{
							switch (type) {
								case 'name':
									scope.liveSearch.filter.name = "";
									break;
								case 'chod':
									scope.liveSearch.filter.chod = "";
									break;
							}
							
							scope.needCheckFocus = true;
						},	

						getResource:function (params, paramsObj) {
							var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&showHidden=0&' + params; //showHidden=0 - не показыать скрытые
							return $http.get(urlApi).then(function (response) {
								
								if (scope.needCheckFocus)
								{
									scope.liveSearch.checkFocus();
									scope.needCheckFocus = false;
								}

								return {
									 'rows': response.data.rows,
									 'header': response.data.header,
									 'pagination': response.data.pagination,
									 'sortBy': response.data['sort-by'],
									 'sortOrder': response.data['sort-order']
								 }
							});
						},

						checkFocus:function() {
							var searchEl = angular.element(document.getElementsByClassName("liveProductSearch"));
							var infoEl = element[0].closest('.positions-table_info');
							setTimeout(function(){
								var rect = infoEl.getBoundingClientRect();
								var clientHeight = document.documentElement.clientHeight;
								var offset = clientHeight - (rect.bottom+30);
								
								if (offset<0)
								{
									//$window.scrollTo(0, window.pageYOffset - offset);
									$('html,body').animate({
						                scrollTop: window.pageYOffset - offset
						            }, 100);
								}
							},50);	
						},	
					}	
		   },

			    
		}
	}
}]);



myApp.factory('number_format', function() {
    return {
        thousands: function (value) {
        	if (typeof value == 'undefined' || value==null) return 0;
			return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		}
    };
});





/*
 * КОНТРОЛЛЕР
 *
 */

myApp.controller('customerController', function($scope, $rootScope, $http, $timeout) {



	$scope.customer = {
		requisitionCustomer:null,

		visible:false,

		customerListVisible:false,

		customersList:null,

		customerStatuses:null,

		customerListFilter:{
			name:"",
		},

		customerListListenOutside:false,

		loadCustomersListAndStauses:function() {
			var url = '/index.php?r=customersApi/getAllCustomersNamesAjax';
			$http.post(url)
			.success(function (data) {
				$scope.customer.customersList = data;				
			});

			var url = '/index.php?r=customersApi/GetAllStatusesAjax';
			$http.post(url)
			.success(function (data) {
				$scope.customer.customerStatuses = data;				
			});
		},

		changeVisible:function(){
			if ($scope.customer.visible==false)
			{
				$scope.customer.loadCustomersListAndStauses();
				$scope.customer.visible = true;
			}
			else
			{
				$scope.customer.visible = false;
			}
			
		},

		showCustomerList:function() {
			$scope.customer.customersListVisble = true;
			$timeout(function(){
					$scope.customer.customerListListenOutside = true;
				},
				1000);
		},

		closeCustomerList:function() {
			$scope.customer.customersListVisble = false;
			$scope.customer.customerListListenOutside = false;
		},

		selectForRequisition:function(customer)
		{
			var customer_id;
			if (customer!=null)
			{
				$scope.customer.requisitionCustomer = angular.copy(customer);
				customer_id = $scope.customer.requisitionCustomer.id;
			}
			else
			{
				$scope.customer.requisitionCustomer = null;
				customer_id = null;
			}
			
			$scope.customer.closeCustomerList();
			var url = '/index.php?r=requisitionApi/AttachCustomerToRequisitionAjax';

			$http.post(
				url,
				{
					requisition_id:requisition_id,
					customer_id:customer_id,
				}
			)
			.success(function (data) {
				if (data.status==true)
				{
					//alertSuccess("Заказчик сохранён!");
				}
				else
				{
					//alertDanger("Не удалось прикрепить заказчика к заявке!");
				}
								
			});	
		},

		unlinkFromReauisition:function()
		{
			$scope.customer.selectForRequisition(null);
		},

		showCustomerEdit:function()
		{
			if ($scope.customer.requisitionCustomer==null) return;

			var url = '/index.php?r=customersApi/getCustomerInfoAjax';
			$http.post(
				url,
				{
					id:$scope.customer.requisitionCustomer.id
				}
			)
			.success(function (data) {
				if (data.status==true)
				{
					$scope.customer.requisitionCustomer = data.customerInfo;
					$scope.customerEdit.show($scope.customer.requisitionCustomer);
				}
				else
				{
					//alertWarning("Не удалось загрузить информацию о заказчике.");
					alert("Не удалось загрузить информацию о заказчике.");
				}
								
			});			
		},

		newCustomerEdit:function()
		{
			$scope.customerEdit.show();
		},

		addCustomer:function(customer)
		{
			$scope.customer.customersList.push(customer);
			$scope.customer.selectForRequisition(customer);
		},


	}

	$scope.customerEdit = {
    	visible:false,
    	customer:null,
    	show:function(customer)
    	{
    		$scope.customerEdit.customer = customer;
    		$scope.customerEdit.visible = true;
    	}
    }

    $scope.init = function(){
    	if (initCustomer!=null)	$scope.customer.requisitionCustomer = initCustomer;
	}
});


myApp.controller('positionsController', function($scope, $rootScope, $http, $compile, $interval, Upload,number_format, $q, $window, ngDialog) {

	$scope.requisition_id = requisition_id; //переменная из vieww/requisition/requisition.php

	//для использования функции thousands во view
	$scope.thousands = number_format.thousands;

	//фикисировать шапку таблицы с позициями в заявке на верху
	$scope.positionsTableEl = document.getElementById('positionsTable');
	$scope.positionsHeaderElAng = angular.element(document.getElementById('positionsTableHead'));
	angular.element($window).bind("scroll", function() {
    	var box = $scope.positionsTableEl.getBoundingClientRect();
		if (box.top<=50) {
			$scope.positionsHeaderElAng.addClass('positionsTableHead__fixed');
		}
		else
		{
			$scope.positionsHeaderElAng.removeClass('positionsTableHead__fixed');
		}
	});

	$scope.providers =  providers;

	//загрузка заявки	
	$scope.loadRequisition = function()
	{
		if (itIsNewRequisition==0)
		{
			var listType = "first";
			
			$scope.positions.lastPositionId = -1; //в циклке ниже найдём максимальный position.id, он и будет lastPositionId

			for (var i=0;i<reqProds.length;i++)
			{	
				var price_outSum;
				if (reqProds[i]['price_outsum']!=null) {
					price_outSum = new Decimal(reqProds[i]['price_outsum']);
				}
				else {
					price_outSum = new Decimal(0);
					console.log('Has price_outSum==null:',reqProds[i]);
				}

				var position =
				{
					id:parseInt(reqProds[i]['position_id']),
					position_num:parseInt(reqProds[i]['position_num']),
					reqProdId:reqProds[i]['id'], //id соответствующей записи в таблице req_prod
					input:reqProds[i]['input_name'] + ' ' + reqProds[i]['input_chod'], //name и сhod не разделены
					inputName:reqProds[i]['input_name'],
					inputChod:reqProds[i]['input_chod'],
					product:reqProds[i]['product'],
					count:parseInt(reqProds[i]['count']),
					productsListVisible:false,
					listType:reqProds[i]['list_type'], //добавляем в активный в настоящий момент лист
					providersListVisible:false,
					provider:null,
					price_outSum:price_outSum,

					questionForPositionVisible:false,
					questionForPositionText:reqProds[i]['question_text'],
					questionForPositionStatus:reqProds[i]['question_status'],
					questionForPositionStatusCheck:true,

					changePriceInSum:$scope.positions.changePriceInSum, // что бы обновить сумму входящую из карточки товара
					calcSizeTotal:$scope.positions.calcSizeTotal, //что бы обновлять суммарный объём из карточки товара
				}					

			
				if (position.id>$scope.positions.lastPositionId) $scope.positions.lastPositionId=position.id;

				if (position.product.id==null) 
				{
					//что бы праильно сортировалось
					position.price_inSum = new Decimal(0); 
					position.price_inSumDisplay = (0).toFixed(2);
					position.price_coef = new Decimal(0); 
					position.price_coefDisplay = (0).toFixed(3); 
					position.price_out = new Decimal(0); 
					position.price_outDisplay = (0).toFixed(2);
					position.price_outSum = new Decimal(0); 
					position.price_outSumDisplay = (0).toFixed(2);
					position.weightSum = new Decimal(0); 
					position.weightSumDisplay = (0).toFixed(3);
					position.sizeSum = new Decimal(0); 
					position.sizeSumDisplay = 0;
					position.original_name = "яяяяя";
				}

				

				if (position.questionForPositionStatus==null) position.questionForPositionStatus="none";
				else if (position.questionForPositionStatus=="complite") position.questionForPositionStatusCheck=false;

				if (position.listType=="confirm" && listType=="first") listType = "confirm";


				if (position.product.id!=null){


					//если поставщик не задан, то в position.provider будет выбран поставщик по-умолчанию
					position.provider = $scope.positions.getProviderForPosition(position,reqProds[i]['provprod_id']);
					

					if (position.product.price_chtz!=null && typeof(position.product.price_chtz)!='undefined') position.product.price_chtz = new Decimal(position.product.price_chtz);
					else position.product.price_chtz = new Decimal(0);
					position.product.price_chtzDispaly = number_format.thousands(position.product.price_chtz.toFixed(2));
				
					position.price_inSum = position.provider.price_in.mul(position.count);
					position.price_inSumDisplay = number_format.thousands(position.price_inSum.toFixed(2));


					position.price_outSum = new Decimal(position.price_outSum);
					position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));

					position.price_out = position.price_outSum.div(position.count);
					position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));

					//если постащик ЧТЗ, то коэффициент рассчитывается по прайсу ЧТЗ
					// if (position.provider.id==1)
					// {
					// 	position.price_coef = position.price_out.div(position.product.price_chtz);
					// }
					// else
					// {
					// 	position.price_coef = position.price_out.div(position.provider.price_in);
					// }


					position.price_coef = $scope.positions.calcPriceCoefForPosition(position); 
					position.price_coefDisplay = position.price_coef.toFixed(3);

					
					if (position.product.weight!=null && typeof(position.product.weight)!='undefined') position.product.weight = new Decimal(position.product.weight);
					else position.product.weight = new Decimal(0);				

					position.weightSum = position.product.weight.mul(position.count);
					position.weightSumDisplay = position.weightSum.toFixed(3);


					var size = $scope.positions.getSizeForPosition(position);
					position.sizeSum = size.sizeSum;
					position.sizeSumDisplay = size.sizeSumDisplay;

					position.original_name = position.product.original_name;	
					
				}	
				
				$scope.positions.items.push(position);
				
			}

			$scope.positionsList.listType = listType;
			if (listType=="confirm") $scope.positionsList.confirmListNotActive=false;
			
			
			$scope.positions.calcAll();
			
		}

		//установка значения для коэффициента по-умолчанию
		for (var i=0;i<$scope.providers.length;i++)
		{
			if ($scope.providers[i].id==$scope.settingProviderCoef.providerId)
			{
				$scope.settingProviderCoef.coef = $scope.providers[i].default_coef;
				break;
			}
		}


		angular.element(document.querySelector(".app-loading")).css('display','none');
		
	}	
	
	
	$scope.searchPositions = {
		multiple: false,

	}

	$scope.positions = {
		items:[],
		order:
		{
			clmName:'position_num',  // orederValue всегда без знака
			orderValue:'position_num', // значение подставляемое в ng-repeat="position in positions.items | orderBy:positions.order.orderValue для задния сортировки
		},
		totalPriceIn:0,
		totalPriceInDisplay:0,
		totalPriceOut:0,
		totalPriceOutDispaly:0,
		totalPriceProc:0,
		totalPriceProcDisplay:0,
		totalWeight:0,
		totalWeightDisplay:0,

		lastPositionId:-1, // храним id последней добавленной позиции. При добавлении новой позиции
						  // её id будет lastPositionId+1. Этим обеспечивает уникальность id у всех позиций
						  // независимо от списка в котором она находится

		liveSearchScopes:{}, //хранит скопы livesearch , для вызова $destroy при закрытии livesearch

		multipleSearch:false,

		switchMultipleSearch: function() {
			this.multipleSearch = !this.multipleSearch;
			
		},

		setOrder: function(clmName, reverse) {

			if ($scope.positions.order.clmName==clmName)
			{
				//в этом случае надо поменять порядок сортировки
				if ($scope.positions.order.orderValue[0]=='-') $scope.positions.order.orderValue = $scope.positions.order.orderValue.substr(1);
				else $scope.positions.order.orderValue = '-' + $scope.positions.order.orderValue;
			}
			else
			{
				//задаём новый параметр сортировки
				$scope.positions.order.clmName = clmName;
				if (reverse==true) $scope.positions.order.orderValue = '-' + clmName;
				else $scope.positions.order.orderValue = clmName;
			}

			

			var headers= angular.element(document.getElementsByClassName("positionsTable_header"));
			headers.removeClass('positionsTable_header__selected');

			var headerSelector;
			if (clmName=="position_num") headerSelector="positionsTable_header-id";
			else if (clmName=="input") headerSelector="positionsTable_header-input";
			else if (clmName=="original_name") headerSelector="positionsTable_header-oroginalName";
			else if (clmName=="count") headerSelector="positionsTable_header-count";
			else if (clmName=="product.price_chtz") headerSelector="positionsTable_header-priceChtz";
			else if (clmName=="provider.name") headerSelector="positionsTable_header-provider";
			else if (clmName=="price_inSum") headerSelector="positionsTable_header-totalPriceIn";
			else if (clmName=="price_coef") headerSelector="positionsTable_header-coef";
			else if (clmName=="price_out") headerSelector="positionsTable_header-priceOut";
			else if (clmName=="price_outSum") headerSelector="positionsTable_header-totalPriceOut";
			else if (clmName=="weightSum") headerSelector="positionsTable_header-weight";

			var header = angular.element(document.getElementsByClassName(headerSelector));
			header.addClass('positionsTable_header__selected');
		},

		searchBtnClick: function() {

			var input = document.getElementById('currentInput_value').value;
			$scope.currentInput =  clearInputForbiddenChars(input);
			
			if (!this.multipleSearch)
			{
				if ((typeof($scope.currentInput) == 'undefined')) 
				{
					alertWarning('Ничего не введено!');
					return;
				}					

				this.addPosition();
			}
			else
			{
				if ((typeof(this.currentMultipleInput) == 'undefined')) 
				{
					alertWarning('Ничего не введено!');
					return;
				}

				var inputLines = this.currentMultipleInput.split("\n");

				var lines = [];
				
				for (var i=0;i<inputLines.length;i++)
				{
					var line = {
						count:1,
						text:inputLines[i],
					}

					lines.push(line);
				}
				// for (var i=0;i<inputLines.length;i++)
				// {
				// 	$scope.currentInput = inputLines[i];
				// 	$scope.positions.addPosition($scope.positions.lastPositionId+1);
				// 	$scope.positions.lastPositionId++;
				// }
				$scope.positions.multipleSearchQueue.start(lines);
			}
		},

		multipleSearchQueue:
		{
			lines:null,
			currentIndex:0,
			visible:false,
			finish:false,
			start:function(lines)
			{
				$scope.positions.multipleSearchQueue.lines = lines;
				$scope.positions.multipleSearchQueue.currentIndex = 0;
				$scope.positions.multipleSearchQueue.next();
				$scope.positions.multipleSearchQueue.finish = false;
				$scope.positions.multipleSearchQueue.visible = true;
			},

			next:function()
			{
				var input = $scope.positions.multipleSearchQueue.lines[$scope.positions.multipleSearchQueue.currentIndex]['text'];
				$scope.currentInput = clearInputForbiddenChars(input);
				var positionCount = $scope.positions.multipleSearchQueue.lines[$scope.positions.multipleSearchQueue.currentIndex]['count'];
				$scope.positions.multipleSearchQueue.currentIndex++;

				$scope.positions.lastPositionId++;
				
				$scope.positions.addPosition($scope.positions.lastPositionId,positionCount,true);				
			},

			stop:function()
			{
				$scope.positions.multipleSearchQueue.finish = true;
			},

			close:function()
			{
				$scope.positions.multipleSearchQueue.visible = false;
			},
		},

		searchFromFile:function(file){

		    Upload.upload({
		        url: '/index.php?r=requisitionApi/searFromFileAjax',
		        data: {file: file}
		    }).then(function (resp) {
		        if (!angular.isArray(resp.data)) {
		        	
		        	return;
		        }

		        var lines = [];

		        for (var i=0;i<resp.data.length;i++)
				{
					if (!isFinite(resp.data[i].count)) resp.data[i].count = 1;

					var line = {
						count:resp.data[i].count,
						text:resp.data[i].input,
					}
					
					lines.push(line);
				}

				$scope.positions.multipleSearchQueue.start(lines);
		   		
		    }, function (resp) {
		       alertDanger('Ошибка загрузки!');

		    }, function (evt) {
		        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		    });
		},

		//пометить позицию, над которой ведётся работа (редктируется), цветом
		setPositionActive:function(position) {
			for (var i=0; i<$scope.positions.items.length;i++)
			{
				$scope.positions.items[i].active = false;
			}
			position.active = true;
		},


		showliveProductSearch:function(position) {
			if (!position.productsListVisible)
			{
				//закроем другие liveSerch
				for (var i=0;i<$scope.positions.items.length;i++)
				{
					if ($scope.positions.liveSearchScopes[$scope.positions.items[i].id]!=null)
					{
						$scope.positions.destroyLiveProductSearch($scope.positions.items[i]);
						$scope.positions.items[i].productsListVisible = false;
						
					}
					

				}
				

				/* ###*/
				position.productsListVisible = !position.productsListVisible; 

				var liveSearchScope = $scope.$new(false); //изолированный скоп для передачи в директиву (изолированный что бы не хранить лишние данные в нём)
														 // этот скоп нужен что бы при закрытии liveSearch его уничтожить
				//position.liveSearchScope = liveSearchScope; 
				$scope.positions.liveSearchScopes[position.id] = liveSearchScope;
				liveSearchScope.position = position;

				var compiledDirective = $compile('<live-search position="position" init-name="' + position.inputName + '" init-chod="' + position.inputChod + '" ></live-search>');
				var directiveElement = compiledDirective(liveSearchScope);
				$('#liveProductSearch-'+position.id).append(directiveElement);
			
				
			}
			else 
			{
				position.productsListVisible = !position.productsListVisible; 
				$scope.positions.destroyLiveProductSearch(position);
			}
			//$scope.positions.setPositionActive(position);
		},

		destroyLiveProductSearch: function(position) {
			$('#liveProductSearch-'+position.id).empty();
			$scope.positions.liveSearchScopes[position.id].$destroy();
			$scope.positions.liveSearchScopes[position.id] = null;
			position.productsListVisible = false;
			
		},

		selectProductForPostion:function(position,product) //при нажатии кнопки добавить в liveSearch
		{
			position.product = product;

			//проверим закреплен ли синоним за этим товаром
			var notFound=true;
			for (var i=0;i<product.inputRequests.length;i++)
			{
				if (product.inputRequests[i].name==position.inputName && product.inputRequests[i].chod==position.inputChod.replace(/[^a-zа-яё0-9]/ig,'')) notFound=false;
			}
			//если такого синонима не закреплено затоваром, добавим его к товару, закрепим товар за позицией
			if (notFound)
			{
				var url = '/index.php?r=requisitionApi/addInputRequestToProductAjax';
				$http.post(url,{name:position.inputName, chod:position.inputChod, product_id:product.id}).success(function (data) {
						if (data!="false")
						{
							var newInputRequest = {id:data,name:position.inputName,chod:position.inputChod.replace(/[^a-zа-яё0-9]/ig,''),chod_display:position.inputChod,original_item:0};
							position.product.inputRequests.push(newInputRequest);
							alertSuccess('К товару добавлен синоним.');
						}
						else alertDanger('Во время добавления синонима произошли ошибки!');								

						
				});
			}

			//Установим поставщика и цену
			position.provider = $scope.positions.getProviderForPosition(position);

			//установим price_coef  для позиции как указан в коэффициенте по-умолчанию для выбранного постащика позиции
			//в данном случае выбранный постащик позиции - поставщик по-умолчанию
			position.price_coef = $scope.positions.getPriceCoefForPosition(position);
			position.price_coefDisplay=position.price_coef.toFixed(3);

			if (position.count==0) position.count = 1;
						

			if (position.product.price_chtz!=null && typeof(position.product.price_chtz)!='undefined') position.product.price_chtz = new Decimal(position.product.price_chtz);
			else position.product.price_chtz = new Decimal(0);
			position.product.price_chtzDispaly = number_format.thousands(position.product.price_chtz.toFixed(2));

			var size = $scope.positions.getSizeForPosition(position);
			position.sizeSum = size.sizeSum;
			position.sizeSumDisplay = size.sizeSumDisplay;

			// if (position.provider.id==1)
			// {
			// 	position.price_out = position.product.price_chtz.mul(position.price_coef);
			// }
			// else
			// {
			// 	position.price_out = position.provider.price_in.mul(position.price_coef);
			// }
			position.price_out = $scope.positions.calcPriceOutForPosition(position);
			position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));	

			position.price_outSum = position.price_out.mul(position.count);
			position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));	
		
			if (position.product.weight!=null && typeof(position.product.weight)!='undefined') position.product.weight = new Decimal(position.product.weight);
			else position.product.weight = new Decimal(0);

			position.weightSumDisplay = position.weightSum.toFixed(3);

			

			if (position.product.sizeSpace!=null && typeof(position.product.sizeSpace)!='undefined') position.product.sizeSpace = new Decimal(position.product.sizeSpace);
			else position.product.sizeSpace = new Decimal(0);
			position.sizeSumDisplay = position.sizeSum.toFixed(3);

			position.original_name = position.product.original_name;

			$scope.positions.calcAll();
			

			//закроем liveSearch
			//$scope.showliveProductSearch(position);

			//сохранить в заявке что за позицией закреплен этот товар 
			var url = '/index.php?r=requisitionApi/changeProductInPosition';
			$http.post(url,{
				reqProdId:position.reqProdId,
				productId:product.id,
				providerId:position.provider.id,
				provProdId:position.provider.provProd_id,
				price_outsum:position.price_outSum.toNumber(),
				count:position.count,
			})
				.success(function (data) {
					
					if (data=="false")
					{
						alertDanger('Во время добавления товара в позицию произошла ошибка!');
					}
					else
					{
						$scope.positions.destroyLiveProductSearch(position);
					}				

				});			
		},

		// возвращает объекст provider с информацией о постащике для позиции.
		// если задан аргумент provprodId, будет возвразен provider с соответствующим поставщиком
		// если не задан, то в provider будет поставщик по-умолчанию для данного товара
		// provprodId должен присутствоать в списке поставщиков для товара - product.provProds
		// поскольку provprodId для позиции хранится в таблице с заявкой, то при загрузке заявки
		// может возникнуть ситуация, когда provprodId сохраненный в заявке уже не закрпелен за товаром (если товар был отредактирован)
		// в этом случае provprodId не находится в product.provProds и функция возвратит первого поставщика из product.provProds
		// 
		// TODO: подсвечивать такие позиции
		//
		// возвращенный объект provider хранит также минимально допустимый коэффициент для данного поставщика - min_price_coef
		// и флаг указывающий на то что цену надо расчитывать по правилам ЧТЗ - chtzprice_rull 
		getProviderForPosition:function(position,provprodId)
		{	
			var provider;

			var setDefaultProvider = false;			
			if (provprodId==null)
			{	
				provprodId = position.product.defaultprovprod_id;
				setDefaultProvider = true;
			}
			

			var providerNotFound = true;
			for (var j=0;j<position.product.provProds.length;j++)
			{				
				if (position.product.provProds[j].id==provprodId)
				{
					var price_in = new Decimal(position.product.provProds[j].price_in);
					var prov_name;
					if (position.product.provProds[j].provider_name.length>8) prov_name=position.product.provProds[j].provider_name.substr(0,8) + '...';
					else prov_name = position.product.provProds[j].provider_name;
					
					var notInPrice = position.product.provProds[j].notInPrice==1 ? true : false;

					provider = 
					{
						id:position.product.provProds[j].provider_id,
						price_in:price_in,
						name:prov_name,
						provProd_id:position.product.provProds[j].id,
						notInPrice: notInPrice,
					}

					providerNotFound = false;
				}
			}
			//если почему-то не нашлось provProd c id == provprodId, выберем в качестве поставщика - первого поставщика
			//и уведомим об этом
			if (providerNotFound)
			{
				var price_in = new Decimal(position.product.provProds[0].price_in);
				if (position.product.provProds[0].provider_name.length>8) prov_name=position.product.provProds[0].provider_name.substr(0,8) + '...';
				else prov_name = position.product.provProds[0].provider_name;
				var notInPrice = position.product.provProds[0].notInPrice==1 ? true : false;
				provider = 
				{
					id:position.product.provProds[0].provider_id,
					price_in:new Decimal(position.product.provProds[0].price_in),
					name:prov_name,
					provProd_id:position.product.provProds[0].id,
					notInPrice:notInPrice,
				}

				if (setDefaultProvider)	alertWarning("Для " + position.product.original_name + " не установлен поставщик по-умолчанию!");
				else alertWarning("Для " + position.product.original_name + " не найден выбранный для позиции поставщик ");
			
				//так же надо сохранить изменения в БД
				var url = '/index.php?r=requisitionApi/updateReqProdAjax';
				$http.post(url,{reqProd_id:position.reqProdId,
								provider_id:provider.id,
								provprod_id:provider.provProd_id,
								count:position.count,
								price_outsum:position.price_outSum.toNumber(),
							}).success(function (data) {
					if (data!="false")
					{
						
					}
					else alertDanger('Ошибка сохранения поставщика!');
				});
			}

			

			//найдём минимально допустимый price_coef для данного поставщика (что бы подсвечивать значение коэффициент красным, когда оно меньше минимального)
			//а так же поставим флаг, chtzprice_rull, указывающий на другой способ расчета исходящей цены (по price_chtz вмесето price_in)
			var min_price_coef = 0;
			var chtzprice_rull = false;
			for (var i=0; i<$scope.providers.length;i++)
			{
				if (provider.id == $scope.providers[i].id)
				{
					min_price_coef = $scope.providers[i].min_coef;
					if ( $scope.providers[i].chtzprice_rull==1) chtzprice_rull = true;
					break;
				}
			}
			provider.min_price_coef = new Decimal(min_price_coef);
			provider.chtzprice_rull = chtzprice_rull;
			
			return provider;
		},

		//возвращает коээфициент по-уиолчанию для поставщика выбранного для позиции
		//если постащик не задан вернёт 1
		getPriceCoefForPosition:function(position) 
		{

			if (position.provider==null) return new Decimal(1);

			var price_coef = new Decimal(1);

			for (var i=0;i<$scope.providers.length;i++)
			{
				if ($scope.providers[i].id==position.provider.id) 
				{
					price_coef = new Decimal($scope.providers[i].default_coef);					
					break;
				}
			}


			return price_coef;
		},

		/**
		 * Возвращает объект size содеражщий рассчитанный объем одной единицы (sizeSpace),
		 * суммарный объем на позицию (sizeSum), отображение суммарного объема (sizeSumDispaly)
		 */
		getSizeForPosition:function(position)
		{
			var sizeSpace = new Decimal(0);
			//из поля size вытащим длину, ширину, высоту (они разделены ";")
			if (position.product.size!=null)
			{
				var sizes = position.product.size.split(';');
				var sizeL=sizes[0].replace(',','.');
				var sizeW=sizes[1].replace(',','.');
				var sizeH=sizes[2].replace(',','.');
				var size = sizeL*sizeW*sizeH*0.000001;
				if (isFinite(size)) sizeSpace = new Decimal(size);
				
			}

			var size = {
				sizeSpace:sizeSpace,
				sizeSum:sizeSpace.mul(position.count),
			}

			size.sizeSumDisplay = size.sizeSum.toFixed(3);

			return size;			
		},
		
		//показать/скрыть блок вопроса к позиции
		showQuestionForPosition:function (position)
		{
			position.questionForPositionVisible = !position.questionForPositionVisible;
			//$scope.positions.setPositionActive(position);
		},

		saveQuestionForPosition:function (position)
		{
			position.questionForPositionVisible = false;
			if (position.questionForPositionStatus=="none") {
						position.questionForPositionStatusCheck = true;
						position.questionForPositionStatus = "active";
			}

			

			var url = '/index.php?r=requisitionApi/updateQuestionReqProdAjax';
			$http.post(url,{reqProdId:position.reqProdId,questionText:position.questionForPositionText, questionStatus:position.questionForPositionStatus}).success(function (data) {
				if (data!="false")
				{
					
				}
				else alertDanger('Ошибка сохранения вопроса!');
			}); 
		},

		changeQuestionStatusForPosition:function (position)
		{
			if (position.questionForPositionStatusCheck)  position.questionForPositionStatus = "active";
			else position.questionForPositionStatus = "complite";
			var url = '/index.php?r=requisitionApi/updateQuestionReqProdAjax';
			$http.post(url,{reqProdId:position.reqProdId, questionStatus:position.questionForPositionStatus}).success(function (data) {
				if (data!="false")
				{

				}
				else alertDanger('Ошибка сохранения вопроса!');
			}); 
		},

		addToCommentQuestionForPosition:function (position)
		{
			var url = '/index.php?r=requisitionApi/addToCommentToProductAjax';
			$http.post(url,{productId:position.product.id,text:position.questionForPositionText }).success(function (data) {
				if (data!="false")
				{
					if (position.product.comment!=null) position.product.comment = position.product.comment + "\r\n" + position.questionForPositionText;
					else position.product.comment = position.questionForPositionText;
					alertSuccess('Добавлено в комментарии');

				}
				else alertDanger('Ошибка при удалении вопроса!');
			}); 
		},


		deleteQuestionForPosition:function (position)
		{
			position.questionForPositionText = "";
			var url = '/index.php?r=requisitionApi/deleteQuestionReqProdAjax';
			$http.post(url,{reqProdId:position.reqProdId}).success(function (data) {
				if (data!="false")
				{
					position.questionForPositionVisible = false;
					position.questionForPositionStatusCheck = true;
					position.questionForPositionStatus="none";


				}
				else alertDanger('Ошибка при удалении вопроса!');
			}); 
		},

		/*
		showProductsInfoWin:function (product)
		{
			$scope.productInfoWindow.showProductsInfoWin(product);
		},
		*/

		showProviderList:function(position)
		{
			if (position.product.provProds.length==1) return;
			position.providersListVisible = !position.providersListVisible;
			//$scope.positions.setPositionActive(position);
		},

		changePriceInSum:function(position)
		{
			//для пресчета вызовем changeCount()
			$scope.positions.changeCount(position);

		},

		changeProvder:function(position,provProd)
		{
			var prov_name;
			if (provProd.provider_name.length>8) prov_name=provProd.provider_name.substr(0,8) + '...';
			else prov_name = provProd.provider_name;

			position.provider= $scope.positions.getProviderForPosition(position,provProd.id);
			

			//установим price_coef  для позиции как указан в коэффициенте по-умолчанию для выбранного постащика позиции
			//в данном случае выбранный постащик позиции - поставщик по-умолчанию
			for (var i=0;i<$scope.providers.length;i++)
			{
				if ($scope.providers[i].id==position.provider.id) 
				{
					position.price_coef = new Decimal($scope.providers[i].default_coef);
					position.price_coefDisplay=position.price_coef.toFixed(3);
					break;
				}
			}

			position.price_inSum = position.provider.price_in.mul(position.count);
			position.price_inSumDisplay = number_format.thousands(position.price_inSum.toFixed(2));


			position.providersListVisible = false;
			$scope.positions.calcPriceInTotal();

			
			// if (position.provider.id==1)
			// {
			// 	position.price_out = position.product.price_chtz.mul(position.price_coef);
			// }
			// else
			// {
			// 	position.price_out = position.provider.price_in.mul(position.price_coef);
			
			// }
			position.price_out = $scope.positions.calcPriceOutForPosition(position);
			position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));	

			position.price_outSum = position.price_out.mul(position.count);
			position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));
			
			$scope.positions.savePriceOut(position);

			var url = '/index.php?r=requisitionApi/updateReqProdAjax';
			$http.post(url,{reqProd_id:position.reqProdId,
							provider_id:position.provider.id,
							provprod_id:position.provider.provProd_id,
							count:position.count,
							price_outsum:position.price_outSum.toNumber(),
						}).success(function (data) {
				if (data!="false")
				{
					
				}
				else alertDanger('Ошибка сохранения поставщика!');
			});
		},

		changeCount:function(position)
		{
			
			
				
			if (position.count==="") {
				return;
			}
			var count = Number(position.count);
			if (!angular.isNumber(count) || isNaN(count)) {
				console.log('Count must be a number!');
				position.count = 1;
				
			}


			if (position.product!=null && position.product.id!=null)
			{
				
				position.price_inSum = position.provider.price_in.mul(position.count);
				position.price_inSumDisplay = number_format.thousands(position.price_inSum.toFixed(2));

				$scope.positions.calcPriceInTotal();

				
				position.price_out = $scope.positions.calcPriceOutForPosition(position);

				position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));	
				
				position.price_outSum = position.price_out.mul(position.count);
				position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));

				$scope.positions.savePriceOut(position);


				position.weightSum = position.product.weight.mul(position.count);
				position.weightSumDisplay = position.weightSum.toFixed(3);

				var size = $scope.positions.getSizeForPosition(position);
				position.sizeSum = size.sizeSum;
				position.sizeSumDisplay = size.sizeSumDisplay;

				$scope.positions.calcWeightTotal();
				$scope.positions.calcSizeTotal();
			}
			else
			{
				position.provider = {
					id:null,
					provProd_id:null,
				}				
			}

			var url = '/index.php?r=requisitionApi/updateReqProdAjax';
			$http.post(url,{reqProd_id:position.reqProdId,
							provider_id:position.provider.id,
							provprod_id:position.provider.provProd_id,
							count:position.count,
							price_outsum:position.price_outSum.toNumber(),
						}).success(function (data) {
				if (data!="false")
				{
					
				}
				else alertDanger('Ошибка сохранения количества!');
			});
		},

		changePriceCoef:function(position)
		{
			if (position.price_coefDisplay=="") {
				return;
			}
			var price_coef = position.price_coefDisplay.replace(/\s+/g, '').replace(",", ".");//удалить пробелы и поменять запятую на точку
			price_coef = Number(price_coef);
			if (!angular.isNumber(price_coef) || isNaN(price_coef)) {
				console.log('price_coef must be a number!');
				position.price_coefDisplay = 1;
				price_coef = 1;				
			}		

			position.price_coef = new Decimal(price_coef);



			// if (position.provider.id==1)
			// {
			// 	position.price_out = position.product.price_chtz.mul(position.price_coef);
			// }
			// else
			// {
			// 	position.price_out = position.provider.price_in.mul(position.price_coef);
			// }
			position.price_out = $scope.positions.calcPriceOutForPosition(position);
			position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));

			position.price_outSum = position.price_out.mul(position.count);
			position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));

			$scope.positions.savePriceOut(position);
		},

		changePriceOut:function(position)
		{
			if (position.price_outDisplay=="") {
				return;
			}
			var price_out = position.price_outDisplay.replace(/\s+/g, '').replace(",", ".");//удалить пробелы и поменять запятую на точку
			price_out = Number(price_out);
			if (!angular.isNumber(price_out) || isNaN(price_out)) {
				console.log('price_out must be a number!');
				position.price_outDisplay = 1;
				price_out = 1;				
			}			
			position.price_out = new Decimal(price_out); 

			position.price_outSum = position.price_out.mul(position.count);
			position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));

			
			// if (position.provider.id==1)
			// {
			// 	position.price_coef = position.price_out.div(position.product.price_chtz);
			// }
			// else
			// {
			// 	position.price_coef = position.price_out.div(position.provider.price_in);
			// }

			position.price_coef = $scope.positions.calcPriceCoefForPosition(position);
			position.price_coefDisplay = position.price_coef.toFixed(3);
						
			$scope.positions.savePriceOut(position);			
		},		

		savePriceOut:function(position)
		{

			$scope.positions.calcPriceOutTotal();
			
			var url = '/index.php?r=requisitionApi/updateReqProdAjax';
			$http.post(url,{reqProd_id:position.reqProdId,
							provider_id:position.provider.id,
							provprod_id:position.provider.provProd_id,
							count:position.count,
							price_outsum:position.price_outSum.toNumber(),
						}).success(function (data) {
				if (data!="false")
				{
					
				}
				else alertDanger('Ошибка сохранения цены!');
			});
		},

		calcPriceOutForPosition:function(position)
		{
			var price_out;
			if (position.provider.chtzprice_rull)
			{
				price_out = position.product.price_chtz.mul(position.price_coef);
			}
			else
			{
				price_out = position.provider.price_in.mul(position.price_coef);
			}
			price_out = price_out.toDecimalPlaces(2);
			return price_out;
		},

		calcPriceCoefForPosition:function(position)
		{
			var price_coef;
			if (position.provider.chtzprice_rull)
			{
				price_coef = position.price_out.div(position.product.price_chtz);
			}
			else
			{
				price_coef = position.price_out.div(position.provider.price_in);
			}

			price_coef = new Decimal(price_coef.toFixed(3));

			return price_coef;
		},

		addPosition:function(positionId,count,multipleAdd) {
			
			//в качестве параметра positionId передается при множественном добалении позиций т.к. если не передавать его
			//то из-за асинзронного вызова addPosition(), id у всех позиций будет одинаков
			if (positionId === undefined)
			{
				positionId = $scope.positions.lastPositionId+1;
				$scope.positions.lastPositionId++;
			} 

			
			if (count==null) count=1;

			//если multipleAdd==true то значит нужно добавить несколько позиций
			//для этого функция addPosition в конце выполнения вызывает сама себя, пока $scope.positions.multipleSearchQueue.currentIndex < $scope.positions.multipleSearchQueue.lines.length 
			if (multipleAdd==null) multipleAdd = false;

			var position =
			{
				id:positionId ,//сквозной id позиции на все списки (listType)
				reqProdId:null, //id соответствующей записи в таблице req_prod
				position_num:0, // отображаемый номер позции
				input:$scope.currentInput, //name и сhod не разделены
				inputName:"",
				inputChod:"",
				product:null,
				count:count,
				productsListVisible:false,
				listType:$scope.positionsList.listType, //добавляем в активный в настоящий момент лист
				provider:null, //выбранный поставщик для этой позиции (по-умолчанию - тот, на которого ссылает provProd c id==product.defaultprovprod_id)
				providersListVisible:false,
				
				price_coef:new Decimal(1), // коэффиент, умножая на который вх. цены получаем исх.
				price_coefDisplay:(1).toFixed(3), //отображение коэффицента
				price_out: new Decimal(0), // исходящая цена позиции: price_in * price_coef
				price_outDisplay:(0).toFixed(2), //отображение price_out
				price_outSum: new Decimal(0), // исходящая цена позиции: price_in * price_coef
				price_outSumDisplay:(0).toFixed(2), //отображение price_out

				price_inSum:new Decimal(0), //входящая цена * количество
				price_inSumDisplay:(0).toFixed(2), //отображение price_inSum
				original_name:"яяяяя", //для сортировки по оригинальному названию (позиции,для которых не выбран товар будут в конце списка)
				weightSum:0,
				weightSumDisplay:(0).toFixed(3),

				questionForPositionVisible:false,
				questionForPositionText:"",
				questionForPositionStatus:"none", //active,complite
				qestionForPositionStatusCheck:true,

				changePriceInSum:$scope.positions.changePriceInSum, // что бы обновить сумму входящую из карточки товара
				calcSizeTotal:$scope.positions.calcSizeTotal, //что бы обновлять суммарный объём из карточки товара
			}

			
			var url = '/index.php?r=requisitionApi/searchPositionAjax';
			$http.post(url,{input:$scope.currentInput}).success(function (data) {
				if (data['product']!=null)
				{
					position.product = data['product'];

					position.provider = $scope.positions.getProviderForPosition(position);

					//установим price_coef  для позиции как указан в коэффициенте по-умолчанию для выбранного постащика позиции
					//в данном случае выбранный постащик позиции - поставщик по-умолчанию

					position.price_coef = $scope.positions.getPriceCoefForPosition(position);
					position.price_coefDisplay=position.price_coef.toFixed(3);
						

					if (position.product.price_chtz!=null && typeof(position.product.price_chtz)!='undefined') position.product.price_chtz = new Decimal(position.product.price_chtz);
					else position.product.price_chtz = new Decimal(0);
					position.product.price_chtzDispaly = number_format.thousands(position.product.price_chtz.toFixed(2));

					position.price_inSum = position.provider.price_in.mul(position.count);
					position.price_inSumDisplay = number_format.thousands(position.price_inSum.toFixed(2));

					// if (position.provider.id==1)
					// {
					// 	position.price_out = position.product.price_chtz.mul(position.price_coef);
					// }
					// else
					// {
					// 	position.price_out = position.provider.price_in.mul(position.price_coef);
					// }
					position.price_out = $scope.positions.calcPriceOutForPosition(position);
					position.price_outDisplay = number_format.thousands(position.price_out.toFixed(2));	

					position.price_outSum = position.price_out.mul(position.count);
					position.price_outSumDisplay = number_format.thousands(position.price_outSum.toFixed(2));									

					if (position.product.weight!=null && typeof(position.product.weight)!='undefined') position.product.weight = new Decimal(position.product.weight);
					else position.product.weight = new Decimal(0);

					position.weightSum = position.product.weight.mul(position.count);
					position.weightSumDisplay = position.weightSum.toFixed(3);

					if (position.product.sizeSpace!=null && typeof(position.product.sizeSpace)!='undefined') position.product.sizeSpace = new Decimal(position.product.sizeSpace);
					else position.product.sizeSpace = new Decimal(0);

					var size = $scope.positions.getSizeForPosition(position);
					position.sizeSum = size.sizeSum;
					position.sizeSumDisplay = size.sizeSumDisplay;

					position.original_name = position.product.original_name;
					
					
					
				}

				

				if (data['product']==null) 
				{
					//что бы праильно сортировалось
					//position.count = 0;
					position.price_coef = new Decimal(1);
					position.price_inSum = new Decimal(0);
					position.price_inSumDisplay = (0).toFixed(2);
					position.price_coef = new Decimal(0);
					position.price_coefDisplay = (0).toFixed(3); 
					position.price_out = new Decimal(0);
					position.price_outDisplay = (0).toFixed(2);
					position.price_outSum = new Decimal(0);
					position.price_outSumDisplay = (0).toFixed(2);
					position.weightSum = new Decimal(0);
					position.weightSumDisplay = (0).toFixed(3);
					position.sizeSum = 0;
					position.sizeSumDisplay = position.sizeSum.toFixed(3);
				}
				

				position.inputName = data['name'];
				position.inputChod = data['chod'];

				//вычислим номер позиции
				var lastPositionNum = 0;
				for (var i=0;i<$scope.positions.items.length;i++)
				{
					if ($scope.positions.items[i].listType==$scope.positionsList.listType)
					{
						if ($scope.positions.items[i].position_num>lastPositionNum) lastPositionNum = $scope.positions.items[i].position_num;
					}
				}
				position.position_num = lastPositionNum + 1;


				$scope.positions.items.push(position);			
				



				$scope.positions.calcAll();

				// сохраним изменения в БД
				var url = '/index.php?r=requisitionApi/addPositionToRequisitionAjax';
				var productId=null;
				var providerId = null;
				var provProd_id = null;

				if (position.product!=null) {
					productId = position.product.id;
					providerId = position.provider.id;
					provProd_id = position.provider.provProd_id;
				}				

				$http.post(url,{
						requisition_id:requisition_id,
						positionId:position.id,
						positionNum:position.position_num, 
						inputName:position.inputName,
						inputChod:position.inputChod,
						productId:productId,
						listType:position.listType,
						count:position.count,
						provider_id:providerId,
						provprod_id:provProd_id,
						priceOutSum:position.price_outSum.toNumber(),
					})
					.success(function (data) {
						if (data=="false")
						{
							alertDanger("Ошибка сохранения позиции в Базе Данных!");
						}
						else
						{
							position.reqProdId = data;	
												
						}

						if (multipleAdd)					
						{
							if ($scope.positions.multipleSearchQueue.currentIndex < $scope.positions.multipleSearchQueue.lines.length)
							{
								$scope.positions.multipleSearchQueue.next();
							}
							else
							{
								$scope.positions.multipleSearchQueue.stop();
							}
						}
					});
			});
			
 		},

 		unbindProductFromPosition:function(position) {
 			//$scope.positions.setPositionActive(position);
 			$scope.showConfirm("Отвязать товар от позиции?").then(
 				function(){

 					var url = '/index.php?r=requisitionApi/unbindProductFromPositionAjax';
		 			$http.post(url,{
						reqProdId:position.reqProdId,	
						
					})
					.success(function (data) {
						if (data.status==false)
						{
							alert("Ошибка при отвязке товара. Причина: "+ data.message);
						}
						else 
						{
							var productId = position.product.id;
							position.product= null;
							position.price_outsum = 0;
							position.provider_id = null;
							position.provprod_id = null;

							$scope.showConfirm("Удалить синоним " + position.inputName + " " + position.inputChod + " из товара?").then(function(){
 							 	var url = '/index.php?r=requisitionApi/removeInputRequetFromProductAfterUnbindingAjax';
					 			$http.post(url,{
									productId:productId,
									inputChod:position.inputChod,
									inputName:position.inputName,
								})
								.success(function (data) {
									if (data.status==false)
									{
										alert("Ошибка при удалении синонима. Причина: "+ data.message);
									}
									else
									{
										alertSuccess('Синоним удалён');
									}
								});							
 							});	
							
						}
					});
 				}			
 			);			
 		},

 		deletePosition:function(position) {
 			$scope.showConfirm("Удалить позицию?").then(
 				function(){
 					var delPositionNum = position.position_num;			

					//пересчитаем номера позиций
					$scope.positions.items.sort(function(a,b){
						if (a.position_num>=b.position_num) return 1;
						if (a.position_num<b.position_num) return -1;
					});

					
					var tmpNum = delPositionNum;
					for (var i=0;i<$scope.positions.items.length;i++)
					{
						if ($scope.positions.items[i].listType==$scope.positionsList.listType)
						{
							if ($scope.positions.items[i].position_num>delPositionNum)
							{
								$scope.positions.items[i].position_num = tmpNum;
								tmpNum++;
							}
						}
					}
					$scope.positions.items.splice($scope.positions.items.indexOf(position),1);

		 			var url = '/index.php?r=requisitionApi/deletePositionFromRequisitionAjax';
		 			$http.post(url,{
								reqProdId:position.reqProdId,
								
							})
							.success(function (data) {
									if (data==false)
									{
										alertDanger("Ошибка при удалении позиции из Базы Данных!");
									}
							});
					$scope.positions.calcAll();
					$scope.positions.updatePositionsNum($scope.positionsList.listType);
 				}			
 			);	
			
 		},

 		calcPriceInTotal:function() {
 			var totalPriceIn = new Decimal(0);
 			for (var i=0;i<$scope.positions.items.length;i++)
 			{
 				
 				if ($scope.positions.items[i].listType==$scope.positionsList.listType && $scope.positions.items[i].provider!=null)
 				{

 					totalPriceIn = totalPriceIn.plus($scope.positions.items[i].price_inSum);
 				}
 			}

 			$scope.positions.totalPriceIn = totalPriceIn;
 			$scope.positions.totalPriceInDisplay = number_format.thousands(totalPriceIn.toFixed(2));

 			$scope.positions.totalPriceProc = $scope.positions.totalPriceIn - $scope.positions.totalPriceOut;
 			$scope.positions.totalPriceProcDisplay = number_format.thousands($scope.positions.totalPriceProc.toFixed(2));	

 			var deferred = $q.defer();
 			deferred.resolve();

 			return deferred.promise;	
 		},

 		calcPriceOutTotal:function() {
 			var totalPriceOut = new Decimal(0);
 			for (var i=0;i<$scope.positions.items.length;i++)
 			{

 				if ($scope.positions.items[i].listType==$scope.positionsList.listType && $scope.positions.items[i].provider!=null) {
 					totalPriceOut = totalPriceOut.plus($scope.positions.items[i].price_outSum);
 				}
 				
 			}

 			$scope.positions.totalPriceOut = totalPriceOut;
 			$scope.positions.totalPriceOutDisplay = number_format.thousands(totalPriceOut.toFixed(2));

 			$scope.positions.totalPriceProc = $scope.positions.totalPriceOut - $scope.positions.totalPriceIn;
 			$scope.positions.totalPriceProcDisplay = number_format.thousands($scope.positions.totalPriceProc.toFixed(2));	
 			
 			var deferred = $q.defer();
 			deferred.resolve();

 			return deferred.promise;
 		},

 		// calcPriceProc:function() {
 			
 		// 	$scope.positions.totalPriceProc = $scope.positions.totalPriceIn - $scope.positions.totalPriceOut;
 		// 	$scope.positions.totalPriceProcDisplay = number_format.thousands($scope.positions.totalPriceProc.toFixed(2));	
 		// },

 		calcWeightTotal:function(){
 			var totalWeight = new Decimal(0);
 			for (var i=0;i<$scope.positions.items.length;i++)
 			{
 				if ($scope.positions.items[i].listType==$scope.positionsList.listType && $scope.positions.items[i].product!=null)
 					totalWeight = totalWeight.plus($scope.positions.items[i].weightSum);
 			}

 			$scope.positions.totalWeight = totalWeight;
 			$scope.positions.totalWeightDisplay = number_format.thousands(totalWeight.toFixed(3));
 		},

 		calcSizeTotal:function(){

 			var totalSize = new Decimal(0);
 			for (var i=0;i<$scope.positions.items.length;i++)
 			{
 				if ($scope.positions.items[i].listType==$scope.positionsList.listType && $scope.positions.items[i].product!=null)
 				{
 					totalSize = totalSize.plus($scope.positions.items[i].sizeSum);
 				}
 			}
 			
 			$scope.positions.totalSize = totalSize.toFixed(3);
 		},

 		calcAll:function() {
 			$scope.positions.calcPriceInTotal().then(function(){
 				$scope.positions.calcPriceOutTotal();
 			});
 			
 			$scope.positions.calcWeightTotal();
 			$scope.positions.calcSizeTotal();
 		},
		
		//сохранить порядковые номера всех позиций активного скписка
		updatePositionsNum:function (listType) {
			
			var positionNumInfos = [];	

			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.items[i].listType==listType)
				{
					var positionNumInfo = {
						num:$scope.positions.items[i].position_num,
						reqProdId:$scope.positions.items[i].reqProdId,
					}
					positionNumInfos.push(positionNumInfo);					
				}
			}

			var url = '/index.php?r=requisitionApi/updatePositionsNumsAjax';
 			$http.post(url,{
				positionNumInfos:positionNumInfos,
				
			})
			.success(function (data) {
					if (data=="false")
					{
						alertDanger("Ошибка при обновлении номера позиции из Базы Данных!");
					}
			});
		},

		fixNums:function() {

			$scope.positions.items.sort(function(a,b){
				if (a.id>=b.id) return 1;
				if (a.id<b.id) return -1;
			});

			var listType = "first";

			var num = 1;
			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.items[i].listType==listType)
				{

					$scope.positions.items[i].position_num = num;
					num++;
				}
			}

			$scope.positions.updatePositionsNum(listType);

			var listType = "confirm";
			var num = 1;
			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.items[i].listType==listType)
				{

					$scope.positions.items[i].position_num = num;
					num++;
				}
			}

			$scope.positions.updatePositionsNum(listType);
		},


		changePositionNum: function(position,newVal) {
			
			if (!isFinite(newVal) || newVal % 1 !== 0) return "Введите целое число";

			var newVal = parseInt(newVal);

			var countOfThisList = 0;
			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.items[i].listType==$scope.positionsList.listType)
				{
					countOfThisList++
				}
			}
			if (newVal>countOfThisList || newVal<=0) return "Введенное значение больше количества позиций в заявке";

			var lastVal = parseInt(position.position_num);
			
			$scope.positions.items.sort(function(a,b){
				if (a.position_num>=b.position_num) return 1;
				if (a.position_num<b.position_num) return -1;
			});

			var currentNum = 1;
			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.items[i].listType==$scope.positionsList.listType)
				{
					if ($scope.positions.items[i].position_num==lastVal)
					{
						$scope.positions.items[i].position_num = newVal;
						
					}

					else if (currentNum==newVal)
					{
						currentNum=currentNum+1;
						$scope.positions.items[i].position_num = currentNum;
						currentNum=currentNum+1;
					}

					else 
					{
						$scope.positions.items[i].position_num = currentNum;
						currentNum++;
					}					
					
				}
			}			

			//$scope.positions.setPositionActive(position);
			$scope.positions.updatePositionsNum($scope.positionsList.listType);

			return false; // что бы xeditable больше ничего не менял
		},	
		

	}

	//переключение списков позици первичный/подтвержденный
	$scope.positionsList = {
		listType:"first",
		confirmListNotActive:true,
		changeListType: function(value) {

			//закроем другие liveSerch
			for (var i=0;i<$scope.positions.items.length;i++)
			{
				if ($scope.positions.liveSearchScopes[$scope.positions.items[i].id]!=null)
				{
					$scope.positions.destroyLiveProductSearch($scope.positions.items[i]);
					$scope.positions.items[i].productsListVisible = false;					
				}
			}

			this.listType = value;
			$scope.positions.calcAll();
		},
		
		createConfirmList: function() {
			if ($scope.positions.items.length==0)
			{
				alertWarning('В первичной заявке нет ни одной позиции!');
				return;
			}			

			var firstListLength = $scope.positions.items.length;
			for (var i=0; i<firstListLength;i++)
			{	
				
				if ($scope.positions.liveSearchScopes[$scope.positions.items[i].id]!=null)  $scope.positions.destroyLiveProductSearch($scope.positions.items[i]); //закрыть liveSearch для этой позиции, если он был открыт
				var positionCopy = angular.copy($scope.positions.items[i]);
				//positionCopy.id = new Date().getTime();
				positionCopy.id = $scope.positions.lastPositionId+1;
				
				$scope.positions.lastPositionId++;
				positionCopy.listType = "confirm";
				$scope.positions.items.push(positionCopy);
				
				var url = '/index.php?r=requisitionApi/addPositionToRequisitionAjax';
				var productId=null;
				if ($scope.positions.items[i].product!=null) productId = $scope.positions.items[i].product.id;
				var providerId = null;
				if ($scope.positions.items[i].provider!=null) providerId = $scope.positions.items[i].provider.id;
				
				$http.post(url,{
						requisition_id:requisition_id,
						positionId:positionCopy.id,
						positionNum:positionCopy.position_num,
						inputName:positionCopy.inputName,
						inputChod:positionCopy.inputChod,
						productId:productId,
						listType:"confirm",
						count:positionCopy.count,
						provider_id:providerId,
						priceOutSum:positionCopy.price_outSum.toNumber(),
					})
					.success(function (data) {
							if (data=="false")
							{
								alertDanger("Ошибка сохранения позиции в Базе Данных!");
							}
							else
							{
								//сохраним reqProdId
								$scope.positions.items[$scope.positions.items.length-1].reqProdId = data;
							}
					});
			}

			this.confirmListNotActive=false;
			this.listType = 'confirm';
			$scope.positions.calcAll();

			//изменить статус заявки в БД
			var url = '/index.php?r=requisitionApi/changeRequisitionStatusAjax';
			$http.post(url,{
						requisition_id:requisition_id,
						status:"confirmed",
					})
					.success(function (data) {
							if (data=="false")
							{
								alertDanger("Ошибка сохранения статуса заявки в Базе Данных!");
							}
							else
							{
																
							}
					});
				
		}
	};

	//для автоподстановки в currentInputName
	$scope.selectedInput = function(selected) {
		
		if (typeof selected != 'undefined' && typeof selected.title != 'undefined')
		{
			$scope.currentInput = clearInputForbiddenChars(selected.title);
			$scope.positions.addPosition();
		}		
    };

    // установка значения коэффицинта цены для выбранного поставщика
    $scope.settingProviderCoef = {
    	providerId:1,
    	coef:1,
    	

    	changeProvider:function() {
    		for (var i=0;i<$scope.providers.length;i++)
    		{
    			if ($scope.providers[i].id==$scope.settingProviderCoef.providerId)
    			{
    				$scope.settingProviderCoef.coef = $scope.providers[i].default_coef;
    				break;
    			}
    		}
    	},

    	set:function() {
    		
    		var coef = $scope.settingProviderCoef.coef.toString().replace(",", "."); 
    		
    		for (var i=0;i<$scope.positions.items.length;i++)
    		{
    			if ($scope.positions.items[i].listType==$scope.positionsList.listType 
    				&& $scope.positions.items[i].provider!=null
    				&&	$scope.positions.items[i].provider.id == $scope.settingProviderCoef.providerId)
    			{
    				$scope.positions.items[i].price_coef = parseFloat(coef);
    				$scope.positions.items[i].price_coefDisplay = $scope.positions.items[i].price_coef.toFixed(3);
    				$scope.positions.changePriceCoef($scope.positions.items[i]);
    			}
    		}
    	},
    }

    $scope.productInfoWindow = {
    	product:null,
    	show:false,
    	position:null,
    	//актуально при вызове карточки в списке позиций, так как изменение в карточке цены поставщика
    	//влияет на цену позиции
    	showProductsInfoWinByPosition:function(position)
    	{
    		$scope.productInfoWindow.position = position;
    		$scope.productInfoWindow.product = position.product;
    		$scope.productInfoWindow.show = true;
    		//$scope.positions.setPositionActive(position);
    	},
    	//актально при вазове из liveSearch
    	showProductsInfoWinByProduct:function(product)
    	{

    		$scope.productInfoWindow.position = null;
    		$scope.productInfoWindow.product = product;
    		$scope.productInfoWindow.show = true;
    	},
    }

    
    $scope.export = {


    	//для отладкм экспорта
    	exportToXls: function() {
    		var url = '/index.php?r=requisitionApi/exportRequisitionToXls';
			$http.post(url,{
						requisition_id:requisition_id,
						list_type:$scope.positionsList.listType,
					})
					.success(function (data) {
							if (data=="false")
							{
								alertDanger("Ошибка при экспорте!");
							}
							else
							{
																
							}
					});
    	}
    }


    $scope.showConfirm = function(message) {
    	$scope.showConfirmMessage = message;

 		return	ngDialog.openConfirm({
 				template: 'confirmTemplate.html',
			    className: 'ngdialog-theme-plain',
			    closeByDocument:false,
			    closeByEscape:false,
			    showClose:false,
			    scope: $scope,
			});
    }


    /**
     * Очистить поле ввода поиска от недопустимых символов, например двойных кавычек
     */
    function clearInputForbiddenChars(inputValue) {
    	return inputValue.toString().replace(/"/g,"'");
    }


	// Всплывающие сообщения с информацией
	function alertTimeout(wait){
	    setTimeout(function(){
	        $('#alert_placeholder').children('.alert:first-child').remove();
	    }, wait);
	}

	function alertSuccess(message) {
	    $('#alert_placeholder').append('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>'+ message +'</h4></div>').hide().fadeIn(500);
	    alertTimeout(2000);
	}

	function alertDanger(message) {
	    $('#alert_placeholder').append('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>'+ message +'</h4></div>').hide().fadeIn(500);
	    alertTimeout(10000);
	}

	function alertWarning(message) {
	    $('#alert_placeholder').append('<div class="alert alert-warning fade in"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>'+ message +'</h4></div>').hide().fadeIn(500);
	    alertTimeout(10000);
	} 
	 

});


//JQUERY

$( document ).ready(function() {
  	


  	
});