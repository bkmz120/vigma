/**
 * Страница все заявки
 */
angular
    .module('AllRequisitions', ['Pagination','ngDialog'])
    .controller('AllRequisitionsController',AllRequisitionsController);

AllRequisitionsController.$inject = ['$scope'];

function AllRequisitionsController($scope) {
  var vm = this;
  vm.confirmedRequisitions = window.confirmedRequisitions;
  vm.confirmedRequisitionsTotalCount = window.confirmedRequisitionsTotalCount;
  vm.waitForConfirmRequisitions = window.waitForConfirmRequisitions;
  vm.waitForConfirmRequisitionsTotalCount = window.waitForConfirmRequisitionsTotalCount;
}