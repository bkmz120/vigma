var allCustomersApp=angular.module('allCustomers', ['customerEdit','ngTasty','ngDialog','xeditable']);

allCustomersApp.run(function(editableOptions) {
	editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default' (для edit in place модуля)
});

allCustomersApp.controller('allCustomersController', function($scope, $http, $window, ngDialog) {
	$scope.init = function()
	{
		$scope.customers = customers;
		$scope.statuses =statuses;

		for (var i=0;i<$scope.customers.length;i++)
		{
			$scope.customers[i].selected = false;
		}	

		angular.element($window).bind("scroll", function() {
			if (this.pageYOffset >= 114) {
				$scope.customersList.fixedTop = true;
			} else {
				$scope.customersList.fixedTop = false;
			}
            $scope.$apply();
        });
    }

    $scope.customerEdit = {
    	visible:false,
    	customer:null,
    	show:function(customer)
    	{
    		$scope.customerEdit.customer = customer;
    		$scope.customerEdit.visible = true;
    	}
    }

    $scope.customersList = {
    	filter:{
    		name:"",
    	},
    	fixedTop:false,

    	order: {
    		clmName:'name',
    		value:'name',
    	},

    	changeOrder:function(value)
    	{
    		// for (var i=0;i<$scope.customers.length;i++)
    		// {
    		// 	$scope.customers[i].forSort = {
    		// 		name:$scope.customers[i].name,
    		// 		reqNum:parseInt($scope.customers[i].reqNum),       				 				
    		// 	}

    		// 	if ($scope.customers[i].status=="") $scope.customers[i].forSort.status = "aaa";
    		// 	else $scope.customers[i].forSort.status = $scope.customers[i].status;
    		// }

    		// if ($scope.customersList.order.clmName==clmName)
    		// {
    		// 	if ($scope.customersList.order.value[0]=='-') $scope.customersList.order.value = 'forSort.' +  $scope.customersList.order.clmName;
    		// 	else $scope.customersList.order.value = '-forSort.' + $scope.customersList.order.clmName;
    		// }
    		// else
    		// {
    		// 	$scope.customersList.order.value = 'forSort.' + clmName;
    		// 	$scope.customersList.order.clmName = clmName;
    		// }

    		var clearValue;
    		if (value[0]=="-") clearValue = value.substr(1);
    		else clearValue = value;

    		var clearCurrentValue;

    		if ($scope.customersList.order.value[0]=="-") clearCurrentValue = $scope.customersList.order.value.substr(1);
    		else clearCurrentValue = $scope.customersList.order.value;

    		if (clearValue==clearCurrentValue) 
    		{
    			if ($scope.customersList.order.value[0]=="-")
    			{
    				$scope.customersList.order.value = clearCurrentValue;
    			}
    			else
    			{
    				$scope.customersList.order.value = "-" + clearCurrentValue;
    			}
    		}
    		else
    		{
    			$scope.customersList.order.value = value;
    		}

    		console.log($scope.customersList.order.value);
    	},

    	select:function(customer)
    	{
    		for (var i=0;i<$scope.customers.length;i++)
			{
				$scope.customers[i].selected = false;
			}				
    		customer.selected = true;

    	},

    	add:function(customer)
    	{
    		$scope.customers.push(customer);
			$scope.customersList.select(customer);
    	},

    	delete:function(customer)
    	{
    		$scope.showConfirm("Удалить контрагента? ").then(
 				function(){
 					var url = '/index.php?r=customersApi/deleteCutomerAjax';
 					$http.post(url,{
 						customer_id:customer.id,
 					}).success(function (data) {
							if (data.status==true)
							{
								for (var i=0;i<$scope.customers.length;i++)
								{
									if ($scope.customers[i].id == customer.id)
									{
										$scope.customers.splice(i,1);
										break;
									}
								}				
							}
							else 
							{
								alert("При удалении возникла ошибка!");
							}
						});					
 				}			
 			);			
    		
    	}
    }

	$scope.statusesEdit = {
		visible:false,
		show:function() {
			$scope.statusesEdit.visible = true;
			
		},

		close:function() {
			$scope.statusesEdit.visible = false;
		},

		updateStatus:function(status) {
			var url = '/index.php?r=customersApi/updateCustomerStatusAjax';
 			$http.post(url,{
				id:status.id,
				name:status.name,	
			})
			.success(function (data) {
				if (data.status==true)
				{
					for (var i=0;i<$scope.customers.length;i++)
					{
						if ($scope.customers[i].status_id == status.id)
						{
							$scope.customers[i].status = status.name;
						}
					}	
				}
				else 
				{
					alert("При сохранении изменений возникла ошибка!");
				}
			});		
		},

		deleteStatus:function(status)
		{
			$scope.showConfirm("Удалить статус? ").then(
 				function(){
 					var url = '/index.php?r=customersApi/deleteCustomerStatusAjax';
		 			$http.post(url,{
						id:status.id,				
					})
					.success(function (data) {
						if (data.status==true)
						{
							for (var i=0;i<$scope.statuses.length;i++)
							{
								if ($scope.statuses[i].id==status.id)
								{
									$scope.statuses.splice(i,1);
									break;
								}
							}

							for (var i=0;i<$scope.customers.length;i++)
							{
								if ($scope.customers[i].status_id == status.id)
								{
									$scope.customers[i].status_id = null;
									$scope.customers[i].status = "";
								}
							}	
						}
						else 
						{
							alert("При удалении возникла ошибка!");
						}
					});
 				}			
 			);
				
		},

		addStatus:function()
		{
			var url = '/index.php?r=customersApi/createCustomerStatusAjax';
 			$http.post(url,{
							
			})
			.success(function (data) {
				if (data.status==true)
				{
					$scope.statuses.push(data.customerStatus);
				}
				else 
				{
					alert("При удалении возникла ошибка!");
				}
			});	
		},
	}

	$scope.showConfirm = function(message) {
    	$scope.showConfirmMessage = message;

 		return	ngDialog.openConfirm({
 				template: 'confirmTemplate.html',
			    className: 'ngdialog-theme-plain',
			    closeByDocument:false,
			    closeByEscape:false,
			    showClose:false,
			    scope: $scope,
			});
    }

		
});
