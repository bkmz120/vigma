angular.module('groups').controller('catalogcatsController', function($scope,$http) {

	$scope.init = function(){
		$scope.groups.init(groups);
		$scope.catalogcats.init(catalogcats);		
	}

	$scope.groups = {
		items:null,
		itemsById:null,

		init:function(items) 
		{
			$scope.groups.items = items;
			$scope.groups.itemsById = [];
			$scope.groups.generateItemsById();	
		},

		generateItemsById:function()
		{
			for (var i=0;i<$scope.groups.items.length;i++)
			{
				var id = $scope.groups.items[i].id;
				$scope.groups.itemsById[id] = angular.copy($scope.groups.items[i]);
			}
		},

		getById:function(id)
		{
			return $scope.groups.itemsById[id];
		},

	}

	$scope.catalogcats = {
		items:null,

		init:function(catalogcats)
		{
			$scope.catalogcats.items = catalogcats;
			$scope.catalogcats.setGroupsByGroupIds();
		},

		setGroupsByGroupIds:function()
		{
			for (var i=0;i<$scope.catalogcats.items.length;i++)
			{
				$scope.catalogcats.items[i].groups = $scope.catalogcats.getGroupsByGroupIdsForItem($scope.catalogcats.items[i].group_ids);
			}
		},

		getGroupsByGroupIdsForItem:function(group_ids)
		{
			var groups = [];
			for (var i=0;i<group_ids.length;i++)
			{
				var group = $scope.groups.getById(group_ids[i]);
				groups.push(group);
			}
			return groups;
		},

		addGroupToCatalogcat:function(catalogcat,group)
		{
			//проверка была ли группа уже привязана к категории
			if (catalogcat.group_ids!=null && angular.isArray(catalogcat.group_ids))
			{

				var groupFound = false;

				for (var i=0;i<catalogcat.group_ids.length;i++)
				{

					if (catalogcat.group_ids[i]==group.id)
					{
						groupFound = true;
						break;
					}
				}
				if (groupFound) {
					alert ("Эта группа уже присвоена!");
					return;
				}
			}

			var url = '/index.php?r=groupsApi/addGroupToCatalogcatAjax';
			$http.post(url,{
				catalogcatId:catalogcat.id,
				groupId:group.id,
			}).then(function (response) {
				var data = response.data;
				if (data.status)
				{
					if (catalogcat.group_ids!=null && angular.isArray(catalogcat.group_ids))
					{
						
						catalogcat.group_ids.push(group.id);
						catalogcat.groups.push(group);
					}
					else
					{
						catalogcat.group_ids = [group.id];
						catalogcat.groups = [group];						
					}
				}
				else
				{
					alert('При привязке группы возникла ошибка: '+ data.message);
				}
				
			});
		},

		removeGroupFromCatalogcat:function(catalogcat,group)
		{
			var url = '/index.php?r=groupsApi/removeGroupFromCatalogcatAjax';
			$http.post(url,{
				catalogcatId:catalogcat.id,
				groupId:group.id,
			}).then(function (response) {
				var data = response.data;
				if (data.status)
				{
					for (var i=0;i<catalogcat.group_ids.length;i++)
					{
						if (catalogcat.group_ids[i]==group.id)
						{
							catalogcat.group_ids.splice(i,1);
							catalogcat.groups.splice(i,1);
						}
					}
				}
			});
		},

		showProducts:function(catalogcat)
		{
			var url = '/index.php?r=groupsApi/getPtoductsByCatalogcatAjax';
			$http.post(url,{
				catalogcatId:catalogcat.id,
				
			}).then(function (response) {
				var data = response.data;
				if (data.status)
				{
					$scope.productsByCat.show(catalogcat.name,data.products);
				}
			});
		}
	}

	$scope.productsByCat = {
		visible:false,
		products:null,
		catalogcatName:null,

		show:function(catalogcatName,products)
		{
			$scope.productsByCat.products = products;
			$scope.productsByCat.catalogcatName = catalogcatName;
			$scope.productsByCat.visible = true;
		},

		close:function() 
		{
			$scope.productsByCat.visible = false;
		},
	}

	$scope.productInfoWindow={
		product:null,
		show:false,
		providers:providers,
		showProductsInfoWin:function(product)
		{
			$scope.productInfoWindow.show = false;
			$scope.productInfoWindow.product = product;
			$scope.productInfoWindow.show = true;
		},
	}

});
