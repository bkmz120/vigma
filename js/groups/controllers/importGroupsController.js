angular.module('groups').controller('importGroupsController', function($scope,$http, Upload,Waitbox, ngDialog) {
	$scope.importGroups	= {

		result:null,		

		file: {
			name:null,//"файл не выбран",
			loading:false,
			loaded:false,

			upload:function(file)
			{
				

				$scope.importGroups.file.loading =  true;
				$scope.importGroups.file.loaded = false;
				Upload.upload({
			        url: '/index.php?r=fileApi/UploadFileAjax',
			        data: {file: file}
			    }).then(function (resp) {
			        $scope.importGroups.file.name = resp.config.data.file.name;
			   		$scope.importGroups.file.loading = false;
			   		$scope.importGroups.file.loaded = true;
			    }, function (resp) {
			        $scope.file.name = "Ошибка загрузки: ".resp.status;
			        $scope.file.loading = false;
			    }, function (evt) {
			        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			        // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
			     });
			},
		},

		startImport:function() {
			//$scope.importGroups.importProcessing = true;
			Waitbox.show("Импорт групп...");
			var url = '/index.php?r=groupsApi/importGroupsAjax';
			$http.post(url,{
				fileName: $scope.importGroups.file.name,
			}).then(function (response) {
				var data = response.data;
				if (data.status)
				{

					for (var i=0;i<data.result.notFound.length;i++)
					{
						data.result.notFound[i].added = false;
					}

					$scope.importGroups.result = data.result;

					Waitbox.show("Импорт завершён!");
				}
				else
				{
					Waitbox.show("Во время импота произошла ошибка: " + data.message);
				}
				
				Waitbox.done();
			});
		},
	}

	
	$scope.addedAndItFirstGroupRel = {
		visible:false,

		show:function()
		{
			$scope.addedAndItFirstGroupRel.visible = true;
		},

		close:function()
		{
			$scope.addedAndItFirstGroupRel.visible = false;
		},
	}

	$scope.addedAndItNotFirstGroupRel = {
		visible:false,

		show:function()
		{
			$scope.addedAndItNotFirstGroupRel.visible = true;
		},

		close:function()
		{
			$scope.addedAndItNotFirstGroupRel.visible = false;
		},
	}

	$scope.alreadyHaveRel = {
		visible:false,

		show:function()
		{
			$scope.alreadyHaveRel.visible = true;
		},

		close:function()
		{
			$scope.alreadyHaveRel.visible = false;
		},
	}

	$scope.notFoundProds = {
		visible:false,

		show:function()
		{
			$scope.notFoundProds.visible = true;
		},

		close:function()
		{
			$scope.notFoundProds.visible = false;
		},
	}

	$scope.productInfoWindow={
		product:null,
		show:false,
		providers:providers,
		showProductsInfoWin:function(product)
		{
			$scope.productInfoWindow.product = product;
			$scope.productInfoWindow.show = true;
		},
	}

	//
	//для liveSearch
	//
	$scope.liveSearch = {

		visible:false,
		
		tableRow:null, // стока из таблицы, для которой открылся liveSearch. Нужно, что бы при выборе
		               // товара поставить флаг added=true

		closeLiveSearch:function()
		{
			$scope.liveSearch.visible = false;
		},

		showLiveSearch:function(tableRow)
		{
			$scope.liveSearch.filter.name = tableRow.fileRow.name;
			$scope.liveSearch.filter.chod = tableRow.fileRow.chod;

			$scope.liveSearch.tableRow = tableRow;
			
			var windowTopOffset = window.pageYOffset +20;
			angular.element(document.getElementById('liveSearch')).css('top',windowTopOffset+'px');
			
			$scope.liveSearch.visible = true;
		},

		selectProduct:function(product)
		{
			var url = '/index.php?r=groupsApi/newGroupProd';
			$http.post(url,{
				productId:product.id,
				groupId:$scope.liveSearch.tableRow.fileRow.groupId,
				groupNum:$scope.liveSearch.tableRow.fileRow.groupNum,
				chod:$scope.liveSearch.tableRow.fileRow.chod,
			})
			.then(function (response) {
				var data = response.data;
				if (data.status)
				{
					$scope.liveSearch.tableRow.added=true;
					$scope.liveSearch.visible = false;
				}
				else 
				{
					alert("Возникла ошибка:" + data.message);
				}				
				
			});
		},

		clearFilter: function(type)
		{
			switch (type) {
				case 'name':
					$scope.liveSearch.filter.name = "";
					break;
				case 'chod':
					$scope.liveSearch.filter.chod = "";
					break;
			}
		},

		// запрос данных 
		getResource:function (params, paramsObj) {
			var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&' + params;
			return $http.get(urlApi).then(function (response) {
				//составим список синонимов для отбражения      
				return {
					 'rows': response.data.rows,
					 'header': response.data.header,
					 'pagination': response.data.pagination,
					 'sortBy': response.data['sort-by'],
					 'sortOrder': response.data['sort-order']
					}
				});
		},

		initTasty:{
			'count': 50,
			'page': 1,
			'sortBy': 'name',
			'sortOrder': 'dsc'
		},
		
		itemsPerPage:50,

		listItemsPerPage:[50,100, 500, 1000],

		filter:{
			name:'',
			chod:'',
		},	
		
	}
	
	$scope.showConfirm = function(message) {
		$scope.showConfirmMessage = message;

			return	ngDialog.openConfirm({
					template: 'confirmTemplate.html',
			    className: 'ngdialog-theme-plain',
			    closeByDocument:false,
			    closeByEscape:false,
			    showClose:false,
			    scope: $scope,
			});
	}

});