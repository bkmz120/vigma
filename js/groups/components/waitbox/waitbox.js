angular.module('groups').factory('Waitbox', function() {
 	var scope;
 	return {    	
    	
  		show:function(t)
  		{
  			scope.text = t;
  			scope.visible = true;  			
  		},

  		close:function() 
  		{
  			scope.text = null;
  			scope.visible = false;
  		},
 
  		text:function(t)
  		{
  			scope.text=t;
  		},

  		done:function()
  		{
  			scope.loadingVisible = false;
  			scope.closeVisible = true;
  		},

  		setScope:function(s)
  		{
  			scope = s;
  		},
  	}
  
});

angular.module('groups').directive('waitbox', ['Waitbox', function (Waitbox) {
    return {
    	template: 	'<div class="waitBox" ng-show="visible">'+
					'	<div class="waitBox_text" ng-bind="text"></div>' + 
					'	<img class="waitBox_img" ng-show="loadingVisible" src="/images/loading.gif">' +
					'   <button class="btn btn-success waitBox_close" ng-show="closeVisible" ng-click="close()">Закрыть</button>' +
					'</div>',

		link: function (scope, element, attrs) {
        	scope.visible = false;
        	scope.loadingVisible = true;
        	scope.closeVisible = false;
            scope.text = "";
            Waitbox.setScope(scope);  
            console.log("waitbox init");  
            
            scope.close = function() 
            {
            	Waitbox.close();
            }
        }
    };
}]);