angular.module('groups')
    .component('groupProdEditor', {
        controller:groupProdEditor,
        controllerAs: 'vm',
        templateUrl:'/js/groups/components/groupProdEditor/groupProdEditor.html',
        bindings: {
            allGroups: '=',
            product: '<',
            updateProductGroups: '&',
            topOffset: '<',
        },
      });

groupProdEditor.$inject = ['$element','$http'];

function groupProdEditor($element,$http) {
    var vm = this;

    vm.visible = false;
    vm.$onInit = onInit;
    vm.$onChanges = onChanges;
    vm.close = close;
    vm.groupSelectedChange = groupSelectedChange;

    function onInit() {
        vm.groups = angular.copy(vm.allGroups);

    }

    function onChanges(changes) {
        if (!changes.product.isFirstChange()) {
            for (var i=0;i<vm.groups.length;i++) {
                vm.groups[i].processing = false;
                vm.groups[i].selected = false;
                for (var j=0;j<vm.product.groups_ids.length;j++) {
                    if (vm.groups[i].id==vm.product.groups_ids[j]) {
                        vm.groups[i].selected = true;
                        break;
                    }
                }
            }
            $element.css('top',vm.topOffset + "px");
            $element[0].querySelector('.js-gp-editor-items').scrollTop = 0;  
            vm.visible = true;
        }
    }

    function close() {
        $element[0].querySelector('.js-gp-editor-items').scrollTop = 0;  
        vm.visible = false;
    }

    function groupSelectedChange(groupIndex) {
        vm.groups[groupIndex].processing = true;
        var url = '/index.php?r=groupsApi/';
        if (vm.groups[groupIndex].selected) {
            url += 'newGroupProd';
        }
        else {
            url += 'unbindGroupFromProductAjax';
        }
        $http.post(url,{
            productId:vm.product.id,
            groupId:vm.groups[groupIndex].id,
        }).then(
            function (data) {
                vm.groups[groupIndex].processing = false;
                if (data.status) {
                    var groups_ids = [];
                    var groups_names = [];
                    for (var i=0;i<vm.groups.length;i++) {
                        if (vm.groups[i].selected===true) {
                            groups_ids.push(vm.groups[i].id);
                            groups_names.push(vm.groups[i].name);
                        }
                    }
                    var groups_str = groups_names.join();                    
                    vm.updateProductGroups({
                        groups_ids:groups_ids,
                        groups_str:groups_str
                    });                
                }
                else {
                    vm.groups[groupIndex].selected = !vm.groups[groupIndex].selected;
                    alert("Во время сохранения произошла ошибка");
                }
            },
            function(){
                vm.groups[groupIndex].processing = false;
                vm.groups[groupIndex].selected = !vm.groups[groupIndex].selected;
                alert("Во время сохранения произошла ошибка");
            }
        );
    }
}